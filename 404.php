<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>
  <section class="main p404">
    <h1>Страница не найдена</h1>
    <p style="width:100%">Неправильный адрес, или такой страницы не существует</p>

    <p style="width:100%">Если вы нашли на сайте неверную ссылку, <a href="#">сообщите нам</a>, и мы исправим ошибку.</p>
  </section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>