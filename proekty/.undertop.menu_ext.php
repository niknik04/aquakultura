<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

$aMenuLinksExt = $APPLICATION->IncludeComponent("bitrix:menu.sections","",Array(
        "IS_SEF" => "Y", 
        "SEF_BASE_URL" => "/proekty/", 
        "SECTION_PAGE_URL" => "#SECTION_CODE#/", 
        "DETAIL_PAGE_URL" => "#SECTION_CODE#/#ELEMENT_CODE#/", 
        "IBLOCK_TYPE" => "projects", 
        "IBLOCK_ID" => "7", 
        "DEPTH_LEVEL" => "1", 
        "CACHE_TYPE" => "A", 
        "CACHE_TIME" => "3600" 
    )
);

$aMenuLinks = array_merge($aMenuLinks,$aMenuLinksExt);