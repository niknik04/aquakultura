  <section class="main success">
    <div class="wraper">
      <h2>Успешный бизнес клиента —<br>наш успех</h2>

      <div class="carousel">
        <a href="#" class="prev"></a>
        <a href="#" class="next"></a>
        <div class="slider">
          <div class="active"><img src="img/main/photo-2.jpg" /></div>
          <div><img src="img/main/photo-2.jpg" /></div>
          <div><img src="img/main/photo-2.jpg" /></div>
          <div><img src="img/main/photo-2.jpg" /></div>
          <div><img src="img/main/photo-2.jpg" /></div>
        </div>
      </div>

      <div class="left">
        <article>
          <h4>ООО «Кока-Кола ЭйчБиСи Евразия»</h4>
          <p>г. Нижний Новгород</p>
        </article>
      </div>

      <div class="description">
          <h4>Задача:</h4>
          <p>Запустить производство лимонада с нуля в сжатые сроки.</p>
          <h4>Результат:</h4>
          <p>Сейчас компания выпускает 700 бутылок лимонада и 400 бутылей чистой воды в час. Заказчик планирует расширять производство и ассортимент в сотрудничестве с нами.</p>
      </div>
    </div>
  </section>