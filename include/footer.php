  <footer>
    <div class="wraper">
      <div class="one-col">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"footermenu", 
	array(
		"ROOT_MENU_TYPE" => "bottom1",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_TITLE" => "Продукция"
	),
	false
);?>
      </div>
      <div class="one-col">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"footermenu", 
	array(
		"ROOT_MENU_TYPE" => "bottom2",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_TITLE" => "Комплексные линии"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"footermenu", 
	array(
		"ROOT_MENU_TYPE" => "bottom3",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_TITLE" => "Услуги"
	),
	false
);?>
      </div>
      <div class="one-col">
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"footermenu", 
	array(
		"ROOT_MENU_TYPE" => "bottom4",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "N",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_TITLE" => "Проекты"
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"footermenu", 
	array(
		"ROOT_MENU_TYPE" => "bottom5",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"MENU_TITLE" => "Компания"
	),
	false
);?>
      </div>
      <div class="one-col">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/include/footer_contacts.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
      </div>

      <div class="btm">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/include/footer_copyright.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
      </div>
    </div>
  </footer>