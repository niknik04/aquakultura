	<?$APPLICATION->ShowHead();?>
  <title><?$APPLICATION->ShowTitle();?></title>

  <link href='http://fonts.googleapis.com/css?family=PT+Sans:400&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<?$APPLICATION->SetAdditionalCSS('/css/main.css');?>
	<?$APPLICATION->SetAdditionalCSS('/css/extra.css');?>
	<?$APPLICATION->SetAdditionalCSS('/css/res_styles.css');?>

  <!--[if lt IE 9]>
  <link rel="stylesheet" href="css/ie.css" />
  <![endif]-->
  <!-- script type="text/javascript" src="js/ie.js"></script -->

  <!-- Vendor JS -->
	<?$APPLICATION->AddHeadScript('/js/vendors/jquery-2.1.3.min.js');?>
  <!-- Vendor JS -->

	<?$APPLICATION->AddHeadScript('/js/main.js');?>