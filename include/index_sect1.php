  <section class="main services">
    <div class="wraper">
      <div class="top">
        <h2>Высокий уровень сервиса<br />на всех этапах работы</h2>

        <ul class="tabs" data-tabs="#services-tabs">
          <li class="active">Инженерная служба</li>
          <li>Сервисные услуги</li>
          <li>Складские программы</li>
          <li>Обучение персонала</li>
          <li>Лизинг</li>
        </ul>
      </div>

      <div class="tabs_content" id="services-tabs">
        <div class="active">
          <div class="img"><img src="/img/main/photo-1.png" /></div>
          <div class="txt">
            <h3>Инженерная служба</h3>
            <p>Мы консультируем по всем видам оборудования с учетом специфики производства и требований клиента. Наши сотрудники — квалифицированными инженеры с опытом работы от 10 лет.</p>
            <a href="#" class="btn btn-orange showDialog" data-dialog="consulting">Заказать бесплатную консультацию</a>
          </div>
        </div>
        <div>
          <div class="img"><img src="/img/main/photo-1.png" /></div>
          <div class="txt">
            <h3>Сервисные услуги</h3>
            <p>Мы консультируем по всем видам оборудования с учетом специфики производства и требований клиента. Наши сотрудники — квалифицированными инженеры с опытом работы от 10 лет.</p>
            <a href="#" class="btn btn-orange showDialog" data-dialog="consulting">Заказать бесплатную консультацию</a>
          </div>
        </div>
        <div>
          <div class="img"><img src="/img/main/photo-1.png" /></div>
          <div class="txt">
            <h3>Складские программы</h3>
            <p>Мы консультируем по всем видам оборудования с учетом специфики производства и требований клиента. Наши сотрудники — квалифицированными инженеры с опытом работы от 10 лет.</p>
            <a href="#" class="btn btn-orange showDialog" data-dialog="consulting">Заказать бесплатную консультацию</a>
          </div>
        </div>
        <div>
          <div class="img"><img src="/img/main/photo-1.png" /></div>
          <div class="txt">
            <h3>Обучение персонала</h3>
            <p>Мы консультируем по всем видам оборудования с учетом специфики производства и требований клиента. Наши сотрудники — квалифицированными инженеры с опытом работы от 10 лет.</p>
            <a href="#" class="btn btn-orange showDialog" data-dialog="consulting">Заказать бесплатную консультацию</a>
          </div>
        </div>
        <div>
          <div class="img"><img src="/img/main/photo-1.png" /></div>
          <div class="txt">
            <h3>Лизинг</h3>
            <p>Мы консультируем по всем видам оборудования с учетом специфики производства и требований клиента. Наши сотрудники — квалифицированными инженеры с опытом работы от 10 лет.</p>
            <a href="#" class="btn btn-orange showDialog" data-dialog="consulting">Заказать бесплатную консультацию</a>
          </div>
        </div>
      </div>
    </div>
  </section>