<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$result = array();
if (count($arResult["ERRORS"]))
{
	$result['ERRORS'] = implode("<br />", $arResult["ERRORS"]);
	$result['STATE'] = 'error';
}
if (strlen($arResult["MESSAGE"]) > 0)
{
	$result['STATE'] = 'send';
	$result['MESSAGE'] = $arResult["MESSAGE"];
}
echo json_encode($result);