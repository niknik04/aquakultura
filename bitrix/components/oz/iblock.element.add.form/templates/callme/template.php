<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
?>
<button class="red show-popup" data-popup-id="callme">Заказать звонок</button>
<div class="popup red" id="callme" style="width:380px">
	<a href="#" class="close"></a>
	<h4>Заказать обратный звонок</h4>
	<div class="popup-text">
<?if (count($arResult["ERRORS"])):?>
	<?=ShowError(implode("<br />", $arResult["ERRORS"]))?>
<?endif?>
<?if (strlen($arResult["MESSAGE"]) > 0):?>
	<?=ShowNote($arResult["MESSAGE"])?>
<?endif?>
<form name="iblock_add" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">

	<?=bitrix_sessid_post()?>
	<input type="text" name="PROPERTY[NAME][0]" value="" placeholder="Ваше имя" />
	<input type="text" name="PROPERTY[33][0]" value="" placeholder="Ваш телефон" />
	<input type="submit" name="iblock_submit" value="Отправить" class="buy-button" />
	
</form>
<script type="text/javascript">
$(document).ready(function(){
	$('[name="iblock_add"]').submit(function(){
		var formdata = $(this).serialize();
		formdata += '&iblock_submit=1&910_field=';
		$('.callmestate').remove();
		$.ajax({
			url:'/ajax/callme.php',
			data:formdata,
			type:'post',
			dataType:'json',
			success:function(data){
				if(data.STATE == 'error')
				{
					if(data.ERRORS)
					{
						show_popup_bg(true);
						show_popup('callme-error');
					}
				}
				else
				if(data.STATE == 'send')
				{
					show_popup_bg(true);
					show_popup('callme-sended');
				}
			}
		});
		return false;
	});
});
</script>
	</div>
</div>
<div class="popup error" id="callme-error" style="width:380px">
	<a href="#" class="close"></a>
	<h4>Ошибка</h4>
	<div class="popup-text">
		<p>Ваша заявка на обратный звонок</p>
		<p><font size="6">не отправлена</font></p>
		<p>Проверьте правильность заполнения<br/>и повторите снова</p>
	</div>
</div>
<div class="popup sended" id="callme-sended" style="width:380px">
	<a href="#" class="close"></a>
	<h4>Успешно</h4>
	<div class="popup-text">
		<p>Ваша заявка на обратный звонок</p>
		<p><font size="6">Успешно отправлена</font></p>
	</div>
</div>