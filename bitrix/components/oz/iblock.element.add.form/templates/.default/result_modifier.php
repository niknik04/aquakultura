<?

	function sort_array($a,$b)
	{
		$sort_ar = array(
			'IBLOCK_SECTION' => 1,
			'NAME' => -1,
			'PREVIEW_TEXT' => 5
		);
		if($sort_ar[$a] < $sort_ar[$b] || (empty($sort_ar[$b]) && !empty($sort_ar[$a])))
			return -1;
		elseif($sort_ar[$a] > $sort_ar[$b] || (empty($sort_ar[$a]) && !empty($sort_ar[$b])))
			return 1;
		else
			return 0;
	}
	
	usort($arResult['PROPERTY_LIST'],'sort_array');
	
	$arResult['PROPERTY_LIST_FULL']['IBLOCK_SECTION']['MULTIPLE'] = 'N';
	foreach($arResult['PROPERTY_LIST_FULL']['IBLOCK_SECTION']['ENUM'] as &$enum)
		$enum['VALUE'] = str_replace(' .','',$enum['VALUE']);