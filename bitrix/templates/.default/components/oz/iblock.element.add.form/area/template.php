<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
?>
  <section class="squares">
    <h2>Выставочная площадь</h2>
    <p>У нас есть собственная выставочная площадь, где расположено<br />действующее оборудование. Оставьте заявку, чтобы мы<br />подготовили линию для демонстрации:</p>

    <form id="area">
			
      <label>
        <input type="text" name="PROPERTY[31][0]" placeholder="Телефон или e-mail" />
      </label>
      <label>
        <input type="text" name="PROPERTY[NAME][0]" placeholder="Имя" />
      </label>
	  <?=bitrix_sessid_post()?>
	  <input name="PROPERTY[IBLOCK_SECTION]" type="hidden" value="19" />
      <button type="submit" class="btn btn-orange">Оставить заявку</button>
    </form>
  </section>
<script type="text/javascript">
$(document).ready(function(){
	$('#area').submit(function(){
		var _this = $(this);
		var formdata = $(this).serialize();
		_this.find('error').remove();
		formdata += '&iblock_submit=1&910_field=';
		$.ajax({
			url:'/ajax/callme2.php',
			data:formdata,
			type:'post',
			dataType:'json',
			success:function(data){
				if(data.STATE == 'error')
				{
					if(data.ERRORS)
					{
						_this.append('<div class="error"></div>');
						_this.find('.error').html(data.ERRORS);
					}
				}
				else
				if(data.STATE == 'send')
				{
					_this.html('<p>Ваша заявка добавлена</p>');
				}
			}
		});
		return false;
	});
});
</script>