<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
?>
    <div class="popup dialog" id="callback">
      <a href="#" class="close"></a>
      <div class="content">
        <h2>Обратный звонок</h2>
        <form id="callbackForm">
          <label>
            <input type="text" name="PROPERTY[NAME][0]" placeholder="Имя" />
          </label>
          <label>
            <input type="text" name="PROPERTY[9][0]" placeholder="Телефон" />
          </label>
			<?=bitrix_sessid_post()?>

          <div class="time">
            <a href="#" onclick="$(this).hide().next().show(); return false;">Указать время для звонка</a>
            <div class="hidden">
              <span>Во сколько вам позвонить?</span>
              C <input class="timefrom" value="10" /> до <input class="timeto" value="18" />&nbsp;&nbsp;
			  <input name="PROPERTY[32][0]" type="hidden" class="timevalue" value="" />
			  <input name="PROPERTY[IBLOCK_SECTION]" type="hidden" value="16" />
            </div>
          </div>

          <button type="submit" class="btn btn-orange">Оставить заявку</button>
        </form>
        <div class="success">
          <p>Заявка отправлена</p>
          <a href="#" class="close"></a>
        </div>
      </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
	$('#callbackForm').submit(function(){
		var _this = $(this);
		if(_this.find('.hidden').css('display') == 'block')
			_this.find('.timevalue').val(_this.find('.timefrom').val()+' - '+_this.find('.timeto').val());
		var formdata = $(this).serialize();
		_this.find('error').remove();
		formdata += '&iblock_submit=1&910_field=';
		$.ajax({
			url:'/ajax/callme.php',
			data:formdata,
			type:'post',
			dataType:'json',
			success:function(data){
				if(data.STATE == 'error')
				{
					if(data.ERRORS)
					{
						_this.append('<div class="error"></div>');
						_this.find('.error').html(data.ERRORS);
					}
				}
				else
				if(data.STATE == 'send')
				{
					_this.hide().parent().find('.success').show();
				}
			}
		});
		return false;
	});
});
</script>