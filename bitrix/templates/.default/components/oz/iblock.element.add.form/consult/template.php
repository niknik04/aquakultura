<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<?
//echo "<pre>Template arParams: "; print_r($arParams); echo "</pre>";
//echo "<pre>Template arResult: "; print_r($arResult); echo "</pre>";
//exit();
?>
    <div class="popup dialog" id="consulting">
      <a href="#" class="close"></a>
      <div class="content">
        <h2>Бесплатная консультация</h2>
        <form id="consultForm">
      <label>
        <input type="text" name="PROPERTY[31][0]" placeholder="Телефон или e-mail" />
      </label>
      <label>
        <input type="text" name="PROPERTY[NAME][0]" placeholder="Имя" />
      </label>
		<?=bitrix_sessid_post()?>
	  <input name="PROPERTY[IBLOCK_SECTION]" type="hidden" value="18" />
          <button type="submit" class="btn btn-orange">Оставить заявку</button>
        </form>
        <div class="success">
          <p>Заявка отправлена</p>
          <a href="#" class="close"></a>
        </div>
      </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
	$('#consultForm').submit(function(){
		var _this = $(this);
		var formdata = $(this).serialize();
		_this.find('error').remove();
		formdata += '&iblock_submit=1&910_field=';
		$.ajax({
			url:'/ajax/callme2.php',
			data:formdata,
			type:'post',
			dataType:'json',
			success:function(data){
				if(data.STATE == 'error')
				{
					if(data.ERRORS)
					{
						_this.append('<div class="error"></div>');
						_this.find('.error').html(data.ERRORS);
					}
				}
				else
				if(data.STATE == 'send')
				{
					_this.hide().parent().find('.success').show();
				}
			}
		});
		return false;
	});
});
</script>