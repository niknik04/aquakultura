<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <article id="<?=$this->GetEditAreaId($arItem['ID']);?>">
      <h3><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></h3>
	<?if($arParams['DISPLAY_PICTURE'] == 'Y' && !empty($arItem['PREVIEW_PICTURE']['SRC'])):?>
	  <?if($arItem['PREVIEW_PICTURE']['WIDTH'] < 600):?>
      <p><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" width="200px" /> <?=$arItem['PREVIEW_TEXT']?></p>
	  <?else:?>
	  <p><?=$arItem['PREVIEW_TEXT']?></p>
	  <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" style="max-width:100%;" />
	  <?endif?>
	<?else:?>
	  <p><?=$arItem['PREVIEW_TEXT']?></p>
	<?endif?>
	<?if($arParams['DISPLAY_DATE'] == 'Y'):?>
      <p><a href="<?=$arItem['DETAIL_PAGE_URL']?>">Читать далее</a><span class="date"><?=$arItem['DISPLAY_ACTIVE_FROM']?></span></p>
	<?endif?>
    </article>
<?endforeach;?>