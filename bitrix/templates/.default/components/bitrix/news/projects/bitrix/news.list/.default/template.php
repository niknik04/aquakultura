<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
  <section class="main projects" id="projets-list">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <div class="project" data-filter="<?=$arItem['CODE']?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
      <div class="wraper">
        <h2><?=$arItem['NAME']?></h2>
        <p><?=$arItem['SECTION_NAME']?></p>

        <div class="proj-frame">
          <div class="carousel">
            <img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" />
          </div>

          <div class="left">
            <article>
              <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" />
              <h4><?=$arItem['NAME']?></h4>
              <p><?=$arItem['DISPLAY_PROPERTIES']['CITY']['DISPLAY_VALUE']?></p>
            </article>
          </div>

          <div class="description">
            <?=$arItem['PREVIEW_TEXT']?>
            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="more">Подробней о проекте</a>
          </div>
        </div>
      </div>
    </div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
  </section>