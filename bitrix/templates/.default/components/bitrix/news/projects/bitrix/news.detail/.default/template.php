<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
  <section class="main projects">
    <div class="project">
      <div class="wraper">
        <h1><?=$arResult['NAME']?></h1>
        <p><?=$arResult['SECTION']['PATH'][0]['NAME']?></p>
<?if(!empty($arResult['DETAIL_PICTURE']['SRC'])):?>
        <div class="carousel">
          <img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" />
        </div>
<?endif?>

        <div class="left">
          <article>
<?if(!empty($arResult['PREVIEW_PICTURE']['SRC'])):?>
            <img src="<?=$arResult['PREVIEW_PICTURE']['SRC']?>" />
<?endif?>
            <h4><?=$arResult['NAME']?></h4>
            <p><?=$arResult['DISPLAY_PROPERTIES']['CITY']['DISPLAY_VALUE']?></p>
          </article>
        </div>

        <div class="description">
<?=$arResult['PREVIEW_TEXT']?>
        </div>

        <div class="content">
<?=$arResult['DETAIL_TEXT']?>
        </div>
      </div>
    </div>
  </section>