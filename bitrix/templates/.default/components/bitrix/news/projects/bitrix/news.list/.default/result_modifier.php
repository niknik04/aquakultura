<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(CModule::IncludeModule('iblock'))
{
	$ids = array();
	foreach($arResult['ITEMS'] as $key => $arItem)
	{
		$ids[$arItem['IBLOCK_SECTION_ID']][] = $key;
	}
	$res = CIBlockSection::GetList(array(),array('ID'=>array_keys($ids)),false,array('ID','NAME'));
	while($r = $res->Fetch())
		foreach($ids[$r['ID']] as $id)
			$arResult['ITEMS'][$id]['SECTION_NAME'] = $r['NAME'];
}