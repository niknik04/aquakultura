<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult['ITEMS'] as $key => $arItem)
{
	if($arItem['PREVIEW_PICTURE']['ID'])
	{
		$img = CFile::ResizeImageGet(
			$arItem['PREVIEW_PICTURE']['ID'],
			array('width' => 148, 'height' => 203),
			BX_RESIZE_IMAGE_EXACT,
			true
		);
		$arResult['ITEMS'][$key]['PREVIEW_PICTURE']['SRC'] = $img['src'];
		$arResult['ITEMS'][$key]['PREVIEW_PICTURE']['WIDTH'] = $img['width'];
		$arResutl['ITEMS'][$key]['PREVIEW_PICTURE']['HEIGHT'] = $img['height'];
	}
}