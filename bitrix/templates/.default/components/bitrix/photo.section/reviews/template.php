<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
  <section class="main reviews">
    <div class="wraper">
      <h1>Отзывы о нашей работе</h1>

      <ul>
		<?foreach($arResult['ITEMS'] as $arItem):?>
				<?
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BPS_ELEMENT_DELETE_CONFIRM')));
				?>
        <li id="<?=$this->GetEditAreaId($arItem['ID']);?>">
          <a class="fancybox-thumb" rel="fancybox-thumb" href="<?=$arItem['DETAIL_PICTURE']['SRC']?>" title="Вимм Билль Данн">
            <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>" />
            <?=$arItem['NAME']?>
          </a>
        </li>
		<?endforeach?>
	  </ul>
	</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
  </section>