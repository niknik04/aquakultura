<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

if (!empty($arResult['ITEMS'])):?>
        <div id="production_inner-tabs_content">
          <div id="production_table-tabs_content" class="content">
<?
foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($arItem['ID']);
?>
            <div <?if(!$key):?> class="active"<?endif?>>
              <h1><?=$arItem['NAME']?></h1>
              <div class="img">
			  <?if($arItem['PREVIEW_PICTURE']['SRC']):?>
                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" />
			  <?endif?>
				<?if($arItem['PROPERTIES']['NALICHIE']['VALUE']):?>
                <span class="availability">В наличии на складе в Москве</span>
				<?endif?>
				<?if($arItem['PROPERTIES']['LIZING']['VALUE']):?>
                <span class="leasing">Это <br>оборудование<br> доступно<br> в лизинг</span>
				<?endif?>
              </div>
              <div class="txt">
                <p><?=$arItem['PREVIEW_TEXT']?></p>
              </div>
            </div>
<?
}
?>
          </div>

          <div class="big-table">
            <h2>Технические характеристики</h2>
            <div class="table-titles" data-maxheight="400">
              <table>
			  <?foreach($arResult['PROPERTIES'] as $propName):?>
                <tr><td><span><?=$propName?></span></td></tr>
			  <?endforeach?>
              </table>
            </div>
            <div class="table-content" data-maxheight="400">
              <table>
			  <?$begin = true;?>
			  <?foreach($arResult['PROPERTIES'] as $key => $propName):?>
				<?if($begin):?>
				<tr>
				  <td>
				  </td>
				  <?foreach($arResult['ITEMS'] as $item_key => $arItem):?>
				  <td>
                    <span class="tab <?=(!$item_key ? 'active' : '')?>">
                      <a href="#">
                        <?=$arItem['NAME']?>
						<?if($arItem['PROPERTIES']['NALICHIE']['VALUE']):?>
                        <span class="availability">В наличии</span>
						<?endif?>
                      </a>
                    </span>
                    <?=$arItem['DISPLAY_PROPERTIES'][$key]['VALUE']?>
                  </td>
				  <?endforeach?>
				</tr>
				<?endif?>
				<tr>
				  <td>
				  </td>
				  <?foreach($arResult['ITEMS'] as $arItem):?>
				  <td><?=$arItem['DISPLAY_PROPERTIES'][$key]['VALUE']?></td>
				  <?endforeach?>
				</tr>
				<?$begin = false;?>
			  <?endforeach?>

              </table>
            </div>
          </div>

          <div class="more-table">
            <a href="#">Все характеристики</a>
          </div>
<?

	if ($arParams["DISPLAY_BOTTOM_PAGER"])
	{
		echo $arResult["NAV_STRING"];
	}
?>
		</div>
<?
else:?>
<div class="content">
<h1><?=$arResult['NAME']?></h1>
<?=$arResult['DESCRIPTION']?>
</div>
<?$this->SetViewTarget('landing_sections');?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/include/index_sect1.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/include/index_sect3.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/include/index_sect4.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
<?$this->EndViewTarget();?> 
<?endif?>