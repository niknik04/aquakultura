<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
        <ul class="tabs" data-tabs="#production_inner-tabs_content">
		<?$in_path = false;?>
<?
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
				?>
				<?if($arSection['DEPTH_LEVEL'] == 2):?>
					<?if($in_path):?>
						</li>
					</ul>
						<?$in_path = false;?>
					<?endif?>
					<?if($arSection['SELECTED']):?>
				<li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>"><?=$arSection['NAME']?>
					<ul>
					<?$in_path = true;?>
					<?else:?>
				<li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>"><a href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a></li>
					<?endif?>
				<?elseif($in_path && $arSection['DEPTH_LEVEL'] > 2):?>
				<li<?if($arSection['SELECTED']):?> class="active"<?endif?> id="<? echo $this->GetEditAreaId($arSection['ID']); ?>"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a></li>
				<?endif?>
<?
			}
?>
					<?if($in_path):?>
					</ul>
						<?$in_path = false;?>
					<?endif?>
		</ul>