<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$APPLICATION->RestartBuffer();
IncludeTemplateLangFile(__FILE__);
?> 
<!DOCTYPE html>
<html>
<head>
<?include $_SERVER['DOCUMENT_ROOT'].'/include/head.php'; ?>
</head>
<body class="frontpage">
<?$APPLICATION->ShowPanel();?>
  <section id="top">
<?include $_SERVER['DOCUMENT_ROOT'].'/include/header.php'; ?>

    <div class="wraper">
<?$APPLICATION->IncludeComponent(
	"bitrix:main.include", 
	".default", 
	array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => "/include/header_text.php",
		"EDIT_TEMPLATE" => ""
	),
	false
);?>
    </div>
  </section>

  <section class="main w-border complex">
    <div class="wraper">
      <div class="top">
        <h3>Комплексная линия</h3>
        <p>для розлива пищевых и непищевых жидкостей в ПЭТ, поликарбонатную и стеклянную тару объёмом от 0,1 л до 19 л.</p>

        <ul class="tabs" data-tabs="#complex-lines-tabs">
          <li class="active">Питьевая вода <a href="#">до 2 л</a> <a href="#">до 5 л</a> <a href="#" class="active">19 л</a></li>
          <li>Газированные напитки</li>
          <li>Бытовая химия и техжидкости</li>
          <li>Масло</li>
        </ul>
      </div>

      <div class="tabs_content" id="complex-lines-tabs">
        <div class="active">
          <img src="img/line.jpg" width="1183" height="684" />
          <div class="consist">
            <h4>Состав линии:</h4>
            <span>Декапер</span>
            <span>Проверка герметичности</span>
            <span>Мойка</span>
            <span>Загрузчик бутылей</span>
            <span>Моноблок розлива</span>
            <span>Световая панель</span>
            <span>Термотоннель</span>
            <span>Надевание пакетов на бутыль</span>
            <span>Конвейер</span>

            <a href="#" class="btn btn-orange" class="showDialog" data-dialog="pricelist">Получить цену и характеристики</a>
          </div>
        </div>
        <div>
          <img src="img/line.jpg" width="1183" height="684" />
          <div class="consist">
            <h4>Состав линии:</h4>
            <span>Проверка герметичности</span>
            <span>Декапер</span>
            <span>Мойка</span>
            <span>Загрузчик бутылей</span>
            <span>Моноблок розлива</span>
            <span>Световая панель</span>
            <span>Термотоннель</span>
            <span>Надевание пакетов на бутыль</span>
            <span>Конвейер</span>

            <a href="#" class="btn btn-orange" class="showDialog" data-dialog="pricelist">Получить цену и характеристики</a>
          </div>
        </div>
        <div>
          <img src="img/line.jpg" width="1183" height="684" />
          <div class="consist">
            <h4>Состав линии:</h4>
            <span>Проверка герметичности</span>
            <span>Мойка</span>
            <span>Загрузчик бутылей</span>
            <span>Моноблок розлива</span>
            <span>Световая панель</span>
            <span>Термотоннель</span>
            <span>Надевание пакетов на бутыль</span>
            <span>Конвейер</span>

            <a href="#" class="btn btn-orange" class="showDialog" data-dialog="pricelist">Получить цену и характеристики</a>
          </div>
        </div>
        <div>
          <img src="img/line.jpg" width="1183" height="684" />
          <div class="consist">
            <h4>Состав линии:</h4>
            <span>Мойка</span>
            <span>Загрузчик бутылей</span>
            <span>Декапер</span>
            <span>Проверка герметичности</span>
            <span>Моноблок розлива</span>
            <span>Световая панель</span>
            <span>Термотоннель</span>
            <span>Надевание пакетов на бутыль</span>
            <span>Конвейер</span>

            <a href="#" class="btn btn-orange" class="showDialog" data-dialog="pricelist">Получить цену и характеристики</a>
          </div>
        </div>
      </div>
    </div>
  </section>