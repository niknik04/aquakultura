<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<? include $_SERVER['DOCUMENT_ROOT'].'/include/footer.php'; ?>

  <!-- popups -->
  <div id="fader">
    <div class="backdoor"></div>
<?$APPLICATION->IncludeComponent(
	"oz:iblock.element.add.form", 
	"consult", 
	array(
		"IBLOCK_TYPE" => "feedback",
		"IBLOCK_ID" => "5",
		"STATUS_NEW" => "ANY",
		"LIST_URL" => "",
		"USE_CAPTCHA" => "N",
		"USER_MESSAGE_EDIT" => "",
		"USER_MESSAGE_ADD" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"RESIZE_IMAGES" => "N",
		"PROPERTY_CODES" => array(
			0 => "NAME",
			1 => "IBLOCK_SECTION",
			2 => "32",
			3 => "9",
		),
		"PROPERTY_CODES_REQUIRED" => array(
			0 => "NAME",
			1 => "32",
		),
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"MAX_USER_ENTRIES" => "100000",
		"MAX_LEVELS" => "100000",
		"LEVEL_LAST" => "Y",
		"MAX_FILE_SIZE" => "0",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"SEF_MODE" => "N",
		"SEF_FOLDER" => "/",
		"EMAIL_TO" => "",
		"EVENT_NAME" => "NEW_USER",
		"EVENT_ID" => "1",
		"CUSTOM_TITLE_NAME" => "Имя",
		"CUSTOM_TITLE_TAGS" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => ""
	),
	false
);?>

<?$APPLICATION->IncludeComponent(
	"oz:iblock.element.add.form", 
	"feedbackcall", 
	array(
		"IBLOCK_TYPE" => "feedback",
		"IBLOCK_ID" => "5",
		"STATUS_NEW" => "ANY",
		"LIST_URL" => "",
		"USE_CAPTCHA" => "N",
		"USER_MESSAGE_EDIT" => "",
		"USER_MESSAGE_ADD" => "",
		"DEFAULT_INPUT_SIZE" => "30",
		"RESIZE_IMAGES" => "N",
		"PROPERTY_CODES" => array(
			0 => "NAME",
			1 => "IBLOCK_SECTION",
			2 => "32",
			3 => "9",
		),
		"PROPERTY_CODES_REQUIRED" => array(
			0 => "NAME",
			1 => "32",
		),
		"GROUPS" => array(
			0 => "2",
		),
		"STATUS" => "ANY",
		"ELEMENT_ASSOC" => "CREATED_BY",
		"MAX_USER_ENTRIES" => "100000",
		"MAX_LEVELS" => "100000",
		"LEVEL_LAST" => "Y",
		"MAX_FILE_SIZE" => "0",
		"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
		"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
		"SEF_MODE" => "N",
		"SEF_FOLDER" => "/",
		"EMAIL_TO" => "",
		"EVENT_NAME" => "NEW_USER",
		"EVENT_ID" => "1",
		"CUSTOM_TITLE_NAME" => "Имя",
		"CUSTOM_TITLE_TAGS" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
		"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
		"CUSTOM_TITLE_IBLOCK_SECTION" => "",
		"CUSTOM_TITLE_PREVIEW_TEXT" => "",
		"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
		"CUSTOM_TITLE_DETAIL_TEXT" => "",
		"CUSTOM_TITLE_DETAIL_PICTURE" => ""
	),
	false
);?>
	
	<div class="popup dialog" id="pricelist">
      <a href="#" class="close"></a>
      <div class="content">
        <h2>Запрос прайс-листа</h2>
        <form id="callbackForm">
          <label>
            <input type="text" placeholder="Имя" />
          </label>
          <label>
            <input type="text" placeholder="E-mail" />
          </label>
          <button type="submit" class="btn btn-orange">Оставить запрос</button>
        </form>
        <div class="success">
          <p>Запрос отправлен</p>
          <a href="#" class="close"></a>
        </div>
      </div>
    </div>
  </div>
  <!-- popups -->

</body>
</html>