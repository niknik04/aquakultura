<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<? include $_SERVER['DOCUMENT_ROOT'].'/include/footer.php'; ?>


  <!-- popups -->
  <div id="fader">
    <div class="backdoor"></div>
    <div class="popup dialog" id="consulting">
      <a href="#" class="close"></a>
      <div class="content">
        <h2>Бесплатная консультация</h2>
        <form id="consultForm">
          <label>
            <input type="text" placeholder="Телефон или e-mail" />
          </label>
          <label>
            <input type="text" placeholder="Имя" />
          </label>
          <button type="submit" class="btn btn-orange">Оставить заявку</button>
        </form>
        <div class="success">
          <p>Заявка отправлена</p>
          <a href="#" class="close"></a>
        </div>
      </div>
    </div>

    <div class="popup dialog" id="callback">
      <a href="#" class="close"></a>
      <div class="content">
        <h2>Обратный звонок</h2>
        <form id="callbackForm">
          <label>
            <input type="text" placeholder="Имя" />
          </label>
          <label>
            <input type="text" placeholder="Телефон" />
          </label>

          <div class="time">
            <a href="#" onclick="$(this).hide().next().show(); return false;">Указать время для звонка</a>
            <div class="hidden">
              <span>Во сколько вам позвонить?</span>
              C <input value="10" /> до <input value="18" />&nbsp;&nbsp;
            </div>
          </div>

          <button type="submit" class="btn btn-orange">Оставить заявку</button>
        </form>
        <div class="success">
          <p>Заявка отправлена</p>
          <a href="#" class="close"></a>
        </div>
      </div>
    </div>
  </div>
  <!-- popups -->

</body>
</html>