<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
IncludeTemplateLangFile(__FILE__);
?> 
<!DOCTYPE html>
<html>
<head>
<?include $_SERVER['DOCUMENT_ROOT'].'/include/head.php'; ?>
<?$APPLICATION->AddHeadScript('/js/vendors/jquery-ui.min.js');	?>
</head>
<body>
<?$APPLICATION->ShowPanel();?>
  <section id="top">
<?include $_SERVER['DOCUMENT_ROOT'].'/include/header.php'; ?>


<?$APPLICATION->IncludeComponent("bitrix:menu", "products", Array(
	"ROOT_MENU_TYPE" => "undertop",	// Тип меню для первого уровня
		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
		"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
	),
	false
);?>
  </section>