-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2015 at 01:44 PM
-- Server version: 5.5.25
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `aquaculture`
--

-- --------------------------------------------------------

--
-- Table structure for table `b_admin_notify`
--

CREATE TABLE IF NOT EXISTS `b_admin_notify` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  `ENABLE_CLOSE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `PUBLIC_SECTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `IX_AD_TAG` (`TAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_admin_notify_lang`
--

CREATE TABLE IF NOT EXISTS `b_admin_notify_lang` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `NOTIFY_ID` int(18) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_ADM_NTFY_LANG` (`NOTIFY_ID`,`LID`),
  KEY `IX_ADM_NTFY_LID` (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_agent`
--

CREATE TABLE IF NOT EXISTS `b_agent` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(18) NOT NULL DEFAULT '100',
  `NAME` text COLLATE utf8_unicode_ci,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `LAST_EXEC` datetime DEFAULT NULL,
  `NEXT_EXEC` datetime NOT NULL,
  `DATE_CHECK` datetime DEFAULT NULL,
  `AGENT_INTERVAL` int(18) DEFAULT '86400',
  `IS_PERIOD` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `USER_ID` int(18) DEFAULT NULL,
  `RUNNING` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `ix_act_next_exec` (`ACTIVE`,`NEXT_EXEC`),
  KEY `ix_agent_user_id` (`USER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

--
-- Dumping data for table `b_agent`
--

INSERT INTO `b_agent` (`ID`, `MODULE_ID`, `SORT`, `NAME`, `ACTIVE`, `LAST_EXEC`, `NEXT_EXEC`, `DATE_CHECK`, `AGENT_INTERVAL`, `IS_PERIOD`, `USER_ID`, `RUNNING`) VALUES
(1, 'main', 100, 'CEvent::CleanUpAgent();', 'Y', '2015-05-11 07:18:39', '2015-05-12 00:00:00', NULL, 86400, 'Y', NULL, 'N'),
(2, 'main', 100, 'CUser::CleanUpHitAuthAgent();', 'Y', '2015-05-11 07:18:39', '2015-05-12 00:00:00', NULL, 86400, 'Y', NULL, 'N'),
(3, 'main', 100, 'CCaptchaAgent::DeleteOldCaptcha(3600);', 'Y', '2015-05-12 19:17:08', '2015-05-12 20:17:08', NULL, 3600, 'N', NULL, 'N'),
(4, 'main', 100, 'CUndo::CleanUpOld();', 'Y', '2015-05-11 07:18:40', '2015-05-12 00:00:00', NULL, 86400, 'Y', NULL, 'N'),
(5, 'main', 100, 'CSiteCheckerTest::CommonTest();', 'Y', '2015-05-12 18:16:39', '2015-05-13 18:16:39', NULL, 86400, 'N', NULL, 'N'),
(6, 'catalog', 100, '\\Bitrix\\Catalog\\CatalogViewedProductTable::clearAgent(0);', 'Y', '2015-05-07 22:56:29', '2015-05-12 22:56:29', NULL, 432000, 'N', NULL, 'N'),
(7, 'currency', 100, '\\Bitrix\\Currency\\CurrencyTable::currencyBaseRateAgent();', 'Y', '2015-05-11 07:18:43', '2015-05-12 00:01:00', NULL, 86400, 'Y', NULL, 'N'),
(8, 'forum', 100, 'CForumStat::CleanUp();', 'Y', '2015-05-12 18:16:56', '2015-05-13 18:16:56', NULL, 86400, 'N', NULL, 'N'),
(9, 'forum', 100, 'CForumFiles::CleanUp();', 'Y', '2015-05-12 18:16:56', '2015-05-13 18:16:56', NULL, 86400, 'N', NULL, 'N'),
(13, 'sale', 100, 'CSaleRecurring::AgentCheckRecurring();', 'Y', '2015-05-12 18:16:56', '2015-05-12 20:16:56', NULL, 7200, 'N', NULL, 'N'),
(14, 'sale', 100, 'CSaleOrder::RemindPayment();', 'Y', '2015-05-12 18:16:56', '2015-05-13 18:16:56', NULL, 86400, 'N', NULL, 'N'),
(15, 'sale', 100, 'CSaleViewedProduct::ClearViewed();', 'Y', '2015-05-12 18:16:57', '2015-05-13 18:16:57', NULL, 86400, 'N', NULL, 'N'),
(16, 'sale', 100, 'CSaleOrder::ClearProductReservedQuantity();', 'Y', '2015-05-11 07:18:57', '2015-05-14 07:18:57', NULL, 259200, 'N', NULL, 'N'),
(19, 'search', 10, 'CSearchSuggest::CleanUpAgent();', 'Y', '2015-05-12 18:16:58', '2015-05-13 18:16:58', NULL, 86400, 'N', NULL, 'N'),
(20, 'search', 10, 'CSearchStatistic::CleanUpAgent();', 'Y', '2015-05-12 18:16:58', '2015-05-13 18:16:58', NULL, 86400, 'N', NULL, 'N'),
(22, 'security', 100, 'CSecuritySession::CleanUpAgent();', 'Y', '2015-05-12 19:18:08', '2015-05-12 19:48:08', NULL, 1800, 'N', NULL, 'N'),
(23, 'security', 100, 'CSecurityIPRule::CleanUpAgent();', 'Y', '2015-05-12 19:17:08', '2015-05-12 20:17:08', NULL, 3600, 'N', NULL, 'N'),
(25, 'seo', 100, 'Bitrix\\Seo\\Engine\\YandexDirect::updateAgent();', 'Y', '2015-05-12 19:17:09', '2015-05-12 20:17:09', NULL, 3600, 'N', NULL, 'N'),
(26, 'seo', 100, 'Bitrix\\Seo\\Adv\\LogTable::clean();', 'Y', '2015-05-12 18:16:57', '2015-05-13 18:16:57', NULL, 86400, 'N', NULL, 'N'),
(29, 'storeassist', 100, 'CStoreAssist::AgentCountDayOrders();', 'Y', '2015-05-12 18:16:57', '2015-05-13 18:16:57', NULL, 86400, 'N', NULL, 'N'),
(31, 'subscribe', 100, 'CSubscription::CleanUp();', 'Y', '2015-05-11 07:18:46', '2015-05-12 03:00:00', NULL, 86400, 'Y', NULL, 'N'),
(34, 'pull', 100, 'CPullChannel::CheckExpireAgent();', 'Y', '2015-05-12 18:16:57', '2015-05-13 06:16:57', NULL, 43200, 'N', NULL, 'N'),
(35, 'pull', 100, 'CPullStack::CheckExpireAgent();', 'Y', '2015-05-12 18:16:58', '2015-05-13 18:16:58', NULL, 86400, 'N', NULL, 'N'),
(36, 'pull', 100, 'CPullWatch::CheckExpireAgent();', 'Y', '2015-05-12 19:37:09', '2015-05-12 19:57:09', NULL, 600, 'N', NULL, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `b_app_password`
--

CREATE TABLE IF NOT EXISTS `b_app_password` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `APPLICATION_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DIGEST_PASSWORD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `DATE_LOGIN` datetime DEFAULT NULL,
  `LAST_IP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYSCOMMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_app_password_user` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_bitrixcloud_option`
--

CREATE TABLE IF NOT EXISTS `b_bitrixcloud_option` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL,
  `PARAM_KEY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PARAM_VALUE` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_bitrixcloud_option_1` (`NAME`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `b_bitrixcloud_option`
--

INSERT INTO `b_bitrixcloud_option` (`ID`, `NAME`, `SORT`, `PARAM_KEY`, `PARAM_VALUE`) VALUES
(1, 'backup_quota', 0, '0', '0'),
(2, 'backup_total_size', 0, '0', '0'),
(3, 'backup_last_backup_time', 0, '0', '1429724641'),
(4, 'monitoring_expire_time', 0, '0', '1431258059');

-- --------------------------------------------------------

--
-- Table structure for table `b_blog`
--

CREATE TABLE IF NOT EXISTS `b_blog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DATE_CREATE` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `OWNER_ID` int(11) DEFAULT NULL,
  `SOCNET_GROUP_ID` int(11) DEFAULT NULL,
  `URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `REAL_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `ENABLE_COMMENTS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ENABLE_IMG_VERIF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ENABLE_RSS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `LAST_POST_ID` int(11) DEFAULT NULL,
  `LAST_POST_DATE` datetime DEFAULT NULL,
  `AUTO_GROUPS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL_NOTIFY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_HTML` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SEARCH_INDEX` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `USE_SOCNET` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_BLOG_BLOG_4` (`URL`),
  KEY `IX_BLOG_BLOG_1` (`GROUP_ID`,`ACTIVE`),
  KEY `IX_BLOG_BLOG_2` (`OWNER_ID`),
  KEY `IX_BLOG_BLOG_5` (`LAST_POST_DATE`),
  KEY `IX_BLOG_BLOG_6` (`SOCNET_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_category`
--

CREATE TABLE IF NOT EXISTS `b_blog_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BLOG_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_BLOG_CAT_1` (`BLOG_ID`,`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_comment`
--

CREATE TABLE IF NOT EXISTS `b_blog_comment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BLOG_ID` int(11) NOT NULL,
  `POST_ID` int(11) NOT NULL,
  `PARENT_ID` int(11) DEFAULT NULL,
  `AUTHOR_ID` int(11) DEFAULT NULL,
  `ICON_ID` int(11) DEFAULT NULL,
  `AUTHOR_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUTHOR_EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUTHOR_IP` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUTHOR_IP1` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_CREATE` datetime NOT NULL,
  `TITLE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POST_TEXT` text COLLATE utf8_unicode_ci NOT NULL,
  `PUBLISH_STATUS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `HAS_PROPS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SHARE_DEST` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_BLOG_COMM_1` (`BLOG_ID`,`POST_ID`),
  KEY `IX_BLOG_COMM_2` (`AUTHOR_ID`),
  KEY `IX_BLOG_COMM_3` (`DATE_CREATE`,`AUTHOR_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_group`
--

CREATE TABLE IF NOT EXISTS `b_blog_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_BLOG_GROUP_1` (`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_image`
--

CREATE TABLE IF NOT EXISTS `b_blog_image` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FILE_ID` int(11) NOT NULL DEFAULT '0',
  `BLOG_ID` int(11) NOT NULL DEFAULT '0',
  `POST_ID` int(11) NOT NULL DEFAULT '0',
  `USER_ID` int(11) NOT NULL DEFAULT '0',
  `TIMESTAMP_X` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TITLE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGE_SIZE` int(11) NOT NULL DEFAULT '0',
  `IS_COMMENT` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `COMMENT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_BLOG_IMAGE_1` (`POST_ID`,`BLOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_post`
--

CREATE TABLE IF NOT EXISTS `b_blog_post` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `BLOG_ID` int(11) NOT NULL,
  `AUTHOR_ID` int(11) NOT NULL,
  `PREVIEW_TEXT` text COLLATE utf8_unicode_ci,
  `PREVIEW_TEXT_TYPE` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `DETAIL_TEXT` text COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL_TEXT_TYPE` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `DATE_CREATE` datetime NOT NULL,
  `DATE_PUBLISH` datetime NOT NULL,
  `KEYWORDS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PUBLISH_STATUS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `CATEGORY_ID` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ATRIBUTE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ENABLE_TRACKBACK` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ENABLE_COMMENTS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ATTACH_IMG` int(11) DEFAULT NULL,
  `NUM_COMMENTS` int(11) NOT NULL DEFAULT '0',
  `NUM_TRACKBACKS` int(11) NOT NULL DEFAULT '0',
  `VIEWS` int(11) DEFAULT NULL,
  `FAVORITE_SORT` int(11) DEFAULT NULL,
  `PATH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MICRO` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `HAS_IMAGES` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HAS_PROPS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HAS_TAGS` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HAS_COMMENT_IMAGES` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HAS_SOCNET_ALL` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEO_TITLE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEO_TAGS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEO_DESCRIPTION` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_BLOG_POST_1` (`BLOG_ID`,`PUBLISH_STATUS`,`DATE_PUBLISH`),
  KEY `IX_BLOG_POST_2` (`BLOG_ID`,`DATE_PUBLISH`,`PUBLISH_STATUS`),
  KEY `IX_BLOG_POST_3` (`BLOG_ID`,`CATEGORY_ID`),
  KEY `IX_BLOG_POST_4` (`PUBLISH_STATUS`,`DATE_PUBLISH`),
  KEY `IX_BLOG_POST_5` (`DATE_PUBLISH`,`AUTHOR_ID`),
  KEY `IX_BLOG_POST_CODE` (`BLOG_ID`,`CODE`),
  KEY `IX_BLOG_POST_6` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_post_category`
--

CREATE TABLE IF NOT EXISTS `b_blog_post_category` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BLOG_ID` int(11) NOT NULL,
  `POST_ID` int(11) NOT NULL,
  `CATEGORY_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_BLOG_POST_CATEGORY` (`POST_ID`,`CATEGORY_ID`),
  KEY `IX_BLOG_POST_CATEGORY_CAT_ID` (`CATEGORY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_post_param`
--

CREATE TABLE IF NOT EXISTS `b_blog_post_param` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `POST_ID` int(11) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_BLOG_PP_1` (`POST_ID`,`USER_ID`),
  KEY `IX_BLOG_PP_2` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_site_path`
--

CREATE TABLE IF NOT EXISTS `b_blog_site_path` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `PATH` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_BLOG_SITE_PATH_2` (`SITE_ID`,`TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_smile`
--

CREATE TABLE IF NOT EXISTS `b_blog_smile` (
  `ID` smallint(3) NOT NULL AUTO_INCREMENT,
  `SMILE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `TYPING` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGE` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CLICKABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(10) NOT NULL DEFAULT '150',
  `IMAGE_WIDTH` int(11) NOT NULL DEFAULT '0',
  `IMAGE_HEIGHT` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `b_blog_smile`
--

INSERT INTO `b_blog_smile` (`ID`, `SMILE_TYPE`, `TYPING`, `IMAGE`, `DESCRIPTION`, `CLICKABLE`, `SORT`, `IMAGE_WIDTH`, `IMAGE_HEIGHT`) VALUES
(1, 'S', ':D :-D', 'icon_biggrin.png', NULL, 'Y', 120, 16, 16),
(2, 'S', ':) :-)', 'icon_smile.png', NULL, 'Y', 100, 16, 16),
(3, 'S', ':( :-(', 'icon_sad.png', NULL, 'Y', 140, 16, 16),
(4, 'S', ':o :-o :shock:', 'icon_eek.png', NULL, 'Y', 180, 16, 16),
(5, 'S', '8) 8-)', 'icon_cool.png', NULL, 'Y', 130, 16, 16),
(6, 'S', ':{} :-{}', 'icon_kiss.png', NULL, 'Y', 200, 16, 16),
(7, 'S', ':oops:', 'icon_redface.png', NULL, 'Y', 190, 16, 16),
(8, 'S', ':cry: :~(', 'icon_cry.png', NULL, 'Y', 160, 16, 16),
(9, 'S', ':evil: >:-<', 'icon_evil.png', NULL, 'Y', 170, 16, 16),
(10, 'S', ';) ;-)', 'icon_wink.png', NULL, 'Y', 110, 16, 16),
(11, 'S', ':!:', 'icon_exclaim.png', NULL, 'Y', 220, 16, 16),
(12, 'S', ':?:', 'icon_question.png', NULL, 'Y', 210, 16, 16),
(13, 'S', ':idea:', 'icon_idea.png', NULL, 'Y', 230, 16, 16),
(14, 'S', ':| :-|', 'icon_neutral.png', NULL, 'Y', 150, 16, 16);

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_smile_lang`
--

CREATE TABLE IF NOT EXISTS `b_blog_smile_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SMILE_ID` int(11) NOT NULL DEFAULT '0',
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_BLOG_SMILE_K` (`SMILE_ID`,`LID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=29 ;

--
-- Dumping data for table `b_blog_smile_lang`
--

INSERT INTO `b_blog_smile_lang` (`ID`, `SMILE_ID`, `LID`, `NAME`) VALUES
(1, 1, 'ru', 'Широкая улыбка'),
(2, 1, 'en', 'Big grin'),
(3, 2, 'ru', 'С улыбкой'),
(4, 2, 'en', 'Smile'),
(5, 3, 'ru', 'Печально'),
(6, 3, 'en', 'Sad'),
(7, 4, 'ru', 'Удивленно'),
(8, 4, 'en', 'Surprised'),
(9, 5, 'ru', 'Здорово'),
(10, 5, 'en', 'Cool'),
(11, 6, 'ru', 'Поцелуй'),
(12, 6, 'en', 'Kiss'),
(13, 7, 'ru', 'Смущенный'),
(14, 7, 'en', 'Embarrassed'),
(15, 8, 'ru', 'Очень грустно'),
(16, 8, 'en', 'Crying'),
(17, 9, 'ru', 'Со злостью'),
(18, 9, 'en', 'Angry'),
(19, 10, 'ru', 'Шутливо'),
(20, 10, 'en', 'Wink'),
(21, 11, 'ru', 'Восклицание'),
(22, 11, 'en', 'Exclamation'),
(23, 12, 'ru', 'Вопрос'),
(24, 12, 'en', 'Question'),
(25, 13, 'ru', 'Идея'),
(26, 13, 'en', 'Idea'),
(27, 14, 'ru', 'Скептически'),
(28, 14, 'en', 'Skeptic');

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_socnet`
--

CREATE TABLE IF NOT EXISTS `b_blog_socnet` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BLOG_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_BLOG_SOCNET` (`BLOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_socnet_rights`
--

CREATE TABLE IF NOT EXISTS `b_blog_socnet_rights` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `POST_ID` int(11) NOT NULL,
  `ENTITY_TYPE` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `ENTITY` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_BLOG_SR_1` (`POST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_trackback`
--

CREATE TABLE IF NOT EXISTS `b_blog_trackback` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PREVIEW_TEXT` text COLLATE utf8_unicode_ci NOT NULL,
  `BLOG_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POST_DATE` datetime NOT NULL,
  `BLOG_ID` int(11) NOT NULL,
  `POST_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_BLOG_TRBK_1` (`BLOG_ID`,`POST_ID`),
  KEY `IX_BLOG_TRBK_2` (`POST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_user`
--

CREATE TABLE IF NOT EXISTS `b_blog_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `ALIAS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `AVATAR` int(11) DEFAULT NULL,
  `INTERESTS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_VISIT` datetime DEFAULT NULL,
  `DATE_REG` datetime NOT NULL,
  `ALLOW_POST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_BLOG_USER_1` (`USER_ID`),
  KEY `IX_BLOG_USER_2` (`ALIAS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_user2blog`
--

CREATE TABLE IF NOT EXISTS `b_blog_user2blog` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `BLOG_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_BLOG_USER2GROUP_1` (`BLOG_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_user2user_group`
--

CREATE TABLE IF NOT EXISTS `b_blog_user2user_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `BLOG_ID` int(11) NOT NULL,
  `USER_GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_BLOG_USER2GROUP_1` (`USER_ID`,`BLOG_ID`,`USER_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_user_group`
--

CREATE TABLE IF NOT EXISTS `b_blog_user_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BLOG_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_BLOG_USER_GROUP_1` (`BLOG_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `b_blog_user_group`
--

INSERT INTO `b_blog_user_group` (`ID`, `BLOG_ID`, `NAME`) VALUES
(1, NULL, 'all'),
(2, NULL, 'registered');

-- --------------------------------------------------------

--
-- Table structure for table `b_blog_user_group_perms`
--

CREATE TABLE IF NOT EXISTS `b_blog_user_group_perms` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BLOG_ID` int(11) NOT NULL,
  `USER_GROUP_ID` int(11) NOT NULL,
  `PERMS_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `POST_ID` int(11) DEFAULT NULL,
  `PERMS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D',
  `AUTOSET` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_BLOG_UG_PERMS_1` (`BLOG_ID`,`USER_GROUP_ID`,`PERMS_TYPE`,`POST_ID`),
  KEY `IX_BLOG_UG_PERMS_2` (`USER_GROUP_ID`,`PERMS_TYPE`,`POST_ID`),
  KEY `IX_BLOG_UG_PERMS_3` (`POST_ID`,`USER_GROUP_ID`,`PERMS_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_cache_tag`
--

CREATE TABLE IF NOT EXISTS `b_cache_tag` (
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CACHE_SALT` char(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RELATIVE_PATH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAG` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `ix_b_cache_tag_0` (`SITE_ID`,`CACHE_SALT`,`RELATIVE_PATH`(50)),
  KEY `ix_b_cache_tag_1` (`TAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_cache_tag`
--

INSERT INTO `b_cache_tag` (`SITE_ID`, `CACHE_SALT`, `RELATIVE_PATH`, `TAG`) VALUES
(NULL, NULL, '0:1431267472', '**'),
('s1', '/d32', '/s1/bitrix/news.list/d32', 'iblock_id_1'),
('s1', '/d32', '/s1/bitrix/news.list/d32', 'iblock_id_6'),
('s1', '/3da', '/s1/bitrix/news.list/3da', 'iblock_id_1'),
('s1', '/774', '/s1/bitrix/news.list/774', 'iblock_id_6'),
('s1', '/39c', '/s1/bitrix/menu/06f', 'bitrix:menu'),
('s1', '/39c', '/s1/bitrix/menu/39c', 'bitrix:menu'),
('s1', '/39c', '/s1/bitrix/menu/345', 'bitrix:menu'),
('s1', '/6f8', '/s1/bitrix/menu/06f', 'bitrix:menu'),
('s1', '/6f8', '/s1/bitrix/menu/345', 'bitrix:menu'),
('s1', '/39c', '/s1/bitrix/menu.sections/06f', 'iblock_id_7'),
('s1', '/39c', '/s1/bitrix/news.list/39c', 'iblock_id_7'),
('s1', '/39c', '/s1/bitrix/news.detail/39c', 'iblock_id_7'),
('s1', '/274', '/s1/bitrix/menu/06f', 'bitrix:menu'),
('s1', '/274', '/s1/bitrix/menu/345', 'bitrix:menu'),
('s1', '/e25', '/s1/bitrix/menu/06f', 'bitrix:menu'),
('s1', '/e25', '/s1/bitrix/menu/345', 'bitrix:menu'),
('s1', '/d32', '/s1/bitrix/menu/06f', 'bitrix:menu'),
('s1', '/d32', '/s1/bitrix/menu/345', 'bitrix:menu'),
('s1', '/842', '/s1/bitrix/menu/06f', 'bitrix:menu'),
('s1', '/842', '/s1/bitrix/menu/345', 'bitrix:menu'),
('s1', '/935', '/s1/bitrix/menu/06f', 'bitrix:menu'),
('s1', '/935', '/s1/bitrix/menu/345', 'bitrix:menu'),
('s1', '/e13', '/s1/bitrix/menu/06f', 'bitrix:menu'),
('s1', '/e13', '/s1/bitrix/menu/345', 'bitrix:menu'),
('s1', '/274', '/s1/bitrix/menu.sections/06f', 'iblock_id_2'),
('s1', '/274', 'iblock_find', 'iblock_id_2'),
('s1', '/274', '/s1/bitrix/catalog.section.list/274', 'iblock_id_2'),
('s1', '/274', '/s1/bitrix/catalog.section/274', 'iblock_id_2'),
('s1', '/274', '/s1/bitrix/menu.sections/345', 'iblock_id_2');

-- --------------------------------------------------------

--
-- Table structure for table `b_captcha`
--

CREATE TABLE IF NOT EXISTS `b_captcha` (
  `ID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `CODE` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `IP` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime NOT NULL,
  UNIQUE KEY `UX_B_CAPTCHA` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_contractor`
--

CREATE TABLE IF NOT EXISTS `b_catalog_contractor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERSON_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `PERSON_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSON_LASTNAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSON_MIDDLENAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHONE` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `POST_INDEX` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COUNTRY` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CITY` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMPANY` varchar(145) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INN` varchar(145) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KPP` varchar(145) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ADDRESS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_MODIFY` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_currency`
--

CREATE TABLE IF NOT EXISTS `b_catalog_currency` (
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `AMOUNT_CNT` int(11) NOT NULL DEFAULT '1',
  `AMOUNT` decimal(18,4) DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DATE_UPDATE` datetime NOT NULL,
  `NUMCODE` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BASE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CREATED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `CURRENT_BASE_RATE` decimal(26,12) DEFAULT NULL,
  PRIMARY KEY (`CURRENCY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_catalog_currency`
--

INSERT INTO `b_catalog_currency` (`CURRENCY`, `AMOUNT_CNT`, `AMOUNT`, `SORT`, `DATE_UPDATE`, `NUMCODE`, `BASE`, `CREATED_BY`, `DATE_CREATE`, `MODIFIED_BY`, `CURRENT_BASE_RATE`) VALUES
('BYR', 10000, '36.7200', 500, '2015-04-22 23:26:30', NULL, 'N', NULL, '2015-04-22 23:26:30', NULL, '0.003672000000'),
('EUR', 1, '43.8000', 300, '2015-04-22 23:26:30', NULL, 'N', NULL, '2015-04-22 23:26:30', NULL, '43.800000000000'),
('RUB', 1, '1.0000', 100, '2015-04-22 23:26:30', NULL, 'Y', NULL, '2015-04-22 23:26:30', NULL, '1.000000000000'),
('UAH', 10, '39.4100', 400, '2015-04-22 23:26:30', NULL, 'N', NULL, '2015-04-22 23:26:30', NULL, '3.941000000000'),
('USD', 1, '32.3000', 200, '2015-04-22 23:26:30', NULL, 'N', NULL, '2015-04-22 23:26:30', NULL, '32.300000000000');

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_currency_lang`
--

CREATE TABLE IF NOT EXISTS `b_catalog_currency_lang` (
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT_STRING` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FULL_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEC_POINT` varchar(5) COLLATE utf8_unicode_ci DEFAULT '.',
  `THOUSANDS_SEP` varchar(5) COLLATE utf8_unicode_ci DEFAULT ' ',
  `DECIMALS` tinyint(4) NOT NULL DEFAULT '2',
  `THOUSANDS_VARIANT` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HIDE_ZERO` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CREATED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  PRIMARY KEY (`CURRENCY`,`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_catalog_currency_lang`
--

INSERT INTO `b_catalog_currency_lang` (`CURRENCY`, `LID`, `FORMAT_STRING`, `FULL_NAME`, `DEC_POINT`, `THOUSANDS_SEP`, `DECIMALS`, `THOUSANDS_VARIANT`, `HIDE_ZERO`, `CREATED_BY`, `DATE_CREATE`, `MODIFIED_BY`, `TIMESTAMP_X`) VALUES
('BYR', 'en', '# BYR', 'Belarusian ruble', '.', '', 2, 'S', 'Y', NULL, '2015-04-22 23:26:30', NULL, '2015-04-22 23:26:30'),
('BYR', 'ru', '# руб.', 'Белорусский рубль', '.', '', 2, 'S', 'Y', NULL, '2015-04-22 23:26:30', NULL, '2015-04-22 23:26:30'),
('EUR', 'en', '&euro;#', 'Euro', '.', '', 2, 'C', 'Y', NULL, '2015-04-22 23:26:30', NULL, '2015-04-22 23:26:30'),
('EUR', 'ru', '&euro;#', 'Евро', '.', '', 2, 'C', 'Y', NULL, '2015-04-22 23:26:30', NULL, '2015-04-22 23:26:30'),
('RUB', 'en', '# rub.', 'Ruble', '.', '', 2, 'S', 'Y', NULL, '2015-04-22 23:26:30', NULL, '2015-04-22 23:26:30'),
('RUB', 'ru', '# руб.', 'Рубль', '.', '', 2, 'S', 'Y', NULL, '2015-04-22 23:26:30', NULL, '2015-04-22 23:26:30'),
('UAH', 'en', 'UAH #', 'UAH', '.', '', 2, 'S', 'Y', NULL, '2015-04-22 23:26:30', NULL, '2015-04-22 23:26:30'),
('UAH', 'ru', '# грн.', 'Гривна', '.', '', 2, 'S', 'Y', NULL, '2015-04-22 23:26:30', NULL, '2015-04-22 23:26:30'),
('USD', 'en', '$#', 'US Dollar', '.', '', 2, 'C', 'Y', NULL, '2015-04-22 23:26:30', NULL, '2015-04-22 23:26:30'),
('USD', 'ru', '$#', 'Доллар США', '.', '', 2, 'C', 'Y', NULL, '2015-04-22 23:26:30', NULL, '2015-04-22 23:26:30');

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_currency_rate`
--

CREATE TABLE IF NOT EXISTS `b_catalog_currency_rate` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_RATE` date NOT NULL,
  `RATE_CNT` int(11) NOT NULL DEFAULT '1',
  `RATE` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `CREATED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_CURRENCY_RATE` (`CURRENCY`,`DATE_RATE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_discount`
--

CREATE TABLE IF NOT EXISTS `b_catalog_discount` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` int(11) NOT NULL DEFAULT '0',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ACTIVE_FROM` datetime DEFAULT NULL,
  `ACTIVE_TO` datetime DEFAULT NULL,
  `RENEWAL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MAX_USES` int(11) NOT NULL DEFAULT '0',
  `COUNT_USES` int(11) NOT NULL DEFAULT '0',
  `COUPON` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `MAX_DISCOUNT` decimal(18,4) DEFAULT NULL,
  `VALUE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `VALUE` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `MIN_ORDER_SUM` decimal(18,4) DEFAULT '0.0000',
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `COUNT_PERIOD` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'U',
  `COUNT_SIZE` int(11) NOT NULL DEFAULT '0',
  `COUNT_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `COUNT_FROM` datetime DEFAULT NULL,
  `COUNT_TO` datetime DEFAULT NULL,
  `ACTION_SIZE` int(11) NOT NULL DEFAULT '0',
  `ACTION_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `PRIORITY` int(18) NOT NULL DEFAULT '1',
  `LAST_DISCOUNT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `VERSION` int(11) NOT NULL DEFAULT '1',
  `NOTES` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONDITIONS` text COLLATE utf8_unicode_ci,
  `UNPACK` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_C_D_COUPON` (`COUPON`),
  KEY `IX_C_D_ACT` (`ACTIVE`,`ACTIVE_FROM`,`ACTIVE_TO`),
  KEY `IX_C_D_ACT_B` (`SITE_ID`,`RENEWAL`,`ACTIVE`,`ACTIVE_FROM`,`ACTIVE_TO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_discount2cat`
--

CREATE TABLE IF NOT EXISTS `b_catalog_discount2cat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `CATALOG_GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_C_D2C_CATDIS` (`CATALOG_GROUP_ID`,`DISCOUNT_ID`),
  UNIQUE KEY `IX_C_D2C_CATDIS_B` (`DISCOUNT_ID`,`CATALOG_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_discount2group`
--

CREATE TABLE IF NOT EXISTS `b_catalog_discount2group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_C_D2G_GRDIS` (`GROUP_ID`,`DISCOUNT_ID`),
  UNIQUE KEY `IX_C_D2G_GRDIS_B` (`DISCOUNT_ID`,`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_discount2iblock`
--

CREATE TABLE IF NOT EXISTS `b_catalog_discount2iblock` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `IBLOCK_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_C_D2I_IBDIS` (`IBLOCK_ID`,`DISCOUNT_ID`),
  UNIQUE KEY `IX_C_D2I_IBDIS_B` (`DISCOUNT_ID`,`IBLOCK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_discount2product`
--

CREATE TABLE IF NOT EXISTS `b_catalog_discount2product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_C_D2P_PRODIS` (`PRODUCT_ID`,`DISCOUNT_ID`),
  UNIQUE KEY `IX_C_D2P_PRODIS_B` (`DISCOUNT_ID`,`PRODUCT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_discount2section`
--

CREATE TABLE IF NOT EXISTS `b_catalog_discount2section` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_C_D2S_SECDIS` (`SECTION_ID`,`DISCOUNT_ID`),
  UNIQUE KEY `IX_C_D2S_SECDIS_B` (`DISCOUNT_ID`,`SECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_discount_cond`
--

CREATE TABLE IF NOT EXISTS `b_catalog_discount_cond` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_GROUP_ID` int(11) NOT NULL DEFAULT '-1',
  `PRICE_TYPE_ID` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_discount_coupon`
--

CREATE TABLE IF NOT EXISTS `b_catalog_discount_coupon` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `COUPON` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_APPLY` datetime DEFAULT NULL,
  `ONE_TIME` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_cat_dc_index1` (`DISCOUNT_ID`,`COUPON`),
  KEY `ix_cat_dc_index2` (`COUPON`,`ACTIVE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_discount_module`
--

CREATE TABLE IF NOT EXISTS `b_catalog_discount_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_DSC_MOD` (`DISCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_disc_save_group`
--

CREATE TABLE IF NOT EXISTS `b_catalog_disc_save_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_DSG_DISCOUNT` (`DISCOUNT_ID`),
  KEY `IX_CAT_DSG_GROUP` (`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_disc_save_range`
--

CREATE TABLE IF NOT EXISTS `b_catalog_disc_save_range` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `RANGE_FROM` double NOT NULL,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `VALUE` double NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_DSR_DISCOUNT` (`DISCOUNT_ID`),
  KEY `IX_CAT_DSR_DISCOUNT2` (`DISCOUNT_ID`,`RANGE_FROM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_disc_save_user`
--

CREATE TABLE IF NOT EXISTS `b_catalog_disc_save_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `ACTIVE_FROM` datetime NOT NULL,
  `ACTIVE_TO` datetime NOT NULL,
  `RANGE_FROM` double NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_DSU_DISCOUNT` (`DISCOUNT_ID`),
  KEY `IX_CAT_DSU_USER` (`DISCOUNT_ID`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_docs_barcode`
--

CREATE TABLE IF NOT EXISTS `b_catalog_docs_barcode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOC_ELEMENT_ID` int(11) NOT NULL,
  `BARCODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_CATALOG_DOCS_BARCODE1` (`DOC_ELEMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_docs_element`
--

CREATE TABLE IF NOT EXISTS `b_catalog_docs_element` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOC_ID` int(11) NOT NULL,
  `STORE_FROM` int(11) DEFAULT NULL,
  `STORE_TO` int(11) DEFAULT NULL,
  `ELEMENT_ID` int(11) DEFAULT NULL,
  `AMOUNT` double DEFAULT NULL,
  `PURCHASING_PRICE` double DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_CATALOG_DOCS_ELEMENT1` (`DOC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_export`
--

CREATE TABLE IF NOT EXISTS `b_catalog_export` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FILE_NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `DEFAULT_PROFILE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IN_MENU` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IN_AGENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IN_CRON` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SETUP_VARS` text COLLATE utf8_unicode_ci,
  `LAST_USE` datetime DEFAULT NULL,
  `IS_EXPORT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NEED_EDIT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BCAT_EX_FILE_NAME` (`FILE_NAME`),
  KEY `IX_CAT_IS_EXPORT` (`IS_EXPORT`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `b_catalog_export`
--

INSERT INTO `b_catalog_export` (`ID`, `FILE_NAME`, `NAME`, `DEFAULT_PROFILE`, `IN_MENU`, `IN_AGENT`, `IN_CRON`, `SETUP_VARS`, `LAST_USE`, `IS_EXPORT`, `NEED_EDIT`, `TIMESTAMP_X`, `MODIFIED_BY`, `DATE_CREATE`, `CREATED_BY`) VALUES
(1, 'yandex', 'IRR.ru ("Из рук в руки")', 'N', 'N', 'N', 'N', NULL, NULL, 'Y', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_extra`
--

CREATE TABLE IF NOT EXISTS `b_catalog_extra` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PERCENTAGE` decimal(18,2) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_group`
--

CREATE TABLE IF NOT EXISTS `b_catalog_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `BASE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SORT` int(11) NOT NULL DEFAULT '100',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_group2group`
--

CREATE TABLE IF NOT EXISTS `b_catalog_group2group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATALOG_GROUP_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `BUY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_CATG2G_UNI` (`CATALOG_GROUP_ID`,`GROUP_ID`,`BUY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_group_lang`
--

CREATE TABLE IF NOT EXISTS `b_catalog_group_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATALOG_GROUP_ID` int(11) NOT NULL,
  `LANG` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_CATALOG_GROUP_ID` (`CATALOG_GROUP_ID`,`LANG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_iblock`
--

CREATE TABLE IF NOT EXISTS `b_catalog_iblock` (
  `IBLOCK_ID` int(11) NOT NULL,
  `YANDEX_EXPORT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SUBSCRIPTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `VAT_ID` int(11) DEFAULT '0',
  `PRODUCT_IBLOCK_ID` int(11) NOT NULL DEFAULT '0',
  `SKU_PROPERTY_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`IBLOCK_ID`),
  KEY `IXS_CAT_IB_PRODUCT` (`PRODUCT_IBLOCK_ID`),
  KEY `IXS_CAT_IB_SKU_PROP` (`SKU_PROPERTY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_load`
--

CREATE TABLE IF NOT EXISTS `b_catalog_load` (
  `NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'I',
  `LAST_USED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`NAME`,`TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_measure`
--

CREATE TABLE IF NOT EXISTS `b_catalog_measure` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` int(11) NOT NULL,
  `MEASURE_TITLE` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYMBOL_RUS` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYMBOL_INTL` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SYMBOL_LETTER_INTL` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_DEFAULT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_CATALOG_MEASURE1` (`CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `b_catalog_measure`
--

INSERT INTO `b_catalog_measure` (`ID`, `CODE`, `MEASURE_TITLE`, `SYMBOL_RUS`, `SYMBOL_INTL`, `SYMBOL_LETTER_INTL`, `IS_DEFAULT`) VALUES
(1, 6, NULL, NULL, 'm', 'MTR', 'N'),
(2, 112, NULL, NULL, 'l', 'LTR', 'N'),
(3, 163, NULL, NULL, 'g', 'GRM', 'N'),
(4, 166, NULL, NULL, 'kg', 'KGM', 'N'),
(5, 796, NULL, NULL, 'pc. 1', 'PCE. NMB', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_measure_ratio`
--

CREATE TABLE IF NOT EXISTS `b_catalog_measure_ratio` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `RATIO` double NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_CATALOG_MEASURE_RATIO` (`PRODUCT_ID`,`RATIO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_price`
--

CREATE TABLE IF NOT EXISTS `b_catalog_price` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `EXTRA_ID` int(11) DEFAULT NULL,
  `CATALOG_GROUP_ID` int(11) NOT NULL,
  `PRICE` decimal(18,2) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `QUANTITY_FROM` int(11) DEFAULT NULL,
  `QUANTITY_TO` int(11) DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_CAT_PRICE_PID` (`PRODUCT_ID`,`CATALOG_GROUP_ID`),
  KEY `IXS_CAT_PRICE_GID` (`CATALOG_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_product`
--

CREATE TABLE IF NOT EXISTS `b_catalog_product` (
  `ID` int(11) NOT NULL,
  `QUANTITY` double NOT NULL,
  `QUANTITY_TRACE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `WEIGHT` double NOT NULL DEFAULT '0',
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PRICE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `RECUR_SCHEME_LENGTH` int(11) DEFAULT NULL,
  `RECUR_SCHEME_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D',
  `TRIAL_PRICE_ID` int(11) DEFAULT NULL,
  `WITHOUT_ORDER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SELECT_BEST_PRICE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `VAT_ID` int(11) DEFAULT '0',
  `VAT_INCLUDED` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `CAN_BUY_ZERO` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `NEGATIVE_AMOUNT_TRACE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D',
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PURCHASING_PRICE` decimal(18,2) DEFAULT NULL,
  `PURCHASING_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BARCODE_MULTI` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `QUANTITY_RESERVED` double DEFAULT '0',
  `SUBSCRIBE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WIDTH` double DEFAULT NULL,
  `LENGTH` double DEFAULT NULL,
  `HEIGHT` double DEFAULT NULL,
  `MEASURE` int(11) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_product2group`
--

CREATE TABLE IF NOT EXISTS `b_catalog_product2group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `ACCESS_LENGTH` int(11) NOT NULL,
  `ACCESS_LENGTH_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_C_P2G_PROD_GROUP` (`PRODUCT_ID`,`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_product_sets`
--

CREATE TABLE IF NOT EXISTS `b_catalog_product_sets` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYPE` int(11) NOT NULL,
  `SET_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `OWNER_ID` int(11) NOT NULL,
  `ITEM_ID` int(11) NOT NULL,
  `QUANTITY` double DEFAULT NULL,
  `MEASURE` int(11) DEFAULT NULL,
  `DISCOUNT_PERCENT` double DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `CREATED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_PR_SET_TYPE` (`TYPE`),
  KEY `IX_CAT_PR_SET_OWNER_ID` (`OWNER_ID`),
  KEY `IX_CAT_PR_SET_SET_ID` (`SET_ID`),
  KEY `IX_CAT_PR_SET_ITEM_ID` (`ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_store`
--

CREATE TABLE IF NOT EXISTS `b_catalog_store` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ADDRESS` varchar(245) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `GPS_N` varchar(15) COLLATE utf8_unicode_ci DEFAULT '0',
  `GPS_S` varchar(15) COLLATE utf8_unicode_ci DEFAULT '0',
  `IMAGE_ID` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOCATION_ID` int(11) DEFAULT NULL,
  `DATE_MODIFY` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DATE_CREATE` datetime DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  `PHONE` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SCHEDULE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ISSUING_CENTER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SHIPPING_CENTER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_store_barcode`
--

CREATE TABLE IF NOT EXISTS `b_catalog_store_barcode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `BARCODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STORE_ID` int(11) DEFAULT NULL,
  `ORDER_ID` int(11) DEFAULT NULL,
  `DATE_MODIFY` datetime DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_CATALOG_STORE_BARCODE1` (`BARCODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_store_docs`
--

CREATE TABLE IF NOT EXISTS `b_catalog_store_docs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOC_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONTRACTOR_ID` int(11) DEFAULT NULL,
  `DATE_MODIFY` datetime DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_STATUS` datetime DEFAULT NULL,
  `DATE_DOCUMENT` datetime DEFAULT NULL,
  `STATUS_BY` int(11) DEFAULT NULL,
  `TOTAL` double DEFAULT NULL,
  `COMMENTARY` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_store_product`
--

CREATE TABLE IF NOT EXISTS `b_catalog_store_product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `AMOUNT` double NOT NULL DEFAULT '0',
  `STORE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_CATALOG_STORE_PRODUCT2` (`PRODUCT_ID`,`STORE_ID`),
  KEY `IX_CATALOG_STORE_PRODUCT1` (`STORE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_vat`
--

CREATE TABLE IF NOT EXISTS `b_catalog_vat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `RATE` decimal(18,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_VAT_ACTIVE` (`ACTIVE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_catalog_viewed_product`
--

CREATE TABLE IF NOT EXISTS `b_catalog_viewed_product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FUSER_ID` int(11) NOT NULL,
  `DATE_VISIT` datetime NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `ELEMENT_ID` int(11) NOT NULL DEFAULT '0',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `VIEW_COUNT` int(11) NOT NULL DEFAULT '1',
  `RECOMMENDATION` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CAT_V_PR_FUSER_ID` (`FUSER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_checklist`
--

CREATE TABLE IF NOT EXISTS `b_checklist` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_CREATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TESTER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMPANY_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PICTURE` int(11) DEFAULT NULL,
  `TOTAL` int(11) DEFAULT NULL,
  `SUCCESS` int(11) DEFAULT NULL,
  `FAILED` int(11) DEFAULT NULL,
  `PENDING` int(11) DEFAULT NULL,
  `SKIP` int(11) DEFAULT NULL,
  `STATE` longtext COLLATE utf8_unicode_ci,
  `REPORT_COMMENT` text COLLATE utf8_unicode_ci,
  `REPORT` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `EMAIL` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHONE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SENDED_TO_BITRIX` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `HIDDEN` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_clouds_file_bucket`
--

CREATE TABLE IF NOT EXISTS `b_clouds_file_bucket` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `SORT` int(11) DEFAULT '500',
  `READ_ONLY` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `SERVICE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUCKET` varchar(63) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOCATION` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CNAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILE_COUNT` int(11) DEFAULT '0',
  `FILE_SIZE` float DEFAULT '0',
  `LAST_FILE_ID` int(11) DEFAULT NULL,
  `PREFIX` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SETTINGS` text COLLATE utf8_unicode_ci,
  `FILE_RULES` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_clouds_file_resize`
--

CREATE TABLE IF NOT EXISTS `b_clouds_file_resize` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ERROR_CODE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `FILE_ID` int(11) DEFAULT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  `FROM_PATH` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TO_PATH` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_file_resize_ts` (`TIMESTAMP_X`),
  KEY `ix_b_file_resize_path` (`TO_PATH`(100)),
  KEY `ix_b_file_resize_file` (`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_clouds_file_upload`
--

CREATE TABLE IF NOT EXISTS `b_clouds_file_upload` (
  `ID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FILE_PATH` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `TMP_FILE` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BUCKET_ID` int(11) NOT NULL,
  `PART_SIZE` int(11) NOT NULL,
  `PART_NO` int(11) NOT NULL,
  `PART_FAIL_COUNTER` int(11) NOT NULL,
  `NEXT_STEP` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_component_params`
--

CREATE TABLE IF NOT EXISTS `b_component_params` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `COMPONENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TEMPLATE_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REAL_PATH` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SEF_MODE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SEF_FOLDER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `START_CHAR` int(11) NOT NULL,
  `END_CHAR` int(11) NOT NULL,
  `PARAMETERS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_comp_params_name` (`COMPONENT_NAME`),
  KEY `ix_comp_params_path` (`SITE_ID`,`REAL_PATH`),
  KEY `ix_comp_params_sname` (`SITE_ID`,`COMPONENT_NAME`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=95 ;

--
-- Dumping data for table `b_component_params`
--

INSERT INTO `b_component_params` (`ID`, `SITE_ID`, `COMPONENT_NAME`, `TEMPLATE_NAME`, `REAL_PATH`, `SEF_MODE`, `SEF_FOLDER`, `START_CHAR`, `END_CHAR`, `PARAMETERS`) VALUES
(10, 's1', 'bitrix:menu', 'maintop', '/bitrix/templates/main_tpl/header.php', 'N', NULL, 1061, 1760, 'a:10:{s:14:"ROOT_MENU_TYPE";s:3:"top";s:15:"MENU_CACHE_TYPE";s:1:"A";s:15:"MENU_CACHE_TIME";s:4:"3600";s:21:"MENU_CACHE_USE_GROUPS";s:1:"Y";s:19:"MENU_CACHE_GET_VARS";s:0:"";s:9:"MAX_LEVEL";s:1:"1";s:15:"CHILD_MENU_TYPE";s:4:"left";s:7:"USE_EXT";s:1:"N";s:5:"DELAY";s:1:"N";s:18:"ALLOW_MULTI_SELECT";s:1:"N";}'),
(11, 's1', 'bitrix:main.include', '.default', '/bitrix/templates/main_tpl/header.php', 'N', NULL, 1766, 1956, 'a:3:{s:14:"AREA_FILE_SHOW";s:4:"file";s:4:"PATH";s:28:"/include/header_contacts.php";s:13:"EDIT_TEMPLATE";s:0:"";}'),
(12, 's1', 'bitrix:main.include', '.default', '/bitrix/templates/main_tpl/header.php', 'N', NULL, 2014, 2200, 'a:3:{s:14:"AREA_FILE_SHOW";s:4:"file";s:4:"PATH";s:24:"/include/header_text.php";s:13:"EDIT_TEMPLATE";s:0:"";}'),
(55, 's1', 'bitrix:menu', 'footermenu', '/bitrix/templates/main_tpl/footer.php', 'N', NULL, 174, 577, 'a:11:{s:14:"ROOT_MENU_TYPE";s:7:"bottom1";s:15:"MENU_CACHE_TYPE";s:1:"A";s:15:"MENU_CACHE_TIME";s:4:"3600";s:21:"MENU_CACHE_USE_GROUPS";s:1:"Y";s:19:"MENU_CACHE_GET_VARS";N;s:9:"MAX_LEVEL";s:1:"1";s:15:"CHILD_MENU_TYPE";s:4:"left";s:7:"USE_EXT";s:1:"Y";s:5:"DELAY";s:1:"N";s:18:"ALLOW_MULTI_SELECT";s:1:"N";s:10:"MENU_TITLE";s:18:"Продукция";}'),
(56, 's1', 'bitrix:menu', 'footermenu', '/bitrix/templates/main_tpl/footer.php', 'N', NULL, 623, 1034, 'a:11:{s:14:"ROOT_MENU_TYPE";s:7:"bottom2";s:15:"MENU_CACHE_TYPE";s:1:"A";s:15:"MENU_CACHE_TIME";s:4:"3600";s:21:"MENU_CACHE_USE_GROUPS";s:1:"Y";s:19:"MENU_CACHE_GET_VARS";N;s:9:"MAX_LEVEL";s:1:"1";s:15:"CHILD_MENU_TYPE";s:4:"left";s:7:"USE_EXT";s:1:"Y";s:5:"DELAY";s:1:"N";s:18:"ALLOW_MULTI_SELECT";s:1:"N";s:10:"MENU_TITLE";s:33:"Комплексные линии";}'),
(57, 's1', 'bitrix:menu', 'footermenu', '/bitrix/templates/main_tpl/footer.php', 'N', NULL, 1039, 1439, 'a:11:{s:14:"ROOT_MENU_TYPE";s:7:"bottom3";s:15:"MENU_CACHE_TYPE";s:1:"A";s:15:"MENU_CACHE_TIME";s:4:"3600";s:21:"MENU_CACHE_USE_GROUPS";s:1:"Y";s:19:"MENU_CACHE_GET_VARS";N;s:9:"MAX_LEVEL";s:1:"1";s:15:"CHILD_MENU_TYPE";s:4:"left";s:7:"USE_EXT";s:1:"Y";s:5:"DELAY";s:1:"N";s:18:"ALLOW_MULTI_SELECT";s:1:"N";s:10:"MENU_TITLE";s:12:"Услуги";}'),
(58, 's1', 'bitrix:menu', 'footermenu', '/bitrix/templates/main_tpl/footer.php', 'N', NULL, 1485, 1886, 'a:11:{s:14:"ROOT_MENU_TYPE";s:7:"bottom4";s:15:"MENU_CACHE_TYPE";s:1:"A";s:15:"MENU_CACHE_TIME";s:4:"3600";s:21:"MENU_CACHE_USE_GROUPS";s:1:"Y";s:19:"MENU_CACHE_GET_VARS";N;s:9:"MAX_LEVEL";s:1:"1";s:15:"CHILD_MENU_TYPE";s:4:"left";s:7:"USE_EXT";s:1:"N";s:5:"DELAY";s:1:"N";s:18:"ALLOW_MULTI_SELECT";s:1:"N";s:10:"MENU_TITLE";s:14:"Проекты";}'),
(59, 's1', 'bitrix:menu', 'footermenu', '/bitrix/templates/main_tpl/footer.php', 'N', NULL, 1891, 2293, 'a:11:{s:14:"ROOT_MENU_TYPE";s:7:"bottom5";s:15:"MENU_CACHE_TYPE";s:1:"A";s:15:"MENU_CACHE_TIME";s:4:"3600";s:21:"MENU_CACHE_USE_GROUPS";s:1:"Y";s:19:"MENU_CACHE_GET_VARS";N;s:9:"MAX_LEVEL";s:1:"1";s:15:"CHILD_MENU_TYPE";s:4:"left";s:7:"USE_EXT";s:1:"Y";s:5:"DELAY";s:1:"N";s:18:"ALLOW_MULTI_SELECT";s:1:"N";s:10:"MENU_TITLE";s:16:"Компания";}'),
(60, 's1', 'bitrix:main.include', '', '/bitrix/templates/main_tpl/footer.php', 'N', NULL, 2339, 2404, 'a:0:{}'),
(61, 's1', 'bitrix:main.include', '.default', '/bitrix/templates/main_tpl/footer.php', 'N', NULL, 2447, 2637, 'a:3:{s:14:"AREA_FILE_SHOW";s:4:"file";s:4:"PATH";s:28:"/include/footer_contacts.php";s:13:"EDIT_TEMPLATE";s:0:"";}'),
(62, 's1', 'bitrix:news.list', 'kompany', '/kompaniya/index.php', 'N', NULL, 633, 3624, 'a:46:{s:12:"DISPLAY_DATE";s:1:"Y";s:12:"DISPLAY_NAME";s:1:"Y";s:15:"DISPLAY_PICTURE";s:1:"Y";s:20:"DISPLAY_PREVIEW_TEXT";s:1:"Y";s:9:"AJAX_MODE";s:1:"Y";s:11:"IBLOCK_TYPE";s:4:"news";s:9:"IBLOCK_ID";s:1:"1";s:10:"NEWS_COUNT";s:1:"4";s:8:"SORT_BY1";s:11:"ACTIVE_FROM";s:11:"SORT_ORDER1";s:4:"DESC";s:8:"SORT_BY2";s:4:"SORT";s:11:"SORT_ORDER2";s:3:"ASC";s:11:"FILTER_NAME";s:0:"";s:10:"FIELD_CODE";s:0:"";s:13:"PROPERTY_CODE";s:0:"";s:11:"CHECK_DATES";s:1:"Y";s:10:"DETAIL_URL";s:0:"";s:20:"PREVIEW_TRUNCATE_LEN";s:0:"";s:18:"ACTIVE_DATE_FORMAT";s:5:"j F Y";s:9:"SET_TITLE";s:1:"N";s:17:"SET_BROWSER_TITLE";s:1:"N";s:17:"SET_META_KEYWORDS";s:1:"N";s:20:"SET_META_DESCRIPTION";s:1:"N";s:14:"SET_STATUS_404";s:1:"N";s:25:"INCLUDE_IBLOCK_INTO_CHAIN";s:1:"N";s:18:"ADD_SECTIONS_CHAIN";s:1:"N";s:24:"HIDE_LINK_WHEN_NO_DETAIL";s:1:"N";s:14:"PARENT_SECTION";s:0:"";s:19:"PARENT_SECTION_CODE";s:0:"";s:19:"INCLUDE_SUBSECTIONS";s:1:"Y";s:10:"CACHE_TYPE";s:1:"A";s:10:"CACHE_TIME";s:4:"3600";s:12:"CACHE_FILTER";s:1:"Y";s:12:"CACHE_GROUPS";s:1:"Y";s:17:"DISPLAY_TOP_PAGER";s:1:"N";s:20:"DISPLAY_BOTTOM_PAGER";s:1:"N";s:11:"PAGER_TITLE";s:14:"Новости";s:17:"PAGER_SHOW_ALWAYS";s:1:"N";s:14:"PAGER_TEMPLATE";s:0:"";s:20:"PAGER_DESC_NUMBERING";s:1:"Y";s:31:"PAGER_DESC_NUMBERING_CACHE_TIME";s:5:"36000";s:14:"PAGER_SHOW_ALL";s:1:"N";s:16:"AJAX_OPTION_JUMP";s:1:"N";s:17:"AJAX_OPTION_STYLE";s:1:"Y";s:19:"AJAX_OPTION_HISTORY";s:1:"N";s:22:"AJAX_OPTION_ADDITIONAL";s:0:"";}'),
(63, 's1', 'bitrix:news.list', '', '/kompaniya/index.php', 'N', NULL, 3690, 5347, 'a:46:{s:12:"DISPLAY_DATE";s:1:"N";s:12:"DISPLAY_NAME";s:1:"Y";s:15:"DISPLAY_PICTURE";s:1:"N";s:20:"DISPLAY_PREVIEW_TEXT";s:1:"Y";s:9:"AJAX_MODE";s:1:"Y";s:11:"IBLOCK_TYPE";s:4:"news";s:9:"IBLOCK_ID";s:1:"6";s:10:"NEWS_COUNT";s:1:"4";s:8:"SORT_BY1";s:4:"SORT";s:11:"SORT_ORDER1";s:3:"ASC";s:8:"SORT_BY2";s:2:"ID";s:11:"SORT_ORDER2";s:4:"DESC";s:11:"FILTER_NAME";s:0:"";s:10:"FIELD_CODE";N;s:13:"PROPERTY_CODE";N;s:11:"CHECK_DATES";s:1:"Y";s:10:"DETAIL_URL";s:0:"";s:20:"PREVIEW_TRUNCATE_LEN";s:0:"";s:18:"ACTIVE_DATE_FORMAT";s:5:"j F Y";s:9:"SET_TITLE";s:1:"N";s:17:"SET_BROWSER_TITLE";s:1:"N";s:17:"SET_META_KEYWORDS";s:1:"N";s:20:"SET_META_DESCRIPTION";s:1:"N";s:14:"SET_STATUS_404";s:1:"N";s:25:"INCLUDE_IBLOCK_INTO_CHAIN";s:1:"N";s:18:"ADD_SECTIONS_CHAIN";s:1:"N";s:24:"HIDE_LINK_WHEN_NO_DETAIL";s:1:"N";s:14:"PARENT_SECTION";s:0:"";s:19:"PARENT_SECTION_CODE";s:0:"";s:19:"INCLUDE_SUBSECTIONS";s:1:"Y";s:10:"CACHE_TYPE";s:1:"A";s:10:"CACHE_TIME";s:4:"3600";s:12:"CACHE_FILTER";s:1:"Y";s:12:"CACHE_GROUPS";s:1:"Y";s:17:"DISPLAY_TOP_PAGER";s:1:"N";s:20:"DISPLAY_BOTTOM_PAGER";s:1:"N";s:11:"PAGER_TITLE";s:12:"Статьи";s:17:"PAGER_SHOW_ALWAYS";s:1:"N";s:14:"PAGER_TEMPLATE";s:0:"";s:20:"PAGER_DESC_NUMBERING";s:1:"Y";s:31:"PAGER_DESC_NUMBERING_CACHE_TIME";s:5:"36000";s:14:"PAGER_SHOW_ALL";s:1:"N";s:16:"AJAX_OPTION_JUMP";s:1:"N";s:17:"AJAX_OPTION_STYLE";s:1:"Y";s:19:"AJAX_OPTION_HISTORY";s:1:"N";s:22:"AJAX_OPTION_ADDITIONAL";s:0:"";}'),
(66, 's1', 'bitrix:search.form', 'flat', '/bitrix/templates/.default/components/bitrix/news/aquaculture/news.php', 'N', NULL, 1065, 1224, 'a:1:{s:4:"PAGE";s:59:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["search"]}";}'),
(67, 's1', 'bitrix:catalog.filter', '', '/bitrix/templates/.default/components/bitrix/news/aquaculture/news.php', 'N', NULL, 1284, 1743, 'a:8:{s:11:"IBLOCK_TYPE";s:27:"={$arParams["IBLOCK_TYPE"]}";s:9:"IBLOCK_ID";s:25:"={$arParams["IBLOCK_ID"]}";s:11:"FILTER_NAME";s:27:"={$arParams["FILTER_NAME"]}";s:10:"FIELD_CODE";s:33:"={$arParams["FILTER_FIELD_CODE"]}";s:13:"PROPERTY_CODE";s:36:"={$arParams["FILTER_PROPERTY_CODE"]}";s:10:"CACHE_TYPE";s:26:"={$arParams["CACHE_TYPE"]}";s:10:"CACHE_TIME";s:26:"={$arParams["CACHE_TIME"]}";s:12:"CACHE_GROUPS";s:28:"={$arParams["CACHE_GROUPS"]}";}'),
(68, 's1', 'bitrix:news.list', '', '/bitrix/templates/.default/components/bitrix/news/aquaculture/news.php', 'N', NULL, 1766, 3929, 'a:39:{s:11:"IBLOCK_TYPE";s:27:"={$arParams["IBLOCK_TYPE"]}";s:9:"IBLOCK_ID";s:25:"={$arParams["IBLOCK_ID"]}";s:10:"NEWS_COUNT";s:26:"={$arParams["NEWS_COUNT"]}";s:8:"SORT_BY1";s:24:"={$arParams["SORT_BY1"]}";s:11:"SORT_ORDER1";s:27:"={$arParams["SORT_ORDER1"]}";s:8:"SORT_BY2";s:24:"={$arParams["SORT_BY2"]}";s:11:"SORT_ORDER2";s:27:"={$arParams["SORT_ORDER2"]}";s:10:"FIELD_CODE";s:31:"={$arParams["LIST_FIELD_CODE"]}";s:13:"PROPERTY_CODE";s:34:"={$arParams["LIST_PROPERTY_CODE"]}";s:10:"DETAIL_URL";s:59:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"]}";s:11:"SECTION_URL";s:60:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"]}";s:10:"IBLOCK_URL";s:57:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"]}";s:13:"DISPLAY_PANEL";s:29:"={$arParams["DISPLAY_PANEL"]}";s:9:"SET_TITLE";s:25:"={$arParams["SET_TITLE"]}";s:14:"SET_STATUS_404";s:30:"={$arParams["SET_STATUS_404"]}";s:25:"INCLUDE_IBLOCK_INTO_CHAIN";s:41:"={$arParams["INCLUDE_IBLOCK_INTO_CHAIN"]}";s:10:"CACHE_TYPE";s:26:"={$arParams["CACHE_TYPE"]}";s:10:"CACHE_TIME";s:26:"={$arParams["CACHE_TIME"]}";s:12:"CACHE_FILTER";s:28:"={$arParams["CACHE_FILTER"]}";s:12:"CACHE_GROUPS";s:28:"={$arParams["CACHE_GROUPS"]}";s:17:"DISPLAY_TOP_PAGER";s:33:"={$arParams["DISPLAY_TOP_PAGER"]}";s:20:"DISPLAY_BOTTOM_PAGER";s:36:"={$arParams["DISPLAY_BOTTOM_PAGER"]}";s:11:"PAGER_TITLE";s:27:"={$arParams["PAGER_TITLE"]}";s:14:"PAGER_TEMPLATE";s:30:"={$arParams["PAGER_TEMPLATE"]}";s:17:"PAGER_SHOW_ALWAYS";s:33:"={$arParams["PAGER_SHOW_ALWAYS"]}";s:20:"PAGER_DESC_NUMBERING";s:36:"={$arParams["PAGER_DESC_NUMBERING"]}";s:31:"PAGER_DESC_NUMBERING_CACHE_TIME";s:47:"={$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]}";s:14:"PAGER_SHOW_ALL";s:30:"={$arParams["PAGER_SHOW_ALL"]}";s:12:"DISPLAY_DATE";s:28:"={$arParams["DISPLAY_DATE"]}";s:12:"DISPLAY_NAME";s:1:"Y";s:15:"DISPLAY_PICTURE";s:31:"={$arParams["DISPLAY_PICTURE"]}";s:20:"DISPLAY_PREVIEW_TEXT";s:36:"={$arParams["DISPLAY_PREVIEW_TEXT"]}";s:20:"PREVIEW_TRUNCATE_LEN";s:36:"={$arParams["PREVIEW_TRUNCATE_LEN"]}";s:18:"ACTIVE_DATE_FORMAT";s:39:"={$arParams["LIST_ACTIVE_DATE_FORMAT"]}";s:15:"USE_PERMISSIONS";s:31:"={$arParams["USE_PERMISSIONS"]}";s:17:"GROUP_PERMISSIONS";s:33:"={$arParams["GROUP_PERMISSIONS"]}";s:11:"FILTER_NAME";s:27:"={$arParams["FILTER_NAME"]}";s:24:"HIDE_LINK_WHEN_NO_DETAIL";s:40:"={$arParams["HIDE_LINK_WHEN_NO_DETAIL"]}";s:11:"CHECK_DATES";s:27:"={$arParams["CHECK_DATES"]}";}'),
(69, 's1', 'bitrix:news', 'aquaculture', '/kompaniya/novosti/index.php', 'Y', '/kompaniya/novosti/', 100, 2182, 'a:61:{s:11:"IBLOCK_TYPE";s:4:"news";s:9:"IBLOCK_ID";s:1:"1";s:10:"NEWS_COUNT";s:2:"20";s:10:"USE_SEARCH";s:1:"N";s:7:"USE_RSS";s:1:"N";s:10:"USE_RATING";s:1:"N";s:14:"USE_CATEGORIES";s:1:"N";s:10:"USE_REVIEW";s:1:"N";s:10:"USE_FILTER";s:1:"N";s:8:"SORT_BY1";s:11:"ACTIVE_FROM";s:11:"SORT_ORDER1";s:4:"DESC";s:8:"SORT_BY2";s:4:"SORT";s:11:"SORT_ORDER2";s:3:"ASC";s:11:"CHECK_DATES";s:1:"Y";s:8:"SEF_MODE";s:1:"Y";s:10:"SEF_FOLDER";s:19:"/kompaniya/novosti/";s:9:"AJAX_MODE";s:1:"N";s:16:"AJAX_OPTION_JUMP";s:1:"N";s:17:"AJAX_OPTION_STYLE";s:1:"N";s:19:"AJAX_OPTION_HISTORY";s:1:"N";s:10:"CACHE_TYPE";s:1:"A";s:10:"CACHE_TIME";s:8:"36000000";s:12:"CACHE_FILTER";s:1:"N";s:12:"CACHE_GROUPS";s:1:"Y";s:14:"SET_STATUS_404";s:1:"Y";s:9:"SET_TITLE";s:1:"Y";s:25:"INCLUDE_IBLOCK_INTO_CHAIN";s:1:"Y";s:18:"ADD_SECTIONS_CHAIN";s:1:"Y";s:17:"ADD_ELEMENT_CHAIN";s:1:"N";s:15:"USE_PERMISSIONS";s:1:"N";s:12:"DISPLAY_DATE";s:1:"Y";s:15:"DISPLAY_PICTURE";s:1:"Y";s:20:"DISPLAY_PREVIEW_TEXT";s:1:"Y";s:9:"USE_SHARE";s:1:"N";s:20:"PREVIEW_TRUNCATE_LEN";s:0:"";s:23:"LIST_ACTIVE_DATE_FORMAT";s:5:"j F Y";s:15:"LIST_FIELD_CODE";a:2:{i:0;s:0:"";i:1;s:0:"";}s:18:"LIST_PROPERTY_CODE";a:2:{i:0;s:0:"";i:1;s:0:"";}s:24:"HIDE_LINK_WHEN_NO_DETAIL";s:1:"N";s:12:"DISPLAY_NAME";s:1:"Y";s:13:"META_KEYWORDS";s:1:"-";s:16:"META_DESCRIPTION";s:1:"-";s:13:"BROWSER_TITLE";s:1:"-";s:25:"DETAIL_ACTIVE_DATE_FORMAT";s:5:"j F Y";s:17:"DETAIL_FIELD_CODE";a:2:{i:0;s:0:"";i:1;s:0:"";}s:20:"DETAIL_PROPERTY_CODE";a:2:{i:0;s:0:"";i:1;s:0:"";}s:24:"DETAIL_DISPLAY_TOP_PAGER";s:1:"N";s:27:"DETAIL_DISPLAY_BOTTOM_PAGER";s:1:"Y";s:18:"DETAIL_PAGER_TITLE";s:16:"Страница";s:21:"DETAIL_PAGER_TEMPLATE";s:0:"";s:21:"DETAIL_PAGER_SHOW_ALL";s:1:"Y";s:14:"PAGER_TEMPLATE";s:6:"arrows";s:17:"DISPLAY_TOP_PAGER";s:1:"N";s:20:"DISPLAY_BOTTOM_PAGER";s:1:"Y";s:11:"PAGER_TITLE";s:14:"Новости";s:17:"PAGER_SHOW_ALWAYS";s:1:"N";s:20:"PAGER_DESC_NUMBERING";s:1:"N";s:31:"PAGER_DESC_NUMBERING_CACHE_TIME";s:5:"36000";s:14:"PAGER_SHOW_ALL";s:1:"N";s:22:"AJAX_OPTION_ADDITIONAL";s:0:"";s:17:"SEF_URL_TEMPLATES";a:3:{s:4:"news";s:0:"";s:7:"section";s:0:"";s:6:"detail";s:15:"#ELEMENT_CODE#/";}}'),
(72, 's1', 'bitrix:search.form', 'flat', '/bitrix/templates/.default/components/bitrix/news/projects/news.php', 'N', NULL, 1065, 1224, 'a:1:{s:4:"PAGE";s:59:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["search"]}";}'),
(73, 's1', 'bitrix:catalog.filter', '', '/bitrix/templates/.default/components/bitrix/news/projects/news.php', 'N', NULL, 1284, 1743, 'a:8:{s:11:"IBLOCK_TYPE";s:27:"={$arParams["IBLOCK_TYPE"]}";s:9:"IBLOCK_ID";s:25:"={$arParams["IBLOCK_ID"]}";s:11:"FILTER_NAME";s:27:"={$arParams["FILTER_NAME"]}";s:10:"FIELD_CODE";s:33:"={$arParams["FILTER_FIELD_CODE"]}";s:13:"PROPERTY_CODE";s:36:"={$arParams["FILTER_PROPERTY_CODE"]}";s:10:"CACHE_TYPE";s:26:"={$arParams["CACHE_TYPE"]}";s:10:"CACHE_TIME";s:26:"={$arParams["CACHE_TIME"]}";s:12:"CACHE_GROUPS";s:28:"={$arParams["CACHE_GROUPS"]}";}'),
(74, 's1', 'bitrix:news.list', '', '/bitrix/templates/.default/components/bitrix/news/projects/news.php', 'N', NULL, 1766, 3929, 'a:39:{s:11:"IBLOCK_TYPE";s:27:"={$arParams["IBLOCK_TYPE"]}";s:9:"IBLOCK_ID";s:25:"={$arParams["IBLOCK_ID"]}";s:10:"NEWS_COUNT";s:26:"={$arParams["NEWS_COUNT"]}";s:8:"SORT_BY1";s:24:"={$arParams["SORT_BY1"]}";s:11:"SORT_ORDER1";s:27:"={$arParams["SORT_ORDER1"]}";s:8:"SORT_BY2";s:24:"={$arParams["SORT_BY2"]}";s:11:"SORT_ORDER2";s:27:"={$arParams["SORT_ORDER2"]}";s:10:"FIELD_CODE";s:31:"={$arParams["LIST_FIELD_CODE"]}";s:13:"PROPERTY_CODE";s:34:"={$arParams["LIST_PROPERTY_CODE"]}";s:10:"DETAIL_URL";s:59:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["detail"]}";s:11:"SECTION_URL";s:60:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"]}";s:10:"IBLOCK_URL";s:57:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["news"]}";s:13:"DISPLAY_PANEL";s:29:"={$arParams["DISPLAY_PANEL"]}";s:9:"SET_TITLE";s:25:"={$arParams["SET_TITLE"]}";s:14:"SET_STATUS_404";s:30:"={$arParams["SET_STATUS_404"]}";s:25:"INCLUDE_IBLOCK_INTO_CHAIN";s:41:"={$arParams["INCLUDE_IBLOCK_INTO_CHAIN"]}";s:10:"CACHE_TYPE";s:26:"={$arParams["CACHE_TYPE"]}";s:10:"CACHE_TIME";s:26:"={$arParams["CACHE_TIME"]}";s:12:"CACHE_FILTER";s:28:"={$arParams["CACHE_FILTER"]}";s:12:"CACHE_GROUPS";s:28:"={$arParams["CACHE_GROUPS"]}";s:17:"DISPLAY_TOP_PAGER";s:33:"={$arParams["DISPLAY_TOP_PAGER"]}";s:20:"DISPLAY_BOTTOM_PAGER";s:36:"={$arParams["DISPLAY_BOTTOM_PAGER"]}";s:11:"PAGER_TITLE";s:27:"={$arParams["PAGER_TITLE"]}";s:14:"PAGER_TEMPLATE";s:30:"={$arParams["PAGER_TEMPLATE"]}";s:17:"PAGER_SHOW_ALWAYS";s:33:"={$arParams["PAGER_SHOW_ALWAYS"]}";s:20:"PAGER_DESC_NUMBERING";s:36:"={$arParams["PAGER_DESC_NUMBERING"]}";s:31:"PAGER_DESC_NUMBERING_CACHE_TIME";s:47:"={$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]}";s:14:"PAGER_SHOW_ALL";s:30:"={$arParams["PAGER_SHOW_ALL"]}";s:12:"DISPLAY_DATE";s:28:"={$arParams["DISPLAY_DATE"]}";s:12:"DISPLAY_NAME";s:1:"Y";s:15:"DISPLAY_PICTURE";s:31:"={$arParams["DISPLAY_PICTURE"]}";s:20:"DISPLAY_PREVIEW_TEXT";s:36:"={$arParams["DISPLAY_PREVIEW_TEXT"]}";s:20:"PREVIEW_TRUNCATE_LEN";s:36:"={$arParams["PREVIEW_TRUNCATE_LEN"]}";s:18:"ACTIVE_DATE_FORMAT";s:39:"={$arParams["LIST_ACTIVE_DATE_FORMAT"]}";s:15:"USE_PERMISSIONS";s:31:"={$arParams["USE_PERMISSIONS"]}";s:17:"GROUP_PERMISSIONS";s:33:"={$arParams["GROUP_PERMISSIONS"]}";s:11:"FILTER_NAME";s:27:"={$arParams["FILTER_NAME"]}";s:24:"HIDE_LINK_WHEN_NO_DETAIL";s:40:"={$arParams["HIDE_LINK_WHEN_NO_DETAIL"]}";s:11:"CHECK_DATES";s:27:"={$arParams["CHECK_DATES"]}";}'),
(80, 's1', 'bitrix:news', 'projects', '/proekty/index.php', 'Y', '/proekty/', 100, 2259, 'a:61:{s:11:"IBLOCK_TYPE";s:8:"projects";s:9:"IBLOCK_ID";s:1:"7";s:10:"NEWS_COUNT";s:1:"5";s:10:"USE_SEARCH";s:1:"N";s:7:"USE_RSS";s:1:"N";s:10:"USE_RATING";s:1:"N";s:14:"USE_CATEGORIES";s:1:"N";s:10:"USE_REVIEW";s:1:"N";s:10:"USE_FILTER";s:1:"N";s:8:"SORT_BY1";s:11:"ACTIVE_FROM";s:11:"SORT_ORDER1";s:4:"DESC";s:8:"SORT_BY2";s:4:"SORT";s:11:"SORT_ORDER2";s:3:"ASC";s:11:"CHECK_DATES";s:1:"Y";s:8:"SEF_MODE";s:1:"Y";s:10:"SEF_FOLDER";s:9:"/proekty/";s:9:"AJAX_MODE";s:1:"N";s:16:"AJAX_OPTION_JUMP";s:1:"N";s:17:"AJAX_OPTION_STYLE";s:1:"Y";s:19:"AJAX_OPTION_HISTORY";s:1:"N";s:10:"CACHE_TYPE";s:1:"A";s:10:"CACHE_TIME";s:8:"36000000";s:12:"CACHE_FILTER";s:1:"N";s:12:"CACHE_GROUPS";s:1:"Y";s:14:"SET_STATUS_404";s:1:"Y";s:9:"SET_TITLE";s:1:"Y";s:25:"INCLUDE_IBLOCK_INTO_CHAIN";s:1:"Y";s:18:"ADD_SECTIONS_CHAIN";s:1:"Y";s:17:"ADD_ELEMENT_CHAIN";s:1:"N";s:15:"USE_PERMISSIONS";s:1:"N";s:12:"DISPLAY_DATE";s:1:"Y";s:15:"DISPLAY_PICTURE";s:1:"Y";s:20:"DISPLAY_PREVIEW_TEXT";s:1:"Y";s:9:"USE_SHARE";s:1:"N";s:20:"PREVIEW_TRUNCATE_LEN";s:0:"";s:23:"LIST_ACTIVE_DATE_FORMAT";s:5:"d.m.Y";s:15:"LIST_FIELD_CODE";a:4:{i:0;s:4:"NAME";i:1;s:15:"PREVIEW_PICTURE";i:2;s:14:"DETAIL_PICTURE";i:3;s:0:"";}s:18:"LIST_PROPERTY_CODE";a:2:{i:0;s:0:"";i:1;s:0:"";}s:24:"HIDE_LINK_WHEN_NO_DETAIL";s:1:"N";s:12:"DISPLAY_NAME";s:1:"Y";s:13:"META_KEYWORDS";s:1:"-";s:16:"META_DESCRIPTION";s:1:"-";s:13:"BROWSER_TITLE";s:1:"-";s:25:"DETAIL_ACTIVE_DATE_FORMAT";s:5:"d.m.Y";s:17:"DETAIL_FIELD_CODE";a:2:{i:0;s:0:"";i:1;s:0:"";}s:20:"DETAIL_PROPERTY_CODE";a:2:{i:0;s:0:"";i:1;s:0:"";}s:24:"DETAIL_DISPLAY_TOP_PAGER";s:1:"N";s:27:"DETAIL_DISPLAY_BOTTOM_PAGER";s:1:"Y";s:18:"DETAIL_PAGER_TITLE";s:16:"Страница";s:21:"DETAIL_PAGER_TEMPLATE";s:0:"";s:21:"DETAIL_PAGER_SHOW_ALL";s:1:"Y";s:14:"PAGER_TEMPLATE";s:6:"arrows";s:17:"DISPLAY_TOP_PAGER";s:1:"N";s:20:"DISPLAY_BOTTOM_PAGER";s:1:"Y";s:11:"PAGER_TITLE";s:14:"Проекты";s:17:"PAGER_SHOW_ALWAYS";s:1:"N";s:20:"PAGER_DESC_NUMBERING";s:1:"N";s:31:"PAGER_DESC_NUMBERING_CACHE_TIME";s:5:"36000";s:14:"PAGER_SHOW_ALL";s:1:"N";s:22:"AJAX_OPTION_ADDITIONAL";s:0:"";s:17:"SEF_URL_TEMPLATES";a:3:{s:4:"news";s:0:"";s:7:"section";s:15:"#SECTION_CODE#/";s:6:"detail";s:30:"#SECTION_CODE#/#ELEMENT_CODE#/";}}'),
(81, 's1', 'bitrix:menu', 'products', '/bitrix/templates/products/header.php', 'N', NULL, 402, 1107, 'a:10:{s:14:"ROOT_MENU_TYPE";s:8:"undertop";s:15:"MENU_CACHE_TYPE";s:1:"A";s:15:"MENU_CACHE_TIME";s:4:"3600";s:21:"MENU_CACHE_USE_GROUPS";s:1:"Y";s:19:"MENU_CACHE_GET_VARS";s:0:"";s:9:"MAX_LEVEL";s:1:"1";s:15:"CHILD_MENU_TYPE";s:4:"left";s:7:"USE_EXT";s:1:"Y";s:5:"DELAY";s:1:"N";s:18:"ALLOW_MULTI_SELECT";s:1:"N";}'),
(85, 's1', 'bitrix:catalog.smart.filter', '', '/bitrix/templates/.default/components/bitrix/catalog/products/section.php', 'N', NULL, 2195, 2973, 'a:15:{s:11:"IBLOCK_TYPE";s:27:"={$arParams["IBLOCK_TYPE"]}";s:9:"IBLOCK_ID";s:25:"={$arParams["IBLOCK_ID"]}";s:10:"SECTION_ID";s:22:"={$arCurSection["ID"]}";s:11:"FILTER_NAME";s:27:"={$arParams["FILTER_NAME"]}";s:10:"PRICE_CODE";s:26:"={$arParams["PRICE_CODE"]}";s:10:"CACHE_TYPE";s:26:"={$arParams["CACHE_TYPE"]}";s:10:"CACHE_TIME";s:26:"={$arParams["CACHE_TIME"]}";s:12:"CACHE_GROUPS";s:28:"={$arParams["CACHE_GROUPS"]}";s:15:"SAVE_IN_SESSION";s:1:"N";s:16:"FILTER_VIEW_MODE";s:32:"={$arParams["FILTER_VIEW_MODE"]}";s:10:"XML_EXPORT";s:1:"Y";s:13:"SECTION_TITLE";s:4:"NAME";s:19:"SECTION_DESCRIPTION";s:11:"DESCRIPTION";s:18:"HIDE_NOT_AVAILABLE";s:34:"={$arParams["HIDE_NOT_AVAILABLE"]}";s:14:"TEMPLATE_THEME";s:30:"={$arParams["TEMPLATE_THEME"]}";}'),
(86, 's1', 'bitrix:catalog.section.list', '', '/bitrix/templates/.default/components/bitrix/catalog/products/section.php', 'N', NULL, 3084, 4072, 'a:14:{s:11:"IBLOCK_TYPE";s:27:"={$arParams["IBLOCK_TYPE"]}";s:9:"IBLOCK_ID";s:25:"={$arParams["IBLOCK_ID"]}";s:10:"SECTION_ID";s:39:"={$arResult["VARIABLES"]["SECTION_ID"]}";s:12:"SECTION_CODE";s:41:"={$arResult["VARIABLES"]["SECTION_CODE"]}";s:10:"CACHE_TYPE";s:26:"={$arParams["CACHE_TYPE"]}";s:10:"CACHE_TIME";s:26:"={$arParams["CACHE_TIME"]}";s:12:"CACHE_GROUPS";s:28:"={$arParams["CACHE_GROUPS"]}";s:14:"COUNT_ELEMENTS";s:38:"={$arParams["SECTION_COUNT_ELEMENTS"]}";s:9:"TOP_DEPTH";s:33:"={$arParams["SECTION_TOP_DEPTH"]}";s:11:"SECTION_URL";s:60:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"]}";s:9:"VIEW_MODE";s:34:"={$arParams["SECTIONS_VIEW_MODE"]}";s:16:"SHOW_PARENT_NAME";s:41:"={$arParams["SECTIONS_SHOW_PARENT_NAME"]}";s:17:"HIDE_SECTION_NAME";s:95:"={(isset($arParams["SECTIONS_HIDE_SECTION_NAME"])?$arParams["SECTIONS_HIDE_SECTION_NAME"]:"N")}";s:18:"ADD_SECTIONS_CHAIN";s:78:"={(isset($arParams["ADD_SECTIONS_CHAIN"])?$arParams["ADD_SECTIONS_CHAIN"]:"")}";}'),
(87, 's1', 'bitrix:catalog.compare.list', '', '/bitrix/templates/.default/components/bitrix/catalog/products/section.php', 'N', NULL, 4118, 4851, 'a:9:{s:11:"IBLOCK_TYPE";s:27:"={$arParams["IBLOCK_TYPE"]}";s:9:"IBLOCK_ID";s:25:"={$arParams["IBLOCK_ID"]}";s:4:"NAME";s:28:"={$arParams["COMPARE_NAME"]}";s:10:"DETAIL_URL";s:60:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"]}";s:11:"COMPARE_URL";s:60:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"]}";s:15:"ACTION_VARIABLE";s:31:"={$arParams["ACTION_VARIABLE"]}";s:19:"PRODUCT_ID_VARIABLE";s:35:"={$arParams["PRODUCT_ID_VARIABLE"]}";s:14:"POSITION_FIXED";s:84:"={isset($arParams["COMPARE_POSITION_FIXED"])?$arParams["COMPARE_POSITION_FIXED"]:""}";s:8:"POSITION";s:72:"={isset($arParams["COMPARE_POSITION"])?$arParams["COMPARE_POSITION"]:""}";}'),
(88, 's1', 'bitrix:catalog.section', '', '/bitrix/templates/.default/components/bitrix/catalog/products/section.php', 'N', NULL, 5243, 9871, 'a:76:{s:11:"IBLOCK_TYPE";s:27:"={$arParams["IBLOCK_TYPE"]}";s:9:"IBLOCK_ID";s:25:"={$arParams["IBLOCK_ID"]}";s:18:"ELEMENT_SORT_FIELD";s:34:"={$arParams["ELEMENT_SORT_FIELD"]}";s:18:"ELEMENT_SORT_ORDER";s:34:"={$arParams["ELEMENT_SORT_ORDER"]}";s:19:"ELEMENT_SORT_FIELD2";s:35:"={$arParams["ELEMENT_SORT_FIELD2"]}";s:19:"ELEMENT_SORT_ORDER2";s:35:"={$arParams["ELEMENT_SORT_ORDER2"]}";s:13:"PROPERTY_CODE";s:34:"={$arParams["LIST_PROPERTY_CODE"]}";s:13:"META_KEYWORDS";s:34:"={$arParams["LIST_META_KEYWORDS"]}";s:16:"META_DESCRIPTION";s:37:"={$arParams["LIST_META_DESCRIPTION"]}";s:13:"BROWSER_TITLE";s:34:"={$arParams["LIST_BROWSER_TITLE"]}";s:19:"INCLUDE_SUBSECTIONS";s:35:"={$arParams["INCLUDE_SUBSECTIONS"]}";s:10:"BASKET_URL";s:26:"={$arParams["BASKET_URL"]}";s:15:"ACTION_VARIABLE";s:31:"={$arParams["ACTION_VARIABLE"]}";s:19:"PRODUCT_ID_VARIABLE";s:35:"={$arParams["PRODUCT_ID_VARIABLE"]}";s:19:"SECTION_ID_VARIABLE";s:35:"={$arParams["SECTION_ID_VARIABLE"]}";s:25:"PRODUCT_QUANTITY_VARIABLE";s:41:"={$arParams["PRODUCT_QUANTITY_VARIABLE"]}";s:22:"PRODUCT_PROPS_VARIABLE";s:38:"={$arParams["PRODUCT_PROPS_VARIABLE"]}";s:11:"FILTER_NAME";s:27:"={$arParams["FILTER_NAME"]}";s:10:"CACHE_TYPE";s:26:"={$arParams["CACHE_TYPE"]}";s:10:"CACHE_TIME";s:26:"={$arParams["CACHE_TIME"]}";s:12:"CACHE_FILTER";s:28:"={$arParams["CACHE_FILTER"]}";s:12:"CACHE_GROUPS";s:28:"={$arParams["CACHE_GROUPS"]}";s:9:"SET_TITLE";s:25:"={$arParams["SET_TITLE"]}";s:14:"SET_STATUS_404";s:30:"={$arParams["SET_STATUS_404"]}";s:15:"DISPLAY_COMPARE";s:27:"={$arParams["USE_COMPARE"]}";s:18:"PAGE_ELEMENT_COUNT";s:34:"={$arParams["PAGE_ELEMENT_COUNT"]}";s:18:"LINE_ELEMENT_COUNT";s:34:"={$arParams["LINE_ELEMENT_COUNT"]}";s:10:"PRICE_CODE";s:26:"={$arParams["PRICE_CODE"]}";s:15:"USE_PRICE_COUNT";s:31:"={$arParams["USE_PRICE_COUNT"]}";s:16:"SHOW_PRICE_COUNT";s:32:"={$arParams["SHOW_PRICE_COUNT"]}";s:17:"PRICE_VAT_INCLUDE";s:33:"={$arParams["PRICE_VAT_INCLUDE"]}";s:20:"USE_PRODUCT_QUANTITY";s:36:"={$arParams["USE_PRODUCT_QUANTITY"]}";s:24:"ADD_PROPERTIES_TO_BASKET";s:90:"={(isset($arParams["ADD_PROPERTIES_TO_BASKET"])?$arParams["ADD_PROPERTIES_TO_BASKET"]:"")}";s:26:"PARTIAL_PRODUCT_PROPERTIES";s:94:"={(isset($arParams["PARTIAL_PRODUCT_PROPERTIES"])?$arParams["PARTIAL_PRODUCT_PROPERTIES"]:"")}";s:18:"PRODUCT_PROPERTIES";s:34:"={$arParams["PRODUCT_PROPERTIES"]}";s:17:"DISPLAY_TOP_PAGER";s:33:"={$arParams["DISPLAY_TOP_PAGER"]}";s:20:"DISPLAY_BOTTOM_PAGER";s:36:"={$arParams["DISPLAY_BOTTOM_PAGER"]}";s:11:"PAGER_TITLE";s:27:"={$arParams["PAGER_TITLE"]}";s:17:"PAGER_SHOW_ALWAYS";s:33:"={$arParams["PAGER_SHOW_ALWAYS"]}";s:14:"PAGER_TEMPLATE";s:30:"={$arParams["PAGER_TEMPLATE"]}";s:20:"PAGER_DESC_NUMBERING";s:36:"={$arParams["PAGER_DESC_NUMBERING"]}";s:31:"PAGER_DESC_NUMBERING_CACHE_TIME";s:47:"={$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]}";s:14:"PAGER_SHOW_ALL";s:30:"={$arParams["PAGER_SHOW_ALL"]}";s:22:"OFFERS_CART_PROPERTIES";s:38:"={$arParams["OFFERS_CART_PROPERTIES"]}";s:17:"OFFERS_FIELD_CODE";s:38:"={$arParams["LIST_OFFERS_FIELD_CODE"]}";s:20:"OFFERS_PROPERTY_CODE";s:41:"={$arParams["LIST_OFFERS_PROPERTY_CODE"]}";s:17:"OFFERS_SORT_FIELD";s:33:"={$arParams["OFFERS_SORT_FIELD"]}";s:17:"OFFERS_SORT_ORDER";s:33:"={$arParams["OFFERS_SORT_ORDER"]}";s:18:"OFFERS_SORT_FIELD2";s:34:"={$arParams["OFFERS_SORT_FIELD2"]}";s:18:"OFFERS_SORT_ORDER2";s:34:"={$arParams["OFFERS_SORT_ORDER2"]}";s:12:"OFFERS_LIMIT";s:33:"={$arParams["LIST_OFFERS_LIMIT"]}";s:10:"SECTION_ID";s:39:"={$arResult["VARIABLES"]["SECTION_ID"]}";s:12:"SECTION_CODE";s:41:"={$arResult["VARIABLES"]["SECTION_CODE"]}";s:11:"SECTION_URL";s:60:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"]}";s:10:"DETAIL_URL";s:60:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"]}";s:16:"CONVERT_CURRENCY";s:32:"={$arParams["CONVERT_CURRENCY"]}";s:11:"CURRENCY_ID";s:27:"={$arParams["CURRENCY_ID"]}";s:18:"HIDE_NOT_AVAILABLE";s:34:"={$arParams["HIDE_NOT_AVAILABLE"]}";s:10:"LABEL_PROP";s:26:"={$arParams["LABEL_PROP"]}";s:13:"ADD_PICT_PROP";s:29:"={$arParams["ADD_PICT_PROP"]}";s:20:"PRODUCT_DISPLAY_MODE";s:36:"={$arParams["PRODUCT_DISPLAY_MODE"]}";s:19:"OFFER_ADD_PICT_PROP";s:35:"={$arParams["OFFER_ADD_PICT_PROP"]}";s:16:"OFFER_TREE_PROPS";s:32:"={$arParams["OFFER_TREE_PROPS"]}";s:20:"PRODUCT_SUBSCRIPTION";s:36:"={$arParams["PRODUCT_SUBSCRIPTION"]}";s:21:"SHOW_DISCOUNT_PERCENT";s:37:"={$arParams["SHOW_DISCOUNT_PERCENT"]}";s:14:"SHOW_OLD_PRICE";s:30:"={$arParams["SHOW_OLD_PRICE"]}";s:12:"MESS_BTN_BUY";s:28:"={$arParams["MESS_BTN_BUY"]}";s:22:"MESS_BTN_ADD_TO_BASKET";s:38:"={$arParams["MESS_BTN_ADD_TO_BASKET"]}";s:18:"MESS_BTN_SUBSCRIBE";s:34:"={$arParams["MESS_BTN_SUBSCRIBE"]}";s:15:"MESS_BTN_DETAIL";s:31:"={$arParams["MESS_BTN_DETAIL"]}";s:18:"MESS_NOT_AVAILABLE";s:34:"={$arParams["MESS_NOT_AVAILABLE"]}";s:14:"TEMPLATE_THEME";s:70:"={(isset($arParams["TEMPLATE_THEME"])?$arParams["TEMPLATE_THEME"]:"")}";s:18:"ADD_SECTIONS_CHAIN";s:1:"N";s:20:"ADD_TO_BASKET_ACTION";s:16:"={$basketAction}";s:16:"SHOW_CLOSE_POPUP";s:86:"={isset($arParams["COMMON_SHOW_CLOSE_POPUP"])?$arParams["COMMON_SHOW_CLOSE_POPUP"]:""}";s:12:"COMPARE_PATH";s:60:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"]}";}'),
(89, 's1', 'bitrix:sale.bestsellers', '', '/bitrix/templates/.default/components/bitrix/catalog/products/section.php', 'N', NULL, 10738, 13439, 'a:40:{s:18:"HIDE_NOT_AVAILABLE";s:34:"={$arParams["HIDE_NOT_AVAILABLE"]}";s:18:"PAGE_ELEMENT_COUNT";s:1:"5";s:21:"SHOW_DISCOUNT_PERCENT";s:37:"={$arParams["SHOW_DISCOUNT_PERCENT"]}";s:20:"PRODUCT_SUBSCRIPTION";s:36:"={$arParams["PRODUCT_SUBSCRIPTION"]}";s:9:"SHOW_NAME";s:1:"Y";s:10:"SHOW_IMAGE";s:1:"Y";s:12:"MESS_BTN_BUY";s:28:"={$arParams["MESS_BTN_BUY"]}";s:15:"MESS_BTN_DETAIL";s:31:"={$arParams["MESS_BTN_DETAIL"]}";s:18:"MESS_NOT_AVAILABLE";s:34:"={$arParams["MESS_NOT_AVAILABLE"]}";s:18:"MESS_BTN_SUBSCRIBE";s:34:"={$arParams["MESS_BTN_SUBSCRIBE"]}";s:18:"LINE_ELEMENT_COUNT";s:1:"5";s:14:"TEMPLATE_THEME";s:70:"={(isset($arParams["TEMPLATE_THEME"])?$arParams["TEMPLATE_THEME"]:"")}";s:10:"DETAIL_URL";s:60:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"]}";s:10:"CACHE_TYPE";s:26:"={$arParams["CACHE_TYPE"]}";s:10:"CACHE_TIME";s:26:"={$arParams["CACHE_TIME"]}";s:12:"CACHE_GROUPS";s:28:"={$arParams["CACHE_GROUPS"]}";s:2:"BY";a:1:{i:0;s:6:"AMOUNT";}s:6:"PERIOD";a:1:{i:0;s:2:"15";}s:6:"FILTER";a:7:{i:0;s:8:"CANCELED";i:1;s:14:"ALLOW_DELIVERY";i:2;s:5:"PAYED";i:3;s:8:"DEDUCTED";i:4;s:1:"N";i:5;s:1:"P";i:6;s:1:"F";}s:11:"FILTER_NAME";s:27:"={$arParams["FILTER_NAME"]}";s:17:"ORDER_FILTER_NAME";s:13:"arOrderFilter";s:15:"DISPLAY_COMPARE";s:27:"={$arParams["USE_COMPARE"]}";s:14:"SHOW_OLD_PRICE";s:30:"={$arParams["SHOW_OLD_PRICE"]}";s:10:"PRICE_CODE";s:26:"={$arParams["PRICE_CODE"]}";s:16:"SHOW_PRICE_COUNT";s:32:"={$arParams["SHOW_PRICE_COUNT"]}";s:17:"PRICE_VAT_INCLUDE";s:33:"={$arParams["PRICE_VAT_INCLUDE"]}";s:16:"CONVERT_CURRENCY";s:32:"={$arParams["CONVERT_CURRENCY"]}";s:11:"CURRENCY_ID";s:27:"={$arParams["CURRENCY_ID"]}";s:10:"BASKET_URL";s:26:"={$arParams["BASKET_URL"]}";s:15:"ACTION_VARIABLE";s:86:"={(!empty($arParams["ACTION_VARIABLE"])?$arParams["ACTION_VARIABLE"]:"action")."_slb"}";s:19:"PRODUCT_ID_VARIABLE";s:35:"={$arParams["PRODUCT_ID_VARIABLE"]}";s:25:"PRODUCT_QUANTITY_VARIABLE";s:41:"={$arParams["PRODUCT_QUANTITY_VARIABLE"]}";s:24:"ADD_PROPERTIES_TO_BASKET";s:90:"={(isset($arParams["ADD_PROPERTIES_TO_BASKET"])?$arParams["ADD_PROPERTIES_TO_BASKET"]:"")}";s:22:"PRODUCT_PROPS_VARIABLE";s:38:"={$arParams["PRODUCT_PROPS_VARIABLE"]}";s:26:"PARTIAL_PRODUCT_PROPERTIES";s:94:"={(isset($arParams["PARTIAL_PRODUCT_PROPERTIES"])?$arParams["PARTIAL_PRODUCT_PROPERTIES"]:"")}";s:20:"USE_PRODUCT_QUANTITY";s:36:"={$arParams["USE_PRODUCT_QUANTITY"]}";s:42:"={"SHOW_PRODUCTS_".$arParams["IBLOCK_ID"]}";s:1:"Y";s:54:"={"OFFER_TREE_PROPS_".$arRecomData["OFFER_IBLOCK_ID"]}";s:32:"={$arParams["OFFER_TREE_PROPS"]}";s:49:"={"ADDITIONAL_PICT_PROP_".$arParams["IBLOCK_ID"]}";s:29:"={$arParams["ADD_PICT_PROP"]}";s:58:"={"ADDITIONAL_PICT_PROP_".$arRecomData["OFFER_IBLOCK_ID"]}";s:35:"={$arParams["OFFER_ADD_PICT_PROP"]}";}'),
(90, 's1', 'bitrix:catalog.bigdata.products', '', '/bitrix/templates/.default/components/bitrix/catalog/products/section.php', 'N', NULL, 13532, 16417, 'a:46:{s:18:"LINE_ELEMENT_COUNT";s:1:"5";s:14:"TEMPLATE_THEME";s:70:"={(isset($arParams["TEMPLATE_THEME"])?$arParams["TEMPLATE_THEME"]:"")}";s:10:"DETAIL_URL";s:60:"={$arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"]}";s:10:"BASKET_URL";s:26:"={$arParams["BASKET_URL"]}";s:15:"ACTION_VARIABLE";s:87:"={(!empty($arParams["ACTION_VARIABLE"])?$arParams["ACTION_VARIABLE"]:"action")."_cbdp"}";s:19:"PRODUCT_ID_VARIABLE";s:35:"={$arParams["PRODUCT_ID_VARIABLE"]}";s:25:"PRODUCT_QUANTITY_VARIABLE";s:41:"={$arParams["PRODUCT_QUANTITY_VARIABLE"]}";s:24:"ADD_PROPERTIES_TO_BASKET";s:90:"={(isset($arParams["ADD_PROPERTIES_TO_BASKET"])?$arParams["ADD_PROPERTIES_TO_BASKET"]:"")}";s:22:"PRODUCT_PROPS_VARIABLE";s:38:"={$arParams["PRODUCT_PROPS_VARIABLE"]}";s:26:"PARTIAL_PRODUCT_PROPERTIES";s:94:"={(isset($arParams["PARTIAL_PRODUCT_PROPERTIES"])?$arParams["PARTIAL_PRODUCT_PROPERTIES"]:"")}";s:14:"SHOW_OLD_PRICE";s:30:"={$arParams["SHOW_OLD_PRICE"]}";s:21:"SHOW_DISCOUNT_PERCENT";s:37:"={$arParams["SHOW_DISCOUNT_PERCENT"]}";s:10:"PRICE_CODE";s:26:"={$arParams["PRICE_CODE"]}";s:16:"SHOW_PRICE_COUNT";s:32:"={$arParams["SHOW_PRICE_COUNT"]}";s:20:"PRODUCT_SUBSCRIPTION";s:36:"={$arParams["PRODUCT_SUBSCRIPTION"]}";s:17:"PRICE_VAT_INCLUDE";s:33:"={$arParams["PRICE_VAT_INCLUDE"]}";s:20:"USE_PRODUCT_QUANTITY";s:36:"={$arParams["USE_PRODUCT_QUANTITY"]}";s:9:"SHOW_NAME";s:1:"Y";s:10:"SHOW_IMAGE";s:1:"Y";s:12:"MESS_BTN_BUY";s:28:"={$arParams["MESS_BTN_BUY"]}";s:15:"MESS_BTN_DETAIL";s:31:"={$arParams["MESS_BTN_DETAIL"]}";s:18:"MESS_BTN_SUBSCRIBE";s:34:"={$arParams["MESS_BTN_SUBSCRIBE"]}";s:18:"MESS_NOT_AVAILABLE";s:34:"={$arParams["MESS_NOT_AVAILABLE"]}";s:18:"PAGE_ELEMENT_COUNT";s:1:"5";s:17:"SHOW_FROM_SECTION";s:1:"Y";s:11:"IBLOCK_TYPE";s:27:"={$arParams["IBLOCK_TYPE"]}";s:9:"IBLOCK_ID";s:25:"={$arParams["IBLOCK_ID"]}";s:5:"DEPTH";s:1:"2";s:10:"CACHE_TYPE";s:26:"={$arParams["CACHE_TYPE"]}";s:10:"CACHE_TIME";s:26:"={$arParams["CACHE_TIME"]}";s:12:"CACHE_GROUPS";s:28:"={$arParams["CACHE_GROUPS"]}";s:42:"={"SHOW_PRODUCTS_".$arParams["IBLOCK_ID"]}";s:1:"Y";s:49:"={"ADDITIONAL_PICT_PROP_".$arParams["IBLOCK_ID"]}";s:29:"={$arParams["ADD_PICT_PROP"]}";s:39:"={"LABEL_PROP_".$arParams["IBLOCK_ID"]}";s:1:"-";s:18:"HIDE_NOT_AVAILABLE";s:34:"={$arParams["HIDE_NOT_AVAILABLE"]}";s:16:"CONVERT_CURRENCY";s:32:"={$arParams["CONVERT_CURRENCY"]}";s:11:"CURRENCY_ID";s:27:"={$arParams["CURRENCY_ID"]}";s:10:"SECTION_ID";s:16:"={$intSectionID}";s:12:"SECTION_CODE";s:0:"";s:18:"SECTION_ELEMENT_ID";s:0:"";s:20:"SECTION_ELEMENT_CODE";s:0:"";s:42:"={"PROPERTY_CODE_".$arParams["IBLOCK_ID"]}";s:34:"={$arParams["LIST_PROPERTY_CODE"]}";s:44:"={"CART_PROPERTIES_".$arParams["IBLOCK_ID"]}";s:34:"={$arParams["PRODUCT_PROPERTIES"]}";s:8:"RCM_TYPE";s:76:"={(isset($arParams["BIG_DATA_RCM_TYPE"])?$arParams["BIG_DATA_RCM_TYPE"]:"")}";s:54:"={"OFFER_TREE_PROPS_".$arRecomData["OFFER_IBLOCK_ID"]}";s:32:"={$arParams["OFFER_TREE_PROPS"]}";s:58:"={"ADDITIONAL_PICT_PROP_".$arRecomData["OFFER_IBLOCK_ID"]}";s:35:"={$arParams["OFFER_ADD_PICT_PROP"]}";}'),
(94, 's1', 'bitrix:catalog', 'products', '/produktsiya/index.php', 'Y', '/produktsiya/', 101, 4745, 'a:111:{s:11:"IBLOCK_TYPE";s:8:"products";s:9:"IBLOCK_ID";s:1:"2";s:18:"HIDE_NOT_AVAILABLE";s:1:"N";s:14:"TEMPLATE_THEME";s:4:"blue";s:23:"COMMON_SHOW_CLOSE_POPUP";s:1:"N";s:21:"SHOW_DISCOUNT_PERCENT";s:1:"N";s:14:"SHOW_OLD_PRICE";s:1:"N";s:24:"DETAIL_SHOW_MAX_QUANTITY";s:1:"N";s:12:"MESS_BTN_BUY";s:12:"Купить";s:22:"MESS_BTN_ADD_TO_BASKET";s:17:"В корзину";s:16:"MESS_BTN_COMPARE";s:18:"Сравнение";s:15:"MESS_BTN_DETAIL";s:18:"Подробнее";s:18:"MESS_NOT_AVAILABLE";s:24:"Нет в наличии";s:22:"DETAIL_USE_VOTE_RATING";s:1:"N";s:19:"DETAIL_USE_COMMENTS";s:1:"N";s:16:"DETAIL_BRAND_USE";s:1:"N";s:8:"SEF_MODE";s:1:"Y";s:9:"AJAX_MODE";s:1:"N";s:16:"AJAX_OPTION_JUMP";s:1:"N";s:17:"AJAX_OPTION_STYLE";s:1:"Y";s:19:"AJAX_OPTION_HISTORY";s:1:"N";s:10:"CACHE_TYPE";s:1:"A";s:10:"CACHE_TIME";s:8:"36000000";s:12:"CACHE_FILTER";s:1:"N";s:12:"CACHE_GROUPS";s:1:"Y";s:14:"SET_STATUS_404";s:1:"N";s:9:"SET_TITLE";s:1:"Y";s:18:"ADD_SECTIONS_CHAIN";s:1:"Y";s:17:"ADD_ELEMENT_CHAIN";s:1:"N";s:19:"USE_ELEMENT_COUNTER";s:1:"Y";s:20:"USE_SALE_BESTSELLERS";s:1:"Y";s:10:"USE_FILTER";s:1:"N";s:16:"FILTER_VIEW_MODE";s:8:"VERTICAL";s:10:"USE_REVIEW";s:1:"N";s:15:"ACTION_VARIABLE";s:6:"action";s:19:"PRODUCT_ID_VARIABLE";s:2:"id";s:11:"USE_COMPARE";s:1:"N";s:10:"PRICE_CODE";N;s:15:"USE_PRICE_COUNT";s:1:"N";s:16:"SHOW_PRICE_COUNT";s:1:"1";s:17:"PRICE_VAT_INCLUDE";s:1:"Y";s:20:"PRICE_VAT_SHOW_VALUE";s:1:"N";s:16:"CONVERT_CURRENCY";s:1:"N";s:10:"BASKET_URL";s:20:"/personal/basket.php";s:20:"USE_PRODUCT_QUANTITY";s:1:"N";s:24:"ADD_PROPERTIES_TO_BASKET";s:1:"Y";s:22:"PRODUCT_PROPS_VARIABLE";s:4:"prop";s:26:"PARTIAL_PRODUCT_PROPERTIES";s:1:"N";s:18:"PRODUCT_PROPERTIES";N;s:32:"USE_COMMON_SETTINGS_BASKET_POPUP";s:1:"N";s:24:"TOP_ADD_TO_BASKET_ACTION";s:3:"ADD";s:28:"SECTION_ADD_TO_BASKET_ACTION";s:3:"ADD";s:27:"DETAIL_ADD_TO_BASKET_ACTION";a:1:{i:0;s:3:"BUY";}s:17:"SHOW_TOP_ELEMENTS";s:1:"Y";s:17:"TOP_ELEMENT_COUNT";s:1:"9";s:22:"TOP_LINE_ELEMENT_COUNT";s:1:"3";s:22:"TOP_ELEMENT_SORT_FIELD";s:4:"sort";s:22:"TOP_ELEMENT_SORT_ORDER";s:3:"asc";s:23:"TOP_ELEMENT_SORT_FIELD2";s:2:"id";s:23:"TOP_ELEMENT_SORT_ORDER2";s:4:"desc";s:17:"TOP_PROPERTY_CODE";a:3:{i:0;s:0:"";i:1;s:9:"undefined";i:2;s:0:"";}s:22:"SECTION_COUNT_ELEMENTS";s:1:"Y";s:17:"SECTION_TOP_DEPTH";s:1:"3";s:18:"SECTIONS_VIEW_MODE";s:4:"LIST";s:25:"SECTIONS_SHOW_PARENT_NAME";s:1:"Y";s:18:"PAGE_ELEMENT_COUNT";s:2:"30";s:18:"LINE_ELEMENT_COUNT";s:1:"3";s:18:"ELEMENT_SORT_FIELD";s:4:"sort";s:18:"ELEMENT_SORT_ORDER";s:3:"asc";s:19:"ELEMENT_SORT_FIELD2";s:2:"id";s:19:"ELEMENT_SORT_ORDER2";s:4:"desc";s:18:"LIST_PROPERTY_CODE";a:19:{i:0;s:6:"VISOTA";i:1;s:5:"DLINA";i:2;s:7:"SHIRINA";i:3;s:15:"DAVELNIE_VIDUVA";i:4;s:20:"KOLICHESTVO_VOZDUKHA";i:5;s:28:"KOLICHESTVO_VOZDUKHA_SISTEMI";i:6;s:11:"KOLICHESTVO";i:7;s:21:"KOLICHESTVO_ELEMENTOV";i:8;s:14:"VISOTA_BUTILKI";i:9;s:16:"PROIZVODITELNOST";i:10;s:15:"DIAMETR_BUTILKI";i:11;s:15:"NARUZHN_DIAMETR";i:12;s:4:"OBEM";i:13;s:22:"MEZHOSEVOE_RASSOTYANIE";i:14;s:17:"RABOCHEE_DAVLENIE";i:15;s:18:"MOSHCHNOST_NAGREVA";i:16;s:17:"USILIE_PRESSFORMI";i:17;s:9:"undefined";i:18;s:0:"";}s:19:"INCLUDE_SUBSECTIONS";s:1:"Y";s:18:"LIST_META_KEYWORDS";s:1:"-";s:21:"LIST_META_DESCRIPTION";s:1:"-";s:18:"LIST_BROWSER_TITLE";s:1:"-";s:20:"DETAIL_PROPERTY_CODE";a:3:{i:0;s:0:"";i:1;s:9:"undefined";i:2;s:0:"";}s:20:"DETAIL_META_KEYWORDS";s:1:"-";s:23:"DETAIL_META_DESCRIPTION";s:1:"-";s:20:"DETAIL_BROWSER_TITLE";s:1:"-";s:19:"SECTION_ID_VARIABLE";s:10:"SECTION_ID";s:32:"DETAIL_CHECK_SECTION_ID_VARIABLE";s:1:"N";s:19:"DETAIL_DISPLAY_NAME";s:1:"Y";s:26:"DETAIL_DETAIL_PICTURE_MODE";s:3:"IMG";s:27:"DETAIL_ADD_DETAIL_TO_SLIDER";s:1:"N";s:32:"DETAIL_DISPLAY_PREVIEW_TEXT_MODE";s:1:"E";s:16:"LINK_IBLOCK_TYPE";s:9:"undefined";s:14:"LINK_IBLOCK_ID";s:9:"undefined";s:17:"LINK_PROPERTY_SID";s:9:"undefined";s:17:"LINK_ELEMENTS_URL";s:39:"link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#";s:12:"USE_ALSO_BUY";s:1:"N";s:9:"USE_STORE";s:1:"N";s:12:"USE_BIG_DATA";s:1:"Y";s:17:"BIG_DATA_RCM_TYPE";s:8:"bestsell";s:14:"PAGER_TEMPLATE";s:8:".default";s:17:"DISPLAY_TOP_PAGER";s:1:"N";s:20:"DISPLAY_BOTTOM_PAGER";s:1:"Y";s:11:"PAGER_TITLE";s:12:"Товары";s:17:"PAGER_SHOW_ALWAYS";s:1:"N";s:20:"PAGER_DESC_NUMBERING";s:1:"N";s:31:"PAGER_DESC_NUMBERING_CACHE_TIME";s:5:"36000";s:14:"PAGER_SHOW_ALL";s:1:"N";s:13:"TOP_VIEW_MODE";s:7:"SECTION";s:10:"SEF_FOLDER";s:13:"/produktsiya/";s:13:"ADD_PICT_PROP";s:1:"-";s:10:"LABEL_PROP";s:1:"-";s:22:"AJAX_OPTION_ADDITIONAL";s:0:"";s:25:"PRODUCT_QUANTITY_VARIABLE";s:8:"quantity";s:27:"COMMON_ADD_TO_BASKET_ACTION";s:3:"ADD";s:17:"SEF_URL_TEMPLATES";a:4:{s:8:"sections";s:0:"";s:7:"section";s:20:"#SECTION_CODE_PATH#/";s:7:"element";s:35:"#SECTION_CODE_PATH#/#ELEMENT_CODE#/";s:7:"compare";s:32:"compare.php?action=#ACTION_CODE#";}s:16:"VARIABLE_ALIASES";a:1:{s:7:"compare";a:1:{s:11:"ACTION_CODE";s:6:"action";}}}');

-- --------------------------------------------------------

--
-- Table structure for table `b_counter_data`
--

CREATE TABLE IF NOT EXISTS `b_counter_data` (
  `ID` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `DATA` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_culture`
--

CREATE TABLE IF NOT EXISTS `b_culture` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_DATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_DATETIME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEEK_START` int(1) DEFAULT '1',
  `CHARSET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIRECTION` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `b_culture`
--

INSERT INTO `b_culture` (`ID`, `CODE`, `NAME`, `FORMAT_DATE`, `FORMAT_DATETIME`, `FORMAT_NAME`, `WEEK_START`, `CHARSET`, `DIRECTION`) VALUES
(1, 'ru', 'ru', 'DD.MM.YYYY', 'DD.MM.YYYY HH:MI:SS', '#NAME# #LAST_NAME#', 1, 'UTF-8', 'Y'),
(2, 'en', 'en', 'MM/DD/YYYY', 'MM/DD/YYYY H:MI:SS T', '#NAME# #LAST_NAME#', 0, 'UTF-8', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `b_event`
--

CREATE TABLE IF NOT EXISTS `b_event` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `EVENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE_ID` int(18) DEFAULT NULL,
  `LID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `C_FIELDS` longtext COLLATE utf8_unicode_ci,
  `DATE_INSERT` datetime DEFAULT NULL,
  `DATE_EXEC` datetime DEFAULT NULL,
  `SUCCESS_EXEC` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DUPLICATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `ix_success` (`SUCCESS_EXEC`),
  KEY `ix_b_event_date_exec` (`DATE_EXEC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_event_log`
--

CREATE TABLE IF NOT EXISTS `b_event_log` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SEVERITY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `AUDIT_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `REMOTE_ADDR` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_AGENT` text COLLATE utf8_unicode_ci,
  `REQUEST_URI` text COLLATE utf8_unicode_ci,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_ID` int(18) DEFAULT NULL,
  `GUEST_ID` int(18) DEFAULT NULL,
  `DESCRIPTION` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_event_log_time` (`TIMESTAMP_X`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=66 ;

--
-- Dumping data for table `b_event_log`
--

INSERT INTO `b_event_log` (`ID`, `TIMESTAMP_X`, `SEVERITY`, `AUDIT_TYPE_ID`, `MODULE_ID`, `ITEM_ID`, `REMOTE_ADDR`, `USER_AGENT`, `REQUEST_URI`, `SITE_ID`, `USER_ID`, `GUEST_ID`, `DESCRIPTION`) VALUES
(1, '2015-04-23 12:01:46', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit_src.php?bxsender=core_window_cdialog&lang=ru&site=ru&back_url=%2F%3Fbitrix_include_areas%3DY%26finish%3D%26clear_cache%3DY&path=%2Fbitrix%2Ftemplates%2F.default%2Fcomponents%2Fbitrix%2Fmenu%2Fmaintop%2Ftemplate.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:69:"bitrix/templates/.default/components/bitrix/menu/maintop/template.php";}'),
(2, '2015-04-23 12:20:30', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:7:"include";}'),
(3, '2015-04-23 12:21:55', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:7:"include";}'),
(4, '2015-04-23 12:23:06', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:27:"include/header_contacts.php";}'),
(5, '2015-04-23 12:25:56', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:7:"include";}'),
(6, '2015-04-23 15:28:05', 'UNKNOWN', 'MENU_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&new=Y&lang=ru&site=s1&back_url=%2F%3Fbitrix_include_areas%3DY%26finish%3D%26clear_cache%3DY&path=&name=bottom1', NULL, 1, NULL, 'a:2:{s:9:"menu_name";N;s:4:"path";b:0;}'),
(7, '2015-04-23 15:44:19', 'UNKNOWN', 'MENU_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&new=Y&lang=ru&site=s1&back_url=%2F%3Fbitrix_include_areas%3DY%26finish%3D%26clear_cache%3DY&path=&name=bottom2', NULL, 1, NULL, 'a:2:{s:9:"menu_name";N;s:4:"path";b:0;}'),
(8, '2015-04-23 15:46:25', 'UNKNOWN', 'MENU_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&new=Y&lang=ru&site=s1&back_url=%2F%3Fbitrix_include_areas%3DY%26finish%3D%26clear_cache%3DY&path=&name=bottom3', NULL, 1, NULL, 'a:2:{s:9:"menu_name";N;s:4:"path";b:0;}'),
(9, '2015-04-23 15:48:03', 'UNKNOWN', 'MENU_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&new=Y&lang=ru&site=s1&back_url=%2F%3Fbitrix_include_areas%3DY%26finish%3D%26clear_cache%3DY&path=&name=bottom4', NULL, 1, NULL, 'a:2:{s:9:"menu_name";N;s:4:"path";b:0;}'),
(10, '2015-04-23 15:48:22', 'UNKNOWN', 'MENU_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&lang=ru&site=s1&back_url=%2F%3Fbitrix_include_areas%3DY%26finish%3D%26clear_cache%3DY&path=%2F&name=bottom4', NULL, 1, NULL, 'a:2:{s:9:"menu_name";N;s:4:"path";b:0;}'),
(11, '2015-04-23 15:49:13', 'UNKNOWN', 'MENU_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&new=Y&lang=ru&site=s1&back_url=%2F%3Fbitrix_include_areas%3DY%26finish%3D%26clear_cache%3DY&path=&name=bottom5', NULL, 1, NULL, 'a:2:{s:9:"menu_name";N;s:4:"path";b:0;}'),
(12, '2015-04-23 15:50:48', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:28:"include/footer_copyright.php";}'),
(13, '2015-04-23 15:52:01', 'UNKNOWN', 'MENU_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&lang=ru&site=s1&back_url=%2F%3Fbitrix_include_areas%3DY%26finish%3D%26clear_cache%3DY&path=%2F&name=top', NULL, 1, NULL, 'a:2:{s:9:"menu_name";s:23:"Верхнее меню";s:4:"path";b:0;}'),
(14, '2015-04-23 15:52:51', 'UNKNOWN', 'PAGE_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F%3Fbitrix_include_areas%3DY%26finish%3D%26clear_cache%3DY', NULL, 1, NULL, 'a:1:{s:4:"path";s:21:"produktsiya/index.php";}'),
(15, '2015-04-23 15:52:51', 'UNKNOWN', 'SECTION_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F%3Fbitrix_include_areas%3DY%26finish%3D%26clear_cache%3DY', NULL, 1, NULL, 'a:1:{s:4:"path";s:11:"produktsiya";}'),
(16, '2015-04-23 15:52:51', 'UNKNOWN', 'MENU_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F%3Fbitrix_include_areas%3DY%26finish%3D%26clear_cache%3DY', NULL, 1, NULL, 'a:2:{s:4:"path";b:0;s:9:"menu_name";s:23:"Верхнее меню";}'),
(17, '2015-04-23 15:52:54', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:21:"produktsiya/index.php";}'),
(18, '2015-04-23 15:53:26', 'UNKNOWN', 'PAGE_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:27:"kompleksnye-linii/index.php";}'),
(19, '2015-04-23 15:53:26', 'UNKNOWN', 'SECTION_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:17:"kompleksnye-linii";}'),
(20, '2015-04-23 15:53:26', 'UNKNOWN', 'MENU_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:2:{s:4:"path";b:0;s:9:"menu_name";s:23:"Верхнее меню";}'),
(21, '2015-04-23 15:53:29', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:27:"kompleksnye-linii/index.php";}'),
(22, '2015-04-23 15:53:47', 'UNKNOWN', 'PAGE_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:16:"uslugi/index.php";}'),
(23, '2015-04-23 15:53:47', 'UNKNOWN', 'SECTION_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:6:"uslugi";}'),
(24, '2015-04-23 15:53:47', 'UNKNOWN', 'MENU_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:2:{s:4:"path";b:0;s:9:"menu_name";s:23:"Верхнее меню";}'),
(25, '2015-04-23 15:53:51', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:16:"uslugi/index.php";}'),
(26, '2015-04-23 15:54:07', 'UNKNOWN', 'PAGE_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:17:"proekty/index.php";}'),
(27, '2015-04-23 15:54:07', 'UNKNOWN', 'SECTION_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:7:"proekty";}'),
(28, '2015-04-23 15:54:07', 'UNKNOWN', 'MENU_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:2:{s:4:"path";b:0;s:9:"menu_name";s:23:"Верхнее меню";}'),
(29, '2015-04-23 15:54:10', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:17:"proekty/index.php";}'),
(30, '2015-04-23 15:54:27', 'UNKNOWN', 'PAGE_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:19:"kompaniya/index.php";}'),
(31, '2015-04-23 15:54:27', 'UNKNOWN', 'SECTION_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:9:"kompaniya";}'),
(32, '2015-04-23 15:54:27', 'UNKNOWN', 'MENU_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:2:{s:4:"path";b:0;s:9:"menu_name";s:23:"Верхнее меню";}'),
(33, '2015-04-23 15:54:30', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:19:"kompaniya/index.php";}'),
(34, '2015-04-23 15:54:54', 'UNKNOWN', 'PAGE_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:18:"kontakty/index.php";}'),
(35, '2015-04-23 15:54:54', 'UNKNOWN', 'SECTION_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:8:"kontakty";}'),
(36, '2015-04-23 15:54:54', 'UNKNOWN', 'MENU_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=main_tpl&newFolder=Y&path=%2F&back_url=%2F', NULL, 1, NULL, 'a:2:{s:4:"path";b:0;s:9:"menu_name";s:23:"Верхнее меню";}'),
(37, '2015-04-23 15:54:58', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:18:"kontakty/index.php";}'),
(38, '2015-04-23 16:10:01', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:7:"include";}'),
(39, '2015-04-23 16:10:15', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:7:"include";}'),
(40, '2015-04-23 16:10:30', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:7:"include";}'),
(41, '2015-04-23 16:10:52', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:7:"include";}'),
(42, '2015-04-23 16:12:12', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:7:"include";}'),
(43, '2015-04-23 16:12:23', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:7:"include";}'),
(44, '2015-04-24 02:07:33', 'UNKNOWN', 'PAGE_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=textpage&newFolder=Y&path=%2Fkompaniya%2F&back_url=%2Fkompaniya%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:29:"kompaniyao-kompanii/index.php";}'),
(45, '2015-04-24 02:07:33', 'UNKNOWN', 'SECTION_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=textpage&newFolder=Y&path=%2Fkompaniya%2F&back_url=%2Fkompaniya%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:20:"kompaniya/o-kompanii";}'),
(46, '2015-04-24 02:07:44', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:30:"kompaniya/o-kompanii/index.php";}'),
(47, '2015-04-24 02:08:20', 'UNKNOWN', 'MENU_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&lang=ru&site=s1&back_url=%2Fkompaniya%2Fo-kompanii%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY&path=%2F&name=bottom5', NULL, 1, NULL, 'a:2:{s:9:"menu_name";N;s:4:"path";b:0;}'),
(48, '2015-04-24 02:40:20', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit_src.php?bxsender=core_window_cdialog&lang=ru&site=ru&back_url=%2Fkompaniya%2F%3Fclear_cache%3DY&path=%2Fbitrix%2Ftemplates%2F.default%2Fcomponents%2Fbitrix%2Fnews.list%2Fkompany%2Ftemplate.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:74:"bitrix/templates/.default/components/bitrix/news.list/kompany/template.php";}'),
(49, '2015-04-24 04:19:44', 'UNKNOWN', 'PAGE_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=textpage_without_title&newFolder=Y&path=%2Fkontakty%2F&back_url=%2Fkontakty%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:25:"kontaktynovosti/index.php";}'),
(50, '2015-04-24 04:19:44', 'UNKNOWN', 'SECTION_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=textpage_without_title&newFolder=Y&path=%2Fkontakty%2F&back_url=%2Fkontakty%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:16:"kontakty/novosti";}'),
(51, '2015-04-24 04:19:49', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:26:"kontakty/novosti/index.php";}'),
(52, '2015-04-24 04:22:31', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit_src.php?bxsender=core_window_cdialog&lang=ru&site=ru&back_url=%2Fkompaniya%2Fnovosti%2F%3Fclear_cache%3DY&path=%2Fbitrix%2Ftemplates%2F.default%2Fcomponents%2Fbitrix%2Fnews%2Faquaculture%2Fnews.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:69:"bitrix/templates/.default/components/bitrix/news/aquaculture/news.php";}'),
(53, '2015-04-24 04:24:51', 'UNKNOWN', 'PAGE_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=textpage_without_title&newFolder=Y&path=%2Fkompaniya%2F&back_url=%2Fkompaniya%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:24:"kompaniyastati/index.php";}'),
(54, '2015-04-24 04:24:51', 'UNKNOWN', 'SECTION_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_new.php?bxsender=core_window_cdialog&lang=ru&site=s1&templateID=textpage_without_title&newFolder=Y&path=%2Fkompaniya%2F&back_url=%2Fkompaniya%2F', NULL, 1, NULL, 'a:1:{s:4:"path";s:15:"kompaniya/stati";}'),
(55, '2015-04-24 04:24:54', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:25:"kompaniya/stati/index.php";}'),
(56, '2015-04-24 07:41:16', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit_src.php?bxsender=core_window_cdialog&lang=ru&site=ru&back_url=%2Fproekty%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY&path=%2Fbitrix%2Ftemplates%2F.default%2Fcomponents%2Fbitrix%2Fnews%2Fprojects%2Fnews.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:66:"bitrix/templates/.default/components/bitrix/news/projects/news.php";}'),
(57, '2015-04-24 07:50:57', 'UNKNOWN', 'MENU_ADD', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&new=Y&lang=ru&site=s1&back_url=%2Fproekty%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY&path=%2Fproekty&name=left', NULL, 1, NULL, 'a:2:{s:9:"menu_name";s:19:"Левое меню";s:4:"path";s:7:"proekty";}'),
(58, '2015-04-24 07:51:11', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit_src.php?bxsender=core_window_cdialog&lang=ru&site=ru&back_url=%2Fproekty%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY&path=%2Fbitrix%2Ftemplates%2F.default%2Fcomponents%2Fbitrix%2Fmenu%2Fproekty%2Ftemplate.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:69:"bitrix/templates/.default/components/bitrix/menu/proekty/template.php";}'),
(59, '2015-04-25 17:36:02', 'UNKNOWN', 'MENU_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_menu_edit.php?bxsender=core_window_cdialog&lang=ru&site=s1&back_url=%2Fproekty%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY&path=%2Fproekty%2F&name=left', NULL, 1, NULL, 'a:2:{s:9:"menu_name";s:19:"Левое меню";s:4:"path";s:7:"proekty";}'),
(60, '2015-04-25 17:40:43', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit_src.php?bxsender=core_window_cdialog&lang=ru&path=%2Fproekty%2Findex.php&site=s1&back_url=%2Fproekty%2F%3Fbitrix_include_areas%3DN%26clear_cache%3DY&templateID=textpage_without_title', 's1', 1, NULL, 'a:1:{s:4:"path";s:17:"proekty/index.php";}'),
(61, '2015-04-28 06:38:43', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit_src.php?bxsender=core_window_cdialog&lang=ru&site=ru&back_url=%2Fproduktsiya%2F%3Fbitrix_include_areas%3DY%26clear_cache%3DY&path=%2Fbitrix%2Ftemplates%2F.default%2Fcomponents%2Fbitrix%2Fmenu%2Fproducts%2Ftemplate.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:70:"bitrix/templates/.default/components/bitrix/menu/products/template.php";}'),
(62, '2015-04-28 06:53:13', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:21:"produktsiya/index.php";}'),
(63, '2015-04-28 06:55:14', 'UNKNOWN', 'PAGE_EDIT', 'main', 'UNKNOWN', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36', '/bitrix/admin/public_file_edit_src.php?bxsender=core_window_cdialog&lang=ru&site=ru&back_url=%2Fproduktsiya%2Fkompressory-%2F&path=%2Fbitrix%2Ftemplates%2F.default%2Fcomponents%2Fbitrix%2Fcatalog%2Fproducts%2Fsection.php', 's1', 1, NULL, 'a:1:{s:4:"path";s:72:"bitrix/templates/.default/components/bitrix/catalog/products/section.php";}'),
(64, '2015-05-10 11:16:57', 'UNKNOWN', 'IBLOCK_EDIT', 'iblock', '2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36', '/bitrix/admin/iblock_edit.php?type=products&lang=ru&admin=Y', NULL, 1, NULL, 'a:1:{s:4:"NAME";s:23:"[s1] Продукция";}'),
(65, '2015-05-10 13:51:32', 'UNKNOWN', 'IBLOCK_EDIT', 'iblock', '2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36', '/bitrix/admin/iblock_edit.php?type=products&lang=ru&admin=Y', NULL, 1, NULL, 'a:1:{s:4:"NAME";s:23:"[s1] Продукция";}');

-- --------------------------------------------------------

--
-- Table structure for table `b_event_message`
--

CREATE TABLE IF NOT EXISTS `b_event_message` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `EVENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `EMAIL_FROM` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#EMAIL_FROM#',
  `EMAIL_TO` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#EMAIL_TO#',
  `SUBJECT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  `BODY_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `BCC` text COLLATE utf8_unicode_ci,
  `REPLY_TO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IN_REPLY_TO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRIORITY` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD1_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD1_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD2_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD2_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_event_message_name` (`EVENT_NAME`(50))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `b_event_message`
--

INSERT INTO `b_event_message` (`ID`, `TIMESTAMP_X`, `EVENT_NAME`, `LID`, `ACTIVE`, `EMAIL_FROM`, `EMAIL_TO`, `SUBJECT`, `MESSAGE`, `BODY_TYPE`, `BCC`, `REPLY_TO`, `CC`, `IN_REPLY_TO`, `PRIORITY`, `FIELD1_NAME`, `FIELD1_VALUE`, `FIELD2_NAME`, `FIELD2_VALUE`) VALUES
(1, '2015-04-22 17:24:56', 'NEW_USER', 's1', 'Y', '#DEFAULT_EMAIL_FROM#', '#DEFAULT_EMAIL_FROM#', '#SITE_NAME#: Зарегистрировался новый пользователь', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nНа сайте #SERVER_NAME# успешно зарегистрирован новый пользователь.\n\nДанные пользователя:\nID пользователя: #USER_ID#\n\nИмя: #NAME#\nФамилия: #LAST_NAME#\nE-Mail: #EMAIL#\n\nLogin: #LOGIN#\n\nПисьмо сгенерировано автоматически.', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '2015-04-22 17:24:56', 'USER_INFO', 's1', 'Y', '#DEFAULT_EMAIL_FROM#', '#EMAIL#', '#SITE_NAME#: Регистрационная информация', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n#NAME# #LAST_NAME#,\n\n#MESSAGE#\n\nВаша регистрационная информация:\n\nID пользователя: #USER_ID#\nСтатус профиля: #STATUS#\nLogin: #LOGIN#\n\nВы можете изменить пароль, перейдя по следующей ссылке:\nhttp://#SERVER_NAME#/auth/index.php?change_password=yes&lang=ru&USER_CHECKWORD=#CHECKWORD#&USER_LOGIN=#URL_LOGIN#\n\nСообщение сгенерировано автоматически.', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '2015-04-22 17:24:56', 'USER_PASS_REQUEST', 's1', 'Y', '#DEFAULT_EMAIL_FROM#', '#EMAIL#', '#SITE_NAME#: Запрос на смену пароля', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n#NAME# #LAST_NAME#,\n\n#MESSAGE#\n\nДля смены пароля перейдите по следующей ссылке:\nhttp://#SERVER_NAME#/auth/index.php?change_password=yes&lang=ru&USER_CHECKWORD=#CHECKWORD#&USER_LOGIN=#URL_LOGIN#\n\nВаша регистрационная информация:\n\nID пользователя: #USER_ID#\nСтатус профиля: #STATUS#\nLogin: #LOGIN#\n\nСообщение сгенерировано автоматически.', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '2015-04-22 17:24:56', 'USER_PASS_CHANGED', 's1', 'Y', '#DEFAULT_EMAIL_FROM#', '#EMAIL#', '#SITE_NAME#: Подтверждение смены пароля', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n#NAME# #LAST_NAME#,\n\n#MESSAGE#\n\nВаша регистрационная информация:\n\nID пользователя: #USER_ID#\nСтатус профиля: #STATUS#\nLogin: #LOGIN#\n\nСообщение сгенерировано автоматически.', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '2015-04-22 17:24:56', 'NEW_USER_CONFIRM', 's1', 'Y', '#DEFAULT_EMAIL_FROM#', '#EMAIL#', '#SITE_NAME#: Подтверждение регистрации нового пользователя', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nЗдравствуйте,\n\nВы получили это сообщение, так как ваш адрес был использован при регистрации нового пользователя на сервере #SERVER_NAME#.\n\nВаш код для подтверждения регистрации: #CONFIRM_CODE#\n\nДля подтверждения регистрации перейдите по следующей ссылке:\nhttp://#SERVER_NAME#/auth/index.php?confirm_registration=yes&confirm_user_id=#USER_ID#&confirm_code=#CONFIRM_CODE#\n\nВы также можете ввести код для подтверждения регистрации на странице:\nhttp://#SERVER_NAME#/auth/index.php?confirm_registration=yes&confirm_user_id=#USER_ID#\n\nВнимание! Ваш профиль не будет активным, пока вы не подтвердите свою регистрацию.\n\n---------------------------------------------------------------------\n\nСообщение сгенерировано автоматически.', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '2015-04-22 17:24:56', 'USER_INVITE', 's1', 'Y', '#DEFAULT_EMAIL_FROM#', '#EMAIL#', '#SITE_NAME#: Приглашение на сайт', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\nЗдравствуйте, #NAME# #LAST_NAME#!\n\nАдминистратором сайта вы добавлены в число зарегистрированных пользователей.\n\nПриглашаем Вас на наш сайт.\n\nВаша регистрационная информация:\n\nID пользователя: #ID#\nLogin: #LOGIN#\n\nРекомендуем вам сменить установленный автоматически пароль.\n\nДля смены пароля перейдите по следующей ссылке:\nhttp://#SERVER_NAME#/auth.php?change_password=yes&USER_LOGIN=#URL_LOGIN#&USER_CHECKWORD=#CHECKWORD#\n', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '2015-04-22 17:24:56', 'FEEDBACK_FORM', 's1', 'Y', '#DEFAULT_EMAIL_FROM#', '#EMAIL_TO#', '#SITE_NAME#: Сообщение из формы обратной связи', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nВам было отправлено сообщение через форму обратной связи\n\nАвтор: #AUTHOR#\nE-mail автора: #AUTHOR_EMAIL#\n\nТекст сообщения:\n#TEXT#\n\nСообщение сгенерировано автоматически.', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '2015-04-22 17:25:41', 'NEW_BLOG_MESSAGE', 's1', 'Y', '#EMAIL_FROM#', '#EMAIL_TO#', '#SITE_NAME#: [B] #BLOG_NAME# : #MESSAGE_TITLE#', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nНовое сообщение в блоге "#BLOG_NAME#"\n\nТема:\n#MESSAGE_TITLE#\n\nАвтор: #AUTHOR#\nДата: #MESSAGE_DATE#\n\nТекст сообщения:\n#MESSAGE_TEXT#\n\nАдрес сообщения:\n#MESSAGE_PATH#\n\nСообщение сгенерировано автоматически.', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '2015-04-22 17:25:41', 'NEW_BLOG_COMMENT', 's1', 'Y', '#EMAIL_FROM#', '#EMAIL_TO#', '#SITE_NAME#: [B] #MESSAGE_TITLE# : #COMMENT_TITLE#', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nНовый комментарий в блоге "#BLOG_NAME#" на сообщение "#MESSAGE_TITLE#"\n\nТема:\n#COMMENT_TITLE#\nАвтор: #AUTHOR#\nДата: #COMMENT_DATE#\n\nТекст сообщения:\n#COMMENT_TEXT#\n\nАдрес сообщения:\n#COMMENT_PATH#\n\nСообщение сгенерировано автоматически.', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '2015-04-22 17:25:41', 'NEW_BLOG_COMMENT2COMMENT', 's1', 'Y', '#EMAIL_FROM#', '#EMAIL_TO#', '#SITE_NAME#: [B] #MESSAGE_TITLE# : #COMMENT_TITLE#', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nНовый комментарий на ваш комментарий в блоге "#BLOG_NAME#" на сообщение "#MESSAGE_TITLE#".\n\nТема:\n#COMMENT_TITLE#\nАвтор: #AUTHOR#\nДата: #COMMENT_DATE#\n\nТекст сообщения:\n#COMMENT_TEXT#\n\nАдрес сообщения:\n#COMMENT_PATH#\n\nСообщение сгенерировано автоматически.', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '2015-04-22 17:25:41', 'NEW_BLOG_COMMENT_WITHOUT_TITLE', 's1', 'Y', '#EMAIL_FROM#', '#EMAIL_TO#', '#SITE_NAME#: [B] #MESSAGE_TITLE#', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nНовый комментарий в блоге "#BLOG_NAME#" на сообщение "#MESSAGE_TITLE#"\n\nАвтор: #AUTHOR#\nДата: #COMMENT_DATE#\n\nТекст сообщения:\n#COMMENT_TEXT#\n\nАдрес сообщения:\n#COMMENT_PATH#\n\nСообщение сгенерировано автоматически.', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '2015-04-22 17:25:41', 'NEW_BLOG_COMMENT2COMMENT_WITHOUT_TITLE', 's1', 'Y', '#EMAIL_FROM#', '#EMAIL_TO#', '#SITE_NAME#: [B] #MESSAGE_TITLE#', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nНовый комментарий на ваш комментарий в блоге "#BLOG_NAME#" на сообщение "#MESSAGE_TITLE#".\n\nАвтор: #AUTHOR#\nДата: #COMMENT_DATE#\n\nТекст сообщения:\n#COMMENT_TEXT#\n\nАдрес сообщения:\n#COMMENT_PATH#\n\nСообщение сгенерировано автоматически.\n', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '2015-04-22 17:25:41', 'BLOG_YOUR_BLOG_TO_USER', 's1', 'Y', '#EMAIL_FROM#', '#EMAIL_TO#', '#SITE_NAME#: [B] Ваш блог "#BLOG_NAME#" был добавлен в друзья к #USER#', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nВаш блог "#BLOG_NAME#" был добавлен в друзья к #USER#.\n\nПрофиль пользователя: #USER_URL#\n\nАдрес вашего блога: #BLOG_ADR#\n\nСообщение сгенерировано автоматически.\n', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '2015-04-22 17:25:41', 'BLOG_YOU_TO_BLOG', 's1', 'Y', '#EMAIL_FROM#', '#EMAIL_TO#', '#SITE_NAME#: [B] Вы были добавлены в друзья блога "#BLOG_NAME#"', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nВы были добавлены в друзья блога "#BLOG_NAME#".\n\nАдрес блога: #BLOG_ADR#\n\nВаш профиль: #USER_URL#\n\nСообщение сгенерировано автоматически.\n', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '2015-04-22 17:25:41', 'BLOG_BLOG_TO_YOU', 's1', 'Y', '#EMAIL_FROM#', '#EMAIL_TO#', '#SITE_NAME#: [B] К вам в друзья был добавлен блог "#BLOG_NAME#"', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nК вам в друзья был добавлен блог "#BLOG_NAME#".\n\nАдрес блога: #BLOG_ADR#\n\nВаш профиль: #USER_URL#\n\nСообщение сгенерировано автоматически.\n', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '2015-04-22 17:25:41', 'BLOG_USER_TO_YOUR_BLOG', 's1', 'Y', '#EMAIL_FROM#', '#EMAIL_TO#', '#SITE_NAME#: [B] В ваш блог "#BLOG_NAME#" был добавлен друг #USER#', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nВ ваш блог "#BLOG_NAME#" был добавлен друг #USER#.\n\nПрофиль пользователя: #USER_URL#\n\nАдрес вашего блога: #BLOG_ADR#\n\nСообщение сгенерировано автоматически.\n', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '2015-04-22 17:26:57', 'NEW_FORUM_MESSAGE', 's1', 'Y', '#FROM_EMAIL#', '#RECIPIENT#', '#SITE_NAME#: [F] #TOPIC_TITLE# : #FORUM_NAME#', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nНовое сообщение на форуме #SERVER_NAME#.\n\nТема:\n#TOPIC_TITLE#\n\nАвтор: #AUTHOR#\nДата : #MESSAGE_DATE#\nТекст сообщения:\n\n#MESSAGE_TEXT#\n\nАдрес сообщения:\nhttp://#SERVER_NAME##PATH2FORUM#\n\nСообщение сгенерировано автоматически.\n', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, '2015-04-22 17:26:57', 'NEW_FORUM_PRIV', 's1', 'Y', '#FROM_EMAIL#', '#TO_EMAIL#', '#SITE_NAME#: [private] #SUBJECT#', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nВы получили персональное сообщение с форума на сайте #SERVER_NAME#.\n\nТема: #SUBJECT#\n\nАвтор: #FROM_NAME# #FROM_EMAIL#\nДата : #MESSAGE_DATE#\nСообщение:\n\n#MESSAGE#\n\nСообщение сгенерировано автоматически.\n', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '2015-04-22 17:26:57', 'NEW_FORUM_PRIVATE_MESSAGE', 's1', 'Y', '#FROM_EMAIL#', '#TO_EMAIL#', '#SITE_NAME#: [private] #SUBJECT#', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nВы получили персональное сообщение с форума на сайте #SERVER_NAME#.\n\nТема: #SUBJECT#\n\nАвтор: #FROM_NAME#\nДата: #MESSAGE_DATE#\nСообщение:\n\n#MESSAGE#\n\nСсылка на сообщение: #MESSAGE_LINK#Сообщение сгенерировано автоматически.\n', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, '2015-04-22 17:26:57', 'EDIT_FORUM_MESSAGE', 's1', 'Y', '#FROM_EMAIL#', '#RECIPIENT#', '#SITE_NAME#: [F] #TOPIC_TITLE# : #FORUM_NAME#', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nИзменение сообщения на форуме #SERVER_NAME#.\n\nТема:\n#TOPIC_TITLE#\n\nАвтор: #AUTHOR#\nДата : #MESSAGE_DATE#\nТекст сообщения:\n\n#MESSAGE_TEXT#\n\nАдрес сообщения:\nhttp://#SERVER_NAME##PATH2FORUM#\n\nСообщение сгенерировано автоматически.\n', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, '2015-04-22 17:26:57', 'FORUM_NEW_MESSAGE_MAIL', 's1', 'Y', '#FROM_EMAIL#', '#RECIPIENT#', '#TOPIC_TITLE#', '#MESSAGE_TEXT#\n\n------------------------------------------  \nВы получили это сообщение, так как выподписаны на форум #FORUM_NAME#.\n\nОтветить на сообщение можно по электронной почте или через форму на сайте:\nhttp://#SERVER_NAME##PATH2FORUM#\n\nНаписать новое сообщение: #FORUM_EMAIL#\n\nАвтор сообщения: #AUTHOR#\n\nСообщение сгенерировано автоматически на сайте #SITE_NAME#.\n', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, '2015-04-22 17:28:11', 'SALE_NEW_ORDER', 's1', 'Y', '#SALE_EMAIL#', '#EMAIL#', '#SITE_NAME#: Новый заказ N#ORDER_ID#', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">\n<head>\n	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>\n	<style>\n		body\n		{\n			font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;\n			font-size: 14px;\n			color: #000;\n		}\n	</style>\n</head>\n<body>\n<table cellpadding="0" cellspacing="0" width="850" style="background-color: #d1d1d1; border-radius: 2px; border:1px solid #d1d1d1; margin: 0 auto;" border="1" bordercolor="#d1d1d1">\n	<tr>\n		<td height="83" width="850" bgcolor="#eaf3f5" style="border: none; padding-top: 23px; padding-right: 17px; padding-bottom: 24px; padding-left: 17px;">\n			<table cellpadding="0" cellspacing="0" border="0" width="100%">\n				<tr>\n					<td bgcolor="#ffffff" height="75" style="font-weight: bold; text-align: center; font-size: 26px; color: #0b3961;">Вами оформлен заказ в магазине #SITE_NAME#</td>\n				</tr>\n				<tr>\n					<td bgcolor="#bad3df" height="11"></td>\n				</tr>\n			</table>\n		</td>\n	</tr>\n	<tr>\n		<td width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 16px; padding-left: 44px;">\n			<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">Уважаемый #ORDER_USER#,</p>\n			<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">Ваш заказ номер #ORDER_ID# от #ORDER_DATE# принят.<br />\n<br />\nСтоимость заказа: #PRICE#.<br />\n<br />\nСостав заказа:<br />\n#ORDER_LIST#<br />\n<br />\nВы можете следить за выполнением своего заказа (на какой стадии выполнения он находится), войдя в Ваш персональный раздел сайта #SITE_NAME#.<br />\n<br />\nОбратите внимание, что для входа в этот раздел Вам необходимо будет ввести логин и пароль пользователя сайта #SITE_NAME#.<br />\n<br />\nДля того, чтобы аннулировать заказ, воспользуйтесь функцией отмены заказа, которая доступна в Вашем персональном разделе сайта #SITE_NAME#.<br />\n<br />\nПожалуйста, при обращении к администрации сайта #SITE_NAME# ОБЯЗАТЕЛЬНО указывайте номер Вашего заказа - #ORDER_ID#.<br />\n<br />\nСпасибо за покупку!<br />\n</p>\n		</td>\n	</tr>\n	<tr>\n		<td height="40px" width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 30px; padding-left: 44px;">\n			<p style="border-top: 1px solid #d1d1d1; margin-bottom: 5px; margin-top: 0; padding-top: 20px; line-height:21px;">С уважением,<br />администрация <a href="http://#SERVER_NAME#" style="color:#2e6eb6;">Интернет-магазина</a><br />\n				E-mail: <a href="mailto:#SALE_EMAIL#" style="color:#2e6eb6;">#SALE_EMAIL#</a>\n			</p>\n		</td>\n	</tr>\n</table>\n</body>\n</html>', 'html', '#BCC#', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, '2015-04-22 17:28:11', 'SALE_ORDER_CANCEL', 's1', 'Y', '#SALE_EMAIL#', '#EMAIL#', '#SITE_NAME#: Отмена заказа N#ORDER_ID#', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">\n<head>\n	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>\n	<style>\n		body\n		{\n			font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;\n			font-size: 14px;\n			color: #000;\n		}\n	</style>\n</head>\n<body>\n<table cellpadding="0" cellspacing="0" width="850" style="background-color: #d1d1d1; border-radius: 2px; border:1px solid #d1d1d1; margin: 0 auto;" border="1" bordercolor="#d1d1d1">\n	<tr>\n		<td height="83" width="850" bgcolor="#eaf3f5" style="border: none; padding-top: 23px; padding-right: 17px; padding-bottom: 24px; padding-left: 17px;">\n			<table cellpadding="0" cellspacing="0" border="0" width="100%">\n				<tr>\n					<td bgcolor="#ffffff" height="75" style="font-weight: bold; text-align: center; font-size: 26px; color: #0b3961;">#SITE_NAME#: Отмена заказа N#ORDER_ID#</td>\n				</tr>\n				<tr>\n					<td bgcolor="#bad3df" height="11"></td>\n				</tr>\n			</table>\n		</td>\n	</tr>\n	<tr>\n		<td width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 16px; padding-left: 44px;">\n			<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">Заказ номер #ORDER_ID# от #ORDER_DATE# отменен.</p>\n			<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">#ORDER_CANCEL_DESCRIPTION#<br />\n<br />\nДля получения подробной информации по заказу пройдите на сайт http://#SERVER_NAME#/personal/order/#ORDER_ID#/<br />\n</p>\n		</td>\n	</tr>\n	<tr>\n		<td height="40px" width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 30px; padding-left: 44px;">\n			<p style="border-top: 1px solid #d1d1d1; margin-bottom: 5px; margin-top: 0; padding-top: 20px; line-height:21px;">С уважением,<br />администрация <a href="http://#SERVER_NAME#" style="color:#2e6eb6;">Интернет-магазина</a><br />\n				E-mail: <a href="mailto:#SALE_EMAIL#" style="color:#2e6eb6;">#SALE_EMAIL#</a>\n			</p>\n		</td>\n	</tr>\n</table>\n</body>\n</html>', 'html', '#BCC#', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '2015-04-22 17:28:11', 'SALE_ORDER_DELIVERY', 's1', 'Y', '#SALE_EMAIL#', '#EMAIL#', '#SITE_NAME#: Доставка заказа N#ORDER_ID# разрешена', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">\n<head>\n	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>\n	<style>\n		body\n		{\n			font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;\n			font-size: 14px;\n			color: #000;\n		}\n	</style>\n</head>\n<body>\n<table cellpadding="0" cellspacing="0" width="850" style="background-color: #d1d1d1; border-radius: 2px; border:1px solid #d1d1d1; margin: 0 auto;" border="1" bordercolor="#d1d1d1">\n	<tr>\n		<td height="83" width="850" bgcolor="#eaf3f5" style="border: none; padding-top: 23px; padding-right: 17px; padding-bottom: 24px; padding-left: 17px;">\n			<table cellpadding="0" cellspacing="0" border="0" width="100%">\n				<tr>\n					<td bgcolor="#ffffff" height="75" style="font-weight: bold; text-align: center; font-size: 26px; color: #0b3961;">Доставка вашего заказа с сайта #SITE_NAME# разрешена</td>\n				</tr>\n				<tr>\n					<td bgcolor="#bad3df" height="11"></td>\n				</tr>\n			</table>\n		</td>\n	</tr>\n	<tr>\n		<td width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 16px; padding-left: 44px;">\n			<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">Доставка заказа номер #ORDER_ID# от #ORDER_DATE# разрешена.</p>\n			<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">Для получения подробной информации по заказу пройдите на сайт http://#SERVER_NAME#/personal/order/#ORDER_ID#/<br />\n</p>\n		</td>\n	</tr>\n	<tr>\n		<td height="40px" width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 30px; padding-left: 44px;">\n			<p style="border-top: 1px solid #d1d1d1; margin-bottom: 5px; margin-top: 0; padding-top: 20px; line-height:21px;">С уважением,<br />администрация <a href="http://#SERVER_NAME#" style="color:#2e6eb6;">Интернет-магазина</a><br />\n				E-mail: <a href="mailto:#SALE_EMAIL#" style="color:#2e6eb6;">#SALE_EMAIL#</a>\n			</p>\n		</td>\n	</tr>\n</table>\n</body>\n</html>', 'html', '#BCC#', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, '2015-04-22 17:28:11', 'SALE_ORDER_PAID', 's1', 'Y', '#SALE_EMAIL#', '#EMAIL#', '#SITE_NAME#: Заказ N#ORDER_ID# оплачен', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">\n<head>\n	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>\n	<style>\n		body\n		{\n			font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;\n			font-size: 14px;\n			color: #000;\n		}\n	</style>\n</head>\n<body>\n<table cellpadding="0" cellspacing="0" width="850" style="background-color: #d1d1d1; border-radius: 2px; border:1px solid #d1d1d1; margin: 0 auto;" border="1" bordercolor="#d1d1d1">\n	<tr>\n		<td height="83" width="850" bgcolor="#eaf3f5" style="border: none; padding-top: 23px; padding-right: 17px; padding-bottom: 24px; padding-left: 17px;">\n			<table cellpadding="0" cellspacing="0" border="0" width="100%">\n				<tr>\n					<td bgcolor="#ffffff" height="75" style="font-weight: bold; text-align: center; font-size: 26px; color: #0b3961;">Вы оплатили заказ на сайте #SITE_NAME#</td>\n				</tr>\n				<tr>\n					<td bgcolor="#bad3df" height="11"></td>\n				</tr>\n			</table>\n		</td>\n	</tr>\n	<tr>\n		<td width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 16px; padding-left: 44px;">\n			<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">Заказ номер #ORDER_ID# от #ORDER_DATE# оплачен.</p>\n			<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">Для получения подробной информации по заказу пройдите на сайт http://#SERVER_NAME#/personal/order/#ORDER_ID#/</p>\n		</td>\n	</tr>\n	<tr>\n		<td height="40px" width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 30px; padding-left: 44px;">\n			<p style="border-top: 1px solid #d1d1d1; margin-bottom: 5px; margin-top: 0; padding-top: 20px; line-height:21px;">С уважением,<br />администрация <a href="http://#SERVER_NAME#" style="color:#2e6eb6;">Интернет-магазина</a><br />\n				E-mail: <a href="mailto:#SALE_EMAIL#" style="color:#2e6eb6;">#SALE_EMAIL#</a>\n			</p>\n		</td>\n	</tr>\n</table>\n</body>\n</html>', 'html', '#BCC#', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, '2015-04-22 17:28:11', 'SALE_ORDER_REMIND_PAYMENT', 's1', 'Y', '#SALE_EMAIL#', '#EMAIL#', '#SITE_NAME#: Напоминание об оплате заказа N#ORDER_ID# ', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">\n<head>\n	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>\n	<style>\n		body\n		{\n			font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;\n			font-size: 14px;\n			color: #000;\n		}\n	</style>\n</head>\n<body>\n<table cellpadding="0" cellspacing="0" width="850" style="background-color: #d1d1d1; border-radius: 2px; border:1px solid #d1d1d1; margin: 0 auto;" border="1" bordercolor="#d1d1d1">\n	<tr>\n		<td height="83" width="850" bgcolor="#eaf3f5" style="border: none; padding-top: 23px; padding-right: 17px; padding-bottom: 24px; padding-left: 17px;">\n			<table cellpadding="0" cellspacing="0" border="0" width="100%">\n				<tr>\n					<td bgcolor="#ffffff" height="75" style="font-weight: bold; text-align: center; font-size: 26px; color: #0b3961;">Напоминаем вам об оплате заказа на сайте #SITE_NAME#</td>\n				</tr>\n				<tr>\n					<td bgcolor="#bad3df" height="11"></td>\n				</tr>\n			</table>\n		</td>\n	</tr>\n	<tr>\n		<td width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 16px; padding-left: 44px;">\n			<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">Уважаемый #ORDER_USER#,</p>\n			<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">Вами был оформлен заказ N #ORDER_ID# от #ORDER_DATE# на сумму #PRICE#.<br />\n<br />\nК сожалению, на сегодняшний день средства по этому заказу не поступили к нам.<br />\n<br />\nВы можете следить за выполнением своего заказа (на какой стадии выполнения он находится), войдя в Ваш персональный раздел сайта #SITE_NAME#.<br />\n<br />\nОбратите внимание, что для входа в этот раздел Вам необходимо будет ввести логин и пароль пользователя сайта #SITE_NAME#.<br />\n<br />\nДля того, чтобы аннулировать заказ, воспользуйтесь функцией отмены заказа, которая доступна в Вашем персональном разделе сайта #SITE_NAME#.<br />\n<br />\nПожалуйста, при обращении к администрации сайта #SITE_NAME# ОБЯЗАТЕЛЬНО указывайте номер Вашего заказа - #ORDER_ID#.<br />\n<br />\nСпасибо за покупку!<br />\n</p>\n		</td>\n	</tr>\n	<tr>\n		<td height="40px" width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 30px; padding-left: 44px;">\n			<p style="border-top: 1px solid #d1d1d1; margin-bottom: 5px; margin-top: 0; padding-top: 20px; line-height:21px;">С уважением,<br />администрация <a href="http://#SERVER_NAME#" style="color:#2e6eb6;">Интернет-магазина</a><br />\n				E-mail: <a href="mailto:#SALE_EMAIL#" style="color:#2e6eb6;">#SALE_EMAIL#</a>\n			</p>\n		</td>\n	</tr>\n</table>\n</body>\n</html>', 'html', '#BCC#', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, '2015-04-22 17:28:11', 'SALE_SUBSCRIBE_PRODUCT', 's1', 'Y', '#SALE_EMAIL#', '#EMAIL#', '#SITE_NAME#: Уведомление о поступлении товара', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">\n<head>\n	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>\n	<style>\n		body\n		{\n			font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;\n			font-size: 14px;\n			color: #000;\n		}\n	</style>\n</head>\n<body>\n<table cellpadding="0" cellspacing="0" width="850" style="background-color: #d1d1d1; border-radius: 2px; border:1px solid #d1d1d1; margin: 0 auto;" border="1" bordercolor="#d1d1d1">\n	<tr>\n		<td height="83" width="850" bgcolor="#eaf3f5" style="border: none; padding-top: 23px; padding-right: 17px; padding-bottom: 24px; padding-left: 17px;">\n			<table cellpadding="0" cellspacing="0" border="0" width="100%">\n				<tr>\n					<td bgcolor="#ffffff" height="75" style="font-weight: bold; text-align: center; font-size: 26px; color: #0b3961;">Уведомление о поступлении товара в магазин #SITE_NAME#</td>\n				</tr>\n				<tr>\n					<td bgcolor="#bad3df" height="11"></td>\n				</tr>\n			</table>\n		</td>\n	</tr>\n	<tr>\n		<td width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 16px; padding-left: 44px;">\n			<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">Уважаемый, #USER_NAME#!</p>\n			<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">Товар "#NAME#" (#PAGE_URL#) поступил на склад.<br />\n<br />\nВы можете Оформить заказ (http://#SERVER_NAME#/personal/cart/).<br />\n<br />\nНе забудьте авторизоваться!<br />\n<br />\nВы получили это сообщение по Вашей просьбе оповестить при появлении товара.<br />\nНе отвечайте на него - письмо сформировано автоматически.<br />\n<br />\nСпасибо за покупку!<br />\n</p>\n		</td>\n	</tr>\n	<tr>\n		<td height="40px" width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 30px; padding-left: 44px;">\n			<p style="border-top: 1px solid #d1d1d1; margin-bottom: 5px; margin-top: 0; padding-top: 20px; line-height:21px;">С уважением,<br />администрация <a href="http://#SERVER_NAME#" style="color:#2e6eb6;">Интернет-магазина</a><br />\n				E-mail: <a href="mailto:#SALE_EMAIL#" style="color:#2e6eb6;">#SALE_EMAIL#</a>\n			</p>\n		</td>\n	</tr>\n</table>\n</body>\n</html>', 'html', '#BCC#', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, '2015-04-22 17:28:11', 'SALE_ORDER_TRACKING_NUMBER', 's1', 'Y', '#SALE_EMAIL#', '#EMAIL#', 'Номер идентификатора отправления вашего заказа на сайте #SITE_NAME#', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">\n<head>\n	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>\n	<style>\n		body\n		{\n			font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;\n			font-size: 14px;\n			color: #000;\n		}\n	</style>\n</head>\n<body>\n<table cellpadding="0" cellspacing="0" width="850" style="background-color: #d1d1d1; border-radius: 2px; border:1px solid #d1d1d1; margin: 0 auto;" border="1" bordercolor="#d1d1d1">\n	<tr>\n		<td height="83" width="850" bgcolor="#eaf3f5" style="border: none; padding-top: 23px; padding-right: 17px; padding-bottom: 24px; padding-left: 17px;">\n			<table cellpadding="0" cellspacing="0" border="0" width="100%">\n				<tr>\n					<td bgcolor="#ffffff" height="75" style="font-weight: bold; text-align: center; font-size: 26px; color: #0b3961;">Номер идентификатора отправления вашего заказа на сайте #SITE_NAME#</td>\n				</tr>\n				<tr>\n					<td bgcolor="#bad3df" height="11"></td>\n				</tr>\n			</table>\n		</td>\n	</tr>\n	<tr>\n		<td width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 16px; padding-left: 44px;">\n			<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;">Уважаемый #ORDER_USER#,</p>\n			<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">Произошла почтовая отправка заказа N #ORDER_ID# от #ORDER_DATE#.<br />\n<br />\nНомер идентификатора отправления: #ORDER_TRACKING_NUMBER#.<br />\n<br />\nДля получения подробной информации по заказу пройдите на сайт http://#SERVER_NAME#/personal/order/detail/#ORDER_ID#/<br />\n<br />\nE-mail: #SALE_EMAIL#<br />\n</p>\n		</td>\n	</tr>\n	<tr>\n		<td height="40px" width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 30px; padding-left: 44px;">\n			<p style="border-top: 1px solid #d1d1d1; margin-bottom: 5px; margin-top: 0; padding-top: 20px; line-height:21px;">С уважением,<br />администрация <a href="http://#SERVER_NAME#" style="color:#2e6eb6;">Интернет-магазина</a><br />\n				E-mail: <a href="mailto:#SALE_EMAIL#" style="color:#2e6eb6;">#SALE_EMAIL#</a>\n			</p>\n		</td>\n	</tr>\n</table>\n</body>\n</html>', 'html', '#BCC#', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, '2015-04-22 17:28:11', 'SALE_NEW_ORDER_RECURRING', 's1', 'Y', '#SALE_EMAIL#', '#EMAIL#', '#SITE_NAME#: Новый заказ N#ORDER_ID# на продление подписки', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nУважаемый #ORDER_USER#,\n\nВаш заказ номер #ORDER_ID# от #ORDER_DATE# на продление подписки принят.\n\nСтоимость заказа: #PRICE#.\n\nСостав заказа:\n#ORDER_LIST#\n\nВы можете следить за выполнением своего заказа (на какой\nстадии выполнения он находится), войдя в Ваш персональный\nраздел сайта #SITE_NAME#. Обратите внимание, что для входа\nв этот раздел Вам необходимо будет ввести логин и пароль\nпользователя сайта #SITE_NAME#.\n\nДля того, чтобы аннулировать заказ, воспользуйтесь функцией\nотмены заказа, которая доступна в Вашем персональном\nразделе сайта #SITE_NAME#.\n\nПожалуйста, при обращении к администрации сайта #SITE_NAME#\nОБЯЗАТЕЛЬНО указывайте номер Вашего заказа - #ORDER_ID#.\n\nСпасибо за покупку!\n', 'text', '#BCC#', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, '2015-04-22 17:28:11', 'SALE_RECURRING_CANCEL', 's1', 'Y', '#SALE_EMAIL#', '#EMAIL#', '#SITE_NAME#: Подписка отменена', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nПодписка отменена\n\n#CANCELED_REASON#\n#SITE_NAME#\n', 'text', '#BCC#', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, '2015-04-22 17:28:11', 'SALE_STATUS_CHANGED_F', 's1', 'Y', '#SALE_EMAIL#', '#EMAIL#', '#SERVER_NAME#: Изменение статуса заказа N#ORDER_ID#', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">\n<head>\n	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>\n	<style>\n		body\n		{\n			font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;\n			font-size: 14px;\n			color: #000;\n		}\n	</style>\n</head>\n<body>\n<table cellpadding="0" cellspacing="0" width="850" style="background-color: #d1d1d1; border-radius: 2px; border:1px solid #d1d1d1; margin: 0 auto;" border="1" bordercolor="#d1d1d1">\n	<tr>\n		<td height="83" width="850" bgcolor="#eaf3f5" style="border: none; padding-top: 23px; padding-right: 17px; padding-bottom: 24px; padding-left: 17px;">\n			<table cellpadding="0" cellspacing="0" border="0" width="100%">\n				<tr>\n					<td bgcolor="#ffffff" height="75" style="font-weight: bold; text-align: center; font-size: 26px; color: #0b3961;">Изменение статуса заказа в магазине #SITE_NAME#</td>\n				</tr>\n				<tr>\n					<td bgcolor="#bad3df" height="11"></td>\n				</tr>\n			</table>\n		</td>\n	</tr>\n	<tr>\n		<td width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 16px; padding-left: 44px;">\n			<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;"></p>\n			<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">Информационное сообщение сайта #SITE_NAME#<br />\n------------------------------------------<br />\n<br />\nСтатус заказа номер #ORDER_ID# от #ORDER_DATE# изменен.<br />\n<br />\nНовый статус заказа:<br />\n#ORDER_STATUS#<br />\n#ORDER_DESCRIPTION#<br />\n#TEXT#<br />\n<br />\nДля получения подробной информации по заказу пройдите на сайт #SERVER_NAME#/personal/order/#ORDER_ID#/<br />\n<br />\nСпасибо за ваш выбор!<br />\n#SITE_NAME#<br />\n</p>\n		</td>\n	</tr>\n	<tr>\n		<td height="40px" width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 30px; padding-left: 44px;">\n			<p style="border-top: 1px solid #d1d1d1; margin-bottom: 5px; margin-top: 0; padding-top: 20px; line-height:21px;">С уважением,<br />администрация <a href="http://#SERVER_NAME#" style="color:#2e6eb6;">Интернет-магазина</a><br />\n				E-mail: <a href="mailto:#SALE_EMAIL#" style="color:#2e6eb6;">#SALE_EMAIL#</a>\n			</p>\n		</td>\n	</tr>\n</table>\n</body>\n</html>', 'html', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, '2015-04-22 17:28:11', 'SALE_STATUS_CHANGED_N', 's1', 'Y', '#SALE_EMAIL#', '#EMAIL#', '#SERVER_NAME#: Изменение статуса заказа N#ORDER_ID#', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">\n<head>\n	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>\n	<style>\n		body\n		{\n			font-family: ''Helvetica Neue'', Helvetica, Arial, sans-serif;\n			font-size: 14px;\n			color: #000;\n		}\n	</style>\n</head>\n<body>\n<table cellpadding="0" cellspacing="0" width="850" style="background-color: #d1d1d1; border-radius: 2px; border:1px solid #d1d1d1; margin: 0 auto;" border="1" bordercolor="#d1d1d1">\n	<tr>\n		<td height="83" width="850" bgcolor="#eaf3f5" style="border: none; padding-top: 23px; padding-right: 17px; padding-bottom: 24px; padding-left: 17px;">\n			<table cellpadding="0" cellspacing="0" border="0" width="100%">\n				<tr>\n					<td bgcolor="#ffffff" height="75" style="font-weight: bold; text-align: center; font-size: 26px; color: #0b3961;">Изменение статуса заказа в магазине #SITE_NAME#</td>\n				</tr>\n				<tr>\n					<td bgcolor="#bad3df" height="11"></td>\n				</tr>\n			</table>\n		</td>\n	</tr>\n	<tr>\n		<td width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 16px; padding-left: 44px;">\n			<p style="margin-top:30px; margin-bottom: 28px; font-weight: bold; font-size: 19px;"></p>\n			<p style="margin-top: 0; margin-bottom: 20px; line-height: 20px;">Информационное сообщение сайта #SITE_NAME#<br />\n------------------------------------------<br />\n<br />\nСтатус заказа номер #ORDER_ID# от #ORDER_DATE# изменен.<br />\n<br />\nНовый статус заказа:<br />\n#ORDER_STATUS#<br />\n#ORDER_DESCRIPTION#<br />\n#TEXT#<br />\n<br />\nДля получения подробной информации по заказу пройдите на сайт #SERVER_NAME#/personal/order/#ORDER_ID#/<br />\n<br />\nСпасибо за ваш выбор!<br />\n#SITE_NAME#<br />\n</p>\n		</td>\n	</tr>\n	<tr>\n		<td height="40px" width="850" bgcolor="#f7f7f7" valign="top" style="border: none; padding-top: 0; padding-right: 44px; padding-bottom: 30px; padding-left: 44px;">\n			<p style="border-top: 1px solid #d1d1d1; margin-bottom: 5px; margin-top: 0; padding-top: 20px; line-height:21px;">С уважением,<br />администрация <a href="http://#SERVER_NAME#" style="color:#2e6eb6;">Интернет-магазина</a><br />\n				E-mail: <a href="mailto:#SALE_EMAIL#" style="color:#2e6eb6;">#SALE_EMAIL#</a>\n			</p>\n		</td>\n	</tr>\n</table>\n</body>\n</html>', 'html', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, '2015-04-22 17:28:34', 'VIRUS_DETECTED', 's1', 'Y', '#DEFAULT_EMAIL_FROM#', '#EMAIL#', '#SITE_NAME#: Обнаружен вирус', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nЗдравствуйте!\n\nВы получили это сообщение, так как модуль проактивной защиты сервера #SERVER_NAME# обнаружил код, похожий на вирус.\n\n1. Подозрительный код был вырезан из html.\n2. Проверьте журнал вторжений и убедитесь, что код действительно вредоносный, а не является кодом какого-либо счетчика или фреймворка.\n (ссылка: http://#SERVER_NAME#/bitrix/admin/event_log.php?lang=ru&set_filter=Y&find_type=audit_type_id&find_audit_type[]=SECURITY_VIRUS )\n3. В случае, если код не является опасным, добавьте его в исключения на странице настройки антивируса.\n (ссылка: http://#SERVER_NAME#/bitrix/admin/security_antivirus.php?lang=ru&tabControl_active_tab=exceptions )\n4. Если код является вирусным, то необходимо выполнить следующие действия:\n\n а) Смените пароли доступа к сайту у администраторов и ответственных сотрудников.\n б) Смените пароли доступа по ssh и ftp.\n в) Проверьте и вылечите компьютеры администраторов, имевших доступ к сайту по ssh или ftp.\n г) В программах доступа к сайту по ssh и ftp отключите сохранение паролей.\n д) Удалите вредоносный код из зараженных файлов. Например, восстановите поврежденные файлы из самой свежей резервной копии.\n\n---------------------------------------------------------------------\nСообщение сгенерировано автоматически.\n', 'text', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, '2015-04-22 17:28:44', 'SUBSCRIBE_CONFIRM', 's1', 'Y', '#DEFAULT_EMAIL_FROM#', '#EMAIL#', '#SITE_NAME#: Подтверждение подписки', 'Информационное сообщение сайта #SITE_NAME#\n------------------------------------------\n\nЗдравствуйте,\n\nВы получили это сообщение, так как ваш адрес был подписан\nна список рассылки сервера #SERVER_NAME#.\n\nДополнительная информация о подписке:\n\nАдрес подписки (email) ............ #EMAIL#\nДата добавления/редактирования .... #DATE_SUBSCR#\n\nВаш код для подтверждения подписки: #CONFIRM_CODE#\n\nДля подтверждения подписки перейдите по следующей ссылке:\nhttp://#SERVER_NAME##SUBSCR_SECTION#subscr_edit.php?ID=#ID#&CONFIRM_CODE=#CONFIRM_CODE#\n\nВы также можете ввести код для подтверждения подписки на странице:\nhttp://#SERVER_NAME##SUBSCR_SECTION#subscr_edit.php?ID=#ID#\n\nВнимание! Вы не будете получать сообщения рассылки, пока не подтвердите\nсвою подписку.\n\n---------------------------------------------------------------------\nСохраните это письмо, так как оно содержит информацию для авторизации.\nИспользуя код подтверждения подписки, вы cможете изменить параметры\nподписки или отписаться от рассылки.\n\nИзменить параметры:\nhttp://#SERVER_NAME##SUBSCR_SECTION#subscr_edit.php?ID=#ID#&CONFIRM_CODE=#CONFIRM_CODE#\n\nОтписаться:\nhttp://#SERVER_NAME##SUBSCR_SECTION#subscr_edit.php?ID=#ID#&CONFIRM_CODE=#CONFIRM_CODE#&action=unsubscribe\n---------------------------------------------------------------------\n\nСообщение сгенерировано автоматически.\n', 'text', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, '2015-04-22 17:28:47', 'VOTE_FOR', 's1', 'Y', '#DEFAULT_EMAIL_FROM#', '#EMAIL_TO#', '#SITE_NAME#: [V] #VOTE_TITLE#', '#USER_NAME# принял участие в опросе "#VOTE_TITLE#":\n#VOTE_STATISTIC#\n\nhttp://#SERVER_NAME##URL#\nСообщение сгенерировано автоматически.', 'text', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_event_message_site`
--

CREATE TABLE IF NOT EXISTS `b_event_message_site` (
  `EVENT_MESSAGE_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`EVENT_MESSAGE_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_event_message_site`
--

INSERT INTO `b_event_message_site` (`EVENT_MESSAGE_ID`, `SITE_ID`) VALUES
(1, 's1'),
(2, 's1'),
(3, 's1'),
(4, 's1'),
(5, 's1'),
(6, 's1'),
(7, 's1'),
(8, 's1'),
(9, 's1'),
(10, 's1'),
(11, 's1'),
(12, 's1'),
(13, 's1'),
(14, 's1'),
(15, 's1'),
(16, 's1'),
(17, 's1'),
(18, 's1'),
(19, 's1'),
(20, 's1'),
(21, 's1'),
(22, 's1'),
(23, 's1'),
(24, 's1'),
(25, 's1'),
(26, 's1'),
(27, 's1'),
(28, 's1'),
(29, 's1'),
(30, 's1'),
(31, 's1'),
(32, 's1'),
(33, 's1'),
(34, 's1'),
(35, 's1');

-- --------------------------------------------------------

--
-- Table structure for table `b_event_type`
--

CREATE TABLE IF NOT EXISTS `b_event_type` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `EVENT_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `SORT` int(18) NOT NULL DEFAULT '150',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_1` (`EVENT_NAME`,`LID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=71 ;

--
-- Dumping data for table `b_event_type`
--

INSERT INTO `b_event_type` (`ID`, `LID`, `EVENT_NAME`, `NAME`, `DESCRIPTION`, `SORT`) VALUES
(1, 'ru', 'NEW_USER', 'Зарегистрировался новый пользователь', '\n\n#USER_ID# - ID пользователя\n#LOGIN# - Логин\n#EMAIL# - EMail\n#NAME# - Имя\n#LAST_NAME# - Фамилия\n#USER_IP# - IP пользователя\n#USER_HOST# - Хост пользователя\n', 1),
(2, 'ru', 'USER_INFO', 'Информация о пользователе', '\n\n#USER_ID# - ID пользователя\n#STATUS# - Статус логина\n#MESSAGE# - Сообщение пользователю\n#LOGIN# - Логин\n#URL_LOGIN# - Логин, закодированный для использования в URL\n#CHECKWORD# - Контрольная строка для смены пароля\n#NAME# - Имя\n#LAST_NAME# - Фамилия\n#EMAIL# - E-Mail пользователя\n', 2),
(3, 'ru', 'NEW_USER_CONFIRM', 'Подтверждение регистрации нового пользователя', '\n\n\n#USER_ID# - ID пользователя\n#LOGIN# - Логин\n#EMAIL# - EMail\n#NAME# - Имя\n#LAST_NAME# - Фамилия\n#USER_IP# - IP пользователя\n#USER_HOST# - Хост пользователя\n#CONFIRM_CODE# - Код подтверждения\n', 3),
(4, 'ru', 'USER_PASS_REQUEST', 'Запрос на смену пароля', '\n\n#USER_ID# - ID пользователя\n#STATUS# - Статус логина\n#MESSAGE# - Сообщение пользователю\n#LOGIN# - Логин\n#URL_LOGIN# - Логин, закодированный для использования в URL\n#CHECKWORD# - Контрольная строка для смены пароля\n#NAME# - Имя\n#LAST_NAME# - Фамилия\n#EMAIL# - E-Mail пользователя\n', 4),
(5, 'ru', 'USER_PASS_CHANGED', 'Подтверждение смены пароля', '\n\n#USER_ID# - ID пользователя\n#STATUS# - Статус логина\n#MESSAGE# - Сообщение пользователю\n#LOGIN# - Логин\n#URL_LOGIN# - Логин, закодированный для использования в URL\n#CHECKWORD# - Контрольная строка для смены пароля\n#NAME# - Имя\n#LAST_NAME# - Фамилия\n#EMAIL# - E-Mail пользователя\n', 5),
(6, 'ru', 'USER_INVITE', 'Приглашение на сайт нового пользователя', '#ID# - ID пользователя\n#LOGIN# - Логин\n#URL_LOGIN# - Логин, закодированный для использования в URL\n#EMAIL# - EMail\n#NAME# - Имя\n#LAST_NAME# - Фамилия\n#PASSWORD# - пароль пользователя \n#CHECKWORD# - Контрольная строка для смены пароля\n#XML_ID# - ID пользователя для связи с внешними источниками\n', 6),
(7, 'ru', 'FEEDBACK_FORM', 'Отправка сообщения через форму обратной связи', '#AUTHOR# - Автор сообщения\n#AUTHOR_EMAIL# - Email автора сообщения\n#TEXT# - Текст сообщения\n#EMAIL_FROM# - Email отправителя письма\n#EMAIL_TO# - Email получателя письма', 7),
(8, 'en', 'NEW_USER', 'New user was registered', '\n\n#USER_ID# - User ID\n#LOGIN# - Login\n#EMAIL# - EMail\n#NAME# - Name\n#LAST_NAME# - Last Name\n#USER_IP# - User IP\n#USER_HOST# - User Host\n', 1),
(9, 'en', 'USER_INFO', 'Account Information', '\n\n#USER_ID# - User ID\n#STATUS# - Account status\n#MESSAGE# - Message for user\n#LOGIN# - Login\n#URL_LOGIN# - Encoded login for use in URL\n#CHECKWORD# - Check string for password change\n#NAME# - Name\n#LAST_NAME# - Last Name\n#EMAIL# - User E-Mail\n', 2),
(10, 'en', 'NEW_USER_CONFIRM', 'New user registration confirmation', '\n\n#USER_ID# - User ID\n#LOGIN# - Login\n#EMAIL# - E-mail\n#NAME# - First name\n#LAST_NAME# - Last name\n#USER_IP# - User IP\n#USER_HOST# - User host\n#CONFIRM_CODE# - Confirmation code\n', 3),
(11, 'en', 'USER_PASS_REQUEST', 'Password Change Request', '\n\n#USER_ID# - User ID\n#STATUS# - Account status\n#MESSAGE# - Message for user\n#LOGIN# - Login\n#URL_LOGIN# - Encoded login for use in URL\n#CHECKWORD# - Check string for password change\n#NAME# - Name\n#LAST_NAME# - Last Name\n#EMAIL# - User E-Mail\n', 4),
(12, 'en', 'USER_PASS_CHANGED', 'Password Change Confirmation', '\n\n#USER_ID# - User ID\n#STATUS# - Account status\n#MESSAGE# - Message for user\n#LOGIN# - Login\n#URL_LOGIN# - Encoded login for use in URL\n#CHECKWORD# - Check string for password change\n#NAME# - Name\n#LAST_NAME# - Last Name\n#EMAIL# - User E-Mail\n', 5),
(13, 'en', 'USER_INVITE', 'Invitation of a new site user', '#ID# - User ID\n#LOGIN# - Login\n#URL_LOGIN# - Encoded login for use in URL\n#EMAIL# - EMail\n#NAME# - Name\n#LAST_NAME# - Last Name\n#PASSWORD# - User password \n#CHECKWORD# - Password check string\n#XML_ID# - User ID to link with external data sources\n\n', 6),
(14, 'en', 'FEEDBACK_FORM', 'Sending a message using a feedback form', '#AUTHOR# - Message author\n#AUTHOR_EMAIL# - Author''s e-mail address\n#TEXT# - Message text\n#EMAIL_FROM# - Sender''s e-mail address\n#EMAIL_TO# - Recipient''s e-mail address', 7),
(15, 'ru', 'NEW_BLOG_MESSAGE', 'Новое сообщение в блоге', '#BLOG_ID# - ID блога \n#BLOG_NAME# - Название блога\n#BLOG_URL# - Название блога латиницей\n#MESSAGE_TITLE# - Тема сообщения\n#MESSAGE_TEXT# - Текст сообщения\n#MESSAGE_DATE# - Дата сообщения\n#MESSAGE_PATH# - URL адрес сообщения\n#AUTHOR# - Автор сообщения\n#EMAIL_FROM# - Email отправителя письма\n#EMAIL_TO# - Email получателя письма', 150),
(16, 'ru', 'NEW_BLOG_COMMENT', 'Новый комментарий в блоге', '#BLOG_ID# - ID блога \n#BLOG_NAME# - Название блога\n#BLOG_URL# - Название блога латиницей\n#MESSAGE_TITLE# - Тема сообщения\n#COMMENT_TITLE# - Заголовок комментария\n#COMMENT_TEXT# - Текст комментария\n#COMMENT_DATE# - Текст комментария\n#COMMENT_PATH# - URL адрес сообщения\n#AUTHOR# - Автор сообщения\n#EMAIL_FROM# - Email отправителя письма\n#EMAIL_TO# - Email получателя письма', 150),
(17, 'ru', 'NEW_BLOG_COMMENT2COMMENT', 'Новый комментарий на ваш комментарий в блоге', '#BLOG_ID# - ID блога \n#BLOG_NAME# - Название блога\n#BLOG_URL# - Название блога латиницей\n#MESSAGE_TITLE# - Тема сообщения\n#COMMENT_TITLE# - Заголовок комментария\n#COMMENT_TEXT# - Текст комментария\n#COMMENT_DATE# - Текст комментария\n#COMMENT_PATH# - URL адрес сообщения\n#AUTHOR# - Автор сообщения\n#EMAIL_FROM# - Email отправителя письма\n#EMAIL_TO# - Email получателя письма', 150),
(18, 'ru', 'NEW_BLOG_COMMENT_WITHOUT_TITLE', 'Новый комментарий в блоге (без темы)', '#BLOG_ID# - ID блога \n#BLOG_NAME# - Название блога\n#BLOG_URL# - Название блога латиницей\n#MESSAGE_TITLE# - Тема сообщения\n#COMMENT_TEXT# - Текст комментария\n#COMMENT_DATE# - Текст комментария\n#COMMENT_PATH# - URL адрес сообщения\n#AUTHOR# - Автор сообщения\n#EMAIL_FROM# - Email отправителя письма\n#EMAIL_TO# - Email получателя письма', 150),
(19, 'ru', 'NEW_BLOG_COMMENT2COMMENT_WITHOUT_TITLE', 'Новый комментарий на ваш комментарий в блоге (без темы)', '#BLOG_ID# - ID блога \n#BLOG_NAME# - Название блога\n#BLOG_URL# - Название блога латиницей\n#COMMENT_TITLE# - Заголовок комментария\n#COMMENT_TEXT# - Текст комментария\n#COMMENT_DATE# - Текст комментария\n#COMMENT_PATH# - URL адрес сообщения\n#AUTHOR# - Автор сообщения\n#EMAIL_FROM# - Email отправителя письма\n#EMAIL_TO# - Email получателя письма', 150),
(20, 'ru', 'BLOG_YOUR_BLOG_TO_USER', 'Ваш блог был добавлен в друзья', '\n#BLOG_ID# - ID блога \n#BLOG_NAME# - Название блога\n#BLOG_URL# - Название блога латиницей\n#BLOG_ADR# - Адрес блога\n#USER_ID# - ID пользователя\n#USER# - Пользователь\n#USER_URL# - Адрес пользователя\n#EMAIL_FROM# - Email отправителя письма\n#EMAIL_TO# - Email получателя письма\n', 150),
(21, 'ru', 'BLOG_YOU_TO_BLOG', 'Вы были добавлены в друзья блога', '\n#BLOG_ID# - ID блога \n#BLOG_NAME# - Название блога\n#BLOG_URL# - Название блога латиницей\n#BLOG_ADR# - Адрес блога\n#USER_ID# - ID пользователя\n#USER# - Пользователь\n#USER_URL# - Адрес пользователя\n#EMAIL_FROM# - Email отправителя письма\n#EMAIL_TO# - Email получателя письма\n', 150),
(22, 'ru', 'BLOG_BLOG_TO_YOU', 'К вам в друзья был добавлен блог', '\n#BLOG_ID# - ID блога \n#BLOG_NAME# - Название блога\n#BLOG_URL# - Название блога латиницей\n#BLOG_ADR# - Адрес блога\n#USER_ID# - ID пользователя\n#USER# - Пользователь\n#USER_URL# - Адрес пользователя\n#EMAIL_FROM# - Email отправителя письма\n#EMAIL_TO# - Email получателя письма\n', 150),
(23, 'ru', 'BLOG_USER_TO_YOUR_BLOG', 'В ваш блог был добавлен друг', '\n#BLOG_ID# - ID блога \n#BLOG_NAME# - Название блога\n#BLOG_URL# - Название блога латиницей\n#BLOG_ADR# - Адрес блога\n#USER_ID# - ID пользователя\n#USER# - Пользователь\n#USER_URL# - Адрес пользователя\n#EMAIL_FROM# - Email отправителя письма\n#EMAIL_TO# - Email получателя письма\n', 150),
(24, 'en', 'NEW_BLOG_MESSAGE', 'New blog message', '#BLOG_ID# - Blog ID\n#BLOG_NAME# - Blog title\n#BLOG_URL# - Blog url\n#MESSAGE_TITLE# - Message title\n#MESSAGE_TEXT# - Message text\n#MESSAGE_DATE# - Message date\n#MESSAGE_PATH# - URL to message\n#AUTHOR# - Message author\n#EMAIL_FROM# - Sender email\n#EMAIL_TO# - Recipient email', 150),
(25, 'en', 'NEW_BLOG_COMMENT', 'New comment in blog', '#BLOG_ID# - Blog ID\n#BLOG_NAME# - Blog title\n#BLOG_URL# - Blog url\n#MESSAGE_TITLE# - Message title\n#COMMENT_TITLE# - Comment title\n#COMMENT_TEXT# - Comment text\n#COMMENT_DATE# - Comment date\n#COMMENT_PATH# - Comment URL\n#AUTHOR# - Comment author\n#EMAIL_FROM# - Sender email\n#EMAIL_TO# - Recipient email', 150),
(26, 'en', 'NEW_BLOG_COMMENT2COMMENT', 'New comment for your in blog', '#BLOG_ID# - Blog ID\n#BLOG_NAME# - Blog title\n#BLOG_URL# - Blog url\n#MESSAGE_TITLE# - Message title\n#COMMENT_TITLE# - Comment title\n#COMMENT_TEXT# - Comment text\n#COMMENT_DATE# - Comment date\n#COMMENT_PATH# - Comment URL\n#AUTHOR# - Comment author\n#EMAIL_FROM# - Sender email\n#EMAIL_TO# - Recipient email', 150),
(27, 'en', 'NEW_BLOG_COMMENT_WITHOUT_TITLE', 'New comment in blog (without subject)', '#BLOG_ID# - Blog ID\n#BLOG_NAME# - Blog title\n#BLOG_URL# - Blog url\n#MESSAGE_TITLE# - Message title\n#COMMENT_TEXT# - Comment text\n#COMMENT_DATE# - Comment date\n#COMMENT_PATH# - Comment URL\n#AUTHOR# - Comment author\n#EMAIL_FROM# - Sender email\n#EMAIL_TO# - Recipient email', 150),
(28, 'en', 'NEW_BLOG_COMMENT2COMMENT_WITHOUT_TITLE', 'New comment for your in blog (without subject)', '#BLOG_ID# - Blog ID\n#BLOG_NAME# - Blog title\n#BLOG_URL# - Blog url\n#MESSAGE_TITLE# - Message title\n#COMMENT_TEXT# - Comment text\n#COMMENT_DATE# - Comment date\n#COMMENT_PATH# - Comment URL\n#AUTHOR# - Comment author\n#EMAIL_FROM# - Sender email\n#EMAIL_TO# - Recipient email', 150),
(29, 'en', 'BLOG_YOUR_BLOG_TO_USER', 'Your blog has been added to friends', '#BLOG_ID# - Blog ID\\r\\n#BLOG_NAME# - Blog name\\r\\n#BLOG_URL# - Blog name, Latin letters only\\r\\n#BLOG_ADR# - Blog address\\r\\n#USER_ID# - User ID\\r\\n#USER# - User\\r\\n#USER_URL# - User URL\\r\\n#EMAIL_FROM# - Sender E-mail\\r\\n#EMAIL_TO# - Recipient E-mail', 150),
(30, 'en', 'BLOG_YOU_TO_BLOG', 'You have been added to blog friends', '#BLOG_ID# - Blog ID\\r\\n#BLOG_NAME# - Blog name\\r\\n#BLOG_URL# - Blog name, Latin letters only\\r\\n#BLOG_ADR# - Blog address\\r\\n#USER_ID# - User ID\\r\\n#USER# - User\\r\\n#USER_URL# - User URL\\r\\n#EMAIL_FROM# - Sender E-mail\\r\\n#EMAIL_TO# - Recipient E-mail', 150),
(31, 'en', 'BLOG_BLOG_TO_YOU', 'A blog has been added to your friends', '#BLOG_ID# - Blog ID\\r\\n#BLOG_NAME# - Blog name\\r\\n#BLOG_URL# - Blog name, Latin letters only\\r\\n#BLOG_ADR# - Blog address\\r\\n#USER_ID# - User ID\\r\\n#USER# - User\\r\\n#USER_URL# - User URL\\r\\n#EMAIL_FROM# - Sender E-mail\\r\\n#EMAIL_TO# - Recipient E-mail', 150),
(32, 'en', 'BLOG_USER_TO_YOUR_BLOG', 'A friend has been added to your blog', '#BLOG_ID# - Blog ID\\r\\n#BLOG_NAME# - Blog name\\r\\n#BLOG_URL# - Blog name, Latin letters only\\r\\n#BLOG_ADR# - Blog address\\r\\n#USER_ID# - User ID\\r\\n#USER# - User\\r\\n#USER_URL# - User URL\\r\\n#EMAIL_FROM# - Sender E-mail\\r\\n#EMAIL_TO# - Recipient E-mail', 150),
(33, 'ru', 'NEW_FORUM_MESSAGE', 'Новое сообщение на форуме', '\n			#FORUM_ID# - ID форума\n			#FORUM_NAME# - Название форума\n			#TOPIC_ID# - ID темы\n			#MESSAGE_ID# - ID сообщения\n			#TOPIC_TITLE# - Тема сообщения\n			#MESSAGE_TEXT# - Текст сообщения\n			#MESSAGE_DATE# - Дата сообщения\n			#AUTHOR# - Автор сообщения\n			#RECIPIENT# - Получатель сообщения\n			#TAPPROVED# - Тема сообщения показывается\n			#MAPPROVED# - Тело сообщения показывается\n			#PATH2FORUM# - Адрес сообщения\n			#FROM_EMAIL# - E-Mail для поля From письма', 150),
(34, 'ru', 'NEW_FORUM_PRIV', 'Приватное письмо посетителю форума', '\n			#FROM_NAME# - Автор сообщения\n			#FROM_EMAIL# - E-Mail автора сообщения\n			#TO_NAME# - Имя получателя сообщения\n			#TO_EMAIL# - E-Mail получателя сообщения\n			#SUBJECT# - Тема сообщения\n			#MESSAGE# - Тело сообщения\n			#MESSAGE_DATE# - Дата сообщения', 150),
(35, 'ru', 'NEW_FORUM_PRIVATE_MESSAGE', 'Приватное сообщение', '\n			#FROM_NAME# - Имя автора сообщения\n			#FROM_USER_ID# - ID автора сообщения\n			#FROM_EMAIL# - E-Mail автора сообщения\n			#TO_NAME# - Имя получателя сообщения\n			#TO_USER_ID# - ID получателя сообщения\n			#TO_EMAIL# - E-Mail получателя сообщения\n			#SUBJECT# - Тема сообщения\n			#MESSAGE# - Текст сообщения\n			#MESSAGE_DATE# - Дата сообщения\n			#MESSAGE_LINK# - Ссылка на сообщение', 150),
(36, 'ru', 'EDIT_FORUM_MESSAGE', 'Изменение сообщения на форуме', '\n			#FORUM_ID# - ID форума\n			#FORUM_NAME# - Название форума\n			#TOPIC_ID# - ID темы\n			#MESSAGE_ID# - ID сообщения\n			#TOPIC_TITLE# - Тема сообщения\n			#MESSAGE_TEXT# - Текст сообщения\n			#MESSAGE_DATE# - Дата сообщения\n			#AUTHOR# - Автор сообщения\n			#RECIPIENT# - Получатель сообщения\n			#TAPPROVED# - Тема сообщения показывается\n			#MAPPROVED# - Тело сообщения показывается\n			#PATH2FORUM# - Адрес сообщения\n			#FROM_EMAIL# - E-Mail для поля From письма', 150),
(37, 'en', 'NEW_FORUM_MESSAGE', 'New forum message', '\n			#FORUM_ID# - Forum ID\n			#FORUM_NAME# - Forum name\n			#TOPIC_ID# - Topic ID\n			#MESSAGE_ID# - Message ID\n			#TOPIC_TITLE# - Topic title\n			#MESSAGE_TEXT# - Message text\n			#MESSAGE_DATE# - Message date\n			#AUTHOR# - Message author\n			#RECIPIENT# - E-Mail recipient\n			#TAPPROVED# - Message topic is approved\n			#MAPPROVED# - Message is approved\n			#PATH2FORUM# - Message Url\n			#FROM_EMAIL# - E-Mail for From field of the EMail', 150),
(38, 'en', 'NEW_FORUM_PRIV', 'Private message for forum user', '\n			#FROM_NAME# - Name of the sender\n			#FROM_EMAIL# - E-Mail of the sender\n			#TO_NAME# - Name of recipient\n			#TO_EMAIL# - E-Mail of recipient\n			#SUBJECT# - Topic\n			#MESSAGE# - Message\n			#MESSAGE_DATE# - Date', 150),
(39, 'en', 'NEW_FORUM_PRIVATE_MESSAGE', 'Private message for forum user', '\n			#FROM_NAME# - Name of the sender\n			#FROM_USER_ID# - ID of the sender\n			#FROM_EMAIL# - E-Mail of the sender\n			#TO_NAME# - Name of recipient\n			#TO_USER_ID# - ID of recipient\n			#TO_EMAIL# - E-Mail of recipient\n			#SUBJECT# - Topic\n			#MESSAGE# - Message\n			#MESSAGE_DATE# - Date\n			#MESSAGE_LINK# - Link to message', 150),
(40, 'en', 'EDIT_FORUM_MESSAGE', 'Changing forum message', '\n			#FORUM_ID# - Forum ID\n			#FORUM_NAME# - Forum name\n			#TOPIC_ID# - Topic ID\n			#MESSAGE_ID# - Message ID\n			#TOPIC_TITLE# - Topic title\n			#MESSAGE_TEXT# - Message text\n			#MESSAGE_DATE# - Message date\n			#AUTHOR# - Message author\n			#RECIPIENT# - E-Mail recipient\n			#TAPPROVED# - Message topic is approved\n			#MAPPROVED# - Message is approved\n			#PATH2FORUM# - Message Url\n			#FROM_EMAIL# - E-Mail for From field of the EMail', 150),
(41, 'ru', 'FORUM_NEW_MESSAGE_MAIL', 'Новое сообщение на форуме в режиме общения по E-Mail', '#FORUM_NAME# - Название форума\n#AUTHOR# - Автор сообщения\n#FROM_EMAIL# - E-Mail для поля From письма\n#RECIPIENT# - Получатель сообщения\n#TOPIC_TITLE# - Тема сообщения\n#MESSAGE_TEXT# - Текст сообщения\n#PATH2FORUM# - Адрес сообщения\n#MESSAGE_DATE# - Дата сообщения\n#FORUM_EMAIL# - Е-Mail адрес для добавления сообщений на форум\n#FORUM_ID# - ID форума\n#TOPIC_ID# - ID темы \n#MESSAGE_ID# - ID сообщения\n#TAPPROVED# - Тема опубликована\n#MAPPROVED# - Сообщение опубликовано\n', 150),
(42, 'en', 'FORUM_NEW_MESSAGE_MAIL', 'New message at the forum (e-mail messaging mode)', '#FORUM_NAME# - Forum name\n#AUTHOR# - Message author\n#FROM_EMAIL# - E-Mail in the &amp;From&amp; field\n#RECIPIENT# - Message recipient\n#TOPIC_TITLE# - Message subject\n#MESSAGE_TEXT# - Message text\n#PATH2FORUM# - Message URL\n#MESSAGE_DATE# - Message date\n#FORUM_EMAIL# - E-Mail to add messages to the forum \n#FORUM_ID# - Forum ID\n#TOPIC_ID# - Topic ID \n#MESSAGE_ID# - Message ID\n#TAPPROVED# - Topic approved and published\n#MAPPROVED# - Message approved and published\n', 150),
(43, 'ru', 'SALE_NEW_ORDER', 'Новый заказ', '#ORDER_ID# - код заказа\n#ORDER_DATE# - дата заказа\n#ORDER_USER# - заказчик\n#PRICE# - сумма заказа\n#EMAIL# - E-Mail заказчика\n#BCC# - E-Mail скрытой копии\n#ORDER_LIST# - состав заказа\n#SALE_EMAIL# - E-Mail отдела продаж', 150),
(44, 'ru', 'SALE_NEW_ORDER_RECURRING', 'Новый заказ на продление подписки', '#ORDER_ID# - код заказа\n#ORDER_DATE# - дата заказа\n#ORDER_USER# - заказчик\n#PRICE# - сумма заказа\n#EMAIL# - E-Mail заказчика\n#BCC# - E-Mail скрытой копии\n#ORDER_LIST# - состав заказа\n#SALE_EMAIL# - E-Mail отдела продаж', 150),
(45, 'ru', 'SALE_ORDER_REMIND_PAYMENT', 'Напоминание об оплате заказа', '#ORDER_ID# - код заказа\n#ORDER_DATE# - дата заказа\n#ORDER_USER# - заказчик\n#PRICE# - сумма заказа\n#EMAIL# - E-Mail заказчика\n#BCC# - E-Mail скрытой копии\n#ORDER_LIST# - состав заказа\n#SALE_EMAIL# - E-Mail отдела продаж', 150),
(46, 'ru', 'SALE_ORDER_CANCEL', 'Отмена заказа', '#ORDER_ID# - код заказа\n#ORDER_DATE# - дата заказа\n#EMAIL# - E-Mail пользователя\n#ORDER_CANCEL_DESCRIPTION# - причина отмены\n#SALE_EMAIL# - E-Mail отдела продаж', 150),
(47, 'ru', 'SALE_ORDER_PAID', 'Заказ оплачен', '#ORDER_ID# - код заказа\n#ORDER_DATE# - дата заказа\n#EMAIL# - E-Mail пользователя\n#SALE_EMAIL# - E-Mail отдела продаж', 150),
(48, 'ru', 'SALE_ORDER_DELIVERY', 'Доставка заказа разрешена', '#ORDER_ID# - код заказа\n#ORDER_DATE# - дата заказа\n#EMAIL# - E-Mail пользователя\n#SALE_EMAIL# - E-Mail отдела продаж', 150),
(49, 'ru', 'SALE_RECURRING_CANCEL', 'Подписка отменена', '#ORDER_ID# - код заказа\n#ORDER_DATE# - дата заказа\n#EMAIL# - E-Mail пользователя\n#CANCELED_REASON# - причина отмены\n#SALE_EMAIL# - E-Mail отдела продаж', 150),
(50, 'ru', 'SALE_SUBSCRIBE_PRODUCT', 'Уведомление о поступлении товара', '#USER_NAME# - имя пользователя\n#EMAIL# - email пользователя\n#NAME# - название товара\n#PAGE_URL# - детальная страница товара', 150),
(51, 'ru', 'SALE_ORDER_TRACKING_NUMBER', 'Уведомление об изменении идентификатора почтового отправления', '#ORDER_ID# - код заказа\n#ORDER_DATE# - дата заказа\n#ORDER_USER# - заказчик\n#ORDER_TRACKING_NUMBER# - идентификатор почтового отправления\n#EMAIL# - E-Mail заказчика\n#BCC# - E-Mail скрытой копии\n#SALE_EMAIL# - E-Mail отдела продаж', 150),
(52, 'ru', 'SALE_STATUS_CHANGED_F', 'Изменение статуса заказа на  "Выполнен"', '#ORDER_ID# - код заказа\n#ORDER_DATE# - дата заказа\n#ORDER_STATUS# - статус заказа\n#EMAIL# - E-Mail пользователя\n#ORDER_DESCRIPTION# - описание статуса заказа\n#TEXT# - текст\n#SALE_EMAIL# - E-Mail отдела продаж\n', 150),
(53, 'ru', 'SALE_STATUS_CHANGED_N', 'Изменение статуса заказа на  "Принят"', '#ORDER_ID# - код заказа\n#ORDER_DATE# - дата заказа\n#ORDER_STATUS# - статус заказа\n#EMAIL# - E-Mail пользователя\n#ORDER_DESCRIPTION# - описание статуса заказа\n#TEXT# - текст\n#SALE_EMAIL# - E-Mail отдела продаж\n', 150),
(54, 'en', 'SALE_NEW_ORDER', 'New order', '#ORDER_ID# - Order ID\n#ORDER_DATE# - Order date\n#ORDER_USER# - User\n#EMAIL# - User E-Mail\n#BCC# - BCC E-Mail\n#ORDER_LIST# - Order list\n#SALE_EMAIL# - Sales department e-mail', 150),
(55, 'en', 'SALE_NEW_ORDER_RECURRING', 'New Order for Subscription Renewal', '#ORDER_ID# - order ID\\r\\n#ORDER_DATE# - order date\\r\\n#ORDER_USER# - customer\\r\\n#PRICE# - order amount\\r\\n#EMAIL# - customer''s e-mail address\\r\\n#BCC# - blind copy e-mail address\\r\\n#ORDER_LIST# - order items\\r\\n#SALE_EMAIL# - sales dept. e-mail address', 150),
(56, 'en', 'SALE_ORDER_REMIND_PAYMENT', 'Order Payment Reminder', '#ORDER_ID# - order ID\n#ORDER_DATE# - order date\n#ORDER_USER# - customer\n#PRICE# - order amount\n#EMAIL# - customer''s e-mail address\n#BCC# - blind copy e-mail address\n#ORDER_LIST# - order items\n#SALE_EMAIL# - sales dept. e-mail address', 150),
(57, 'en', 'SALE_ORDER_CANCEL', 'Cancel order', '#ORDER_ID# - Order ID\n#ORDER_DATE# - Order date\n#EMAIL# - User E-Mail\n#ORDER_CANCEL_DESCRIPTION# - Order cancel description\n#SALE_EMAIL# - Sales department e-mail', 150),
(58, 'en', 'SALE_ORDER_PAID', 'Paid order', '#ORDER_ID# - Order ID\n#ORDER_DATE# - Order date\n#EMAIL# - User E-Mail\n#SALE_EMAIL# - Sales department e-mail', 150),
(59, 'en', 'SALE_ORDER_DELIVERY', 'Order delivery allowed', '#ORDER_ID# - Order ID\n#ORDER_DATE# - Order date\n#EMAIL# - User E-Mail', 150),
(60, 'en', 'SALE_RECURRING_CANCEL', 'Recurring payment canceled', '#ORDER_ID# - Order ID\n#ORDER_DATE# - Order date\n#EMAIL# - User E-Mail\n#CANCELED_REASON# - Reason\n#SALE_EMAIL# - Sales department e-mail', 150),
(61, 'en', 'SALE_SUBSCRIBE_PRODUCT', 'Back in stock notification', '#USER_NAME# - user name\n#EMAIL# - user e-mail \n#NAME# - product name\n#PAGE_URL# - product details page', 150),
(62, 'en', 'SALE_ORDER_TRACKING_NUMBER', 'Notification of change in tracking number ', '#ORDER_ID# - order ID number\n#ORDER_DATE# - order date\n#ORDER_USER# - customer\n#ORDER_TRACKING_NUMBER# - tracking number for order\n#EMAIL# - E-Mail of customer\n#BCC# - E-Mail for blind copy\n#SALE_EMAIL# - E-Mail of sales department or associate', 150),
(63, 'en', 'SALE_STATUS_CHANGED_F', 'Changing order status to "Completed"', '#ORDER_ID# - order ID\n#ORDER_DATE# - order date\n#ORDER_STATUS# - order status\n#EMAIL# - customer e-mail\n#ORDER_DESCRIPTION# - order status description\n#TEXT# - text\n#SALE_EMAIL# - Sales department e-mail\n', 150),
(64, 'en', 'SALE_STATUS_CHANGED_N', 'Changing order status to "Accepted"', '#ORDER_ID# - order ID\n#ORDER_DATE# - order date\n#ORDER_STATUS# - order status\n#EMAIL# - customer e-mail\n#ORDER_DESCRIPTION# - order status description\n#TEXT# - text\n#SALE_EMAIL# - Sales department e-mail\n', 150),
(65, 'ru', 'VIRUS_DETECTED', 'Обнаружен вирус', '#EMAIL# - E-Mail администратора сайта (из настроек главного модуля)', 150),
(66, 'en', 'VIRUS_DETECTED', 'Virus detected', '#EMAIL# - Site administrator''s e-mail address (from the Kernel module settings)', 150),
(67, 'ru', 'SUBSCRIBE_CONFIRM', 'Подтверждение подписки', '#ID# - идентификатор подписки\n#EMAIL# - адрес подписки\n#CONFIRM_CODE# - код подтверждения\n#SUBSCR_SECTION# - раздел, где находится страница редактирования подписки (задается в настройках)\n#USER_NAME# - имя подписчика (может отсутствовать)\n#DATE_SUBSCR# - дата добавления/изменения адреса\n', 150),
(68, 'en', 'SUBSCRIBE_CONFIRM', 'Confirmation of subscription', '#ID# - subscription ID\n#EMAIL# - subscription email\n#CONFIRM_CODE# - confirmation code\n#SUBSCR_SECTION# - section with subscription edit page (specifies in the settings)\n#USER_NAME# - subscriber''s name (optional)\n#DATE_SUBSCR# - date of adding/change of address\n', 150),
(69, 'ru', 'VOTE_FOR', 'Новый голос', '#ID# - ID результата голосования\n#TIME# - время голосования\n#VOTE_TITLE# - наименование опроса\n#VOTE_DESCRIPTION# - описание опроса\n#VOTE_ID# - ID опроса\n#CHANNEL# - наименование группы опроса\n#CHANNEL_ID# - ID группы опроса\n#VOTER_ID# - ID проголосовавшего посетителя\n#USER_NAME# - ФИО пользователя\n#LOGIN# - логин\n#USER_ID# - ID пользователя\n#STAT_GUEST_ID# - ID посетителя модуля статистики\n#SESSION_ID# - ID сессии модуля статистики\n#IP# - IP адрес\n#VOTE_STATISTIC# - Сводная статистика опроса типа ( - Вопрос - Ответ )\n#URL# - Путь к опросу\n', 150),
(70, 'en', 'VOTE_FOR', 'New vote', '#ID# - Vote result ID\n#TIME# - Time of vote\n#VOTE_TITLE# - Poll name\n#VOTE_DESCRIPTION# - Poll description\n#VOTE_ID# - Poll ID\n#CHANNEL# - Poll group name\n#CHANNEL_ID# - Poll group ID\n#VOTER_ID# - Voter''s user ID\n#USER_NAME# - User full name\n#LOGIN# - login\n#USER_ID# - User ID\n#STAT_GUEST_ID# - Visitor ID in web analytics module\n#SESSION_ID# - Session ID in web analytics module\n#IP# - IP address\n#VOTE_STATISTIC# - Summary statistics of this poll type ( - Question - Answer)\n#URL# - Poll URL', 150);

-- --------------------------------------------------------

--
-- Table structure for table `b_favorite`
--

CREATE TABLE IF NOT EXISTS `b_favorite` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` text COLLATE utf8_unicode_ci,
  `COMMENTS` text COLLATE utf8_unicode_ci,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `CODE_ID` int(18) DEFAULT NULL,
  `COMMON` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `MENU_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_file`
--

CREATE TABLE IF NOT EXISTS `b_file` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HEIGHT` int(18) DEFAULT NULL,
  `WIDTH` int(18) DEFAULT NULL,
  `FILE_SIZE` int(18) NOT NULL,
  `CONTENT_TYPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'IMAGE',
  `SUBDIR` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILE_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ORIGINAL_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HANDLER_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EXTERNAL_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FILE_EXTERNAL_ID` (`EXTERNAL_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=68 ;

--
-- Dumping data for table `b_file`
--

INSERT INTO `b_file` (`ID`, `TIMESTAMP_X`, `MODULE_ID`, `HEIGHT`, `WIDTH`, `FILE_SIZE`, `CONTENT_TYPE`, `SUBDIR`, `FILE_NAME`, `ORIGINAL_NAME`, `DESCRIPTION`, `HANDLER_ID`, `EXTERNAL_ID`) VALUES
(53, '2015-04-22 17:43:48', 'iblock', 97, 115, 5781, 'image/gif', 'iblock/454', '454e2c9dffc722fd8847b6a75eb79772.gif', '8b0b3bc52fec8070e3561af5d05cd3a8.gif', '', NULL, '4a3d72dcce6a28ea9aa42a81e7c61665'),
(54, '2015-04-22 17:43:48', 'iblock', 97, 116, 7345, 'image/gif', 'iblock/5b7', '5b72586b4e560b1c2409d8b1e11f1025.gif', '38bf3b317397e56d07fe06731ca3a441.gif', '', NULL, '383d86d3cfb7a3513c84f410f09b59a6'),
(55, '2015-04-24 02:32:02', 'iblock', 496, 1184, 149121, 'image/jpeg', 'iblock/f08', 'f08c782e8a07dc7b7b866f256a20e7e2.jpg', 'photo-2.jpg', '', NULL, '60f46925839549965df8b81e30a835f8'),
(56, '2015-04-24 02:34:38', 'iblock', 74, 119, 3435, 'image/jpeg', 'iblock/c4a', 'c4a0dce108d3b7794924946763f1d1f9.jpg', 'photo-4_sm.jpg', '', NULL, '729969ecc36d4ac3183249ee5b79d274'),
(57, '2015-04-27 15:44:07', 'iblock', 53, 167, 4856, 'image/png', 'iblock/1f7', '1f764e40ecc96ad4ad703bdbfa99fcd3.png', 'proj_logo.png', '', NULL, '96bb58c0d14745c43988fc096eedcf02'),
(58, '2015-04-27 15:44:07', 'iblock', 496, 1184, 154170, 'image/jpeg', 'iblock/5f3', '5f3cf1b1f7ab947cd5dca8154ceeef13.jpg', 'photo-1.jpg', '', NULL, '41445ac4c02fa203d496722df46046d0'),
(59, '2015-04-27 15:44:51', 'iblock', 53, 167, 4856, 'image/png', 'iblock/4a6', '4a67ee4d47cc868a7f806cc1f7735d91.png', 'proj_logo.png', '', NULL, '57d03ff91e049d699746f32e5a9b9aff'),
(60, '2015-04-27 15:44:51', 'iblock', 496, 1184, 149121, 'image/jpeg', 'iblock/a7c', 'a7cabbec98c5db1dc216f26eba92c804.jpg', 'photo-2.jpg', '', NULL, '2ba44be0459dd7537057e3e2b218ebeb'),
(61, '2015-04-27 15:45:40', 'iblock', 53, 167, 4856, 'image/png', 'iblock/0f5', '0f587c8b1089ebf03d8558de9e9648c2.png', 'proj_logo.png', '', NULL, 'c30024009f422f0bc2b814b8c9cd9fc2'),
(64, '2015-04-27 15:47:16', 'iblock', 496, 1184, 112604, 'image/jpeg', 'iblock/2a6', '2a604ed82f670274510bbba3c28ee195.jpg', 'photo-3.jpg', '', NULL, 'c517cd0864cf893f4b659e203defe2f7'),
(65, '2015-04-28 08:55:06', 'iblock', 288, 366, 68589, 'image/png', 'iblock/9a3', '9a31bf67d8d0a4fd1c938bac7d7dacba.png', 'photo-1.png', '', NULL, 'd28ad94207298ce98f93c83ca092d6d9'),
(66, '2015-04-28 08:57:49', 'iblock', 288, 366, 68589, 'image/png', 'iblock/b1a', 'b1a62e393895b95f67efbac484337b53.png', 'photo-1.png', '', NULL, '4c8eb08895cf18ac9ad844586f8c8ef7'),
(67, '2015-04-28 08:58:22', 'iblock', 288, 366, 68589, 'image/png', 'iblock/d7c', 'd7c71af563e5e5e23c65c0e329508f8c.png', 'photo-1.png', '', NULL, '1f5c150176059c3671fb1df54f569ced');

-- --------------------------------------------------------

--
-- Table structure for table `b_file_search`
--

CREATE TABLE IF NOT EXISTS `b_file_search` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SESS_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `F_PATH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `B_DIR` int(11) NOT NULL DEFAULT '0',
  `F_SIZE` int(11) NOT NULL DEFAULT '0',
  `F_TIME` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_filters`
--

CREATE TABLE IF NOT EXISTS `b_filters` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) DEFAULT NULL,
  `FILTER_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FIELDS` text COLLATE utf8_unicode_ci NOT NULL,
  `COMMON` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRESET` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRESET_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(18) DEFAULT NULL,
  `SORT_FIELD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form`
--

CREATE TABLE IF NOT EXISTS `b_form` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `BUTTON` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `C_SORT` int(18) DEFAULT '100',
  `FIRST_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGE_ID` int(18) DEFAULT NULL,
  `USE_CAPTCHA` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DESCRIPTION_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'html',
  `FORM_TEMPLATE` text COLLATE utf8_unicode_ci,
  `USE_DEFAULT_TEMPLATE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `SHOW_TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MAIL_EVENT_TYPE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SHOW_RESULT_TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRINT_RESULT_TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDIT_RESULT_TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILTER_RESULT_TEMPLATE` text COLLATE utf8_unicode_ci,
  `TABLE_RESULT_TEMPLATE` text COLLATE utf8_unicode_ci,
  `USE_RESTRICTIONS` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `RESTRICT_USER` int(5) DEFAULT '0',
  `RESTRICT_TIME` int(10) DEFAULT '0',
  `RESTRICT_STATUS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STAT_EVENT1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STAT_EVENT2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STAT_EVENT3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_SID` (`SID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_2_group`
--

CREATE TABLE IF NOT EXISTS `b_form_2_group` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `GROUP_ID` int(18) NOT NULL DEFAULT '0',
  `PERMISSION` int(5) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_ID` (`FORM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_2_mail_template`
--

CREATE TABLE IF NOT EXISTS `b_form_2_mail_template` (
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `MAIL_TEMPLATE_ID` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`FORM_ID`,`MAIL_TEMPLATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_2_site`
--

CREATE TABLE IF NOT EXISTS `b_form_2_site` (
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`FORM_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_answer`
--

CREATE TABLE IF NOT EXISTS `b_form_answer` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FIELD_ID` int(18) NOT NULL DEFAULT '0',
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci,
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `FIELD_WIDTH` int(18) DEFAULT NULL,
  `FIELD_HEIGHT` int(18) DEFAULT NULL,
  `FIELD_PARAM` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_FIELD_ID` (`FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_crm`
--

CREATE TABLE IF NOT EXISTS `b_form_crm` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `AUTH_HASH` varchar(32) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_crm_field`
--

CREATE TABLE IF NOT EXISTS `b_form_crm_field` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `LINK_ID` int(18) NOT NULL DEFAULT '0',
  `FIELD_ID` int(18) DEFAULT '0',
  `FIELD_ALT` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `CRM_FIELD` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `ix_b_form_crm_field_1` (`LINK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_crm_link`
--

CREATE TABLE IF NOT EXISTS `b_form_crm_link` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `CRM_ID` int(18) NOT NULL DEFAULT '0',
  `LINK_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'M',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_form_crm_link_1` (`FORM_ID`,`CRM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_field`
--

CREATE TABLE IF NOT EXISTS `b_form_field` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `TITLE` text COLLATE utf8_unicode_ci,
  `TITLE_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `SID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `ADDITIONAL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `REQUIRED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IN_FILTER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IN_RESULTS_TABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IN_EXCEL_TABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `FIELD_TYPE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGE_ID` int(18) DEFAULT NULL,
  `COMMENTS` text COLLATE utf8_unicode_ci,
  `FILTER_TITLE` text COLLATE utf8_unicode_ci,
  `RESULTS_TABLE_TITLE` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_ID` (`FORM_ID`),
  KEY `IX_SID` (`SID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_field_filter`
--

CREATE TABLE IF NOT EXISTS `b_form_field_filter` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FIELD_ID` int(18) NOT NULL DEFAULT '0',
  `PARAMETER_NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FILTER_TYPE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FIELD_ID` (`FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_field_validator`
--

CREATE TABLE IF NOT EXISTS `b_form_field_validator` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `FIELD_ID` int(18) NOT NULL DEFAULT '0',
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'y',
  `C_SORT` int(18) DEFAULT '100',
  `VALIDATOR_SID` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `PARAMS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_ID` (`FORM_ID`),
  KEY `IX_FIELD_ID` (`FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_menu`
--

CREATE TABLE IF NOT EXISTS `b_form_menu` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `MENU` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_ID` (`FORM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_result`
--

CREATE TABLE IF NOT EXISTS `b_form_result` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `STATUS_ID` int(18) NOT NULL DEFAULT '0',
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `USER_ID` int(18) DEFAULT NULL,
  `USER_AUTH` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `STAT_GUEST_ID` int(18) DEFAULT NULL,
  `STAT_SESSION_ID` int(18) DEFAULT NULL,
  `SENT_TO_CRM` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_ID` (`FORM_ID`),
  KEY `IX_STATUS_ID` (`STATUS_ID`),
  KEY `IX_SENT_TO_CRM` (`SENT_TO_CRM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_result_answer`
--

CREATE TABLE IF NOT EXISTS `b_form_result_answer` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `RESULT_ID` int(18) NOT NULL DEFAULT '0',
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `FIELD_ID` int(18) NOT NULL DEFAULT '0',
  `ANSWER_ID` int(18) DEFAULT NULL,
  `ANSWER_TEXT` text COLLATE utf8_unicode_ci,
  `ANSWER_TEXT_SEARCH` longtext COLLATE utf8_unicode_ci,
  `ANSWER_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ANSWER_VALUE_SEARCH` longtext COLLATE utf8_unicode_ci,
  `USER_TEXT` longtext COLLATE utf8_unicode_ci,
  `USER_TEXT_SEARCH` longtext COLLATE utf8_unicode_ci,
  `USER_DATE` datetime DEFAULT NULL,
  `USER_FILE_ID` int(18) DEFAULT NULL,
  `USER_FILE_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_FILE_IS_IMAGE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_FILE_HASH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_FILE_SUFFIX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_FILE_SIZE` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_RESULT_ID` (`RESULT_ID`),
  KEY `IX_FIELD_ID` (`FIELD_ID`),
  KEY `IX_ANSWER_ID` (`ANSWER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_status`
--

CREATE TABLE IF NOT EXISTS `b_form_status` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORM_ID` int(18) NOT NULL DEFAULT '0',
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DEFAULT_VALUE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CSS` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'statusgreen',
  `HANDLER_OUT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HANDLER_IN` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MAIL_EVENT_TYPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_ID` (`FORM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_status_2_group`
--

CREATE TABLE IF NOT EXISTS `b_form_status_2_group` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `STATUS_ID` int(18) NOT NULL DEFAULT '0',
  `GROUP_ID` int(18) NOT NULL DEFAULT '0',
  `PERMISSION` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FORM_STATUS_GROUP` (`STATUS_ID`,`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_form_status_2_mail_template`
--

CREATE TABLE IF NOT EXISTS `b_form_status_2_mail_template` (
  `STATUS_ID` int(18) NOT NULL DEFAULT '0',
  `MAIL_TEMPLATE_ID` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`STATUS_ID`,`MAIL_TEMPLATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum`
--

CREATE TABLE IF NOT EXISTS `b_forum` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `FORUM_GROUP_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `SORT` int(10) NOT NULL DEFAULT '150',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_HTML` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ALLOW_ANCHOR` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_BIU` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_IMG` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_VIDEO` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_LIST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_QUOTE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_CODE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_FONT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_SMILES` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_UPLOAD` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ALLOW_TABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ALLOW_ALIGN` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_UPLOAD_EXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ALLOW_MOVE_TOPIC` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ALLOW_TOPIC_TITLED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ALLOW_NL2BR` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ALLOW_SIGNATURE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `PATH2FORUM_MESSAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ASK_GUEST_EMAIL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `USE_CAPTCHA` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `INDEXATION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `DEDUPLICATION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `MODERATION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ORDER_BY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `ORDER_DIRECTION` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'DESC',
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ru',
  `TOPICS` int(11) NOT NULL DEFAULT '0',
  `POSTS` int(11) NOT NULL DEFAULT '0',
  `LAST_POSTER_ID` int(11) DEFAULT NULL,
  `LAST_POSTER_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_POST_DATE` datetime DEFAULT NULL,
  `LAST_MESSAGE_ID` bigint(20) DEFAULT NULL,
  `POSTS_UNAPPROVED` int(11) DEFAULT '0',
  `ABS_LAST_POSTER_ID` int(11) DEFAULT NULL,
  `ABS_LAST_POSTER_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABS_LAST_POST_DATE` datetime DEFAULT NULL,
  `ABS_LAST_MESSAGE_ID` bigint(20) DEFAULT NULL,
  `EVENT1` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'forum',
  `EVENT2` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'message',
  `EVENT3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HTML` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FORUM_SORT` (`SORT`),
  KEY `IX_FORUM_ACTIVE` (`ACTIVE`),
  KEY `IX_FORUM_GROUP_ID` (`FORUM_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum2site`
--

CREATE TABLE IF NOT EXISTS `b_forum2site` (
  `FORUM_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `PATH2FORUM_MESSAGE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`FORUM_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_dictionary`
--

CREATE TABLE IF NOT EXISTS `b_forum_dictionary` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `b_forum_dictionary`
--

INSERT INTO `b_forum_dictionary` (`ID`, `TITLE`, `TYPE`) VALUES
(1, '[ru] Словарь слов', 'W'),
(2, '[ru] Словарь транслита', 'T'),
(3, '[en] Bad words', 'W'),
(4, '[en] Transliteration', 'T');

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_email`
--

CREATE TABLE IF NOT EXISTS `b_forum_email` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL_FORUM_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `FORUM_ID` int(11) NOT NULL,
  `SOCNET_GROUP_ID` int(11) DEFAULT NULL,
  `MAIL_FILTER_ID` int(11) NOT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `USE_EMAIL` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL_GROUP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUBJECT_SUF` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USE_SUBJECT` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL_TEMPLATES_MESSAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOT_MEMBER_POST` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FORUM_EMAIL_FORUM_SOC` (`FORUM_ID`,`SOCNET_GROUP_ID`),
  KEY `IX_B_FORUM_EMAIL_FILTER_ID` (`MAIL_FILTER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_file`
--

CREATE TABLE IF NOT EXISTS `b_forum_file` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `FORUM_ID` int(18) DEFAULT NULL,
  `TOPIC_ID` int(20) DEFAULT NULL,
  `MESSAGE_ID` int(20) DEFAULT NULL,
  `FILE_ID` int(18) NOT NULL,
  `USER_ID` int(18) DEFAULT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `HITS` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FORUM_FILE_FILE` (`FILE_ID`),
  KEY `IX_FORUM_FILE_FORUM` (`FORUM_ID`),
  KEY `IX_FORUM_FILE_TOPIC` (`TOPIC_ID`),
  KEY `IX_FORUM_FILE_MESSAGE` (`MESSAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_filter`
--

CREATE TABLE IF NOT EXISTS `b_forum_filter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DICTIONARY_ID` int(11) DEFAULT NULL,
  `WORDS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATTERN` text COLLATE utf8_unicode_ci,
  `REPLACEMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `USE_IT` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PATTERN_CREATE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FORUM_FILTER_2` (`USE_IT`),
  KEY `IX_B_FORUM_FILTER_3` (`PATTERN_CREATE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=152 ;

--
-- Dumping data for table `b_forum_filter`
--

INSERT INTO `b_forum_filter` (`ID`, `DICTIONARY_ID`, `WORDS`, `PATTERN`, `REPLACEMENT`, `DESCRIPTION`, `USE_IT`, `PATTERN_CREATE`) VALUES
(1, 1, '*пизд*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])([^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*([ПпPp]+)([ИиIi]+)([ЗзZz3]+)([ДдDd]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(2, 1, '*пизж*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])([^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*([ПпPp]+)([ИиIi]+)([ЗзZz3]+)([ЖжGg]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(3, 1, '*сра%', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])([^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*([СсCc]+)([РрPpRr]+)([АаAa]+)([[Цц]+([Аа]+|[Оо]+)]+|[[Тт]+([Ьь]+|)[Сс]+[Яя]+]+))(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(4, 1, 'анобляд*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([АаAa]+)([НнNn]+)([ОоOo]+)([БбBb]+)([ЛлLl]+)([Яя]+)([ДдDd]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(5, 1, 'взъеб*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвVv]+)([ЗзZz3]+)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(6, 1, 'бля', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([БбBb]+)([ЛлLl]+)([Яя]+))(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(7, 1, 'долбоеб*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ДдDd]+)([ОоOo]+)([ЛлLl]+)([БбBb]+)([ОоOo]+)([ЁёЕеEe]+)([БбBb]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(8, 1, 'дуроеб*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ДдDd]+)([УуUu]+)([РрPpRr]+)([ОоOo]+)([ЁёЕеEe]+)([БбBb]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(9, 1, 'еби', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ЁёЕеEe]+)([БбBb]+)([ИиIi]+))(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(10, 1, 'ебисти*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ЁёЕеEe]+)([БбBb]+)([ИиIi]+)([СсCc]+)([ТтTt]+)([ИиIi]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(11, 1, 'ебическ*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ЁёЕеEe]+)([БбBb]+)([ИиIi]+)([Чч]+)([ЁёЕеEe]+)([СсCc]+)([КкKk]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(12, 1, 'еблив*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ЁёЕеEe]+)([БбBb]+)([ЛлLl]+)([ИиIi]+)([ВвVv]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(13, 1, 'ебло*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ЁёЕеEe]+)([БбBb]+)([ЛлLl]+)([ОоOo]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(14, 1, 'еблыс*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ЁёЕеEe]+)([БбBb]+)([ЛлLl]+)([Ыы]+)([СсCc]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(15, 1, 'ебля', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ЁёЕеEe]+)([БбBb]+)([ЛлLl]+)([Яя]+))(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(16, 1, 'ебс', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ЁёЕеEe]+)([БбBb]+)([СсCc]+))(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(17, 1, 'ебукент*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ЁёЕеEe]+)([БбBb]+)([УуUu]+)([КкKk]+)([ЁёЕеEe]+)([НнNn]+)([ТтTt]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(18, 1, 'ебурген*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ЁёЕеEe]+)([БбBb]+)([УуUu]+)([РрPpRr]+)([Гг]+)([ЁёЕеEe]+)([НнNn]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(19, 1, 'коноебит*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([КкKk]+)([ОоOo]+)([НнNn]+)([ОоOo]+)([ЁёЕеEe]+)([БбBb]+)([ИиIi]+)([ТтTt]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(20, 1, 'мозгоеб*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([МмMm]+)([ОоOo]+)([ЗзZz3]+)([Гг]+)([ОоOo]+)([ЁёЕеEe]+)([БбBb]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(21, 1, 'мудоеб*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([МмMm]+)([УуUu]+)([ДдDd]+)([ОоOo]+)([ЁёЕеEe]+)([БбBb]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(22, 1, 'однохуйствен*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ОоOo]+)([ДдDd]+)([НнNn]+)([ОоOo]+)([ХхXx]+)([УуUu]+)([ЙйИиYy]+)([СсCc]+)([ТтTt]+)([ВвVv]+)([ЁёЕеEe]+)([НнNn]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(23, 1, 'охуе*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ОоOo]+)([ХхXx]+)([УуUu]+)([ЁёЕеEe]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(24, 1, 'охуи*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ОоOo]+)([ХхXx]+)([УуUu]+)([ИиIi]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(25, 1, 'охуя*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ОоOo]+)([ХхXx]+)([УуUu]+)([Яя]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(26, 1, 'разъеба*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([РрPpRr]+)([АаAa]+)([ЗзZz3]+)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)([АаAa]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(27, 1, 'распиздон*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([РрPpRr]+)([АаAa]+)([СсCc]+)([ПпPp]+)([ИиIi]+)([ЗзZz3]+)([ДдDd]+)([ОоOo]+)([НнNn]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(28, 1, 'расхуюж*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([РрPpRr]+)([АаAa]+)([СсCc]+)([ХхXx]+)([УуUu]+)([Юю]+|[[Йй]+[Оо]+]+)([ЖжGg]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(29, 1, 'худоебин*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ХхXx]+)([УуUu]+)([ДдDd]+)([ОоOo]+)([ЁёЕеEe]+)([БбBb]+)([ИиIi]+)([НнNn]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(30, 1, 'хуе', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ХхXx]+)([УуUu]+)([ЁёЕеEe]+))(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(31, 1, 'хуебрат*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ХхXx]+)([УуUu]+)([ЁёЕеEe]+)([БбBb]+)([РрPpRr]+)([АаAa]+)([ТтTt]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(32, 1, 'хуеглот*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ХхXx]+)([УуUu]+)([ЁёЕеEe]+)([Гг]+)([ЛлLl]+)([ОоOo]+)([ТтTt]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(33, 1, 'хуеплёт*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ХхXx]+)([УуUu]+)([ЁёЕеEe]+)([ПпPp]+)([ЛлLl]+)([ЁёЕеEe]+|[[Йй]+[Оо]+]+)([ТтTt]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(34, 1, 'хует*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ХхXx]+)([УуUu]+)([ЁёЕеEe]+)([ТтTt]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(35, 1, 'хуила', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ХхXx]+)([УуUu]+)([ИиIi]+)([ЛлLl]+)([АаAa]+))(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(36, 1, 'хул?', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ХхXx]+)([УуUu]+)([ЛлLl]+).?)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(37, 1, 'хуя', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ХхXx]+)([УуUu]+)([Яя]+))(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(38, 1, '^бляд*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([БбBb]+)([ЛлLl]+)([Яя]+)([ДдDd]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(39, 1, '^пидор*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ПпPp]+)([ИиIi]+)([ДдDd]+)([ОоOo]+)([РрPpRr]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(40, 1, '^хуев*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ХхXx]+)([УуUu]+)([ЁёЕеEe]+)([ВвVv]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(41, 1, '^хуем*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ХхXx]+)([УуUu]+)([ЁёЕеEe]+)([МмMm]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(42, 1, '^хуй*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ХхXx]+)([УуUu]+)([ЙйИиYy]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(43, 1, '^хуяк*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ХхXx]+)([УуUu]+)([Яя]+)([КкKk]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(44, 1, '^хуям*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ХхXx]+)([УуUu]+)([Яя]+)([МмMm]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(45, 1, '^хуяр*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ХхXx]+)([УуUu]+)([Яя]+)([РрPpRr]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(46, 1, '^хуяч*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ХхXx]+)([УуUu]+)([Яя]+)([Чч]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(47, 1, '^ъебал*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)([АаAa]+)([ЛлLl]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(48, 1, '^ъебан*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)([АаAa]+)([НнNn]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(49, 1, '^ъебар*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)([АаAa]+)([РрPpRr]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(50, 1, '^ъебат*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)([АаAa]+)([ТтTt]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(51, 1, '^ъебен*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)([ЁёЕеEe]+)([НнNn]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(52, 1, '^ъеби', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)([ИиIi]+))(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(53, 1, '^ъебис*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)([ИиIi]+)([СсCc]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(54, 1, '^ъебит*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)([ИиIi]+)([ТтTt]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(55, 1, '^ъёбля*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+|[[Йй]+[Оо]+]+)([БбBb]+)([ЛлLl]+)([Яя]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(56, 1, '^ъёбну*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+|[[Йй]+[Оо]+]+)([БбBb]+)([НнNn]+)([УуUu]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(57, 1, '^ъебу', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)([УуUu]+))(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(58, 1, '^ъебуч*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)([УуUu]+)([Чч]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(59, 1, '^ъебыв*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(([ВвЗзСс]+|[ВвЫы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)+|)([ЪъЬь"'']+|)([ЁёЕеEe]+)([БбBb]+)([Ыы]+)([ВвVv]+)[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(60, 1, '/(?<=[s.,;:!?-#*|[]()])(?![Вв][ЕеЁё][Бб])(([ВвЗзСс]+|[Ввы]+|[ДдОо]+|[ЗзАа]+|[ИиЗзСс]+|[НнАа]+|[НнЕе]+|[ОоТт]+|([Пп]*[Ее]+[Рр]+[Ее]+)|)([ЬьЪъ]+|)([ЁёЕеEe]+|[Йй]+[Оо]+|[Yy]+[Oo]+)([BbБб]+))(?=[s.,;:!?-#*|[]()])/is', '', '', '', 'Y', 'PTTRN'),
(61, 3, 'angry', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(a+n+g+r+y+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(62, 3, 'ass', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(a+s+s+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(63, 3, 'asshole', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(a+s+s+h+o+l+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(64, 3, 'banger', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+a+n+g+e+r+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(65, 3, 'bastard', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+a+s+t+a+r+d+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(66, 3, 'batter', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+a+t+t+e+r+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(67, 3, 'bicho', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+i+c+h+o+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(68, 3, 'bisexual', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+i+s+e+x+u+a+l+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(69, 3, 'bitch', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+i+t+c+h+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(70, 3, 'blumpkin', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+l+u+m+p+k+i+n+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(71, 3, 'booger', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+o+o+g+e+r+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(72, 3, 'bugger*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+u+g+g+e+r+[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(73, 3, 'bukakke', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+u+k+a+k+k+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(74, 3, 'bull', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+u+l+l+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(75, 3, 'bulldyke', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+u+l+l+d+y+k+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(76, 3, 'bullshit', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+u+l+l+s+h+i+t+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(77, 3, 'bunny', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+u+n+n+y+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(78, 3, 'bunnyfuck', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(b+u+n+n+y+f+u+c+k+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(79, 3, 'chocha', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+h+o+c+h+a+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(80, 3, 'chode', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+h+o+d+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(81, 3, 'clap', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+l+a+p+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(82, 3, 'coconuts', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+o+c+o+n+u+t+s+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(83, 3, 'cohones', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+o+h+o+n+e+s+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(84, 3, 'cojones', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+o+j+o+n+e+s+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(85, 3, 'coon', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+o+o+n+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(86, 3, 'cootch', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+o+o+t+c+h+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(87, 3, 'cooter', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+o+o+t+e+r+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(88, 3, 'cornhole', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+o+r+n+h+o+l+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(89, 3, 'cracka', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+r+a+c+k+a+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(90, 3, 'crap', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+r+a+p+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(91, 3, 'cum', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+u+m+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(92, 3, 'cunnilingus', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+u+n+n+i+l+i+n+g+u+s+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(93, 3, 'cunt*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(c+u+n+t+[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(94, 3, 'damn*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(d+a+m+n+[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(95, 3, 'dark*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(d+a+r+k+[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(96, 3, 'dick', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(d+i+c+k+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(97, 3, 'dickhead', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(d+i+c+k+h+e+a+d+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(98, 3, 'diddle', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(d+i+d+d+l+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(99, 3, 'dildo', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(d+i+l+d+o+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(100, 3, 'dilhole', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(d+i+l+h+o+l+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(101, 3, 'dingleberry', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(d+i+n+g+l+e+b+e+r+r+y+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(102, 3, 'doodle', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(d+o+o+d+l+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(103, 3, 'dork', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(d+o+r+k+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(104, 3, 'dumpster', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(d+u+m+p+s+t+e+r+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(105, 3, 'faggot', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(f+a+g+g+o+t+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(106, 3, 'fart', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(f+a+r+t+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(107, 3, 'frig', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(f+r+i+g+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(108, 3, 'fuck*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(f+u+c+k+[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(109, 3, 'fucker', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(f+u+c+k+e+r+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(110, 3, 'giz', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(g+i+z+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(111, 3, 'goatse', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(g+o+a+t+s+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(112, 3, 'gook', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(g+o+o+k+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(113, 3, 'gringo', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(g+r+i+n+g+o+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(114, 3, 'hobo', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(h+o+b+o+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(115, 3, 'honky', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(h+o+n+k+y+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(116, 3, 'jackass', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(j+a+c+k+a+s+s+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(117, 3, 'jackoff', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(j+a+c+k+o+f+f+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(118, 3, 'jerkoff', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(j+e+r+k+o+f+f+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(119, 3, 'jiggaboo', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(j+i+g+g+a+b+o+o+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(120, 3, 'jizz', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(j+i+z+z+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(121, 3, 'kike', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(k+i+k+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(122, 3, 'mayo', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(m+a+y+o+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(123, 3, 'moose', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(m+o+o+s+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(124, 3, 'nigg*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(n+i+g+g+[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(125, 3, 'paki', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(p+a+k+i+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(126, 3, 'pecker', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(p+e+c+k+e+r+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(127, 3, 'piss', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(p+i+s+s+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(128, 3, 'poonanni', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(p+o+o+n+a+n+n+i+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(129, 3, 'poontang', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(p+o+o+n+t+a+n+g+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(130, 3, 'prick', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(p+r+i+c+k+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(131, 3, 'punch', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(p+u+n+c+h+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(132, 3, 'queef', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(q+u+e+e+f+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(133, 3, 'rogue', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(r+o+g+u+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(134, 3, 'sanchez', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(s+a+n+c+h+e+z+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(135, 3, 'schlong', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(s+c+h+l+o+n+g+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(136, 3, 'shit', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(s+h+i+t+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(137, 3, 'skank', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(s+k+a+n+k+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(138, 3, 'spaz', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(s+p+a+z+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(139, 3, 'spic', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(s+p+i+c+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(140, 3, 'teabag*', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(t+e+a+b+a+g+[^\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)]*)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(141, 3, 'tits', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(t+i+t+s+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(142, 3, 'twat', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(t+w+a+t+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(143, 3, 'twot', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(t+w+o+t+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(144, 3, 'vart', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(v+a+r+t+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(145, 3, 'wanker', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(w+a+n+k+e+r+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(146, 3, 'waste', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(w+a+s+t+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(147, 3, 'wetback', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(w+e+t+b+a+c+k+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(148, 3, 'whore', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(w+h+o+r+e+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(149, 3, 'wigger', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(w+i+g+g+e+r+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(150, 3, 'wog', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(w+o+g+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL'),
(151, 3, 'wop', '/(?<=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])(w+o+p+)(?=[\\s.,;:!?\\#\\-\\*\\|\\[\\]\\(\\)])/isu', '', '', 'Y', 'TRNSL');

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_group`
--

CREATE TABLE IF NOT EXISTS `b_forum_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SORT` int(11) NOT NULL DEFAULT '150',
  `PARENT_ID` int(11) DEFAULT NULL,
  `LEFT_MARGIN` int(11) DEFAULT NULL,
  `RIGHT_MARGIN` int(11) DEFAULT NULL,
  `DEPTH_LEVEL` int(11) DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_group_lang`
--

CREATE TABLE IF NOT EXISTS `b_forum_group_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORUM_GROUP_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_FORUM_GROUP` (`FORUM_GROUP_ID`,`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_letter`
--

CREATE TABLE IF NOT EXISTS `b_forum_letter` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DICTIONARY_ID` int(11) DEFAULT '0',
  `LETTER` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REPLACEMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=38 ;

--
-- Dumping data for table `b_forum_letter`
--

INSERT INTO `b_forum_letter` (`ID`, `DICTIONARY_ID`, `LETTER`, `REPLACEMENT`) VALUES
(1, 2, 'а', 'АаAa'),
(2, 2, 'б', 'БбBb'),
(3, 2, 'в', 'ВвVv'),
(4, 2, 'г', 'Гг'),
(5, 2, 'д', 'ДдDd'),
(6, 2, 'е', 'ЁёЕеEe'),
(7, 2, 'ё', 'ЁёЕеEe, [Йй]+[Оо]+'),
(8, 2, 'ж', 'ЖжGg'),
(9, 2, 'з', 'ЗзZz3'),
(10, 2, 'и', 'ИиIi'),
(11, 2, 'й', 'ЙйИиYy'),
(12, 2, 'к', 'КкKk'),
(13, 2, 'л', 'ЛлLl'),
(14, 2, 'м', 'МмMm'),
(15, 2, 'н', 'НнNn'),
(16, 2, 'о', 'ОоOo'),
(17, 2, 'п', 'ПпPp'),
(18, 2, 'р', 'РрPpRr'),
(19, 2, 'с', 'СсCc'),
(20, 2, 'т', 'ТтTt'),
(21, 2, 'у', 'УуUu'),
(22, 2, 'ф', 'ФфFf'),
(23, 2, 'х', 'ХхXx'),
(24, 2, 'ц', 'ЦцCc'),
(25, 2, 'ч', 'Чч'),
(26, 2, 'ш', 'Шш'),
(27, 2, 'щ', 'Щщ'),
(28, 2, 'ь', 'ЪъЬь"'','),
(29, 2, 'ы', 'Ыы'),
(30, 2, 'ъ', 'ЪъЬь"'','),
(31, 2, 'э', 'Ээ'),
(32, 2, 'ю', 'Юю, [Йй]+[Оо]+'),
(33, 2, 'я', 'Яя'),
(34, 2, '%', '[Цц]+([Аа]+|[Оо]+), [Тт]+([Ьь]+|)[Сс]+[Яя]+'),
(35, 2, '^', ',ВвЗзСс,ВвЫы,ДдОо,ЗзАа,ИиЗзСс,НнАа,НнЕе,ОоТт,([Пп]*[Ее]+[Рр]+[Ее]+)'),
(36, 2, 'тся', '%'),
(37, 2, 'ться', '%');

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_message`
--

CREATE TABLE IF NOT EXISTS `b_forum_message` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FORUM_ID` int(10) NOT NULL,
  `TOPIC_ID` bigint(20) NOT NULL,
  `USE_SMILES` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NEW_TOPIC` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `APPROVED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SOURCE_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'WEB',
  `POST_DATE` datetime NOT NULL,
  `POST_MESSAGE` text COLLATE utf8_unicode_ci,
  `POST_MESSAGE_HTML` text COLLATE utf8_unicode_ci,
  `POST_MESSAGE_FILTER` text COLLATE utf8_unicode_ci,
  `POST_MESSAGE_CHECK` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ATTACH_IMG` int(11) DEFAULT NULL,
  `PARAM1` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PARAM2` int(11) DEFAULT NULL,
  `AUTHOR_ID` int(10) DEFAULT NULL,
  `AUTHOR_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUTHOR_EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUTHOR_IP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUTHOR_REAL_IP` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GUEST_ID` int(10) DEFAULT NULL,
  `EDITOR_ID` int(10) DEFAULT NULL,
  `EDITOR_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDITOR_EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDIT_REASON` text COLLATE utf8_unicode_ci,
  `EDIT_DATE` datetime DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HTML` text COLLATE utf8_unicode_ci,
  `MAIL_HEADER` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_FORUM_MESSAGE_FORUM` (`FORUM_ID`,`APPROVED`),
  KEY `IX_FORUM_MESSAGE_TOPIC` (`TOPIC_ID`,`APPROVED`,`ID`),
  KEY `IX_FORUM_MESSAGE_AUTHOR` (`AUTHOR_ID`,`APPROVED`,`FORUM_ID`,`ID`),
  KEY `IX_FORUM_MESSAGE_APPROVED` (`APPROVED`),
  KEY `IX_FORUM_MESSAGE_PARAM2` (`PARAM2`),
  KEY `IX_FORUM_MESSAGE_XML_ID` (`XML_ID`),
  KEY `IX_FORUM_MESSAGE_DATE_AUTHOR_ID` (`POST_DATE`,`AUTHOR_ID`),
  KEY `IX_FORUM_MESSAGE_AUTHOR_TOPIC_ID` (`AUTHOR_ID`,`TOPIC_ID`,`ID`),
  KEY `IX_FORUM_MESSAGE_AUTHOR_FORUM_ID` (`AUTHOR_ID`,`FORUM_ID`,`ID`,`APPROVED`,`TOPIC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_perms`
--

CREATE TABLE IF NOT EXISTS `b_forum_perms` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORUM_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `PERMISSION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'M',
  PRIMARY KEY (`ID`),
  KEY `IX_FORUM_PERMS_FORUM` (`FORUM_ID`,`GROUP_ID`),
  KEY `IX_FORUM_PERMS_GROUP` (`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_pm_folder`
--

CREATE TABLE IF NOT EXISTS `b_forum_pm_folder` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `SORT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FORUM_PM_FOLDER_USER_IST` (`USER_ID`,`ID`,`SORT`,`TITLE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `b_forum_pm_folder`
--

INSERT INTO `b_forum_pm_folder` (`ID`, `TITLE`, `USER_ID`, `SORT`) VALUES
(1, 'SYSTEM_FOLDER_1', 0, 0),
(2, 'SYSTEM_FOLDER_2', 0, 0),
(3, 'SYSTEM_FOLDER_3', 0, 0),
(4, 'SYSTEM_FOLDER_4', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_points`
--

CREATE TABLE IF NOT EXISTS `b_forum_points` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MIN_POINTS` int(11) NOT NULL,
  `CODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VOTES` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_FORUM_P_MP` (`MIN_POINTS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_points2post`
--

CREATE TABLE IF NOT EXISTS `b_forum_points2post` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MIN_NUM_POSTS` int(11) NOT NULL,
  `POINTS_PER_POST` decimal(18,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_FORUM_P2P_MNP` (`MIN_NUM_POSTS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_points_lang`
--

CREATE TABLE IF NOT EXISTS `b_forum_points_lang` (
  `POINTS_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`POINTS_ID`,`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_private_message`
--

CREATE TABLE IF NOT EXISTS `b_forum_private_message` (
  `ID` bigint(10) NOT NULL AUTO_INCREMENT,
  `AUTHOR_ID` int(11) DEFAULT '0',
  `RECIPIENT_ID` int(11) DEFAULT '0',
  `POST_DATE` datetime NOT NULL,
  `POST_SUBJ` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `POST_MESSAGE` text COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `FOLDER_ID` int(11) NOT NULL,
  `IS_READ` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `REQUEST_IS_READ` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `USE_SMILES` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FORUM_PM_USER` (`USER_ID`),
  KEY `IX_B_FORUM_PM_AFR` (`AUTHOR_ID`,`FOLDER_ID`,`IS_READ`),
  KEY `IX_B_FORUM_PM_UFP` (`USER_ID`,`FOLDER_ID`,`POST_DATE`),
  KEY `IX_B_FORUM_PM_POST_DATE` (`POST_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_rank`
--

CREATE TABLE IF NOT EXISTS `b_forum_rank` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MIN_NUM_POSTS` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_rank_lang`
--

CREATE TABLE IF NOT EXISTS `b_forum_rank_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RANK_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_FORUM_RANK` (`RANK_ID`,`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_smile`
--

CREATE TABLE IF NOT EXISTS `b_forum_smile` (
  `ID` smallint(3) NOT NULL AUTO_INCREMENT,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `TYPING` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CLICKABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(10) NOT NULL DEFAULT '150',
  `IMAGE_WIDTH` int(11) NOT NULL DEFAULT '0',
  `IMAGE_HEIGHT` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=90 ;

--
-- Dumping data for table `b_forum_smile`
--

INSERT INTO `b_forum_smile` (`ID`, `TYPE`, `TYPING`, `IMAGE`, `DESCRIPTION`, `CLICKABLE`, `SORT`, `IMAGE_WIDTH`, `IMAGE_HEIGHT`) VALUES
(68, 'S', ':D :-D', 'icon_biggrin.png', 'FICON_BIGGRIN', 'N', 30, 16, 16),
(69, 'S', ':) :-)', 'icon_smile.png', 'FICON_SMILE', 'N', 10, 16, 16),
(70, 'S', ':( :-(', 'icon_sad.png', 'FICON_SAD', 'N', 50, 16, 16),
(71, 'S', ':o :-o :shock:', 'icon_eek.png', 'FICON_EEK', 'N', 90, 16, 16),
(72, 'S', '8) 8-)', 'icon_cool.png', 'FICON_COOL', 'N', 40, 16, 16),
(73, 'S', ':{} :-{}', 'icon_kiss.png', 'FICON_KISS', 'N', 110, 16, 16),
(74, 'S', ':oops:', 'icon_redface.png', 'FICON_REDFACE', 'Y', 100, 16, 16),
(75, 'S', ':cry: :~(', 'icon_cry.png', 'FICON_CRY', 'Y', 70, 16, 16),
(76, 'S', ':evil: >:-<', 'icon_evil.png', 'FICON_EVIL', 'N', 80, 16, 16),
(77, 'S', ';) ;-)', 'icon_wink.png', 'FICON_WINK', 'N', 20, 16, 16),
(78, 'S', ':!:', 'icon_exclaim.png', 'FICON_EXCLAIM', 'Y', 130, 16, 16),
(79, 'S', ':?:', 'icon_question.png', 'FICON_QUESTION', 'Y', 120, 16, 16),
(80, 'S', ':idea:', 'icon_idea.png', 'FICON_IDEA', 'Y', 140, 16, 16),
(81, 'S', ':| :-|', 'icon_neutral.png', 'FICON_NEUTRAL', 'N', 60, 16, 16),
(82, 'S', ':\\  :-\\  :/ :-/', 'icon_confuse.png', 'FICON_CONFUSE', 'N', 60, 16, 16),
(83, 'I', '', 'icon1.gif', 'FICON_NOTE', 'Y', 150, 15, 15),
(84, 'I', '', 'icon2.gif', 'FICON_DIRRECTION', 'Y', 150, 15, 15),
(85, 'I', '', 'icon3.gif', 'FICON_IDEA2', 'Y', 150, 15, 15),
(86, 'I', '', 'icon4.gif', 'FICON_ATTANSION', 'Y', 150, 15, 15),
(87, 'I', '', 'icon5.gif', 'FICON_QUESTION2', 'Y', 150, 15, 15),
(88, 'I', '', 'icon6.gif', 'FICON_BAD', 'Y', 150, 15, 15),
(89, 'I', '', 'icon7.gif', 'FICON_GOOD', 'Y', 150, 15, 15);

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_smile_lang`
--

CREATE TABLE IF NOT EXISTS `b_forum_smile_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SMILE_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_FORUM_SMILE_K` (`SMILE_ID`,`LID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=45 ;

--
-- Dumping data for table `b_forum_smile_lang`
--

INSERT INTO `b_forum_smile_lang` (`ID`, `SMILE_ID`, `LID`, `NAME`) VALUES
(1, 68, 'ru', 'Широкая улыбка'),
(2, 69, 'ru', 'С улыбкой'),
(3, 70, 'ru', 'Печально'),
(4, 71, 'ru', 'Удивленно'),
(5, 72, 'ru', 'Здорово'),
(6, 73, 'ru', 'Поцелуй'),
(7, 74, 'ru', 'Смущенно'),
(8, 75, 'ru', 'Очень грустно'),
(9, 76, 'ru', 'Со злостью'),
(10, 77, 'ru', 'Шутливо'),
(11, 78, 'ru', 'Восклицание'),
(12, 79, 'ru', 'Вопрос'),
(13, 80, 'ru', 'Идея'),
(14, 81, 'ru', 'Нет слов'),
(15, 82, 'ru', 'Озадаченно'),
(16, 83, 'ru', 'Заметка'),
(17, 84, 'ru', 'Направление'),
(18, 85, 'ru', 'Идея'),
(19, 86, 'ru', 'Внимание'),
(20, 87, 'ru', 'Вопрос'),
(21, 88, 'ru', 'Плохо'),
(22, 89, 'ru', 'Хорошо'),
(23, 68, 'en', 'Big grin'),
(24, 69, 'en', 'Smile'),
(25, 70, 'en', 'Sad'),
(26, 71, 'en', 'Surprised'),
(27, 72, 'en', 'Cool'),
(28, 73, 'en', 'Kiss'),
(29, 74, 'en', 'Embaressed'),
(30, 75, 'en', 'Crying'),
(31, 79, 'en', 'Question'),
(32, 78, 'en', 'Exclamation'),
(33, 77, 'en', 'Wink'),
(34, 76, 'en', 'Angry'),
(35, 80, 'en', 'Idea'),
(36, 81, 'en', 'Speechless'),
(37, 82, 'en', 'Skeptical'),
(38, 83, 'en', 'Note'),
(39, 84, 'en', 'Direction'),
(40, 85, 'en', 'Idea'),
(41, 86, 'en', 'Attention'),
(42, 87, 'en', 'Question'),
(43, 88, 'en', 'Thumbs Down'),
(44, 89, 'en', 'Thumbs Up');

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_stat`
--

CREATE TABLE IF NOT EXISTS `b_forum_stat` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(10) DEFAULT NULL,
  `IP_ADDRESS` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PHPSESSID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_VISIT` datetime DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORUM_ID` smallint(5) NOT NULL DEFAULT '0',
  `TOPIC_ID` int(10) DEFAULT NULL,
  `SHOW_NAME` varchar(101) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FORUM_STAT_SITE_ID` (`SITE_ID`,`LAST_VISIT`),
  KEY `IX_B_FORUM_STAT_TOPIC_ID` (`TOPIC_ID`,`LAST_VISIT`),
  KEY `IX_B_FORUM_STAT_FORUM_ID` (`FORUM_ID`,`LAST_VISIT`),
  KEY `IX_B_FORUM_STAT_PHPSESSID` (`PHPSESSID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_subscribe`
--

CREATE TABLE IF NOT EXISTS `b_forum_subscribe` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(10) NOT NULL,
  `FORUM_ID` int(10) NOT NULL,
  `TOPIC_ID` int(10) DEFAULT NULL,
  `START_DATE` datetime NOT NULL,
  `LAST_SEND` int(10) DEFAULT NULL,
  `NEW_TOPIC_ONLY` char(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ru',
  `SOCNET_GROUP_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_FORUM_SUBSCRIBE_USER` (`USER_ID`,`FORUM_ID`,`TOPIC_ID`,`SOCNET_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_topic`
--

CREATE TABLE IF NOT EXISTS `b_forum_topic` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FORUM_ID` int(10) NOT NULL,
  `TOPIC_ID` bigint(20) DEFAULT NULL,
  `TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TITLE_SEO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAGS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ICON_ID` tinyint(2) DEFAULT NULL,
  `STATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `APPROVED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(10) NOT NULL DEFAULT '150',
  `VIEWS` int(10) NOT NULL DEFAULT '0',
  `USER_START_ID` int(10) DEFAULT NULL,
  `USER_START_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `START_DATE` datetime NOT NULL,
  `POSTS` int(10) NOT NULL DEFAULT '0',
  `LAST_POSTER_ID` int(10) DEFAULT NULL,
  `LAST_POSTER_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LAST_POST_DATE` datetime NOT NULL,
  `LAST_MESSAGE_ID` bigint(20) DEFAULT NULL,
  `POSTS_UNAPPROVED` int(11) DEFAULT '0',
  `ABS_LAST_POSTER_ID` int(10) DEFAULT NULL,
  `ABS_LAST_POSTER_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ABS_LAST_POST_DATE` datetime DEFAULT NULL,
  `ABS_LAST_MESSAGE_ID` bigint(20) DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HTML` text COLLATE utf8_unicode_ci,
  `SOCNET_GROUP_ID` int(10) DEFAULT NULL,
  `OWNER_ID` int(10) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_FORUM_TOPIC_FORUM` (`FORUM_ID`,`APPROVED`),
  KEY `IX_FORUM_TOPIC_APPROVED` (`APPROVED`),
  KEY `IX_FORUM_TOPIC_ABS_L_POST_DATE` (`ABS_LAST_POST_DATE`),
  KEY `IX_FORUM_TOPIC_LAST_POST_DATE` (`LAST_POST_DATE`),
  KEY `IX_FORUM_TOPIC_USER_START_ID` (`USER_START_ID`),
  KEY `IX_FORUM_TOPIC_DATE_USER_START_ID` (`START_DATE`,`USER_START_ID`),
  KEY `IX_FORUM_TOPIC_XML_ID` (`XML_ID`),
  KEY `IX_FORUM_TOPIC_TITLE_SEO` (`FORUM_ID`,`TITLE_SEO`),
  KEY `IX_FORUM_TOPIC_TITLE_SEO2` (`TITLE_SEO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_user`
--

CREATE TABLE IF NOT EXISTS `b_forum_user` (
  `ID` bigint(10) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(10) NOT NULL,
  `ALIAS` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IP_ADDRESS` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AVATAR` int(10) DEFAULT NULL,
  `NUM_POSTS` int(10) DEFAULT '0',
  `INTERESTS` text COLLATE utf8_unicode_ci,
  `LAST_POST` int(10) DEFAULT NULL,
  `ALLOW_POST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `LAST_VISIT` datetime NOT NULL,
  `DATE_REG` date NOT NULL,
  `REAL_IP_ADDRESS` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SIGNATURE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SHOW_NAME` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `RANK_ID` int(11) DEFAULT NULL,
  `POINTS` int(11) NOT NULL DEFAULT '0',
  `HIDE_FROM_ONLINE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SUBSC_GROUP_MESSAGE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SUBSC_GET_MY_MESSAGE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_FORUM_USER_USER6` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_user_forum`
--

CREATE TABLE IF NOT EXISTS `b_forum_user_forum` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) DEFAULT NULL,
  `FORUM_ID` int(11) DEFAULT NULL,
  `LAST_VISIT` datetime DEFAULT NULL,
  `MAIN_LAST_VISIT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_FORUM_USER_FORUM_ID1` (`USER_ID`,`FORUM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_user_points`
--

CREATE TABLE IF NOT EXISTS `b_forum_user_points` (
  `FROM_USER_ID` int(11) NOT NULL,
  `TO_USER_ID` int(11) NOT NULL,
  `POINTS` int(11) NOT NULL DEFAULT '0',
  `DATE_UPDATE` datetime DEFAULT NULL,
  PRIMARY KEY (`FROM_USER_ID`,`TO_USER_ID`),
  KEY `IX_B_FORUM_USER_POINTS_TO_USER` (`TO_USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_forum_user_topic`
--

CREATE TABLE IF NOT EXISTS `b_forum_user_topic` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TOPIC_ID` int(11) NOT NULL DEFAULT '0',
  `USER_ID` int(11) NOT NULL DEFAULT '0',
  `FORUM_ID` int(11) DEFAULT NULL,
  `LAST_VISIT` datetime DEFAULT NULL,
  PRIMARY KEY (`TOPIC_ID`,`USER_ID`),
  KEY `ID` (`ID`),
  KEY `IX_B_FORUM_USER_FORUM_ID2` (`USER_ID`,`FORUM_ID`,`TOPIC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_group`
--

CREATE TABLE IF NOT EXISTS `b_group` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `C_SORT` int(18) NOT NULL DEFAULT '100',
  `ANONYMOUS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECURITY_POLICY` text COLLATE utf8_unicode_ci,
  `STRING_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `b_group`
--

INSERT INTO `b_group` (`ID`, `TIMESTAMP_X`, `ACTIVE`, `C_SORT`, `ANONYMOUS`, `NAME`, `DESCRIPTION`, `SECURITY_POLICY`, `STRING_ID`) VALUES
(1, '2015-04-22 17:24:54', 'Y', 1, 'N', 'Администраторы', 'Полный доступ к управлению сайтом.', NULL, NULL),
(2, '2015-04-22 17:24:54', 'Y', 2, 'Y', 'Все пользователи (в том числе неавторизованные)', 'Все пользователи, включая неавторизованных.', NULL, NULL),
(3, '2015-04-22 17:24:54', 'Y', 3, 'N', 'Пользователи, имеющие право голосовать за рейтинг', 'В эту группу пользователи добавляются автоматически.', NULL, 'RATING_VOTE'),
(4, '2015-04-22 17:24:54', 'Y', 4, 'N', 'Пользователи имеющие право голосовать за авторитет', 'В эту группу пользователи добавляются автоматически.', NULL, 'RATING_VOTE_AUTHORITY'),
(5, '2015-04-22 17:43:29', 'Y', 300, 'N', 'Контент-редакторы', NULL, NULL, 'content_editor');

-- --------------------------------------------------------

--
-- Table structure for table `b_group_collection_task`
--

CREATE TABLE IF NOT EXISTS `b_group_collection_task` (
  `GROUP_ID` int(11) NOT NULL,
  `TASK_ID` int(11) NOT NULL,
  `COLLECTION_ID` int(11) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`TASK_ID`,`COLLECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_group_subordinate`
--

CREATE TABLE IF NOT EXISTS `b_group_subordinate` (
  `ID` int(18) NOT NULL,
  `AR_SUBGROUP_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_group_task`
--

CREATE TABLE IF NOT EXISTS `b_group_task` (
  `GROUP_ID` int(18) NOT NULL,
  `TASK_ID` int(18) NOT NULL,
  `EXTERNAL_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`GROUP_ID`,`TASK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_group_task`
--

INSERT INTO `b_group_task` (`GROUP_ID`, `TASK_ID`, `EXTERNAL_ID`) VALUES
(5, 50, '');

-- --------------------------------------------------------

--
-- Table structure for table `b_hlblock_entity`
--

CREATE TABLE IF NOT EXISTS `b_hlblock_entity` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `TABLE_NAME` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_hot_keys`
--

CREATE TABLE IF NOT EXISTS `b_hot_keys` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `KEYS_STRING` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CODE_ID` int(18) NOT NULL,
  `USER_ID` int(18) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_b_hot_keys_co_u` (`CODE_ID`,`USER_ID`),
  KEY `ix_hot_keys_code` (`CODE_ID`),
  KEY `ix_hot_keys_user` (`USER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `b_hot_keys`
--

INSERT INTO `b_hot_keys` (`ID`, `KEYS_STRING`, `CODE_ID`, `USER_ID`) VALUES
(1, 'Ctrl+Alt+85', 139, 0),
(2, 'Ctrl+Alt+80', 17, 0),
(3, 'Ctrl+Alt+70', 120, 0),
(4, 'Ctrl+Alt+68', 117, 0),
(5, 'Ctrl+Alt+81', 3, 0),
(6, 'Ctrl+Alt+75', 106, 0),
(7, 'Ctrl+Alt+79', 133, 0),
(8, 'Ctrl+Alt+70', 121, 0),
(9, 'Ctrl+Alt+69', 118, 0),
(10, 'Ctrl+Shift+83', 87, 0),
(11, 'Ctrl+Shift+88', 88, 0),
(12, 'Ctrl+Shift+76', 89, 0),
(13, 'Ctrl+Alt+85', 139, 1),
(14, 'Ctrl+Alt+80', 17, 1),
(15, 'Ctrl+Alt+70', 120, 1),
(16, 'Ctrl+Alt+68', 117, 1),
(17, 'Ctrl+Alt+81', 3, 1),
(18, 'Ctrl+Alt+75', 106, 1),
(19, 'Ctrl+Alt+79', 133, 1),
(20, 'Ctrl+Alt+70', 121, 1),
(21, 'Ctrl+Alt+69', 118, 1),
(22, 'Ctrl+Shift+83', 87, 1),
(23, 'Ctrl+Shift+88', 88, 1),
(24, 'Ctrl+Shift+76', 89, 1);

-- --------------------------------------------------------

--
-- Table structure for table `b_hot_keys_code`
--

CREATE TABLE IF NOT EXISTS `b_hot_keys_code` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `CLASS_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COMMENTS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TITLE_OBJ` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_CUSTOM` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID`),
  KEY `ix_hot_keys_code_cn` (`CLASS_NAME`),
  KEY `ix_hot_keys_code_url` (`URL`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=140 ;

--
-- Dumping data for table `b_hot_keys_code`
--

INSERT INTO `b_hot_keys_code` (`ID`, `CLASS_NAME`, `CODE`, `NAME`, `COMMENTS`, `TITLE_OBJ`, `URL`, `IS_CUSTOM`) VALUES
(3, 'CAdminTabControl', 'NextTab();', 'HK_DB_CADMINTC', 'HK_DB_CADMINTC_C', 'tab-container', '', 0),
(5, 'btn_new', 'var d=BX (''btn_new''); if (d) location.href = d.href;', 'HK_DB_BUT_ADD', 'HK_DB_BUT_ADD_C', 'btn_new', '', 0),
(6, 'btn_excel', 'var d=BX(''btn_excel''); if (d) location.href = d.href;', 'HK_DB_BUT_EXL', 'HK_DB_BUT_EXL_C', 'btn_excel', '', 0),
(7, 'btn_settings', 'var d=BX(''btn_settings''); if (d) location.href = d.href;', 'HK_DB_BUT_OPT', 'HK_DB_BUT_OPT_C', 'btn_settings', '', 0),
(8, 'btn_list', 'var d=BX(''btn_list''); if (d) location.href = d.href;', 'HK_DB_BUT_LST', 'HK_DB_BUT_LST_C', 'btn_list', '', 0),
(9, 'Edit_Save_Button', 'var d=BX .findChild(document, {attribute: {''name'': ''save''}}, true );  if (d) d.click();', 'HK_DB_BUT_SAVE', 'HK_DB_BUT_SAVE_C', 'Edit_Save_Button', '', 0),
(10, 'btn_delete', 'var d=BX(''btn_delete''); if (d) location.href = d.href;', 'HK_DB_BUT_DEL', 'HK_DB_BUT_DEL_C', 'btn_delete', '', 0),
(12, 'CAdminFilter', 'var d=BX .findChild(document, {attribute: {''name'': ''find''}}, true ); if (d) d.focus();', 'HK_DB_FLT_FND', 'HK_DB_FLT_FND_C', 'find', '', 0),
(13, 'CAdminFilter', 'var d=BX .findChild(document, {attribute: {''name'': ''set_filter''}}, true );  if (d) d.click();', 'HK_DB_FLT_BUT_F', 'HK_DB_FLT_BUT_F_C', 'set_filter', '', 0),
(14, 'CAdminFilter', 'var d=BX .findChild(document, {attribute: {''name'': ''del_filter''}}, true );  if (d) d.click();', 'HK_DB_FLT_BUT_CNL', 'HK_DB_FLT_BUT_CNL_C', 'del_filter', '', 0),
(15, 'bx-panel-admin-button-help-icon-id', 'var d=BX(''bx-panel-admin-button-help-icon-id''); if (d) location.href = d.href;', 'HK_DB_BUT_HLP', 'HK_DB_BUT_HLP_C', 'bx-panel-admin-button-help-icon-id', '', 0),
(17, 'Global', 'BXHotKeys.ShowSettings();', 'HK_DB_SHW_L', 'HK_DB_SHW_L_C', 'bx-panel-hotkeys', '', 0),
(19, 'Edit_Apply_Button', 'var d=BX .findChild(document, {attribute: {''name'': ''apply''}}, true );  if (d) d.click();', 'HK_DB_BUT_APPL', 'HK_DB_BUT_APPL_C', 'Edit_Apply_Button', '', 0),
(20, 'Edit_Cancel_Button', 'var d=BX .findChild(document, {attribute: {''name'': ''cancel''}}, true );  if (d) d.click();', 'HK_DB_BUT_CANCEL', 'HK_DB_BUT_CANCEL_C', 'Edit_Cancel_Button', '', 0),
(54, 'top_panel_org_fav', '', '-=AUTONAME=-', NULL, 'top_panel_org_fav', NULL, 0),
(55, 'top_panel_module_settings', '', '-=AUTONAME=-', NULL, 'top_panel_module_settings', '', 0),
(56, 'top_panel_interface_settings', '', '-=AUTONAME=-', NULL, 'top_panel_interface_settings', '', 0),
(57, 'top_panel_help', '', '-=AUTONAME=-', NULL, 'top_panel_help', '', 0),
(58, 'top_panel_bizproc_tasks', '', '-=AUTONAME=-', NULL, 'top_panel_bizproc_tasks', '', 0),
(59, 'top_panel_add_fav', '', '-=AUTONAME=-', NULL, 'top_panel_add_fav', NULL, 0),
(60, 'top_panel_create_page', '', '-=AUTONAME=-', NULL, 'top_panel_create_page', '', 0),
(62, 'top_panel_create_folder', '', '-=AUTONAME=-', NULL, 'top_panel_create_folder', '', 0),
(63, 'top_panel_edit_page', '', '-=AUTONAME=-', NULL, 'top_panel_edit_page', '', 0),
(64, 'top_panel_page_prop', '', '-=AUTONAME=-', NULL, 'top_panel_page_prop', '', 0),
(65, 'top_panel_edit_page_html', '', '-=AUTONAME=-', NULL, 'top_panel_edit_page_html', '', 0),
(67, 'top_panel_edit_page_php', '', '-=AUTONAME=-', NULL, 'top_panel_edit_page_php', '', 0),
(68, 'top_panel_del_page', '', '-=AUTONAME=-', NULL, 'top_panel_del_page', '', 0),
(69, 'top_panel_folder_prop', '', '-=AUTONAME=-', NULL, 'top_panel_folder_prop', '', 0),
(70, 'top_panel_access_folder_new', '', '-=AUTONAME=-', NULL, 'top_panel_access_folder_new', '', 0),
(71, 'main_top_panel_struct_panel', '', '-=AUTONAME=-', NULL, 'main_top_panel_struct_panel', '', 0),
(72, 'top_panel_cache_page', '', '-=AUTONAME=-', NULL, 'top_panel_cache_page', '', 0),
(73, 'top_panel_cache_comp', '', '-=AUTONAME=-', NULL, 'top_panel_cache_comp', '', 0),
(74, 'top_panel_cache_not', '', '-=AUTONAME=-', NULL, 'top_panel_cache_not', '', 0),
(75, 'top_panel_edit_mode', '', '-=AUTONAME=-', NULL, 'top_panel_edit_mode', '', 0),
(76, 'top_panel_templ_site_css', '', '-=AUTONAME=-', NULL, 'top_panel_templ_site_css', '', 0),
(77, 'top_panel_templ_templ_css', '', '-=AUTONAME=-', NULL, 'top_panel_templ_templ_css', '', 0),
(78, 'top_panel_templ_site', '', '-=AUTONAME=-', NULL, 'top_panel_templ_site', '', 0),
(81, 'top_panel_debug_time', '', '-=AUTONAME=-', NULL, 'top_panel_debug_time', '', 0),
(82, 'top_panel_debug_incl', '', '-=AUTONAME=-', NULL, 'top_panel_debug_incl', '', 0),
(83, 'top_panel_debug_sql', '', '-=AUTONAME=-', NULL, 'top_panel_debug_sql', NULL, 0),
(84, 'top_panel_debug_compr', '', '-=AUTONAME=-', NULL, 'top_panel_debug_compr', '', 0),
(85, 'MTP_SHORT_URI1', '', '-=AUTONAME=-', NULL, 'MTP_SHORT_URI1', '', 0),
(86, 'MTP_SHORT_URI_LIST', '', '-=AUTONAME=-', NULL, 'MTP_SHORT_URI_LIST', '', 0),
(87, 'FMST_PANEL_STICKER_ADD', '', '-=AUTONAME=-', NULL, 'FMST_PANEL_STICKER_ADD', '', 0),
(88, 'FMST_PANEL_STICKERS_SHOW', '', '-=AUTONAME=-', NULL, 'FMST_PANEL_STICKERS_SHOW', '', 0),
(89, 'FMST_PANEL_CUR_STICKER_LIST', '', '-=AUTONAME=-', NULL, 'FMST_PANEL_CUR_STICKER_LIST', '', 0),
(90, 'FMST_PANEL_ALL_STICKER_LIST', '', '-=AUTONAME=-', NULL, 'FMST_PANEL_ALL_STICKER_LIST', '', 0),
(91, 'top_panel_menu', 'var d=BX("bx-panel-menu"); if (d) d.click();', '-=AUTONAME=-', NULL, 'bx-panel-menu', '', 0),
(92, 'top_panel_admin', 'var d=BX(''bx-panel-admin-tab''); if (d) location.href = d.href;', '-=AUTONAME=-', NULL, 'bx-panel-admin-tab', '', 0),
(93, 'admin_panel_site', 'var d=BX(''bx-panel-view-tab''); if (d) location.href = d.href;', '-=AUTONAME=-', NULL, 'bx-panel-view-tab', '', 0),
(94, 'admin_panel_admin', 'var d=BX(''bx-panel-admin-tab''); if (d) location.href = d.href;', '-=AUTONAME=-', NULL, 'bx-panel-admin-tab', '', 0),
(96, 'top_panel_folder_prop_new', '', '-=AUTONAME=-', NULL, 'top_panel_folder_prop_new', '', 0),
(97, 'main_top_panel_structure', '', '-=AUTONAME=-', NULL, 'main_top_panel_structure', '', 0),
(98, 'top_panel_clear_cache', '', '-=AUTONAME=-', NULL, 'top_panel_clear_cache', '', 0),
(99, 'top_panel_templ', '', '-=AUTONAME=-', NULL, 'top_panel_templ', '', 0),
(100, 'top_panel_debug', '', '-=AUTONAME=-', NULL, 'top_panel_debug', '', 0),
(101, 'MTP_SHORT_URI', '', '-=AUTONAME=-', NULL, 'MTP_SHORT_URI', '', 0),
(102, 'FMST_PANEL_STICKERS', '', '-=AUTONAME=-', NULL, 'FMST_PANEL_STICKERS', '', 0),
(103, 'top_panel_settings', '', '-=AUTONAME=-', NULL, 'top_panel_settings', '', 0),
(104, 'top_panel_fav', '', '-=AUTONAME=-', NULL, 'top_panel_fav', '', 0),
(106, 'Global', 'location.href=''/bitrix/admin/hot_keys_list.php?lang=ru'';', 'HK_DB_SHW_HK', '', '', '', 0),
(107, 'top_panel_edit_new', '', '-=AUTONAME=-', NULL, 'top_panel_edit_new', '', 0),
(108, 'FLOW_PANEL_CREATE_WITH_WF', '', '-=AUTONAME=-', NULL, 'FLOW_PANEL_CREATE_WITH_WF', '', 0),
(109, 'FLOW_PANEL_EDIT_WITH_WF', '', '-=AUTONAME=-', NULL, 'FLOW_PANEL_EDIT_WITH_WF', '', 0),
(110, 'FLOW_PANEL_HISTORY', '', '-=AUTONAME=-', NULL, 'FLOW_PANEL_HISTORY', '', 0),
(111, 'top_panel_create_new', '', '-=AUTONAME=-', NULL, 'top_panel_create_new', '', 0),
(112, 'top_panel_create_folder_new', '', '-=AUTONAME=-', NULL, 'top_panel_create_folder_new', '', 0),
(116, 'bx-panel-toggle', '', '-=AUTONAME=-', NULL, 'bx-panel-toggle', '', 0),
(117, 'bx-panel-small-toggle', '', '-=AUTONAME=-', NULL, 'bx-panel-small-toggle', '', 0),
(118, 'bx-panel-expander', 'var d=BX(''bx-panel-expander''); if (d) BX.fireEvent(d, ''click'');', '-=AUTONAME=-', NULL, 'bx-panel-expander', '', 0),
(119, 'bx-panel-hider', 'var d=BX(''bx-panel-hider''); if (d) d.click();', '-=AUTONAME=-', NULL, 'bx-panel-hider', '', 0),
(120, 'search-textbox-input', 'var d=BX(''search-textbox-input''); if (d) { d.click(); d.focus();}', '-=AUTONAME=-', '', 'search', '', 0),
(121, 'bx-search-input', 'var d=BX(''bx-search-input''); if (d) { d.click(); d.focus(); }', '-=AUTONAME=-', '', 'bx-search-input', '', 0),
(133, 'bx-panel-logout', 'var d=BX(''bx-panel-logout''); if (d) location.href = d.href;', '-=AUTONAME=-', '', 'bx-panel-logout', '', 0),
(135, 'CDialog', 'var d=BX(''cancel''); if (d) d.click();', 'HK_DB_D_CANCEL', '', 'cancel', '', 0),
(136, 'CDialog', 'var d=BX(''close''); if (d) d.click();', 'HK_DB_D_CLOSE', '', 'close', '', 0),
(137, 'CDialog', 'var d=BX(''savebtn''); if (d) d.click();', 'HK_DB_D_SAVE', '', 'savebtn', '', 0),
(138, 'CDialog', 'var d=BX(''btn_popup_save''); if (d) d.click();', 'HK_DB_D_EDIT_SAVE', '', 'btn_popup_save', '', 0),
(139, 'Global', 'location.href=''/bitrix/admin/user_admin.php?lang=''+phpVars.LANGUAGE_ID;', 'HK_DB_SHW_U', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock`
--

CREATE TABLE IF NOT EXISTS `b_iblock` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IBLOCK_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `LIST_PAGE_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DETAIL_PAGE_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECTION_PAGE_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PICTURE` int(18) DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DESCRIPTION_TYPE` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `RSS_TTL` int(11) NOT NULL DEFAULT '24',
  `RSS_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `RSS_FILE_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `RSS_FILE_LIMIT` int(11) DEFAULT NULL,
  `RSS_FILE_DAYS` int(11) DEFAULT NULL,
  `RSS_YANDEX_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INDEX_ELEMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `INDEX_SECTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `WORKFLOW` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `BIZPROC` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SECTION_CHOOSER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIST_MODE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RIGHTS_MODE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECTION_PROPERTY` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PROPERTY_INDEX` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(11) NOT NULL DEFAULT '1',
  `LAST_CONV_ELEMENT` int(11) NOT NULL DEFAULT '0',
  `SOCNET_GROUP_ID` int(18) DEFAULT NULL,
  `EDIT_FILE_BEFORE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDIT_FILE_AFTER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECTIONS_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SECTION_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ELEMENTS_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ELEMENT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock` (`IBLOCK_TYPE_ID`,`LID`,`ACTIVE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `b_iblock`
--

INSERT INTO `b_iblock` (`ID`, `TIMESTAMP_X`, `IBLOCK_TYPE_ID`, `LID`, `CODE`, `NAME`, `ACTIVE`, `SORT`, `LIST_PAGE_URL`, `DETAIL_PAGE_URL`, `SECTION_PAGE_URL`, `PICTURE`, `DESCRIPTION`, `DESCRIPTION_TYPE`, `RSS_TTL`, `RSS_ACTIVE`, `RSS_FILE_ACTIVE`, `RSS_FILE_LIMIT`, `RSS_FILE_DAYS`, `RSS_YANDEX_ACTIVE`, `XML_ID`, `TMP_ID`, `INDEX_ELEMENT`, `INDEX_SECTION`, `WORKFLOW`, `BIZPROC`, `SECTION_CHOOSER`, `LIST_MODE`, `RIGHTS_MODE`, `SECTION_PROPERTY`, `PROPERTY_INDEX`, `VERSION`, `LAST_CONV_ELEMENT`, `SOCNET_GROUP_ID`, `EDIT_FILE_BEFORE`, `EDIT_FILE_AFTER`, `SECTIONS_NAME`, `SECTION_NAME`, `ELEMENTS_NAME`, `ELEMENT_NAME`) VALUES
(1, '2015-04-24 01:41:36', 'news', 's1', 'furniture_news_s1', 'Новости', 'Y', 500, '#SITE_DIR#kompaniya/novosti/', '#SITE_DIR#kompaniya/novosti/#ELEMENT_CODE#/', NULL, NULL, '', 'text', 24, 'Y', 'N', 0, 0, 'N', 'furniture_news_s1', '1e0fba4a2854fa9b8bd8ac2f3d045bad', 'Y', 'N', 'N', 'N', 'L', '', 'S', 'N', 'N', 1, 0, NULL, '', '', 'Разделы', 'Раздел', 'Новости', 'Новость'),
(2, '2015-05-10 03:51:36', 'products', 's1', 'products', '[s1] Продукция', 'Y', 500, '#SITE_DIR#produktsiya/', '#SITE_DIR#produktsiya/#SECTION_CODE_PATH#/#ELEMENT_CODE#/', '#SITE_DIR#produktsiya/#SECTION_CODE_PATH#/', NULL, '', 'text', 24, 'Y', 'N', NULL, NULL, 'N', 'furniture_products_s1', '631cffbc7180842ae1ffa74cd8169686', 'Y', 'Y', 'N', 'N', 'L', '', 'S', 'N', 'N', 1, 0, NULL, '', '', 'Разделы', 'Раздел', 'Товары', 'Товар'),
(3, '2015-04-22 17:43:48', 'products', 's1', 'furniture_services_s1', '[s1] Услуги', 'Y', 500, '#SITE_DIR#/services/', '#SITE_DIR#/services/#ID#/', NULL, NULL, NULL, 'text', 24, 'Y', 'N', NULL, NULL, 'N', 'furniture_services_s1', 'afcb4739d8d3f0af09a8f7e790ea5fd3', 'Y', 'Y', 'N', 'N', 'L', NULL, 'S', NULL, NULL, 1, 0, NULL, NULL, NULL, 'Разделы', 'Раздел', 'Услуги', 'Услуга'),
(5, '2015-04-23 16:52:57', 'feedback', 's1', 'feedback', 'Обратная связь', 'Y', 500, '', '', '', NULL, '', 'text', 24, 'Y', 'N', NULL, NULL, 'N', NULL, '936886a9ffba9e390280f52b1ba2b9bd', 'N', 'N', 'N', 'N', 'L', '', 'S', NULL, NULL, 1, 0, NULL, '', '', 'Темы сообщений', 'Тема сообщений', 'Сообщения', 'Сообщение'),
(6, '2015-04-24 01:20:29', 'news', 's1', 'articles', 'Статьи', 'Y', 500, '#SITE_DIR#kompaniya/stati/', '#SITE_DIR#kompaniya/stati/#ELEMENT_CODE#/', NULL, NULL, '', 'text', 24, 'N', 'N', 10, 7, 'N', NULL, NULL, 'Y', 'N', 'N', 'N', 'L', '', 'S', 'N', 'N', 1, 0, NULL, '', '', NULL, NULL, 'Статьи', 'Статья'),
(7, '2015-04-27 15:31:27', 'projects', 's1', 'projects', 'Проекты', 'Y', 500, '#SITE_DIR#proekty/', '#SITE_DIR#proekty/#SECTION_CODE#/#ELEMENT_CODE#/', '#SITE_DIR#proekty/#SECTION_CODE#/', NULL, '', 'text', 24, 'Y', 'N', NULL, NULL, 'N', NULL, 'd6c30ad198c78a907c6ff1999b9a76f1', 'Y', 'Y', 'N', 'N', 'L', '', 'S', 'N', 'N', 1, 0, NULL, '', '', 'Разделы', 'Раздел', 'Проекты', 'Проект');

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_cache`
--

CREATE TABLE IF NOT EXISTS `b_iblock_cache` (
  `CACHE_KEY` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `CACHE` longtext COLLATE utf8_unicode_ci NOT NULL,
  `CACHE_DATE` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`CACHE_KEY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_element`
--

CREATE TABLE IF NOT EXISTS `b_iblock_element` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `IBLOCK_ID` int(11) NOT NULL DEFAULT '0',
  `IBLOCK_SECTION_ID` int(11) DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ACTIVE_FROM` datetime DEFAULT NULL,
  `ACTIVE_TO` datetime DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '500',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PREVIEW_PICTURE` int(18) DEFAULT NULL,
  `PREVIEW_TEXT` text COLLATE utf8_unicode_ci,
  `PREVIEW_TEXT_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `DETAIL_PICTURE` int(18) DEFAULT NULL,
  `DETAIL_TEXT` longtext COLLATE utf8_unicode_ci,
  `DETAIL_TEXT_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `SEARCHABLE_CONTENT` text COLLATE utf8_unicode_ci,
  `WF_STATUS_ID` int(18) DEFAULT '1',
  `WF_PARENT_ELEMENT_ID` int(11) DEFAULT NULL,
  `WF_NEW` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WF_LOCKED_BY` int(18) DEFAULT NULL,
  `WF_DATE_LOCK` datetime DEFAULT NULL,
  `WF_COMMENTS` text COLLATE utf8_unicode_ci,
  `IN_SECTIONS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAGS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WF_LAST_HISTORY_ID` int(11) DEFAULT NULL,
  `SHOW_COUNTER` int(18) DEFAULT NULL,
  `SHOW_COUNTER_START` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_element_1` (`IBLOCK_ID`,`IBLOCK_SECTION_ID`),
  KEY `ix_iblock_element_4` (`IBLOCK_ID`,`XML_ID`,`WF_PARENT_ELEMENT_ID`),
  KEY `ix_iblock_element_3` (`WF_PARENT_ELEMENT_ID`),
  KEY `ix_iblock_element_code` (`IBLOCK_ID`,`CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=47 ;

--
-- Dumping data for table `b_iblock_element`
--

INSERT INTO `b_iblock_element` (`ID`, `TIMESTAMP_X`, `MODIFIED_BY`, `DATE_CREATE`, `CREATED_BY`, `IBLOCK_ID`, `IBLOCK_SECTION_ID`, `ACTIVE`, `ACTIVE_FROM`, `ACTIVE_TO`, `SORT`, `NAME`, `PREVIEW_PICTURE`, `PREVIEW_TEXT`, `PREVIEW_TEXT_TYPE`, `DETAIL_PICTURE`, `DETAIL_TEXT`, `DETAIL_TEXT_TYPE`, `SEARCHABLE_CONTENT`, `WF_STATUS_ID`, `WF_PARENT_ELEMENT_ID`, `WF_NEW`, `WF_LOCKED_BY`, `WF_DATE_LOCK`, `WF_COMMENTS`, `IN_SECTIONS`, `XML_ID`, `CODE`, `TAGS`, `TMP_ID`, `WF_LAST_HISTORY_ID`, `SHOW_COUNTER`, `SHOW_COUNTER_START`) VALUES
(28, '2015-04-22 23:43:48', 1, '2015-04-22 23:43:48', 1, 3, NULL, 'Y', NULL, NULL, 500, 'Мебель на заказ', 53, 'Наша компания занимается разработкой мебели на заказ.', 'html', NULL, '        <p>Наша компания занимается разработкой мебели на заказ по индивидуальным проектам: встроенные и корпусные шкафы купе,\r\n        гардеробные комнаты, прихожие, библиотеки, платяные шкафы, комоды и другие сложные конструкции.</p>\r\n        <p>Мы создаем мебель идеально подходящую именно к вашему дому и офису, интерьеры, максимально отображающие вашу индивидуальность.\r\n        По Вашей заявке наш специалист приезжает со всеми образцами материалов, с которыми мы работаем, в любое удобное для Вас время и\r\n        произведет все необходимые замеры. Учитывая все Ваши пожелания и особенности помещения, составляется эскизный проект.</p>\r\n        <p>После заключения договора, в котором оговариваются сроки доставки и монтажа мебели, эскизный проект передается на производство,\r\n        где опытными специалистами производятся расчеты в программе трехмерного моделирования. После всех расчетов готовый проект поступает\r\n        непосредственно на производство, где производят раскрой деталей, их обработку, и сборку некоторых элементов. Накануне дня доставки\r\n        сотрудники отдела транспортных услуг свяжутся с Вами и более конкретно оговорят время доставки. После заключения договора вами\r\n        вносится предоплата в размере 30% от суммы заказанной Вами мебели. Остальная сумма оплачивается Вами по доставке.</p>\r\n        \r\n      ', 'html', 'МЕБЕЛЬ НА ЗАКАЗ\r\nНАША КОМПАНИЯ ЗАНИМАЕТСЯ РАЗРАБОТКОЙ МЕБЕЛИ \r\nНА ЗАКАЗ.\r\nНАША КОМПАНИЯ ЗАНИМАЕТСЯ РАЗРАБОТКОЙ МЕБЕЛИ \r\nНА ЗАКАЗ ПО ИНДИВИДУАЛЬНЫМ ПРОЕКТАМ: ВСТРОЕННЫЕ \r\nИ КОРПУСНЫЕ ШКАФЫ КУПЕ, ГАРДЕРОБНЫЕ КОМНАТЫ, \r\nПРИХОЖИЕ, БИБЛИОТЕКИ, ПЛАТЯНЫЕ ШКАФЫ, КОМОДЫ \r\nИ ДРУГИЕ СЛОЖНЫЕ КОНСТРУКЦИИ. \r\n\r\nМЫ СОЗДАЕМ МЕБЕЛЬ ИДЕАЛЬНО ПОДХОДЯЩУЮ \r\nИМЕННО К ВАШЕМУ ДОМУ И ОФИСУ, ИНТЕРЬЕРЫ, \r\nМАКСИМАЛЬНО ОТОБРАЖАЮЩИЕ ВАШУ ИНДИВИДУАЛЬНОСТЬ. \r\nПО ВАШЕЙ ЗАЯВКЕ НАШ СПЕЦИАЛИСТ ПРИЕЗЖАЕТ \r\nСО ВСЕМИ ОБРАЗЦАМИ МАТЕРИАЛОВ, С КОТОРЫМИ \r\nМЫ РАБОТАЕМ, В ЛЮБОЕ УДОБНОЕ ДЛЯ ВАС ВРЕМЯ \r\nИ ПРОИЗВЕДЕТ ВСЕ НЕОБХОДИМЫЕ ЗАМЕРЫ. УЧИТЫВАЯ \r\nВСЕ ВАШИ ПОЖЕЛАНИЯ И ОСОБЕННОСТИ ПОМЕЩЕНИЯ, \r\nСОСТАВЛЯЕТСЯ ЭСКИЗНЫЙ ПРОЕКТ. \r\n\r\nПОСЛЕ ЗАКЛЮЧЕНИЯ ДОГОВОРА, В КОТОРОМ ОГОВАРИВАЮТСЯ \r\nСРОКИ ДОСТАВКИ И МОНТАЖА МЕБЕЛИ, ЭСКИЗНЫЙ \r\nПРОЕКТ ПЕРЕДАЕТСЯ НА ПРОИЗВОДСТВО, ГДЕ \r\nОПЫТНЫМИ СПЕЦИАЛИСТАМИ ПРОИЗВОДЯТСЯ РАСЧЕТЫ \r\nВ ПРОГРАММЕ ТРЕХМЕРНОГО МОДЕЛИРОВАНИЯ. \r\nПОСЛЕ ВСЕХ РАСЧЕТОВ ГОТОВЫЙ ПРОЕКТ ПОСТУПАЕТ \r\nНЕПОСРЕДСТВЕННО НА ПРОИЗВОДСТВО, ГДЕ ПРОИЗВОДЯТ \r\nРАСКРОЙ ДЕТАЛЕЙ, ИХ ОБРАБОТКУ, И СБОРКУ \r\nНЕКОТОРЫХ ЭЛЕМЕНТОВ. НАКАНУНЕ ДНЯ ДОСТАВКИ \r\nСОТРУДНИКИ ОТДЕЛА ТРАНСПОРТНЫХ УСЛУГ СВЯЖУТСЯ \r\nС ВАМИ И БОЛЕЕ КОНКРЕТНО ОГОВОРЯТ ВРЕМЯ \r\nДОСТАВКИ. ПОСЛЕ ЗАКЛЮЧЕНИЯ ДОГОВОРА ВАМИ \r\nВНОСИТСЯ ПРЕДОПЛАТА В РАЗМЕРЕ 30% ОТ СУММЫ \r\nЗАКАЗАННОЙ ВАМИ МЕБЕЛИ. ОСТАЛЬНАЯ СУММА \r\nОПЛАЧИВАЕТСЯ ВАМИ ПО ДОСТАВКЕ.', 1, NULL, NULL, NULL, NULL, NULL, 'N', '5', '', '', '841280321', NULL, NULL, NULL),
(29, '2015-04-22 23:43:48', 1, '2015-04-22 23:43:48', 1, 3, NULL, 'Y', NULL, NULL, 500, 'Услуги дизайнера', 54, 'Мы предлагаем широкий спектр услуг по дизайну мебели.', 'html', NULL, '        \r\n        <ul>\r\n          <li>Диагностика возможностей преобразования помещений – определение вариантов перепланировки, отделки, разработка новых решений колористки, освещения, перестановки мебели и декора, разработка специальных функциональных зон для переключения в различные режимы жизни.</li>\r\n          <li>Разработка Идеи-Образа, проведение предварительных расчётов и создание 3D-модели: изображение передает цвет и фактуру, предлагая клиенту экспериментировать и подбирать оптимальный вариант.</li>\r\n          <li>Разработка компьютерных цветных трехмерных моделей мебели. Натуралистичность изображений, их высокая схожесть с оригиналом позволяют представить, как будет выглядеть готовое изделие,  рассмотреть его в деталях.</li>\r\n          <li>Подбор и расстановка мебели.</li>\r\n          <li>Декорирование - подбор декора и аксессуаров интерьера в соответствии с образом и стилем помещения. Возможно создание по индивидуальному запросу эксклюзивных, авторских коллекций.</li>\r\n        </ul>\r\n        \r\n      ', 'html', 'УСЛУГИ ДИЗАЙНЕРА\r\nМЫ ПРЕДЛАГАЕМ ШИРОКИЙ СПЕКТР УСЛУГ ПО ДИЗАЙНУ \r\nМЕБЕЛИ.\r\n- ДИАГНОСТИКА ВОЗМОЖНОСТЕЙ ПРЕОБРАЗОВАНИЯ \r\nПОМЕЩЕНИЙ – ОПРЕДЕЛЕНИЕ ВАРИАНТОВ ПЕРЕПЛАНИРОВКИ, \r\nОТДЕЛКИ, РАЗРАБОТКА НОВЫХ РЕШЕНИЙ КОЛОРИСТКИ, \r\nОСВЕЩЕНИЯ, ПЕРЕСТАНОВКИ МЕБЕЛИ И ДЕКОРА, \r\nРАЗРАБОТКА СПЕЦИАЛЬНЫХ ФУНКЦИОНАЛЬНЫХ \r\nЗОН ДЛЯ ПЕРЕКЛЮЧЕНИЯ В РАЗЛИЧНЫЕ РЕЖИМЫ \r\nЖИЗНИ. \r\n- РАЗРАБОТКА ИДЕИ-ОБРАЗА, ПРОВЕДЕНИЕ ПРЕДВАРИТЕЛЬНЫХ \r\nРАСЧЁТОВ И СОЗДАНИЕ 3D-МОДЕЛИ: ИЗОБРАЖЕНИЕ \r\nПЕРЕДАЕТ ЦВЕТ И ФАКТУРУ, ПРЕДЛАГАЯ КЛИЕНТУ \r\nЭКСПЕРИМЕНТИРОВАТЬ И ПОДБИРАТЬ ОПТИМАЛЬНЫЙ \r\nВАРИАНТ. \r\n- РАЗРАБОТКА КОМПЬЮТЕРНЫХ ЦВЕТНЫХ ТРЕХМЕРНЫХ \r\nМОДЕЛЕЙ МЕБЕЛИ. НАТУРАЛИСТИЧНОСТЬ ИЗОБРАЖЕНИЙ, \r\nИХ ВЫСОКАЯ СХОЖЕСТЬ С ОРИГИНАЛОМ ПОЗВОЛЯЮТ \r\nПРЕДСТАВИТЬ, КАК БУДЕТ ВЫГЛЯДЕТЬ ГОТОВОЕ \r\nИЗДЕЛИЕ, РАССМОТРЕТЬ ЕГО В ДЕТАЛЯХ. \r\n- ПОДБОР И РАССТАНОВКА МЕБЕЛИ. \r\n- ДЕКОРИРОВАНИЕ - ПОДБОР ДЕКОРА И АКСЕССУАРОВ \r\nИНТЕРЬЕРА В СООТВЕТСТВИИ С ОБРАЗОМ И СТИЛЕМ \r\nПОМЕЩЕНИЯ. ВОЗМОЖНО СОЗДАНИЕ ПО ИНДИВИДУАЛЬНОМУ \r\nЗАПРОСУ ЭКСКЛЮЗИВНЫХ, АВТОРСКИХ КОЛЛЕКЦИЙ.', 1, NULL, NULL, NULL, NULL, NULL, 'N', '6', '', '', '-1080381355', NULL, NULL, NULL),
(33, '2015-04-24 08:30:51', 1, '2015-04-24 08:30:51', 1, 1, NULL, 'Y', '2015-04-24 00:00:00', NULL, 500, 'Отчёт о выставке Росупак 2012', NULL, 'В 2012 году выставка Росупак открыла посетителям свои двери на новой площадке. В 75-ом павильоне ВВЦ собрались ведущие производители и поставщики оборудования, всего 700 компаний из 31 страны мира. Компания Аквакультура, как один из ведущих поставщиков упаковочного оборудования в России и СНГ, традиционно приняла участие в выставке. На стенде компании была представлена линия по выдуву ПЭТ бутылок производительностью 6000 бутылок в час.', 'text', NULL, '', 'text', 'ОТЧЁТ О ВЫСТАВКЕ РОСУПАК 2012\r\nВ 2012 ГОДУ ВЫСТАВКА РОСУПАК ОТКРЫЛА ПОСЕТИТЕЛЯМ СВОИ ДВЕРИ НА НОВОЙ ПЛОЩАДКЕ. В 75-ОМ ПАВИЛЬОНЕ ВВЦ СОБРАЛИСЬ ВЕДУЩИЕ ПРОИЗВОДИТЕЛИ И ПОСТАВЩИКИ ОБОРУДОВАНИЯ, ВСЕГО 700 КОМПАНИЙ ИЗ 31 СТРАНЫ МИРА. КОМПАНИЯ АКВАКУЛЬТУРА, КАК ОДИН ИЗ ВЕДУЩИХ ПОСТАВЩИКОВ УПАКОВОЧНОГО ОБОРУДОВАНИЯ В РОССИИ И СНГ, ТРАДИЦИОННО ПРИНЯЛА УЧАСТИЕ В ВЫСТАВКЕ. НА СТЕНДЕ КОМПАНИИ БЫЛА ПРЕДСТАВЛЕНА ЛИНИЯ ПО ВЫДУВУ ПЭТ БУТЫЛОК ПРОИЗВОДИТЕЛЬНОСТЬЮ 6000 БУТЫЛОК В ЧАС.\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'N', '33', 'otchyet-o-vystavke-rosupak-2012', '', '0', NULL, NULL, NULL),
(34, '2015-04-24 08:35:13', 1, '2015-04-24 08:32:02', 1, 1, NULL, 'Y', '2015-04-24 00:00:00', NULL, 500, 'Выставка Rosupack 2013', 55, 'Уважаемые коллеги и друзья! Приглашаем Вас посетить наш стенд А 215 в зале 1, первого павильона в МВЦ "Крокус Экспо" во время проведения выставки RosUpack 2013 с 18 по 21 июня 2013 года!', 'text', NULL, '', 'text', 'ВЫСТАВКА ROSUPACK 2013\r\nУВАЖАЕМЫЕ КОЛЛЕГИ И ДРУЗЬЯ! ПРИГЛАШАЕМ ВАС ПОСЕТИТЬ НАШ СТЕНД А 215 В ЗАЛЕ 1, ПЕРВОГО ПАВИЛЬОНА В МВЦ "КРОКУС ЭКСПО" ВО ВРЕМЯ ПРОВЕДЕНИЯ ВЫСТАВКИ ROSUPACK 2013 С 18 ПО 21 ИЮНЯ 2013 ГОДА!\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'N', '34', 'vystavka-rosupack-2013', '', '0', NULL, NULL, NULL),
(35, '2015-04-24 08:34:38', 1, '2015-04-24 08:34:38', 1, 1, NULL, 'Y', '2015-04-24 00:00:00', NULL, 500, 'Watershow 2014', 56, 'Уважаемые коллеги! Приглашаем Вас посетить самые ожидаемые и посещаемые события водной отрасли Тринадцатый Форум производителей бутилированной воды России «WaterShow» и Второй Международный Конгресс производителей минеральной, питьевой воды и безалкогольных напитков «Beverages Industry Conference», мероприятия пройдут 18-20 марта 2014 года, в Москве, отель Милан.', 'text', NULL, '', 'text', 'WATERSHOW 2014\r\nУВАЖАЕМЫЕ КОЛЛЕГИ! ПРИГЛАШАЕМ ВАС ПОСЕТИТЬ САМЫЕ ОЖИДАЕМЫЕ И ПОСЕЩАЕМЫЕ СОБЫТИЯ ВОДНОЙ ОТРАСЛИ ТРИНАДЦАТЫЙ ФОРУМ ПРОИЗВОДИТЕЛЕЙ БУТИЛИРОВАННОЙ ВОДЫ РОССИИ «WATERSHOW» И ВТОРОЙ МЕЖДУНАРОДНЫЙ КОНГРЕСС ПРОИЗВОДИТЕЛЕЙ МИНЕРАЛЬНОЙ, ПИТЬЕВОЙ ВОДЫ И БЕЗАЛКОГОЛЬНЫХ НАПИТКОВ «BEVERAGES INDUSTRY CONFERENCE», МЕРОПРИЯТИЯ ПРОЙДУТ 18-20 МАРТА 2014 ГОДА, В МОСКВЕ, ОТЕЛЬ МИЛАН.\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'N', '35', 'watershow-2014', '', '0', NULL, NULL, NULL),
(36, '2015-04-24 08:34:55', 1, '2015-04-24 08:34:55', 1, 1, NULL, 'Y', '2015-04-24 00:00:00', NULL, 500, 'Отчёт о Watershow 2014', NULL, 'Как всегда очень успешно прошел Тринадцатый Форум производителей бутилированной воды России Watershow 2014. Многие участники уже назвали его одним из лучших за необыкновенно приятную атмосферу и отличную организацию мероприятия. На Watershow приехало около 250 человек из 128 компаний России, Казахстана, Украины, Узбекистана, Италии, Польши, Китая, Армении, Белоруси.', 'text', NULL, '', 'text', 'ОТЧЁТ О WATERSHOW 2014\r\nКАК ВСЕГДА ОЧЕНЬ УСПЕШНО ПРОШЕЛ ТРИНАДЦАТЫЙ ФОРУМ ПРОИЗВОДИТЕЛЕЙ БУТИЛИРОВАННОЙ ВОДЫ РОССИИ WATERSHOW 2014. МНОГИЕ УЧАСТНИКИ УЖЕ НАЗВАЛИ ЕГО ОДНИМ ИЗ ЛУЧШИХ ЗА НЕОБЫКНОВЕННО ПРИЯТНУЮ АТМОСФЕРУ И ОТЛИЧНУЮ ОРГАНИЗАЦИЮ МЕРОПРИЯТИЯ. НА WATERSHOW ПРИЕХАЛО ОКОЛО 250 ЧЕЛОВЕК ИЗ 128 КОМПАНИЙ РОССИИ, КАЗАХСТАНА, УКРАИНЫ, УЗБЕКИСТАНА, ИТАЛИИ, ПОЛЬШИ, КИТАЯ, АРМЕНИИ, БЕЛОРУСИ.\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'N', '36', 'otchyet-o-watershow-2014', '', '0', NULL, NULL, NULL),
(37, '2015-04-24 08:37:24', 1, '2015-04-24 08:37:24', 1, 6, NULL, 'Y', '2015-04-24 02:36:43', NULL, 500, 'Системы и фильтры очистки воды для дома', NULL, 'Когда-то, чтобы утолить жажду, для наших предков было совершенно естественным спуститься к реке, озеру или ручью, зачерпнуть ладонями и выпить свежую воду. С тех пор прошли века...', 'text', NULL, '', 'text', 'СИСТЕМЫ И ФИЛЬТРЫ ОЧИСТКИ ВОДЫ ДЛЯ ДОМА\r\nКОГДА-ТО, ЧТОБЫ УТОЛИТЬ ЖАЖДУ, ДЛЯ НАШИХ ПРЕДКОВ БЫЛО СОВЕРШЕННО ЕСТЕСТВЕННЫМ СПУСТИТЬСЯ К РЕКЕ, ОЗЕРУ ИЛИ РУЧЬЮ, ЗАЧЕРПНУТЬ ЛАДОНЯМИ И ВЫПИТЬ СВЕЖУЮ ВОДУ. С ТЕХ ПОР ПРОШЛИ ВЕКА...\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'N', '37', 'sistemy-i-filtry-ochistki-vody-dlya-doma', '', '0', NULL, NULL, NULL),
(38, '2015-04-24 08:37:38', 1, '2015-04-24 08:37:38', 1, 6, NULL, 'Y', '2015-04-24 02:37:24', NULL, 500, 'Дезинфекция питьевой воды с помощью УФ излучения', NULL, 'Вода имеете очень важное значение в жизни человека. Без воды человек может прожить не более 3 суток. Вода оставляет ориентировочно две трети массы тела взрослого человека.', 'text', NULL, '', 'text', 'ДЕЗИНФЕКЦИЯ ПИТЬЕВОЙ ВОДЫ С ПОМОЩЬЮ УФ ИЗЛУЧЕНИЯ\r\nВОДА ИМЕЕТЕ ОЧЕНЬ ВАЖНОЕ ЗНАЧЕНИЕ В ЖИЗНИ ЧЕЛОВЕКА. БЕЗ ВОДЫ ЧЕЛОВЕК МОЖЕТ ПРОЖИТЬ НЕ БОЛЕЕ 3 СУТОК. ВОДА ОСТАВЛЯЕТ ОРИЕНТИРОВОЧНО ДВЕ ТРЕТИ МАССЫ ТЕЛА ВЗРОСЛОГО ЧЕЛОВЕКА.\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'N', '38', 'dezinfektsiya-pitevoy-vody-s-pomoshchyu-uf-izlucheniya', '', '0', NULL, NULL, NULL),
(39, '2015-04-24 08:37:54', 1, '2015-04-24 08:37:54', 1, 6, NULL, 'Y', '2015-04-24 02:37:39', NULL, 500, 'Очередной семинар, организованный Союзом производителей бутилированных вод России', NULL, 'Семинар прошел 2-3 июня в Подмосковном отеле «Фореста Фестиваль Парк». В качестве приглашенного специалиста семинар «Влияние финансового кризиса на водные компании» провел независимый бизнес-консультант Константин Раков.', 'text', NULL, '', 'text', 'ОЧЕРЕДНОЙ СЕМИНАР, ОРГАНИЗОВАННЫЙ СОЮЗОМ ПРОИЗВОДИТЕЛЕЙ БУТИЛИРОВАННЫХ ВОД РОССИИ\r\nСЕМИНАР ПРОШЕЛ 2-3 ИЮНЯ В ПОДМОСКОВНОМ ОТЕЛЕ «ФОРЕСТА ФЕСТИВАЛЬ ПАРК». В КАЧЕСТВЕ ПРИГЛАШЕННОГО СПЕЦИАЛИСТА СЕМИНАР «ВЛИЯНИЕ ФИНАНСОВОГО КРИЗИСА НА ВОДНЫЕ КОМПАНИИ» ПРОВЕЛ НЕЗАВИСИМЫЙ БИЗНЕС-КОНСУЛЬТАНТ КОНСТАНТИН РАКОВ.\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'N', '39', 'ocherednoy-seminar-organizovannyy-soyuzom-proizvoditeley-butilirovannykh-vod-rossii', '', '0', NULL, NULL, NULL),
(40, '2015-04-24 08:38:11', 1, '2015-04-24 08:38:11', 1, 6, NULL, 'Y', '2015-04-24 02:37:55', NULL, 500, 'Технологии очистки воды', NULL, 'Вода имеете очень важное значение в жизни человека. Без воды человек может прожить не более 3 суток. Вода оставляет ориентировочно две трети массы тела взрослого человека.', 'text', NULL, '', 'text', 'ТЕХНОЛОГИИ ОЧИСТКИ ВОДЫ\r\nВОДА ИМЕЕТЕ ОЧЕНЬ ВАЖНОЕ ЗНАЧЕНИЕ В ЖИЗНИ ЧЕЛОВЕКА. БЕЗ ВОДЫ ЧЕЛОВЕК МОЖЕТ ПРОЖИТЬ НЕ БОЛЕЕ 3 СУТОК. ВОДА ОСТАВЛЯЕТ ОРИЕНТИРОВОЧНО ДВЕ ТРЕТИ МАССЫ ТЕЛА ВЗРОСЛОГО ЧЕЛОВЕКА.\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'N', '40', 'tekhnologii-ochistki-vody', '', '0', NULL, NULL, NULL),
(41, '2015-04-27 21:44:07', 1, '2015-04-27 21:44:07', 1, 7, 22, 'Y', NULL, NULL, 500, 'Завод ООО «Кока-Кола ЭйчБиСи Евразия»', 57, '<h4>Задача:</h4>\r\n<p>\r\n	 Запустить производство лимонада с нуля в сжатые сроки.\r\n</p>\r\n<h4>Результат:</h4>\r\n<p>\r\n	 Сейчас компания выпускает 700 бутылок лимонада и 400 бутылей чистой воды в час. Заказчик планирует расширять производство и ассортимент в сотрудничестве с нами.\r\n</p>', 'html', 58, '', 'text', 'ЗАВОД ООО «КОКА-КОЛА ЭЙЧБИСИ ЕВРАЗИЯ»\r\nЗАДАЧА: \r\n\r\nЗАПУСТИТЬ ПРОИЗВОДСТВО ЛИМОНАДА С НУЛЯ \r\nВ СЖАТЫЕ СРОКИ. РЕЗУЛЬТАТ: \r\n\r\nСЕЙЧАС КОМПАНИЯ ВЫПУСКАЕТ 700 БУТЫЛОК ЛИМОНАДА \r\nИ 400 БУТЫЛЕЙ ЧИСТОЙ ВОДЫ В ЧАС. ЗАКАЗЧИК \r\nПЛАНИРУЕТ РАСШИРЯТЬ ПРОИЗВОДСТВО И АССОРТИМЕНТ \r\nВ СОТРУДНИЧЕСТВЕ С НАМИ.\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'Y', '41', 'zavod-ooo-koka-kola-eychbisi-evraziya', '', '0', NULL, NULL, NULL),
(42, '2015-04-27 21:44:51', 1, '2015-04-27 21:44:51', 1, 7, 21, 'Y', NULL, NULL, 500, 'Цех розлива МПБК «Очаково»', 59, '<h4>Задача:</h4>\r\n<p>\r\n	 Запустить производство лимонада с нуля в сжатые сроки.\r\n</p>\r\n<h4>Результат:</h4>\r\n<p>\r\n	 Сейчас компания выпускает 700 бутылок лимонада и 400 бутылей чистой воды в час. Заказчик планирует расширять производство и ассортимент в сотрудничестве с нами.\r\n</p>', 'html', 60, '', 'text', 'ЦЕХ РОЗЛИВА МПБК «ОЧАКОВО»\r\nЗАДАЧА: \r\n\r\nЗАПУСТИТЬ ПРОИЗВОДСТВО ЛИМОНАДА С НУЛЯ \r\nВ СЖАТЫЕ СРОКИ. РЕЗУЛЬТАТ: \r\n\r\nСЕЙЧАС КОМПАНИЯ ВЫПУСКАЕТ 700 БУТЫЛОК ЛИМОНАДА \r\nИ 400 БУТЫЛЕЙ ЧИСТОЙ ВОДЫ В ЧАС. ЗАКАЗЧИК \r\nПЛАНИРУЕТ РАСШИРЯТЬ ПРОИЗВОДСТВО И АССОРТИМЕНТ \r\nВ СОТРУДНИЧЕСТВЕ С НАМИ.\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'Y', '42', 'tsekh-rozliva-mpbk-ochakovo', '', '0', NULL, NULL, NULL),
(43, '2015-04-27 21:47:16', 1, '2015-04-27 21:45:40', 1, 7, 23, 'Y', NULL, NULL, 500, 'Завод ОАО «Брянскпиво»', 61, '<h4>Задача:</h4>\r\n<p>\r\n	 Запустить производство лимонада с нуля в сжатые сроки.\r\n</p>\r\n<h4>Результат:</h4>\r\n<p>\r\n	 Сейчас компания выпускает 700 бутылок лимонада и 400 бутылей чистой воды в час. Заказчик планирует расширять производство и ассортимент в сотрудничестве с нами.\r\n</p>', 'html', 64, '', 'text', 'ЗАВОД ОАО «БРЯНСКПИВО»\r\nЗАДАЧА: \r\n\r\nЗАПУСТИТЬ ПРОИЗВОДСТВО ЛИМОНАДА С НУЛЯ \r\nВ СЖАТЫЕ СРОКИ. РЕЗУЛЬТАТ: \r\n\r\nСЕЙЧАС КОМПАНИЯ ВЫПУСКАЕТ 700 БУТЫЛОК ЛИМОНАДА \r\nИ 400 БУТЫЛЕЙ ЧИСТОЙ ВОДЫ В ЧАС. ЗАКАЗЧИК \r\nПЛАНИРУЕТ РАСШИРЯТЬ ПРОИЗВОДСТВО И АССОРТИМЕНТ \r\nВ СОТРУДНИЧЕСТВЕ С НАМИ.\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'Y', '43', 'zavod-oao-bryanskpivo', '', '0', NULL, 1, '2015-04-27 22:24:55'),
(44, '2015-04-28 14:55:06', 1, '2015-04-28 14:55:06', 1, 2, 24, 'Y', NULL, NULL, 500, 'SN-CSSS2000', 65, '<p style="background-color: #ffffff;">\r\n	 Автоматическая выдувная машина РN-PP 2000E предназначена для производства ПП (Полипропиленовых) бутылок объёмом от 0,1 до 0.6 литра, путём двухстадийного пневматического формования из предварительно разогретых заготовок (преформ). Для каждого типоразмера бутылки изготавливается своя пресс-форма, точно повторяющая внешний вид и геометрию бутылки, заданные заказчиком.\r\n</p>\r\n<p style="background-color: #ffffff;">\r\n	 Моноблоки серии DGF предназначены для розлива и укупоривания ПЭТ-бутылок емкостью от 0,25 до 2 л газированными жидкостями производительностью до 24000 бут/час.\r\n</p>\r\n<h4 style="background-color: #ffffff;">Конструктивно моноблок состоит из:</h4>\r\n<ul style="background-color: #ffffff;">\r\n	<li>GX-200 встраиваются в линию, путём стыковки конвейеров с двух сторон автомата</li>\r\n	<li>все детали и механизмы контактирующие с продукцией изготовлены из нержавеющей стали</li>\r\n</ul>\r\n<p style="background-color: #ffffff;">\r\n	 Машина полностью автоматизирована после загрузки преформ в приемный бункер до подачи готовой продукции на воздушный транспортер и не требует постоянного присутствия обслуживающего персонала.\r\n</p>\r\n<p style="background-color: #ffffff;">\r\n	 Основным преимуществом данной модели является простота наладки, стабильность параметров выдува и качества продукции, её компактность и удобство в обслуживании. Уже через 15 минут после запуска машина готова выдавать продукцию.\r\n</p>', 'html', NULL, '', 'text', 'SN-CSSS2000\r\nАВТОМАТИЧЕСКАЯ ВЫДУВНАЯ МАШИНА РN-PP 2000E \r\nПРЕДНАЗНАЧЕНА ДЛЯ ПРОИЗВОДСТВА ПП (ПОЛИПРОПИЛЕНОВЫХ) \r\nБУТЫЛОК ОБЪЁМОМ ОТ 0,1 ДО 0.6 ЛИТРА, ПУТЁМ ДВУХСТАДИЙНОГО \r\nПНЕВМАТИЧЕСКОГО ФОРМОВАНИЯ ИЗ ПРЕДВАРИТЕЛЬНО \r\nРАЗОГРЕТЫХ ЗАГОТОВОК (ПРЕФОРМ). ДЛЯ КАЖДОГО \r\nТИПОРАЗМЕРА БУТЫЛКИ ИЗГОТАВЛИВАЕТСЯ СВОЯ \r\nПРЕСС-ФОРМА, ТОЧНО ПОВТОРЯЮЩАЯ ВНЕШНИЙ \r\nВИД И ГЕОМЕТРИЮ БУТЫЛКИ, ЗАДАННЫЕ ЗАКАЗЧИКОМ. \r\n\r\nМОНОБЛОКИ СЕРИИ DGF ПРЕДНАЗНАЧЕНЫ ДЛЯ РОЗЛИВА \r\nИ УКУПОРИВАНИЯ ПЭТ-БУТЫЛОК ЕМКОСТЬЮ ОТ \r\n0,25 ДО 2 Л ГАЗИРОВАННЫМИ ЖИДКОСТЯМИ ПРОИЗВОДИТЕЛЬНОСТЬЮ \r\nДО 24000 БУТ/ЧАС. КОНСТРУКТИВНО МОНОБЛОК СОСТОИТ \r\nИЗ: \r\n\r\n- GX-200 ВСТРАИВАЮТСЯ В ЛИНИЮ, ПУТЁМ СТЫКОВКИ \r\nКОНВЕЙЕРОВ С ДВУХ СТОРОН АВТОМАТА \r\n- ВСЕ ДЕТАЛИ И МЕХАНИЗМЫ КОНТАКТИРУЮЩИЕ \r\nС ПРОДУКЦИЕЙ ИЗГОТОВЛЕНЫ ИЗ НЕРЖАВЕЮЩЕЙ \r\nСТАЛИ \r\n\r\nМАШИНА ПОЛНОСТЬЮ АВТОМАТИЗИРОВАНА ПОСЛЕ \r\nЗАГРУЗКИ ПРЕФОРМ В ПРИЕМНЫЙ БУНКЕР ДО ПОДАЧИ \r\nГОТОВОЙ ПРОДУКЦИИ НА ВОЗДУШНЫЙ ТРАНСПОРТЕР \r\nИ НЕ ТРЕБУЕТ ПОСТОЯННОГО ПРИСУТСТВИЯ ОБСЛУЖИВАЮЩЕГО \r\nПЕРСОНАЛА. \r\n\r\nОСНОВНЫМ ПРЕИМУЩЕСТВОМ ДАННОЙ МОДЕЛИ ЯВЛЯЕТСЯ \r\nПРОСТОТА НАЛАДКИ, СТАБИЛЬНОСТЬ ПАРАМЕТРОВ \r\nВЫДУВА И КАЧЕСТВА ПРОДУКЦИИ, ЕЁ КОМПАКТНОСТЬ \r\nИ УДОБСТВО В ОБСЛУЖИВАНИИ. УЖЕ ЧЕРЕЗ 15 МИНУТ \r\nПОСЛЕ ЗАПУСКА МАШИНА ГОТОВА ВЫДАВАТЬ ПРОДУКЦИЮ.\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'Y', '44', 'sn-csss2000', '', '0', NULL, NULL, NULL),
(45, '2015-04-28 14:57:49', 1, '2015-04-28 14:57:49', 1, 2, 24, 'Y', NULL, NULL, 500, 'SN-CSSS2001', 66, '<p style="background-color: #ffffff;">\r\n	 Автоматическая выдувная машина РN-PP 2000E предназначена для производства ПП (Полипропиленовых) бутылок объёмом от 0,1 до 0.6 литра, путём двухстадийного пневматического формования из предварительно разогретых заготовок (преформ). Для каждого типоразмера бутылки изготавливается своя пресс-форма, точно повторяющая внешний вид и геометрию бутылки, заданные заказчиком.\r\n</p>\r\n<p style="background-color: #ffffff;">\r\n	 Моноблоки серии DGF предназначены для розлива и укупоривания ПЭТ-бутылок емкостью от 0,25 до 2 л газированными жидкостями производительностью до 24000 бут/час.\r\n</p>\r\n<h4 style="background-color: #ffffff;">Конструктивно моноблок состоит из:</h4>\r\n<ul style="background-color: #ffffff;">\r\n	<li>GX-200 встраиваются в линию, путём стыковки конвейеров с двух сторон автомата</li>\r\n	<li>все детали и механизмы контактирующие с продукцией изготовлены из нержавеющей стали</li>\r\n</ul>\r\n<p style="background-color: #ffffff;">\r\n	 Машина полностью автоматизирована после загрузки преформ в приемный бункер до подачи готовой продукции на воздушный транспортер и не требует постоянного присутствия обслуживающего персонала.\r\n</p>\r\n<p style="background-color: #ffffff;">\r\n	 Основным преимуществом данной модели является простота наладки, стабильность параметров выдува и качества продукции, её компактность и удобство в обслуживании. Уже через 15 минут после запуска машина готова выдавать продукцию.\r\n</p>', 'html', NULL, '', 'text', 'SN-CSSS2001\r\nАВТОМАТИЧЕСКАЯ ВЫДУВНАЯ МАШИНА РN-PP 2000E \r\nПРЕДНАЗНАЧЕНА ДЛЯ ПРОИЗВОДСТВА ПП (ПОЛИПРОПИЛЕНОВЫХ) \r\nБУТЫЛОК ОБЪЁМОМ ОТ 0,1 ДО 0.6 ЛИТРА, ПУТЁМ ДВУХСТАДИЙНОГО \r\nПНЕВМАТИЧЕСКОГО ФОРМОВАНИЯ ИЗ ПРЕДВАРИТЕЛЬНО \r\nРАЗОГРЕТЫХ ЗАГОТОВОК (ПРЕФОРМ). ДЛЯ КАЖДОГО \r\nТИПОРАЗМЕРА БУТЫЛКИ ИЗГОТАВЛИВАЕТСЯ СВОЯ \r\nПРЕСС-ФОРМА, ТОЧНО ПОВТОРЯЮЩАЯ ВНЕШНИЙ \r\nВИД И ГЕОМЕТРИЮ БУТЫЛКИ, ЗАДАННЫЕ ЗАКАЗЧИКОМ. \r\n\r\nМОНОБЛОКИ СЕРИИ DGF ПРЕДНАЗНАЧЕНЫ ДЛЯ РОЗЛИВА \r\nИ УКУПОРИВАНИЯ ПЭТ-БУТЫЛОК ЕМКОСТЬЮ ОТ \r\n0,25 ДО 2 Л ГАЗИРОВАННЫМИ ЖИДКОСТЯМИ ПРОИЗВОДИТЕЛЬНОСТЬЮ \r\nДО 24000 БУТ/ЧАС. КОНСТРУКТИВНО МОНОБЛОК СОСТОИТ \r\nИЗ: \r\n\r\n- GX-200 ВСТРАИВАЮТСЯ В ЛИНИЮ, ПУТЁМ СТЫКОВКИ \r\nКОНВЕЙЕРОВ С ДВУХ СТОРОН АВТОМАТА \r\n- ВСЕ ДЕТАЛИ И МЕХАНИЗМЫ КОНТАКТИРУЮЩИЕ \r\nС ПРОДУКЦИЕЙ ИЗГОТОВЛЕНЫ ИЗ НЕРЖАВЕЮЩЕЙ \r\nСТАЛИ \r\n\r\nМАШИНА ПОЛНОСТЬЮ АВТОМАТИЗИРОВАНА ПОСЛЕ \r\nЗАГРУЗКИ ПРЕФОРМ В ПРИЕМНЫЙ БУНКЕР ДО ПОДАЧИ \r\nГОТОВОЙ ПРОДУКЦИИ НА ВОЗДУШНЫЙ ТРАНСПОРТЕР \r\nИ НЕ ТРЕБУЕТ ПОСТОЯННОГО ПРИСУТСТВИЯ ОБСЛУЖИВАЮЩЕГО \r\nПЕРСОНАЛА. \r\n\r\nОСНОВНЫМ ПРЕИМУЩЕСТВОМ ДАННОЙ МОДЕЛИ ЯВЛЯЕТСЯ \r\nПРОСТОТА НАЛАДКИ, СТАБИЛЬНОСТЬ ПАРАМЕТРОВ \r\nВЫДУВА И КАЧЕСТВА ПРОДУКЦИИ, ЕЁ КОМПАКТНОСТЬ \r\nИ УДОБСТВО В ОБСЛУЖИВАНИИ. УЖЕ ЧЕРЕЗ 15 МИНУТ \r\nПОСЛЕ ЗАПУСКА МАШИНА ГОТОВА ВЫДАВАТЬ ПРОДУКЦИЮ.\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'Y', '45', 'sn-csss2001', '', '0', NULL, NULL, NULL),
(46, '2015-05-10 19:52:14', 1, '2015-04-28 14:58:22', 1, 2, 24, 'Y', NULL, NULL, 500, 'SN-CSSS2002', 67, '<p style="background-color: #ffffff;">\r\n	 Автоматическая выдувная машина РN-PP 2000E предназначена для производства ПП (Полипропиленовых) бутылок объёмом от 0,1 до 0.6 литра, путём двухстадийного пневматического формования из предварительно разогретых заготовок (преформ). Для каждого типоразмера бутылки изготавливается своя пресс-форма, точно повторяющая внешний вид и геометрию бутылки, заданные заказчиком.\r\n</p>\r\n<p style="background-color: #ffffff;">\r\n	 Моноблоки серии DGF предназначены для розлива и укупоривания ПЭТ-бутылок емкостью от 0,25 до 2 л газированными жидкостями производительностью до 24000 бут/час.\r\n</p>\r\n<h4 style="background-color: #ffffff;">Конструктивно моноблок состоит из:</h4>\r\n<ul style="background-color: #ffffff;">\r\n	<li>GX-200 встраиваются в линию, путём стыковки конвейеров с двух сторон автомата</li>\r\n	<li>все детали и механизмы контактирующие с продукцией изготовлены из нержавеющей стали</li>\r\n</ul>\r\n<p style="background-color: #ffffff;">\r\n	 Машина полностью автоматизирована после загрузки преформ в приемный бункер до подачи готовой продукции на воздушный транспортер и не требует постоянного присутствия обслуживающего персонала.\r\n</p>\r\n<p style="background-color: #ffffff;">\r\n	 Основным преимуществом данной модели является простота наладки, стабильность параметров выдува и качества продукции, её компактность и удобство в обслуживании. Уже через 15 минут после запуска машина готова выдавать продукцию.\r\n</p>', 'html', NULL, '', 'text', 'SN-CSSS2002\r\nАВТОМАТИЧЕСКАЯ ВЫДУВНАЯ МАШИНА РN-PP 2000E \r\nПРЕДНАЗНАЧЕНА ДЛЯ ПРОИЗВОДСТВА ПП (ПОЛИПРОПИЛЕНОВЫХ) \r\nБУТЫЛОК ОБЪЁМОМ ОТ 0,1 ДО 0.6 ЛИТРА, ПУТЁМ ДВУХСТАДИЙНОГО \r\nПНЕВМАТИЧЕСКОГО ФОРМОВАНИЯ ИЗ ПРЕДВАРИТЕЛЬНО \r\nРАЗОГРЕТЫХ ЗАГОТОВОК (ПРЕФОРМ). ДЛЯ КАЖДОГО \r\nТИПОРАЗМЕРА БУТЫЛКИ ИЗГОТАВЛИВАЕТСЯ СВОЯ \r\nПРЕСС-ФОРМА, ТОЧНО ПОВТОРЯЮЩАЯ ВНЕШНИЙ \r\nВИД И ГЕОМЕТРИЮ БУТЫЛКИ, ЗАДАННЫЕ ЗАКАЗЧИКОМ. \r\n\r\nМОНОБЛОКИ СЕРИИ DGF ПРЕДНАЗНАЧЕНЫ ДЛЯ РОЗЛИВА \r\nИ УКУПОРИВАНИЯ ПЭТ-БУТЫЛОК ЕМКОСТЬЮ ОТ \r\n0,25 ДО 2 Л ГАЗИРОВАННЫМИ ЖИДКОСТЯМИ ПРОИЗВОДИТЕЛЬНОСТЬЮ \r\nДО 24000 БУТ/ЧАС. КОНСТРУКТИВНО МОНОБЛОК СОСТОИТ \r\nИЗ: \r\n\r\n- GX-200 ВСТРАИВАЮТСЯ В ЛИНИЮ, ПУТЁМ СТЫКОВКИ \r\nКОНВЕЙЕРОВ С ДВУХ СТОРОН АВТОМАТА \r\n- ВСЕ ДЕТАЛИ И МЕХАНИЗМЫ КОНТАКТИРУЮЩИЕ \r\nС ПРОДУКЦИЕЙ ИЗГОТОВЛЕНЫ ИЗ НЕРЖАВЕЮЩЕЙ \r\nСТАЛИ \r\n\r\nМАШИНА ПОЛНОСТЬЮ АВТОМАТИЗИРОВАНА ПОСЛЕ \r\nЗАГРУЗКИ ПРЕФОРМ В ПРИЕМНЫЙ БУНКЕР ДО ПОДАЧИ \r\nГОТОВОЙ ПРОДУКЦИИ НА ВОЗДУШНЫЙ ТРАНСПОРТЕР \r\nИ НЕ ТРЕБУЕТ ПОСТОЯННОГО ПРИСУТСТВИЯ ОБСЛУЖИВАЮЩЕГО \r\nПЕРСОНАЛА. \r\n\r\nОСНОВНЫМ ПРЕИМУЩЕСТВОМ ДАННОЙ МОДЕЛИ ЯВЛЯЕТСЯ \r\nПРОСТОТА НАЛАДКИ, СТАБИЛЬНОСТЬ ПАРАМЕТРОВ \r\nВЫДУВА И КАЧЕСТВА ПРОДУКЦИИ, ЕЁ КОМПАКТНОСТЬ \r\nИ УДОБСТВО В ОБСЛУЖИВАНИИ. УЖЕ ЧЕРЕЗ 15 МИНУТ \r\nПОСЛЕ ЗАПУСКА МАШИНА ГОТОВА ВЫДАВАТЬ ПРОДУКЦИЮ.\r\n', 1, NULL, NULL, NULL, NULL, NULL, 'Y', '46', 'sn-csss2002', '', '0', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_element_iprop`
--

CREATE TABLE IF NOT EXISTS `b_iblock_element_iprop` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `ELEMENT_ID` int(11) NOT NULL,
  `IPROP_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ELEMENT_ID`,`IPROP_ID`),
  KEY `ix_b_iblock_element_iprop_0` (`IPROP_ID`),
  KEY `ix_b_iblock_element_iprop_1` (`IBLOCK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_element_lock`
--

CREATE TABLE IF NOT EXISTS `b_iblock_element_lock` (
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `DATE_LOCK` datetime DEFAULT NULL,
  `LOCKED_BY` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ELEMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_element_property`
--

CREATE TABLE IF NOT EXISTS `b_iblock_element_property` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_PROPERTY_ID` int(11) NOT NULL,
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  `VALUE_TYPE` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `VALUE_ENUM` int(11) DEFAULT NULL,
  `VALUE_NUM` decimal(18,4) DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_element_property_1` (`IBLOCK_ELEMENT_ID`,`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_element_property_2` (`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_element_prop_enum` (`VALUE_ENUM`,`IBLOCK_PROPERTY_ID`),
  KEY `ix_iblock_element_prop_num` (`VALUE_NUM`,`IBLOCK_PROPERTY_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=62 ;

--
-- Dumping data for table `b_iblock_element_property`
--

INSERT INTO `b_iblock_element_property` (`ID`, `IBLOCK_PROPERTY_ID`, `IBLOCK_ELEMENT_ID`, `VALUE`, `VALUE_TYPE`, `VALUE_ENUM`, `VALUE_NUM`, `DESCRIPTION`) VALUES
(1, 10, 41, 'г. Нижний Новгород', 'text', NULL, '0.0000', ''),
(2, 10, 42, 'г. Москва', 'text', NULL, '0.0000', ''),
(3, 10, 43, 'г. Нижний Новгород', 'text', NULL, '0.0000', ''),
(4, 11, 44, '12', 'text', NULL, '12.0000', ''),
(5, 12, 44, '5346', 'text', NULL, '5346.0000', ''),
(6, 13, 44, '346', 'text', NULL, '346.0000', ''),
(7, 14, 44, '4536', 'text', NULL, '4536.0000', ''),
(8, 15, 44, '6444', 'text', NULL, '6444.0000', ''),
(9, 16, 44, '2344', 'text', NULL, '2344.0000', ''),
(10, 17, 44, '940', 'text', NULL, '940.0000', ''),
(11, 18, 44, '1455', 'text', NULL, '1455.0000', ''),
(12, 19, 44, '1830', 'text', NULL, '1830.0000', ''),
(13, 20, 44, '1000', 'text', NULL, '1000.0000', ''),
(14, 21, 44, '2342', 'text', NULL, '2342.0000', ''),
(15, 22, 44, '5546', 'text', NULL, '5546.0000', ''),
(16, 23, 44, '5326', 'text', NULL, '5326.0000', ''),
(17, 24, 44, '343', 'text', NULL, '343.0000', ''),
(18, 25, 44, '2', 'text', NULL, '2.0000', ''),
(19, 26, 44, '3554', 'text', NULL, '3554.0000', ''),
(20, 27, 44, '234', 'text', NULL, '234.0000', ''),
(21, 28, 44, '2654', 'text', NULL, '2654.0000', ''),
(22, 11, 45, '12', 'text', NULL, '12.0000', ''),
(23, 12, 45, '5346', 'text', NULL, '5346.0000', ''),
(24, 13, 45, '346', 'text', NULL, '346.0000', ''),
(25, 14, 45, '4536', 'text', NULL, '4536.0000', ''),
(26, 15, 45, '6444', 'text', NULL, '6444.0000', ''),
(27, 16, 45, '2344', 'text', NULL, '2344.0000', ''),
(28, 17, 45, '940', 'text', NULL, '940.0000', ''),
(29, 18, 45, '1455', 'text', NULL, '1455.0000', ''),
(30, 19, 45, '1830', 'text', NULL, '1830.0000', ''),
(31, 20, 45, '1000', 'text', NULL, '1000.0000', ''),
(32, 21, 45, '2342', 'text', NULL, '2342.0000', ''),
(33, 22, 45, '5546', 'text', NULL, '5546.0000', ''),
(34, 23, 45, '5326', 'text', NULL, '5326.0000', ''),
(35, 24, 45, '343', 'text', NULL, '343.0000', ''),
(36, 25, 45, '2', 'text', NULL, '2.0000', ''),
(37, 26, 45, '3554', 'text', NULL, '3554.0000', ''),
(38, 27, 45, '234', 'text', NULL, '234.0000', ''),
(39, 28, 45, '2654', 'text', NULL, '2654.0000', ''),
(40, 29, 45, '5', 'text', 5, NULL, NULL),
(41, 11, 46, '12', 'text', NULL, '12.0000', ''),
(42, 12, 46, '5346', 'text', NULL, '5346.0000', ''),
(43, 13, 46, '346', 'text', NULL, '346.0000', ''),
(44, 14, 46, '4536', 'text', NULL, '4536.0000', ''),
(45, 15, 46, '6444', 'text', NULL, '6444.0000', ''),
(46, 16, 46, '2344', 'text', NULL, '2344.0000', ''),
(47, 17, 46, '940', 'text', NULL, '940.0000', ''),
(48, 18, 46, '1455', 'text', NULL, '1455.0000', ''),
(49, 19, 46, '1830', 'text', NULL, '1830.0000', ''),
(50, 20, 46, '1000', 'text', NULL, '1000.0000', ''),
(51, 21, 46, '2342', 'text', NULL, '2342.0000', ''),
(52, 22, 46, '5546', 'text', NULL, '5546.0000', ''),
(53, 23, 46, '5326', 'text', NULL, '5326.0000', ''),
(54, 24, 46, '343', 'text', NULL, '343.0000', ''),
(55, 25, 46, '2', 'text', NULL, '2.0000', ''),
(56, 26, 46, '3554', 'text', NULL, '3554.0000', ''),
(57, 27, 46, '234', 'text', NULL, '234.0000', ''),
(58, 28, 46, '2654', 'text', NULL, '2654.0000', ''),
(60, 29, 46, '5', 'text', 5, NULL, NULL),
(61, 30, 46, '6', 'text', 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_element_right`
--

CREATE TABLE IF NOT EXISTS `b_iblock_element_right` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `ELEMENT_ID` int(11) NOT NULL,
  `RIGHT_ID` int(11) NOT NULL,
  `IS_INHERITED` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RIGHT_ID`,`ELEMENT_ID`,`SECTION_ID`),
  KEY `ix_b_iblock_element_right_1` (`ELEMENT_ID`,`IBLOCK_ID`),
  KEY `ix_b_iblock_element_right_2` (`IBLOCK_ID`,`RIGHT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_fields`
--

CREATE TABLE IF NOT EXISTS `b_iblock_fields` (
  `IBLOCK_ID` int(18) NOT NULL,
  `FIELD_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `IS_REQUIRED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEFAULT_VALUE` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`IBLOCK_ID`,`FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_iblock_fields`
--

INSERT INTO `b_iblock_fields` (`IBLOCK_ID`, `FIELD_ID`, `IS_REQUIRED`, `DEFAULT_VALUE`) VALUES
(1, 'ACTIVE', 'Y', 'Y'),
(1, 'ACTIVE_FROM', 'N', '=today'),
(1, 'ACTIVE_TO', 'N', ''),
(1, 'CODE', 'Y', 'a:8:{s:6:"UNIQUE";s:1:"Y";s:15:"TRANSLITERATION";s:1:"Y";s:9:"TRANS_LEN";i:100;s:10:"TRANS_CASE";s:1:"L";s:11:"TRANS_SPACE";s:1:"-";s:11:"TRANS_OTHER";s:1:"-";s:9:"TRANS_EAT";s:1:"Y";s:10:"USE_GOOGLE";s:1:"N";}'),
(1, 'DETAIL_PICTURE', 'N', 'a:17:{s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:0:"";s:11:"COMPRESSION";s:0:"";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(1, 'DETAIL_TEXT', 'N', ''),
(1, 'DETAIL_TEXT_TYPE', 'Y', 'text'),
(1, 'IBLOCK_SECTION', 'N', ''),
(1, 'LOG_ELEMENT_ADD', 'N', NULL),
(1, 'LOG_ELEMENT_DELETE', 'N', NULL),
(1, 'LOG_ELEMENT_EDIT', 'N', NULL),
(1, 'LOG_SECTION_ADD', 'N', NULL),
(1, 'LOG_SECTION_DELETE', 'N', NULL),
(1, 'LOG_SECTION_EDIT', 'N', NULL),
(1, 'NAME', 'Y', ''),
(1, 'PREVIEW_PICTURE', 'N', 'a:20:{s:11:"FROM_DETAIL";s:1:"N";s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:0:"";s:11:"COMPRESSION";s:0:"";s:18:"DELETE_WITH_DETAIL";s:1:"N";s:18:"UPDATE_WITH_DETAIL";s:1:"N";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(1, 'PREVIEW_TEXT', 'N', ''),
(1, 'PREVIEW_TEXT_TYPE', 'Y', 'text'),
(1, 'SECTION_CODE', 'N', 'a:8:{s:6:"UNIQUE";s:1:"N";s:15:"TRANSLITERATION";s:1:"N";s:9:"TRANS_LEN";i:100;s:10:"TRANS_CASE";s:1:"L";s:11:"TRANS_SPACE";s:1:"-";s:11:"TRANS_OTHER";s:1:"-";s:9:"TRANS_EAT";s:1:"Y";s:10:"USE_GOOGLE";s:1:"N";}'),
(1, 'SECTION_DESCRIPTION', 'N', ''),
(1, 'SECTION_DESCRIPTION_TYPE', 'Y', 'text'),
(1, 'SECTION_DETAIL_PICTURE', 'N', 'a:17:{s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(1, 'SECTION_NAME', 'Y', ''),
(1, 'SECTION_PICTURE', 'N', 'a:20:{s:11:"FROM_DETAIL";s:1:"N";s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"DELETE_WITH_DETAIL";s:1:"N";s:18:"UPDATE_WITH_DETAIL";s:1:"N";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(1, 'SECTION_XML_ID', 'N', ''),
(1, 'SORT', 'N', '0'),
(1, 'TAGS', 'N', ''),
(1, 'XML_ID', 'N', ''),
(1, 'XML_IMPORT_START_TIME', 'N', '2015-04-22 17:43:33'),
(2, 'ACTIVE', 'Y', 'Y'),
(2, 'ACTIVE_FROM', 'N', ''),
(2, 'ACTIVE_TO', 'N', ''),
(2, 'CODE', 'Y', 'a:8:{s:6:"UNIQUE";s:1:"Y";s:15:"TRANSLITERATION";s:1:"Y";s:9:"TRANS_LEN";i:100;s:10:"TRANS_CASE";s:1:"L";s:11:"TRANS_SPACE";s:1:"-";s:11:"TRANS_OTHER";s:1:"-";s:9:"TRANS_EAT";s:1:"Y";s:10:"USE_GOOGLE";s:1:"N";}'),
(2, 'DETAIL_PICTURE', 'N', 'a:17:{s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:0:"";s:11:"COMPRESSION";s:0:"";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(2, 'DETAIL_TEXT', 'N', ''),
(2, 'DETAIL_TEXT_TYPE', 'Y', 'text'),
(2, 'IBLOCK_SECTION', 'N', ''),
(2, 'LOG_ELEMENT_ADD', 'N', NULL),
(2, 'LOG_ELEMENT_DELETE', 'N', NULL),
(2, 'LOG_ELEMENT_EDIT', 'N', NULL),
(2, 'LOG_SECTION_ADD', 'N', NULL),
(2, 'LOG_SECTION_DELETE', 'N', NULL),
(2, 'LOG_SECTION_EDIT', 'N', NULL),
(2, 'NAME', 'Y', ''),
(2, 'PREVIEW_PICTURE', 'N', 'a:20:{s:11:"FROM_DETAIL";s:1:"N";s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:0:"";s:11:"COMPRESSION";s:0:"";s:18:"DELETE_WITH_DETAIL";s:1:"N";s:18:"UPDATE_WITH_DETAIL";s:1:"N";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(2, 'PREVIEW_TEXT', 'N', ''),
(2, 'PREVIEW_TEXT_TYPE', 'Y', 'text'),
(2, 'SECTION_CODE', 'Y', 'a:8:{s:6:"UNIQUE";s:1:"Y";s:15:"TRANSLITERATION";s:1:"Y";s:9:"TRANS_LEN";i:100;s:10:"TRANS_CASE";s:1:"L";s:11:"TRANS_SPACE";s:1:"-";s:11:"TRANS_OTHER";s:1:"-";s:9:"TRANS_EAT";s:1:"Y";s:10:"USE_GOOGLE";s:1:"N";}'),
(2, 'SECTION_DESCRIPTION', 'N', ''),
(2, 'SECTION_DESCRIPTION_TYPE', 'Y', 'text'),
(2, 'SECTION_DETAIL_PICTURE', 'N', 'a:17:{s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(2, 'SECTION_NAME', 'Y', ''),
(2, 'SECTION_PICTURE', 'N', 'a:20:{s:11:"FROM_DETAIL";s:1:"N";s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"DELETE_WITH_DETAIL";s:1:"N";s:18:"UPDATE_WITH_DETAIL";s:1:"N";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(2, 'SECTION_XML_ID', 'N', ''),
(2, 'SORT', 'N', '0'),
(2, 'TAGS', 'N', ''),
(2, 'XML_ID', 'N', ''),
(2, 'XML_IMPORT_START_TIME', 'N', '2015-04-22 17:43:40'),
(3, 'ACTIVE', 'Y', 'Y'),
(3, 'ACTIVE_FROM', 'N', ''),
(3, 'ACTIVE_TO', 'N', ''),
(3, 'CODE', 'N', ''),
(3, 'DETAIL_PICTURE', 'N', 'a:17:{s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:0:"";s:11:"COMPRESSION";s:0:"";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";N;s:19:"WATERMARK_TEXT_FONT";N;s:20:"WATERMARK_TEXT_COLOR";N;s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";N;s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";N;s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";N;s:20:"WATERMARK_FILE_ORDER";N;}'),
(3, 'DETAIL_TEXT', 'N', ''),
(3, 'DETAIL_TEXT_TYPE', 'Y', 'text'),
(3, 'IBLOCK_SECTION', 'N', ''),
(3, 'LOG_ELEMENT_ADD', 'N', NULL),
(3, 'LOG_ELEMENT_DELETE', 'N', NULL),
(3, 'LOG_ELEMENT_EDIT', 'N', NULL),
(3, 'LOG_SECTION_ADD', 'N', NULL),
(3, 'LOG_SECTION_DELETE', 'N', NULL),
(3, 'LOG_SECTION_EDIT', 'N', NULL),
(3, 'NAME', 'Y', ''),
(3, 'PREVIEW_PICTURE', 'N', 'a:20:{s:11:"FROM_DETAIL";s:1:"N";s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:0:"";s:11:"COMPRESSION";s:0:"";s:18:"DELETE_WITH_DETAIL";s:1:"N";s:18:"UPDATE_WITH_DETAIL";s:1:"N";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";N;s:19:"WATERMARK_TEXT_FONT";N;s:20:"WATERMARK_TEXT_COLOR";N;s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";N;s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";N;s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";N;s:20:"WATERMARK_FILE_ORDER";N;}'),
(3, 'PREVIEW_TEXT', 'N', ''),
(3, 'PREVIEW_TEXT_TYPE', 'Y', 'text'),
(3, 'SECTION_CODE', 'N', 'a:8:{s:6:"UNIQUE";s:1:"N";s:15:"TRANSLITERATION";s:1:"N";s:9:"TRANS_LEN";i:100;s:10:"TRANS_CASE";s:1:"L";s:11:"TRANS_SPACE";s:1:"-";s:11:"TRANS_OTHER";s:1:"-";s:9:"TRANS_EAT";s:1:"Y";s:10:"USE_GOOGLE";s:1:"N";}'),
(3, 'SECTION_DESCRIPTION', 'N', NULL),
(3, 'SECTION_DESCRIPTION_TYPE', 'Y', NULL),
(3, 'SECTION_DETAIL_PICTURE', 'N', 'a:17:{s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";N;s:19:"WATERMARK_TEXT_FONT";N;s:20:"WATERMARK_TEXT_COLOR";N;s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";N;s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";N;s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";N;s:20:"WATERMARK_FILE_ORDER";N;}'),
(3, 'SECTION_NAME', 'Y', NULL),
(3, 'SECTION_PICTURE', 'N', 'a:20:{s:11:"FROM_DETAIL";s:1:"N";s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"DELETE_WITH_DETAIL";s:1:"N";s:18:"UPDATE_WITH_DETAIL";s:1:"N";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";N;s:19:"WATERMARK_TEXT_FONT";N;s:20:"WATERMARK_TEXT_COLOR";N;s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";N;s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";N;s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";N;s:20:"WATERMARK_FILE_ORDER";N;}'),
(3, 'SECTION_XML_ID', 'N', NULL),
(3, 'SORT', 'N', '0'),
(3, 'TAGS', 'N', ''),
(3, 'XML_ID', 'N', ''),
(3, 'XML_IMPORT_START_TIME', 'N', '2015-04-22 17:43:47'),
(5, 'ACTIVE', 'Y', 'Y'),
(5, 'ACTIVE_FROM', 'N', ''),
(5, 'ACTIVE_TO', 'N', ''),
(5, 'CODE', 'N', 'a:8:{s:6:"UNIQUE";s:1:"N";s:15:"TRANSLITERATION";s:1:"N";s:9:"TRANS_LEN";i:100;s:10:"TRANS_CASE";s:1:"L";s:11:"TRANS_SPACE";s:1:"-";s:11:"TRANS_OTHER";s:1:"-";s:9:"TRANS_EAT";s:1:"Y";s:10:"USE_GOOGLE";s:1:"N";}'),
(5, 'DETAIL_PICTURE', 'N', 'a:17:{s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(5, 'DETAIL_TEXT', 'N', ''),
(5, 'DETAIL_TEXT_TYPE', 'Y', 'text'),
(5, 'IBLOCK_SECTION', 'N', ''),
(5, 'LOG_ELEMENT_ADD', 'N', NULL),
(5, 'LOG_ELEMENT_DELETE', 'N', NULL),
(5, 'LOG_ELEMENT_EDIT', 'N', NULL),
(5, 'LOG_SECTION_ADD', 'N', NULL),
(5, 'LOG_SECTION_DELETE', 'N', NULL),
(5, 'LOG_SECTION_EDIT', 'N', NULL),
(5, 'NAME', 'Y', ''),
(5, 'PREVIEW_PICTURE', 'N', 'a:20:{s:11:"FROM_DETAIL";s:1:"N";s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"DELETE_WITH_DETAIL";s:1:"N";s:18:"UPDATE_WITH_DETAIL";s:1:"N";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(5, 'PREVIEW_TEXT', 'N', ''),
(5, 'PREVIEW_TEXT_TYPE', 'Y', 'text'),
(5, 'SECTION_CODE', 'N', 'a:8:{s:6:"UNIQUE";s:1:"N";s:15:"TRANSLITERATION";s:1:"N";s:9:"TRANS_LEN";i:100;s:10:"TRANS_CASE";s:1:"L";s:11:"TRANS_SPACE";s:1:"-";s:11:"TRANS_OTHER";s:1:"-";s:9:"TRANS_EAT";s:1:"Y";s:10:"USE_GOOGLE";s:1:"N";}'),
(5, 'SECTION_DESCRIPTION', 'N', ''),
(5, 'SECTION_DESCRIPTION_TYPE', 'Y', 'text'),
(5, 'SECTION_DETAIL_PICTURE', 'N', 'a:17:{s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(5, 'SECTION_NAME', 'Y', ''),
(5, 'SECTION_PICTURE', 'N', 'a:20:{s:11:"FROM_DETAIL";s:1:"N";s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"DELETE_WITH_DETAIL";s:1:"N";s:18:"UPDATE_WITH_DETAIL";s:1:"N";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(5, 'SECTION_XML_ID', 'N', ''),
(5, 'SORT', 'N', '0'),
(5, 'TAGS', 'N', ''),
(5, 'XML_ID', 'N', ''),
(5, 'XML_IMPORT_START_TIME', 'N', NULL),
(6, 'ACTIVE', 'Y', 'Y'),
(6, 'ACTIVE_FROM', 'N', '=now'),
(6, 'ACTIVE_TO', 'N', ''),
(6, 'CODE', 'Y', 'a:8:{s:6:"UNIQUE";s:1:"Y";s:15:"TRANSLITERATION";s:1:"Y";s:9:"TRANS_LEN";i:100;s:10:"TRANS_CASE";s:1:"L";s:11:"TRANS_SPACE";s:1:"-";s:11:"TRANS_OTHER";s:1:"-";s:9:"TRANS_EAT";s:1:"Y";s:10:"USE_GOOGLE";s:1:"N";}'),
(6, 'DETAIL_PICTURE', 'N', 'a:17:{s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(6, 'DETAIL_TEXT', 'N', ''),
(6, 'DETAIL_TEXT_TYPE', 'Y', 'text'),
(6, 'IBLOCK_SECTION', 'N', ''),
(6, 'LOG_ELEMENT_ADD', 'N', NULL),
(6, 'LOG_ELEMENT_DELETE', 'N', NULL),
(6, 'LOG_ELEMENT_EDIT', 'N', NULL),
(6, 'LOG_SECTION_ADD', 'N', NULL),
(6, 'LOG_SECTION_DELETE', 'N', NULL),
(6, 'LOG_SECTION_EDIT', 'N', NULL),
(6, 'NAME', 'Y', ''),
(6, 'PREVIEW_PICTURE', 'N', 'a:20:{s:11:"FROM_DETAIL";s:1:"N";s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"DELETE_WITH_DETAIL";s:1:"N";s:18:"UPDATE_WITH_DETAIL";s:1:"N";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(6, 'PREVIEW_TEXT', 'N', ''),
(6, 'PREVIEW_TEXT_TYPE', 'Y', 'text'),
(6, 'SECTION_CODE', 'N', 'a:8:{s:6:"UNIQUE";s:1:"N";s:15:"TRANSLITERATION";s:1:"N";s:9:"TRANS_LEN";i:100;s:10:"TRANS_CASE";s:1:"L";s:11:"TRANS_SPACE";s:1:"-";s:11:"TRANS_OTHER";s:1:"-";s:9:"TRANS_EAT";s:1:"Y";s:10:"USE_GOOGLE";s:1:"N";}'),
(6, 'SECTION_DESCRIPTION', 'N', ''),
(6, 'SECTION_DESCRIPTION_TYPE', 'Y', 'text'),
(6, 'SECTION_DETAIL_PICTURE', 'N', 'a:17:{s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(6, 'SECTION_NAME', 'Y', ''),
(6, 'SECTION_PICTURE', 'N', 'a:20:{s:11:"FROM_DETAIL";s:1:"N";s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"DELETE_WITH_DETAIL";s:1:"N";s:18:"UPDATE_WITH_DETAIL";s:1:"N";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(6, 'SECTION_XML_ID', 'N', ''),
(6, 'SORT', 'N', '0'),
(6, 'TAGS', 'N', ''),
(6, 'XML_ID', 'N', ''),
(6, 'XML_IMPORT_START_TIME', 'N', NULL),
(7, 'ACTIVE', 'Y', 'Y'),
(7, 'ACTIVE_FROM', 'N', ''),
(7, 'ACTIVE_TO', 'N', ''),
(7, 'CODE', 'Y', 'a:8:{s:6:"UNIQUE";s:1:"Y";s:15:"TRANSLITERATION";s:1:"Y";s:9:"TRANS_LEN";i:100;s:10:"TRANS_CASE";s:1:"L";s:11:"TRANS_SPACE";s:1:"-";s:11:"TRANS_OTHER";s:1:"-";s:9:"TRANS_EAT";s:1:"Y";s:10:"USE_GOOGLE";s:1:"N";}'),
(7, 'DETAIL_PICTURE', 'N', 'a:17:{s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(7, 'DETAIL_TEXT', 'N', ''),
(7, 'DETAIL_TEXT_TYPE', 'Y', 'text'),
(7, 'IBLOCK_SECTION', 'N', ''),
(7, 'LOG_ELEMENT_ADD', 'N', NULL),
(7, 'LOG_ELEMENT_DELETE', 'N', NULL),
(7, 'LOG_ELEMENT_EDIT', 'N', NULL),
(7, 'LOG_SECTION_ADD', 'N', NULL),
(7, 'LOG_SECTION_DELETE', 'N', NULL),
(7, 'LOG_SECTION_EDIT', 'N', NULL),
(7, 'NAME', 'Y', ''),
(7, 'PREVIEW_PICTURE', 'N', 'a:20:{s:11:"FROM_DETAIL";s:1:"N";s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"DELETE_WITH_DETAIL";s:1:"N";s:18:"UPDATE_WITH_DETAIL";s:1:"N";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(7, 'PREVIEW_TEXT', 'N', ''),
(7, 'PREVIEW_TEXT_TYPE', 'Y', 'text'),
(7, 'SECTION_CODE', 'Y', 'a:8:{s:6:"UNIQUE";s:1:"Y";s:15:"TRANSLITERATION";s:1:"Y";s:9:"TRANS_LEN";i:100;s:10:"TRANS_CASE";s:1:"L";s:11:"TRANS_SPACE";s:1:"-";s:11:"TRANS_OTHER";s:1:"-";s:9:"TRANS_EAT";s:1:"Y";s:10:"USE_GOOGLE";s:1:"N";}'),
(7, 'SECTION_DESCRIPTION', 'N', ''),
(7, 'SECTION_DESCRIPTION_TYPE', 'Y', 'text'),
(7, 'SECTION_DETAIL_PICTURE', 'N', 'a:17:{s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(7, 'SECTION_NAME', 'Y', ''),
(7, 'SECTION_PICTURE', 'N', 'a:20:{s:11:"FROM_DETAIL";s:1:"N";s:5:"SCALE";s:1:"N";s:5:"WIDTH";s:0:"";s:6:"HEIGHT";s:0:"";s:13:"IGNORE_ERRORS";s:1:"N";s:6:"METHOD";s:8:"resample";s:11:"COMPRESSION";i:95;s:18:"DELETE_WITH_DETAIL";s:1:"N";s:18:"UPDATE_WITH_DETAIL";s:1:"N";s:18:"USE_WATERMARK_TEXT";s:1:"N";s:14:"WATERMARK_TEXT";s:0:"";s:19:"WATERMARK_TEXT_FONT";s:0:"";s:20:"WATERMARK_TEXT_COLOR";s:0:"";s:19:"WATERMARK_TEXT_SIZE";s:0:"";s:23:"WATERMARK_TEXT_POSITION";s:2:"tl";s:18:"USE_WATERMARK_FILE";s:1:"N";s:14:"WATERMARK_FILE";s:0:"";s:20:"WATERMARK_FILE_ALPHA";s:0:"";s:23:"WATERMARK_FILE_POSITION";s:2:"tl";s:20:"WATERMARK_FILE_ORDER";N;}'),
(7, 'SECTION_XML_ID', 'N', ''),
(7, 'SORT', 'N', '0'),
(7, 'TAGS', 'N', ''),
(7, 'XML_ID', 'N', ''),
(7, 'XML_IMPORT_START_TIME', 'N', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_group`
--

CREATE TABLE IF NOT EXISTS `b_iblock_group` (
  `IBLOCK_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `PERMISSION` char(1) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `ux_iblock_group_1` (`IBLOCK_ID`,`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_iblock_group`
--

INSERT INTO `b_iblock_group` (`IBLOCK_ID`, `GROUP_ID`, `PERMISSION`) VALUES
(1, 1, 'X'),
(1, 2, 'R'),
(1, 5, 'W'),
(2, 1, 'X'),
(2, 2, 'R'),
(2, 5, 'W'),
(3, 1, 'X'),
(3, 2, 'R'),
(3, 5, 'W'),
(5, 1, 'X'),
(6, 1, 'X'),
(6, 2, 'R'),
(7, 1, 'X'),
(7, 2, 'R');

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_iblock_iprop`
--

CREATE TABLE IF NOT EXISTS `b_iblock_iblock_iprop` (
  `IBLOCK_ID` int(11) NOT NULL,
  `IPROP_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`IPROP_ID`),
  KEY `ix_b_iblock_iblock_iprop_0` (`IPROP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_iproperty`
--

CREATE TABLE IF NOT EXISTS `b_iblock_iproperty` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ID` int(11) NOT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `TEMPLATE` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_iblock_iprop_0` (`IBLOCK_ID`,`ENTITY_TYPE`,`ENTITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_messages`
--

CREATE TABLE IF NOT EXISTS `b_iblock_messages` (
  `IBLOCK_ID` int(18) NOT NULL,
  `MESSAGE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE_TEXT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`MESSAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_iblock_messages`
--

INSERT INTO `b_iblock_messages` (`IBLOCK_ID`, `MESSAGE_ID`, `MESSAGE_TEXT`) VALUES
(1, 'ELEMENT_ADD', 'Добавить новость'),
(1, 'ELEMENT_DELETE', 'Удалить новость'),
(1, 'ELEMENT_EDIT', 'Изменить новость'),
(1, 'ELEMENT_NAME', 'Новость'),
(1, 'ELEMENTS_NAME', 'Новости'),
(1, 'SECTION_ADD', 'Добавить раздел'),
(1, 'SECTION_DELETE', 'Удалить раздел'),
(1, 'SECTION_EDIT', 'Изменить раздел'),
(1, 'SECTION_NAME', 'Раздел'),
(1, 'SECTIONS_NAME', 'Разделы'),
(2, 'ELEMENT_ADD', 'Добавить товар'),
(2, 'ELEMENT_DELETE', 'Удалить товар'),
(2, 'ELEMENT_EDIT', 'Изменить товар'),
(2, 'ELEMENT_NAME', 'Товар'),
(2, 'ELEMENTS_NAME', 'Товары'),
(2, 'SECTION_ADD', 'Добавить раздел'),
(2, 'SECTION_DELETE', 'Удалить раздел'),
(2, 'SECTION_EDIT', 'Изменить раздел'),
(2, 'SECTION_NAME', 'Раздел'),
(2, 'SECTIONS_NAME', 'Разделы'),
(3, 'ELEMENT_ADD', 'Добавить услугу'),
(3, 'ELEMENT_DELETE', 'Удалить услугу'),
(3, 'ELEMENT_EDIT', 'Изменить услугу'),
(3, 'ELEMENT_NAME', 'Услуга'),
(3, 'ELEMENTS_NAME', 'Услуги'),
(3, 'SECTION_ADD', 'Добавить раздел'),
(3, 'SECTION_DELETE', 'Удалить раздел'),
(3, 'SECTION_EDIT', 'Изменить раздел'),
(3, 'SECTION_NAME', 'Раздел'),
(3, 'SECTIONS_NAME', 'Разделы'),
(5, 'ELEMENT_ADD', 'Добавить сообщение'),
(5, 'ELEMENT_DELETE', 'Удалить сообщение'),
(5, 'ELEMENT_EDIT', 'Изменить сообщение'),
(5, 'ELEMENT_NAME', 'Сообщение'),
(5, 'ELEMENTS_NAME', 'Сообщения'),
(5, 'SECTION_ADD', 'Добавить тему сообщений'),
(5, 'SECTION_DELETE', 'Удалить тему сообщений'),
(5, 'SECTION_EDIT', 'Изменить тему сообщений'),
(5, 'SECTION_NAME', 'Тема сообщений'),
(5, 'SECTIONS_NAME', 'Темы сообщений'),
(6, 'ELEMENT_ADD', 'Добавить статью'),
(6, 'ELEMENT_DELETE', 'Удалить статью'),
(6, 'ELEMENT_EDIT', 'Изменить статью'),
(6, 'ELEMENT_NAME', 'Статья'),
(6, 'ELEMENTS_NAME', 'Статьи'),
(7, 'ELEMENT_ADD', 'Добавить проект'),
(7, 'ELEMENT_DELETE', 'Удалить проект'),
(7, 'ELEMENT_EDIT', 'Изменить проект'),
(7, 'ELEMENT_NAME', 'Проект'),
(7, 'ELEMENTS_NAME', 'Проекты'),
(7, 'SECTION_ADD', 'Добавить раздел'),
(7, 'SECTION_DELETE', 'Удалить раздел'),
(7, 'SECTION_EDIT', 'Изменить раздел'),
(7, 'SECTION_NAME', 'Раздел'),
(7, 'SECTIONS_NAME', 'Разделы');

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_offers_tmp`
--

CREATE TABLE IF NOT EXISTS `b_iblock_offers_tmp` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PRODUCT_IBLOCK_ID` int(11) unsigned NOT NULL,
  `OFFERS_IBLOCK_ID` int(11) unsigned NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_property`
--

CREATE TABLE IF NOT EXISTS `b_iblock_property` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IBLOCK_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEFAULT_VALUE` text COLLATE utf8_unicode_ci,
  `PROPERTY_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `ROW_COUNT` int(11) NOT NULL DEFAULT '1',
  `COL_COUNT` int(11) NOT NULL DEFAULT '30',
  `LIST_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'L',
  `MULTIPLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `XML_ID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILE_TYPE` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MULTIPLE_CNT` int(11) DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LINK_IBLOCK_ID` int(18) DEFAULT NULL,
  `WITH_DESCRIPTION` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEARCHABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `FILTRABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_REQUIRED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(11) NOT NULL DEFAULT '1',
  `USER_TYPE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_TYPE_SETTINGS` text COLLATE utf8_unicode_ci,
  `HINT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_property_1` (`IBLOCK_ID`),
  KEY `ix_iblock_property_3` (`LINK_IBLOCK_ID`),
  KEY `ix_iblock_property_2` (`CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

--
-- Dumping data for table `b_iblock_property`
--

INSERT INTO `b_iblock_property` (`ID`, `TIMESTAMP_X`, `IBLOCK_ID`, `NAME`, `ACTIVE`, `SORT`, `CODE`, `DEFAULT_VALUE`, `PROPERTY_TYPE`, `ROW_COUNT`, `COL_COUNT`, `LIST_TYPE`, `MULTIPLE`, `XML_ID`, `FILE_TYPE`, `MULTIPLE_CNT`, `TMP_ID`, `LINK_IBLOCK_ID`, `WITH_DESCRIPTION`, `SEARCHABLE`, `FILTRABLE`, `IS_REQUIRED`, `VERSION`, `USER_TYPE`, `USER_TYPE_SETTINGS`, `HINT`) VALUES
(9, '2015-04-23 16:51:18', 5, 'Телефон', 'Y', 500, 'PHONE', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(10, '2015-04-27 15:36:25', 7, 'Город', 'Y', 500, 'CITY', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(11, '2015-04-28 08:51:27', 2, 'Количество одновременно выдуваемых бутылок', 'Y', 500, 'KOLICHESTVO', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(12, '2015-04-28 08:51:27', 2, 'Максимальная производительность, шт/час', 'Y', 500, 'PROIZVODITELNOST', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(13, '2015-04-28 08:51:27', 2, 'Максимальный объём выдуваемой тары, л', 'Y', 500, 'OBEM', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(14, '2015-04-28 08:51:27', 2, 'Максимальный наружный диаметр горловины бутылки, мм', 'Y', 500, 'NARUZHN_DIAMETR', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(15, '2015-04-28 08:51:27', 2, 'Максимальный диаметр выдуваемой бутылки, мм', 'Y', 500, 'DIAMETR_BUTILKI', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(16, '2015-04-28 08:51:27', 2, 'Максимальная высота выдуваемой бутылки, мм', 'Y', 500, 'VISOTA_BUTILKI', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(17, '2015-04-28 08:51:27', 2, 'Габаритные размеры, мм (длина)', 'Y', 500, 'DLINA', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(18, '2015-04-28 08:51:27', 2, 'Габаритные размеры, мм (ширина)', 'Y', 500, 'SHIRINA', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(19, '2015-04-28 08:51:27', 2, 'Габаритные размеры, мм (высота)', 'Y', 500, 'VISOTA', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(20, '2015-04-28 08:51:27', 2, 'Вес, кг', 'Y', 500, 'VES', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(21, '2015-04-28 08:51:27', 2, 'Усилие смыкания пресс-формы, кг', 'Y', 500, 'USILIE_PRESSFORMI', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(22, '2015-04-28 08:51:27', 2, 'Суммарная мощность нагревательных элементов, кВт ', 'Y', 500, 'MOSHCHNOST_NAGREVA', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(23, '2015-04-28 08:51:27', 2, 'Количество элементов в цепи переноса', 'Y', 500, 'KOLICHESTVO_ELEMENTOV', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(24, '2015-04-28 08:51:27', 2, 'Межосевое расстояние в цепи переноса преформ, мм ', 'Y', 500, 'MEZHOSEVOE_RASSOTYANIE', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(25, '2015-04-28 08:51:27', 2, 'Давление выдува, Бар ', 'Y', 500, 'DAVELNIE_VIDUVA', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(26, '2015-04-28 08:51:27', 2, 'Количество воздуха для выдува, л/мин', 'Y', 500, 'KOLICHESTVO_VOZDUKHA', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(27, '2015-04-28 08:51:27', 2, 'Рабочее давление системы управления, Бар', 'Y', 500, 'RABOCHEE_DAVLENIE', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(28, '2015-04-28 08:51:27', 2, 'Количество воздуха для системы управления, л/мин', 'Y', 500, 'KOLICHESTVO_VOZDUKHA_SISTEMI', '', 'S', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(29, '2015-04-28 08:56:14', 2, 'Доступно в лизинг', 'Y', 500, 'LIZING', '', 'L', 1, 30, 'C', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, ''),
(30, '2015-05-10 11:16:57', 2, 'Наличие на складе в Москве', 'Y', 500, 'NALICHIE', '', 'L', 1, 30, 'L', 'N', NULL, '', 5, NULL, 0, 'N', 'N', 'N', 'N', 1, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_property_enum`
--

CREATE TABLE IF NOT EXISTS `b_iblock_property_enum` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PROPERTY_ID` int(11) NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `XML_ID` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_iblock_property_enum` (`PROPERTY_ID`,`XML_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `b_iblock_property_enum`
--

INSERT INTO `b_iblock_property_enum` (`ID`, `PROPERTY_ID`, `VALUE`, `DEF`, `SORT`, `XML_ID`, `TMP_ID`) VALUES
(5, 29, 'Да', 'N', 500, 'fd5f587576069fe1e4dec671241d7b1c', NULL),
(6, 30, 'Да', 'N', 500, 'a7f205c4080d7878ab35eabeb337b33c', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_right`
--

CREATE TABLE IF NOT EXISTS `b_iblock_right` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ID` int(11) NOT NULL,
  `GROUP_CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_TYPE` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `DO_INHERIT` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `TASK_ID` int(11) NOT NULL,
  `OP_SREAD` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `OP_EREAD` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `XML_ID` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_iblock_right_iblock_id` (`IBLOCK_ID`,`ENTITY_TYPE`,`ENTITY_ID`),
  KEY `ix_b_iblock_right_group_code` (`GROUP_CODE`,`IBLOCK_ID`),
  KEY `ix_b_iblock_right_entity` (`ENTITY_ID`,`ENTITY_TYPE`),
  KEY `ix_b_iblock_right_op_eread` (`ID`,`OP_EREAD`,`GROUP_CODE`),
  KEY `ix_b_iblock_right_op_sread` (`ID`,`OP_SREAD`,`GROUP_CODE`),
  KEY `ix_b_iblock_right_task_id` (`TASK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_rss`
--

CREATE TABLE IF NOT EXISTS `b_iblock_rss` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ID` int(11) NOT NULL,
  `NODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NODE_VALUE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_section`
--

CREATE TABLE IF NOT EXISTS `b_iblock_section` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `IBLOCK_ID` int(11) NOT NULL,
  `IBLOCK_SECTION_ID` int(11) DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `GLOBAL_ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PICTURE` int(18) DEFAULT NULL,
  `LEFT_MARGIN` int(18) DEFAULT NULL,
  `RIGHT_MARGIN` int(18) DEFAULT NULL,
  `DEPTH_LEVEL` int(18) DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DESCRIPTION_TYPE` char(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `SEARCHABLE_CONTENT` text COLLATE utf8_unicode_ci,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TMP_ID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DETAIL_PICTURE` int(18) DEFAULT NULL,
  `SOCNET_GROUP_ID` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_iblock_section_1` (`IBLOCK_ID`,`IBLOCK_SECTION_ID`),
  KEY `ix_iblock_section_depth_level` (`IBLOCK_ID`,`DEPTH_LEVEL`),
  KEY `ix_iblock_section_left_margin` (`IBLOCK_ID`,`LEFT_MARGIN`,`RIGHT_MARGIN`),
  KEY `ix_iblock_section_right_margin` (`IBLOCK_ID`,`RIGHT_MARGIN`,`LEFT_MARGIN`),
  KEY `ix_iblock_section_code` (`IBLOCK_ID`,`CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

--
-- Dumping data for table `b_iblock_section`
--

INSERT INTO `b_iblock_section` (`ID`, `TIMESTAMP_X`, `MODIFIED_BY`, `DATE_CREATE`, `CREATED_BY`, `IBLOCK_ID`, `IBLOCK_SECTION_ID`, `ACTIVE`, `GLOBAL_ACTIVE`, `SORT`, `NAME`, `PICTURE`, `LEFT_MARGIN`, `RIGHT_MARGIN`, `DEPTH_LEVEL`, `DESCRIPTION`, `DESCRIPTION_TYPE`, `SEARCHABLE_CONTENT`, `CODE`, `XML_ID`, `TMP_ID`, `DETAIL_PICTURE`, `SOCNET_GROUP_ID`) VALUES
(5, '2015-04-23 15:32:19', 1, '2015-04-23 21:32:19', 1, 2, NULL, 'Y', 'Y', 500, 'Выдув ПЭТ-тары', NULL, 1, 16, 1, '', 'text', 'ВЫДУВ ПЭТ-ТАРЫ\r\n', 'vyduv-pet-tary', NULL, NULL, NULL, NULL),
(6, '2015-04-23 15:32:24', 1, '2015-04-23 21:32:24', 1, 2, NULL, 'Y', 'Y', 500, 'Оборудование розлива ', NULL, 21, 22, 1, '', 'text', 'ОБОРУДОВАНИЕ РОЗЛИВА \r\n', 'oborudovanie-rozliva-', NULL, NULL, NULL, NULL),
(7, '2015-04-23 15:32:48', 1, '2015-04-23 21:32:48', 1, 2, NULL, 'Y', 'Y', 500, 'Укупорочное оборудование', NULL, 31, 32, 1, '', 'text', 'УКУПОРОЧНОЕ ОБОРУДОВАНИЕ\r\n', 'ukuporochnoe-oborudovanie', NULL, NULL, NULL, NULL),
(8, '2015-04-23 15:32:55', 1, '2015-04-23 21:32:55', 1, 2, NULL, 'Y', 'Y', 500, 'Этикеровщики ', NULL, 35, 36, 1, '', 'text', 'ЭТИКЕРОВЩИКИ \r\n', 'etikerovshchiki-', NULL, NULL, NULL, NULL),
(9, '2015-04-23 15:33:00', 1, '2015-04-23 21:33:00', 1, 2, NULL, 'Y', 'Y', 500, 'Принтеры-датировщики ', NULL, 25, 26, 1, '', 'text', 'ПРИНТЕРЫ-ДАТИРОВЩИКИ \r\n', 'printery-datirovshchiki-', NULL, NULL, NULL, NULL),
(10, '2015-04-23 15:33:09', 1, '2015-04-23 21:33:09', 1, 2, NULL, 'Y', 'Y', 500, 'Упаковочное оборудование', NULL, 33, 34, 1, '', 'text', 'УПАКОВОЧНОЕ ОБОРУДОВАНИЕ\r\n', 'upakovochnoe-oborudovanie', NULL, NULL, NULL, NULL),
(11, '2015-04-23 15:33:16', 1, '2015-04-23 21:33:16', 1, 2, NULL, 'Y', 'Y', 500, 'Паллетообмотчики ', NULL, 23, 24, 1, '', 'text', 'ПАЛЛЕТООБМОТЧИКИ \r\n', 'palletoobmotchiki-', NULL, NULL, NULL, NULL),
(12, '2015-04-23 15:33:21', 1, '2015-04-23 21:33:21', 1, 2, NULL, 'Y', 'Y', 500, 'Конвейерные системы ', NULL, 19, 20, 1, '', 'text', 'КОНВЕЙЕРНЫЕ СИСТЕМЫ \r\n', 'konveyernye-sistemy-', NULL, NULL, NULL, NULL),
(13, '2015-04-23 15:33:26', 1, '2015-04-23 21:33:26', 1, 2, NULL, 'Y', 'Y', 500, 'Компрессоры ', NULL, 17, 18, 1, '', 'text', 'КОМПРЕССОРЫ \r\n', 'kompressory-', NULL, NULL, NULL, NULL),
(14, '2015-04-23 15:33:31', 1, '2015-04-23 21:33:31', 1, 2, NULL, 'Y', 'Y', 500, 'Сатураторы ', NULL, 29, 30, 1, '', 'text', 'САТУРАТОРЫ \r\n', 'saturatory-', NULL, NULL, NULL, NULL),
(15, '2015-04-23 15:33:37', 1, '2015-04-23 21:33:37', 1, 2, NULL, 'Y', 'Y', 500, 'Ресиверы', NULL, 27, 28, 1, '', 'text', 'РЕСИВЕРЫ\r\n', 'resivery', NULL, NULL, NULL, NULL),
(16, '2015-04-23 16:52:05', 1, '2015-04-23 22:52:05', 1, 5, NULL, 'Y', 'Y', 500, 'Запрос обратного звонка', NULL, 1, 2, 1, '', 'text', 'ЗАПРОС ОБРАТНОГО ЗВОНКА\r\n', '', NULL, NULL, NULL, NULL),
(17, '2015-04-23 16:52:14', 1, '2015-04-23 22:52:14', 1, 5, NULL, 'Y', 'Y', 500, 'Запрос прайс-листа', NULL, 3, 4, 1, '', 'text', 'ЗАПРОС ПРАЙС-ЛИСТА\r\n', '', NULL, NULL, NULL, NULL),
(18, '2015-04-23 16:52:38', 1, '2015-04-23 22:52:38', 1, 5, NULL, 'Y', 'Y', 500, 'Запрос связи с менеджером', NULL, 5, 6, 1, '', 'text', 'ЗАПРОС СВЯЗИ С МЕНЕДЖЕРОМ\r\n', '', NULL, NULL, NULL, NULL),
(19, '2015-04-23 16:52:57', 1, '2015-04-23 22:52:57', 1, 5, NULL, 'Y', 'Y', 500, 'Заявка на выставочную площадь', NULL, 7, 8, 1, '', 'text', 'ЗАЯВКА НА ВЫСТАВОЧНУЮ ПЛОЩАДЬ\r\n', '', NULL, NULL, NULL, NULL),
(20, '2015-04-27 15:31:27', 1, '2015-04-27 20:53:13', 1, 7, NULL, 'Y', 'Y', 500, 'Вода', NULL, 3, 4, 1, '', 'text', 'ВОДА\r\n', 'voda', NULL, NULL, NULL, NULL),
(21, '2015-04-27 15:31:17', 1, '2015-04-27 20:53:24', 1, 7, NULL, 'Y', 'Y', 500, 'Газированные напитки', NULL, 5, 6, 1, '', 'text', 'ГАЗИРОВАННЫЕ НАПИТКИ\r\n', 'gazirovannye-napitki', NULL, NULL, NULL, NULL),
(22, '2015-04-27 15:31:05', 1, '2015-04-27 20:53:38', 1, 7, NULL, 'Y', 'Y', 500, 'Бытовая химия и техжидкости', NULL, 1, 2, 1, '', 'text', 'БЫТОВАЯ ХИМИЯ И ТЕХЖИДКОСТИ\r\n', 'bytovaya-khimiya-i-tekhzhidkosti', NULL, NULL, NULL, NULL),
(23, '2015-04-27 15:30:52', 1, '2015-04-27 20:53:49', 1, 7, NULL, 'Y', 'Y', 500, 'Масло', NULL, 7, 8, 1, '', 'text', 'МАСЛО\r\n', 'maslo', NULL, NULL, NULL, NULL),
(24, '2015-05-10 03:51:36', 1, '2015-04-28 12:56:22', 1, 2, 29, 'Y', 'Y', 500, 'Тара до 2 литров', NULL, 11, 12, 3, '', 'text', 'ТАРА ДО 2 ЛИТРОВ\r\n', 'tara-do-2-litrov', NULL, NULL, NULL, NULL),
(25, '2015-05-10 03:51:36', 1, '2015-04-28 12:56:33', 1, 2, 29, 'Y', 'Y', 500, 'Тара более 2 литров', NULL, 9, 10, 3, '', 'text', 'ТАРА БОЛЕЕ 2 ЛИТРОВ\r\n', 'tara-bolee-2-litrov', NULL, NULL, NULL, NULL),
(26, '2015-05-10 03:51:36', 1, '2015-04-28 12:57:20', 1, 2, 29, 'Y', 'Y', 500, 'Косметика, толстостенная тара', NULL, 5, 6, 3, '', 'text', 'КОСМЕТИКА, ТОЛСТОСТЕННАЯ ТАРА\r\n', 'kosmetika-tolstostennaya-tara', NULL, NULL, NULL, NULL),
(27, '2015-05-10 03:51:36', 1, '2015-04-28 12:57:36', 1, 2, 29, 'Y', 'Y', 500, 'Банки, увеличенный диаметр горла', NULL, 3, 4, 3, '', 'text', 'БАНКИ, УВЕЛИЧЕННЫЙ ДИАМЕТР ГОРЛА\r\n', 'banki-uvelichennyy-diametr-gorla', NULL, NULL, NULL, NULL),
(28, '2015-05-10 03:51:36', 1, '2015-04-28 12:57:51', 1, 2, 29, 'Y', 'Y', 500, 'С автоустановкой ручки', NULL, 7, 8, 3, '', 'text', 'С АВТОУСТАНОВКОЙ РУЧКИ\r\n', 's-avtoustanovkoy-ruchki', NULL, NULL, NULL, NULL),
(29, '2015-05-10 03:48:31', 1, '2015-05-10 09:48:31', 1, 2, 5, 'Y', 'Y', 500, 'Автоматический выдув', NULL, 2, 13, 2, '', 'text', 'АВТОМАТИЧЕСКИЙ ВЫДУВ\r\n', 'avtomaticheskiy-vyduv', NULL, NULL, NULL, NULL),
(30, '2015-05-10 03:48:45', 1, '2015-05-10 09:48:45', 1, 2, 5, 'Y', 'Y', 500, 'Полуавтоматический вывод', NULL, 14, 15, 2, '', 'text', 'ПОЛУАВТОМАТИЧЕСКИЙ ВЫВОД\r\n', 'poluavtomaticheskiy-vyvod', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_section_element`
--

CREATE TABLE IF NOT EXISTS `b_iblock_section_element` (
  `IBLOCK_SECTION_ID` int(11) NOT NULL,
  `IBLOCK_ELEMENT_ID` int(11) NOT NULL,
  `ADDITIONAL_PROPERTY_ID` int(18) DEFAULT NULL,
  UNIQUE KEY `ux_iblock_section_element` (`IBLOCK_SECTION_ID`,`IBLOCK_ELEMENT_ID`,`ADDITIONAL_PROPERTY_ID`),
  KEY `UX_IBLOCK_SECTION_ELEMENT2` (`IBLOCK_ELEMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_iblock_section_element`
--

INSERT INTO `b_iblock_section_element` (`IBLOCK_SECTION_ID`, `IBLOCK_ELEMENT_ID`, `ADDITIONAL_PROPERTY_ID`) VALUES
(21, 42, NULL),
(22, 41, NULL),
(23, 43, NULL),
(24, 44, NULL),
(24, 45, NULL),
(24, 46, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_section_iprop`
--

CREATE TABLE IF NOT EXISTS `b_iblock_section_iprop` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `IPROP_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`SECTION_ID`,`IPROP_ID`),
  KEY `ix_b_iblock_section_iprop_0` (`IPROP_ID`),
  KEY `ix_b_iblock_section_iprop_1` (`IBLOCK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_section_property`
--

CREATE TABLE IF NOT EXISTS `b_iblock_section_property` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `PROPERTY_ID` int(11) NOT NULL,
  `SMART_FILTER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISPLAY_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISPLAY_EXPANDED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`SECTION_ID`,`PROPERTY_ID`),
  KEY `ix_b_iblock_section_property_1` (`PROPERTY_ID`),
  KEY `ix_b_iblock_section_property_2` (`SECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_section_right`
--

CREATE TABLE IF NOT EXISTS `b_iblock_section_right` (
  `IBLOCK_ID` int(11) NOT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `RIGHT_ID` int(11) NOT NULL,
  `IS_INHERITED` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RIGHT_ID`,`SECTION_ID`),
  KEY `ix_b_iblock_section_right_1` (`SECTION_ID`,`IBLOCK_ID`),
  KEY `ix_b_iblock_section_right_2` (`IBLOCK_ID`,`RIGHT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_sequence`
--

CREATE TABLE IF NOT EXISTS `b_iblock_sequence` (
  `IBLOCK_ID` int(18) NOT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SEQ_VALUE` int(11) DEFAULT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_site`
--

CREATE TABLE IF NOT EXISTS `b_iblock_site` (
  `IBLOCK_ID` int(18) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`IBLOCK_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_iblock_site`
--

INSERT INTO `b_iblock_site` (`IBLOCK_ID`, `SITE_ID`) VALUES
(1, 's1'),
(2, 's1'),
(3, 's1'),
(5, 's1'),
(6, 's1'),
(7, 's1');

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_type`
--

CREATE TABLE IF NOT EXISTS `b_iblock_type` (
  `ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SECTIONS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `EDIT_FILE_BEFORE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EDIT_FILE_AFTER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IN_RSS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SORT` int(18) NOT NULL DEFAULT '500',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_iblock_type`
--

INSERT INTO `b_iblock_type` (`ID`, `SECTIONS`, `EDIT_FILE_BEFORE`, `EDIT_FILE_AFTER`, `IN_RSS`, `SORT`) VALUES
('feedback', 'Y', '', '', 'N', 500),
('news', 'N', NULL, NULL, 'Y', 50),
('products', 'Y', NULL, NULL, 'N', 100),
('projects', 'Y', '', '', 'N', 500);

-- --------------------------------------------------------

--
-- Table structure for table `b_iblock_type_lang`
--

CREATE TABLE IF NOT EXISTS `b_iblock_type_lang` (
  `IBLOCK_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SECTION_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ELEMENT_NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_iblock_type_lang`
--

INSERT INTO `b_iblock_type_lang` (`IBLOCK_TYPE_ID`, `LID`, `NAME`, `SECTION_NAME`, `ELEMENT_NAME`) VALUES
('news', 'en', 'News', '', 'News'),
('news', 'ru', 'Новости', '', 'Новости'),
('products', 'en', 'Products and services', 'Sections', 'Products and services'),
('products', 'ru', 'Товары и услуги', 'Разделы', 'Товары и услуги'),
('feedback', 'ru', 'Обратная связь', '', 'Сообщения'),
('feedback', 'en', 'Обратная связь', '', 'Сообщения'),
('projects', 'ru', 'Проекты', '', 'Проекты'),
('projects', 'en', 'Проекты', '', 'Проекты');

-- --------------------------------------------------------

--
-- Table structure for table `b_lang`
--

CREATE TABLE IF NOT EXISTS `b_lang` (
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(18) NOT NULL DEFAULT '100',
  `DEF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DIR` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT_DATE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_DATETIME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEEK_START` int(11) DEFAULT NULL,
  `CHARSET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `DOC_ROOT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DOMAIN_LIMITED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SERVER_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SITE_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CULTURE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_lang`
--

INSERT INTO `b_lang` (`LID`, `SORT`, `DEF`, `ACTIVE`, `NAME`, `DIR`, `FORMAT_DATE`, `FORMAT_DATETIME`, `FORMAT_NAME`, `WEEK_START`, `CHARSET`, `LANGUAGE_ID`, `DOC_ROOT`, `DOMAIN_LIMITED`, `SERVER_NAME`, `SITE_NAME`, `EMAIL`, `CULTURE_ID`) VALUES
('s1', 1, 'Y', 'Y', 'Аквакультура', '/', NULL, NULL, NULL, NULL, NULL, 'ru', '', 'N', '', 'Аквакультура', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `b_language`
--

CREATE TABLE IF NOT EXISTS `b_language` (
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DEF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT_DATE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_DATETIME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FORMAT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEEK_START` int(11) DEFAULT NULL,
  `CHARSET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DIRECTION` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CULTURE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_language`
--

INSERT INTO `b_language` (`LID`, `SORT`, `DEF`, `ACTIVE`, `NAME`, `FORMAT_DATE`, `FORMAT_DATETIME`, `FORMAT_NAME`, `WEEK_START`, `CHARSET`, `DIRECTION`, `CULTURE_ID`) VALUES
('en', 2, 'N', 'Y', 'English', NULL, NULL, NULL, NULL, NULL, NULL, 2),
('ru', 1, 'Y', 'Y', 'Russian', NULL, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `b_lang_domain`
--

CREATE TABLE IF NOT EXISTS `b_lang_domain` (
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `DOMAIN` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`LID`,`DOMAIN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_list_rubric`
--

CREATE TABLE IF NOT EXISTS `b_list_rubric` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `CODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `AUTO` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DAYS_OF_MONTH` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DAYS_OF_WEEK` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIMES_OF_DAY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEMPLATE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_EXECUTED` datetime DEFAULT NULL,
  `VISIBLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `FROM_FIELD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_medialib_collection`
--

CREATE TABLE IF NOT EXISTS `b_medialib_collection` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `DATE_UPDATE` datetime NOT NULL,
  `OWNER_ID` int(11) DEFAULT NULL,
  `PARENT_ID` int(11) DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KEYWORDS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ITEMS_COUNT` int(11) DEFAULT NULL,
  `ML_TYPE` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_medialib_collection_item`
--

CREATE TABLE IF NOT EXISTS `b_medialib_collection_item` (
  `COLLECTION_ID` int(11) NOT NULL,
  `ITEM_ID` int(11) NOT NULL,
  PRIMARY KEY (`ITEM_ID`,`COLLECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_medialib_item`
--

CREATE TABLE IF NOT EXISTS `b_medialib_item` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM_TYPE` char(30) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DATE_CREATE` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `SOURCE_ID` int(11) NOT NULL,
  `KEYWORDS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEARCHABLE_CONTENT` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_medialib_type`
--

CREATE TABLE IF NOT EXISTS `b_medialib_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `EXT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SYSTEM` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `b_medialib_type`
--

INSERT INTO `b_medialib_type` (`ID`, `NAME`, `CODE`, `EXT`, `SYSTEM`, `DESCRIPTION`) VALUES
(1, 'image_name', 'image', 'jpg,jpeg,gif,png', 'Y', 'image_desc'),
(2, 'video_name', 'video', 'flv,mp4,wmv', 'Y', 'video_desc'),
(3, 'sound_name', 'sound', 'mp3,wma,aac', 'Y', 'sound_desc');

-- --------------------------------------------------------

--
-- Table structure for table `b_mobileapp_app`
--

CREATE TABLE IF NOT EXISTS `b_mobileapp_app` (
  `CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci NOT NULL,
  `FILES` text COLLATE utf8_unicode_ci NOT NULL,
  `FOLDER` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime NOT NULL,
  PRIMARY KEY (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_mobileapp_config`
--

CREATE TABLE IF NOT EXISTS `b_mobileapp_config` (
  `APP_CODE` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `PLATFORM` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime NOT NULL,
  PRIMARY KEY (`APP_CODE`,`PLATFORM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_module`
--

CREATE TABLE IF NOT EXISTS `b_module` (
  `ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_ACTIVE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_module`
--

INSERT INTO `b_module` (`ID`, `DATE_ACTIVE`) VALUES
('bitrix.eshop', '2015-04-22 17:25:22'),
('bitrix.sitecorporate', '2015-04-22 17:25:28'),
('bitrixcloud', '2015-04-22 17:25:30'),
('blog', '2015-04-22 17:25:41'),
('catalog', '2015-04-22 17:26:20'),
('clouds', '2015-04-22 17:26:26'),
('compression', '2015-04-22 17:26:28'),
('currency', '2015-04-22 17:26:30'),
('fileman', '2015-04-22 17:26:37'),
('form', '2015-04-22 17:26:47'),
('forum', '2015-04-22 17:26:55'),
('highloadblock', '2015-04-22 17:27:07'),
('iblock', '2015-04-22 17:27:19'),
('main', '2015-04-22 17:24:54'),
('mobileapp', '2015-04-22 17:27:39'),
('perfmon', '2015-04-22 17:27:43'),
('photogallery', '2015-04-22 17:27:49'),
('pull', '2015-04-22 17:28:00'),
('sale', '2015-04-22 17:28:10'),
('scale', '2015-04-22 17:28:26'),
('search', '2015-04-22 17:28:29'),
('security', '2015-04-22 17:28:33'),
('seo', '2015-04-22 17:28:37'),
('socialservices', '2015-04-22 17:28:39'),
('storeassist', '2015-04-22 17:28:41'),
('subscribe', '2015-04-22 17:28:44'),
('vote', '2015-04-22 17:28:47');

-- --------------------------------------------------------

--
-- Table structure for table `b_module_group`
--

CREATE TABLE IF NOT EXISTS `b_module_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `G_ACCESS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_GROUP_MODULE` (`MODULE_ID`,`GROUP_ID`,`SITE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `b_module_group`
--

INSERT INTO `b_module_group` (`ID`, `MODULE_ID`, `GROUP_ID`, `G_ACCESS`, `SITE_ID`) VALUES
(1, 'main', 5, 'Q', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_module_to_module`
--

CREATE TABLE IF NOT EXISTS `b_module_to_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SORT` int(18) NOT NULL DEFAULT '100',
  `FROM_MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `TO_MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `TO_PATH` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TO_CLASS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TO_METHOD` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TO_METHOD_ARG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_module_to_module` (`FROM_MODULE_ID`,`MESSAGE_ID`,`TO_MODULE_ID`,`TO_CLASS`,`TO_METHOD`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=279 ;

--
-- Dumping data for table `b_module_to_module`
--

INSERT INTO `b_module_to_module` (`ID`, `TIMESTAMP_X`, `SORT`, `FROM_MODULE_ID`, `MESSAGE_ID`, `TO_MODULE_ID`, `TO_PATH`, `TO_CLASS`, `TO_METHOD`, `TO_METHOD_ARG`, `VERSION`) VALUES
(1, '2015-04-22 17:24:55', 100, 'iblock', 'OnIBlockPropertyBuildList', 'main', '/modules/main/tools/prop_userid.php', 'CIBlockPropertyUserID', 'GetUserTypeDescription', '', 1),
(2, '2015-04-22 17:24:55', 100, 'main', 'OnUserDelete', 'main', '/modules/main/classes/mysql/favorites.php', 'CFavorites', 'OnUserDelete', '', 1),
(3, '2015-04-22 17:24:55', 100, 'main', 'OnLanguageDelete', 'main', '/modules/main/classes/mysql/favorites.php', 'CFavorites', 'OnLanguageDelete', '', 1),
(4, '2015-04-22 17:24:55', 100, 'main', 'OnUserDelete', 'main', '', 'CUserOptions', 'OnUserDelete', '', 1),
(5, '2015-04-22 17:24:55', 100, 'main', 'OnChangeFile', 'main', '', 'CMain', 'OnChangeFileComponent', '', 1),
(6, '2015-04-22 17:24:55', 100, 'main', 'OnUserTypeRightsCheck', 'main', '', 'CUser', 'UserTypeRightsCheck', '', 1),
(7, '2015-04-22 17:24:55', 100, 'main', 'OnUserLogin', 'main', '', 'UpdateTools', 'CheckUpdates', '', 1),
(8, '2015-04-22 17:24:55', 100, 'main', 'OnModuleUpdate', 'main', '', 'UpdateTools', 'SetUpdateResult', '', 1),
(9, '2015-04-22 17:24:55', 100, 'main', 'OnUpdateCheck', 'main', '', 'UpdateTools', 'SetUpdateError', '', 1),
(10, '2015-04-22 17:24:55', 100, 'main', 'OnPanelCreate', 'main', '', 'CUndo', 'CheckNotifyMessage', '', 1),
(11, '2015-04-22 17:24:55', 100, 'main', 'OnAfterAddRating', 'main', '', 'CRatingsComponentsMain', 'OnAfterAddRating', '', 1),
(12, '2015-04-22 17:24:55', 100, 'main', 'OnAfterUpdateRating', 'main', '', 'CRatingsComponentsMain', 'OnAfterUpdateRating', '', 1),
(13, '2015-04-22 17:24:55', 100, 'main', 'OnSetRatingsConfigs', 'main', '', 'CRatingsComponentsMain', 'OnSetRatingConfigs', '', 1),
(14, '2015-04-22 17:24:55', 100, 'main', 'OnGetRatingsConfigs', 'main', '', 'CRatingsComponentsMain', 'OnGetRatingConfigs', '', 1),
(15, '2015-04-22 17:24:55', 100, 'main', 'OnGetRatingsObjects', 'main', '', 'CRatingsComponentsMain', 'OnGetRatingObject', '', 1),
(16, '2015-04-22 17:24:55', 100, 'main', 'OnGetRatingContentOwner', 'main', '', 'CRatingsComponentsMain', 'OnGetRatingContentOwner', '', 1),
(17, '2015-04-22 17:24:55', 100, 'main', 'OnAfterAddRatingRule', 'main', '', 'CRatingRulesMain', 'OnAfterAddRatingRule', '', 1),
(18, '2015-04-22 17:24:55', 100, 'main', 'OnAfterUpdateRatingRule', 'main', '', 'CRatingRulesMain', 'OnAfterUpdateRatingRule', '', 1),
(19, '2015-04-22 17:24:55', 100, 'main', 'OnGetRatingRuleObjects', 'main', '', 'CRatingRulesMain', 'OnGetRatingRuleObjects', '', 1),
(20, '2015-04-22 17:24:55', 100, 'main', 'OnGetRatingRuleConfigs', 'main', '', 'CRatingRulesMain', 'OnGetRatingRuleConfigs', '', 1),
(21, '2015-04-22 17:24:55', 100, 'main', 'OnAfterUserAdd', 'main', '', 'CRatings', 'OnAfterUserRegister', '', 1),
(22, '2015-04-22 17:24:55', 100, 'main', 'OnUserDelete', 'main', '', 'CRatings', 'OnUserDelete', '', 1),
(23, '2015-04-22 17:24:55', 100, 'main', 'OnUserDelete', 'main', '', 'CAccess', 'OnUserDelete', '', 1),
(24, '2015-04-22 17:24:55', 100, 'main', 'OnAfterGroupAdd', 'main', '', 'CGroupAuthProvider', 'OnAfterGroupAdd', '', 1),
(25, '2015-04-22 17:24:55', 100, 'main', 'OnBeforeGroupUpdate', 'main', '', 'CGroupAuthProvider', 'OnBeforeGroupUpdate', '', 1),
(26, '2015-04-22 17:24:55', 100, 'main', 'OnBeforeGroupDelete', 'main', '', 'CGroupAuthProvider', 'OnBeforeGroupDelete', '', 1),
(27, '2015-04-22 17:24:55', 100, 'main', 'OnAfterSetUserGroup', 'main', '', 'CGroupAuthProvider', 'OnAfterSetUserGroup', '', 1),
(28, '2015-04-22 17:24:55', 100, 'main', 'OnUserLogin', 'main', '', 'CGroupAuthProvider', 'OnUserLogin', '', 1),
(29, '2015-04-22 17:24:55', 100, 'main', 'OnEventLogGetAuditTypes', 'main', '', 'CEventMain', 'GetAuditTypes', '', 1),
(30, '2015-04-22 17:24:55', 100, 'main', 'OnEventLogGetAuditHandlers', 'main', '', 'CEventMain', 'MakeMainObject', '', 1),
(31, '2015-04-22 17:24:55', 100, 'perfmon', 'OnGetTableSchema', 'main', '', 'CTableSchema', 'OnGetTableSchema', '', 1),
(32, '2015-04-22 17:24:55', 110, 'main', 'OnUserTypeBuildList', 'main', '', 'CUserTypeString', 'GetUserTypeDescription', '', 1),
(33, '2015-04-22 17:24:55', 120, 'main', 'OnUserTypeBuildList', 'main', '', 'CUserTypeInteger', 'GetUserTypeDescription', '', 1),
(34, '2015-04-22 17:24:55', 130, 'main', 'OnUserTypeBuildList', 'main', '', 'CUserTypeDouble', 'GetUserTypeDescription', '', 1),
(35, '2015-04-22 17:24:55', 140, 'main', 'OnUserTypeBuildList', 'main', '', 'CUserTypeDateTime', 'GetUserTypeDescription', '', 1),
(36, '2015-04-22 17:24:55', 145, 'main', 'OnUserTypeBuildList', 'main', '', 'CUserTypeDate', 'GetUserTypeDescription', '', 1),
(37, '2015-04-22 17:24:55', 150, 'main', 'OnUserTypeBuildList', 'main', '', 'CUserTypeBoolean', 'GetUserTypeDescription', '', 1),
(38, '2015-04-22 17:24:55', 160, 'main', 'OnUserTypeBuildList', 'main', '', 'CUserTypeFile', 'GetUserTypeDescription', '', 1),
(39, '2015-04-22 17:24:55', 170, 'main', 'OnUserTypeBuildList', 'main', '', 'CUserTypeEnum', 'GetUserTypeDescription', '', 1),
(40, '2015-04-22 17:24:55', 180, 'main', 'OnUserTypeBuildList', 'main', '', 'CUserTypeIBlockSection', 'GetUserTypeDescription', '', 1),
(41, '2015-04-22 17:24:55', 190, 'main', 'OnUserTypeBuildList', 'main', '', 'CUserTypeIBlockElement', 'GetUserTypeDescription', '', 1),
(42, '2015-04-22 17:24:55', 200, 'main', 'OnUserTypeBuildList', 'main', '', 'CUserTypeStringFormatted', 'GetUserTypeDescription', '', 1),
(43, '2015-04-22 17:24:55', 100, 'main', 'OnBeforeEndBufferContent', 'main', '', '\\Bitrix\\Main\\Analytics\\Counter', 'onBeforeEndBufferContent', '', 1),
(44, '2015-04-22 17:24:55', 100, 'main', 'OnBeforeRestartBuffer', 'main', '', '\\Bitrix\\Main\\Analytics\\Counter', 'onBeforeRestartBuffer', '', 1),
(45, '2015-04-22 17:25:22', 100, 'main', 'OnBeforeProlog', 'bitrix.eshop', '', 'CEShop', 'ShowPanel', '', 1),
(46, '2015-04-22 17:25:28', 100, 'main', 'OnBeforeProlog', 'bitrix.sitecorporate', '', 'CSiteCorporate', 'ShowPanel', '', 1),
(47, '2015-04-22 17:25:30', 100, 'main', 'OnAdminInformerInsertItems', 'bitrixcloud', '', 'CBitrixCloudCDN', 'OnAdminInformerInsertItems', '', 1),
(48, '2015-04-22 17:25:30', 100, 'main', 'OnAdminInformerInsertItems', 'bitrixcloud', '', 'CBitrixCloudBackup', 'OnAdminInformerInsertItems', '', 1),
(49, '2015-04-22 17:25:30', 100, 'mobileapp', 'OnBeforeAdminMobileMenuBuild', 'bitrixcloud', '', 'CBitrixCloudMobile', 'OnBeforeAdminMobileMenuBuild', '', 1),
(50, '2015-04-22 17:25:41', 100, 'search', 'OnReindex', 'blog', '', 'CBlogSearch', 'OnSearchReindex', '', 1),
(51, '2015-04-22 17:25:41', 100, 'main', 'OnUserDelete', 'blog', '', 'CBlogUser', 'Delete', '', 1),
(52, '2015-04-22 17:25:41', 100, 'main', 'OnSiteDelete', 'blog', '', 'CBlogSitePath', 'DeleteBySiteID', '', 1),
(53, '2015-04-22 17:25:41', 100, 'socialnetwork', 'OnSocNetGroupDelete', 'blog', '', 'CBlogSoNetPost', 'OnGroupDelete', '', 1),
(54, '2015-04-22 17:25:41', 100, 'socialnetwork', 'OnSocNetFeaturesAdd', 'blog', '', 'CBlogSearch', 'SetSoNetFeatureIndexSearch', '', 1),
(55, '2015-04-22 17:25:41', 100, 'socialnetwork', 'OnSocNetFeaturesUpdate', 'blog', '', 'CBlogSearch', 'SetSoNetFeatureIndexSearch', '', 1),
(56, '2015-04-22 17:25:41', 100, 'socialnetwork', 'OnSocNetFeaturesPermsAdd', 'blog', '', 'CBlogSearch', 'SetSoNetFeaturePermIndexSearch', '', 1),
(57, '2015-04-22 17:25:41', 100, 'socialnetwork', 'OnSocNetFeaturesPermsUpdate', 'blog', '', 'CBlogSearch', 'SetSoNetFeaturePermIndexSearch', '', 1),
(58, '2015-04-22 17:25:41', 200, 'main', 'OnAfterAddRating', 'blog', '', 'CRatingsComponentsBlog', 'OnAfterAddRating', '', 1),
(59, '2015-04-22 17:25:41', 200, 'main', 'OnAfterUpdateRating', 'blog', '', 'CRatingsComponentsBlog', 'OnAfterUpdateRating', '', 1),
(60, '2015-04-22 17:25:41', 200, 'main', 'OnSetRatingsConfigs', 'blog', '', 'CRatingsComponentsBlog', 'OnSetRatingConfigs', '', 1),
(61, '2015-04-22 17:25:41', 200, 'main', 'OnGetRatingsConfigs', 'blog', '', 'CRatingsComponentsBlog', 'OnGetRatingConfigs', '', 1),
(62, '2015-04-22 17:25:41', 200, 'main', 'OnGetRatingsObjects', 'blog', '', 'CRatingsComponentsBlog', 'OnGetRatingObject', '', 1),
(63, '2015-04-22 17:25:41', 200, 'main', 'OnGetRatingContentOwner', 'blog', '', 'CRatingsComponentsBlog', 'OnGetRatingContentOwner', '', 1),
(64, '2015-04-22 17:25:41', 100, 'im', 'OnGetNotifySchema', 'blog', '', 'CBlogNotifySchema', 'OnGetNotifySchema', '', 1),
(65, '2015-04-22 17:26:20', 100, 'iblock', 'OnIBlockDelete', 'catalog', '', 'CCatalog', 'OnIBlockDelete', '', 1),
(66, '2015-04-22 17:26:20', 100, 'iblock', 'OnIBlockElementDelete', 'catalog', '', 'CCatalogProduct', 'OnIBlockElementDelete', '', 1),
(67, '2015-04-22 17:26:20', 100, 'iblock', 'OnIBlockElementDelete', 'catalog', '', 'CPrice', 'OnIBlockElementDelete', '', 1),
(68, '2015-04-22 17:26:20', 100, 'iblock', 'OnIBlockElementDelete', 'catalog', '', 'CCatalogStoreProduct', 'OnIBlockElementDelete', '', 1),
(69, '2015-04-22 17:26:20', 100, 'iblock', 'OnIBlockElementDelete', 'catalog', '', 'CCatalogDocs', 'OnIBlockElementDelete', '', 1),
(70, '2015-04-22 17:26:20', 100, 'iblock', 'OnBeforeIBlockElementDelete', 'catalog', '', 'CCatalogDocs', 'OnBeforeIBlockElementDelete', '', 1),
(71, '2015-04-22 17:26:20', 100, 'currency', 'OnCurrencyDelete', 'catalog', '', 'CPrice', 'OnCurrencyDelete', '', 1),
(72, '2015-04-22 17:26:20', 100, 'main', 'OnGroupDelete', 'catalog', '', 'CCatalogProductGroups', 'OnGroupDelete', '', 1),
(73, '2015-04-22 17:26:20', 100, 'iblock', 'OnAfterIBlockElementUpdate', 'catalog', '', 'CCatalogProduct', 'OnAfterIBlockElementUpdate', '', 1),
(74, '2015-04-22 17:26:20', 100, 'currency', 'OnModuleUnInstall', 'catalog', '', '', 'CurrencyModuleUnInstallCatalog', '', 1),
(75, '2015-04-22 17:26:20', 300, 'iblock', 'OnBeforeIBlockDelete', 'catalog', '', 'CCatalog', 'OnBeforeCatalogDelete', '', 1),
(76, '2015-04-22 17:26:20', 10000, 'iblock', 'OnBeforeIBlockElementDelete', 'catalog', '', 'CCatalog', 'OnBeforeIBlockElementDelete', '', 1),
(77, '2015-04-22 17:26:20', 100, 'main', 'OnEventLogGetAuditTypes', 'catalog', '', 'CCatalogEvent', 'GetAuditTypes', '', 1),
(78, '2015-04-22 17:26:20', 100, 'sale', 'OnSetCouponList', 'catalog', '', 'CCatalogDiscountCoupon', 'OnSetCouponList', '', 1),
(79, '2015-04-22 17:26:20', 100, 'sale', 'OnClearCouponList', 'catalog', '', 'CCatalogDiscountCoupon', 'OnClearCouponList', '', 1),
(80, '2015-04-22 17:26:20', 100, 'main', 'OnBuildGlobalMenu', 'catalog', '', 'CCatalogAdmin', 'OnBuildGlobalMenu', '', 1),
(81, '2015-04-22 17:26:20', 100, 'main', 'OnAdminListDisplay', 'catalog', '', 'CCatalogAdmin', 'OnAdminListDisplay', '', 1),
(82, '2015-04-22 17:26:20', 100, 'main', 'OnBuildGlobalMenu', 'catalog', '', 'CCatalogAdmin', 'OnBuildSaleMenu', '', 1),
(83, '2015-04-22 17:26:20', 100, 'catalog', 'OnCondCatControlBuildList', 'catalog', '', 'CCatalogCondCtrlGroup', 'GetControlDescr', '', 1),
(84, '2015-04-22 17:26:20', 200, 'catalog', 'OnCondCatControlBuildList', 'catalog', '', 'CCatalogCondCtrlIBlockFields', 'GetControlDescr', '', 1),
(85, '2015-04-22 17:26:20', 300, 'catalog', 'OnCondCatControlBuildList', 'catalog', '', 'CCatalogCondCtrlIBlockProps', 'GetControlDescr', '', 1),
(86, '2015-04-22 17:26:20', 100, 'catalog', 'OnDocumentBarcodeDelete', 'catalog', '', 'CCatalogStoreDocsElement', 'OnDocumentBarcodeDelete', '', 1),
(87, '2015-04-22 17:26:20', 100, 'catalog', 'OnBeforeDocumentDelete', 'catalog', '', 'CCatalogStoreDocsBarcode', 'OnBeforeDocumentDelete', '', 1),
(88, '2015-04-22 17:26:20', 100, 'catalog', 'OnCatalogStoreDelete', 'catalog', '', 'CCatalogDocs', 'OnCatalogStoreDelete', '', 1),
(89, '2015-04-22 17:26:20', 100, 'iblock', 'OnBeforeIBlockPropertyDelete', 'catalog', '', 'CCatalog', 'OnBeforeIBlockPropertyDelete', '', 1),
(90, '2015-04-22 17:26:20', 1100, 'sale', 'OnCondSaleControlBuildList', 'catalog', '', 'CCatalogCondCtrlBasketProductFields', 'GetControlDescr', '', 1),
(91, '2015-04-22 17:26:20', 1200, 'sale', 'OnCondSaleControlBuildList', 'catalog', '', 'CCatalogCondCtrlBasketProductProps', 'GetControlDescr', '', 1),
(92, '2015-04-22 17:26:20', 1200, 'sale', 'OnCondSaleActionsControlBuildList', 'catalog', '', 'CCatalogActionCtrlBasketProductFields', 'GetControlDescr', '', 1),
(93, '2015-04-22 17:26:20', 1300, 'sale', 'OnCondSaleActionsControlBuildList', 'catalog', '', 'CCatalogActionCtrlBasketProductProps', 'GetControlDescr', '', 1),
(94, '2015-04-22 17:26:20', 100, 'sale', 'OnExtendBasketItems', 'catalog', '', 'CCatalogDiscount', 'ExtendBasketItems', '', 1),
(95, '2015-04-22 17:26:20', 100, 'iblock', 'OnModuleUnInstall', 'catalog', '', 'CCatalog', 'OnIBlockModuleUnInstall', '', 1),
(96, '2015-04-22 17:26:26', 100, 'main', 'OnEventLogGetAuditTypes', 'clouds', '', 'CCloudStorage', 'GetAuditTypes', '', 1),
(97, '2015-04-22 17:26:26', 100, 'main', 'OnBeforeProlog', 'clouds', '', 'CCloudStorage', 'OnBeforeProlog', '', 1),
(98, '2015-04-22 17:26:26', 100, 'main', 'OnAdminListDisplay', 'clouds', '', 'CCloudStorage', 'OnAdminListDisplay', '', 1),
(99, '2015-04-22 17:26:26', 100, 'main', 'OnBuildGlobalMenu', 'clouds', '', 'CCloudStorage', 'OnBuildGlobalMenu', '', 1),
(100, '2015-04-22 17:26:26', 100, 'main', 'OnFileSave', 'clouds', '', 'CCloudStorage', 'OnFileSave', '', 1),
(101, '2015-04-22 17:26:26', 100, 'main', 'OnGetFileSRC', 'clouds', '', 'CCloudStorage', 'OnGetFileSRC', '', 1),
(102, '2015-04-22 17:26:26', 100, 'main', 'OnFileCopy', 'clouds', '', 'CCloudStorage', 'OnFileCopy', '', 1),
(103, '2015-04-22 17:26:26', 100, 'main', 'OnFileDelete', 'clouds', '', 'CCloudStorage', 'OnFileDelete', '', 1),
(104, '2015-04-22 17:26:26', 100, 'main', 'OnMakeFileArray', 'clouds', '', 'CCloudStorage', 'OnMakeFileArray', '', 1),
(105, '2015-04-22 17:26:26', 100, 'main', 'OnBeforeResizeImage', 'clouds', '', 'CCloudStorage', 'OnBeforeResizeImage', '', 1),
(106, '2015-04-22 17:26:26', 100, 'main', 'OnAfterResizeImage', 'clouds', '', 'CCloudStorage', 'OnAfterResizeImage', '', 1),
(107, '2015-04-22 17:26:26', 100, 'clouds', 'OnGetStorageService', 'clouds', '', 'CCloudStorageService_AmazonS3', 'GetObject', '', 1),
(108, '2015-04-22 17:26:26', 100, 'clouds', 'OnGetStorageService', 'clouds', '', 'CCloudStorageService_GoogleStorage', 'GetObject', '', 1),
(109, '2015-04-22 17:26:26', 100, 'clouds', 'OnGetStorageService', 'clouds', '', 'CCloudStorageService_OpenStackStorage', 'GetObject', '', 1),
(110, '2015-04-22 17:26:26', 100, 'clouds', 'OnGetStorageService', 'clouds', '', 'CCloudStorageService_RackSpaceCloudFiles', 'GetObject', '', 1),
(111, '2015-04-22 17:26:26', 100, 'clouds', 'OnGetStorageService', 'clouds', '', 'CCloudStorageService_ClodoRU', 'GetObject', '', 1),
(112, '2015-04-22 17:26:26', 100, 'clouds', 'OnGetStorageService', 'clouds', '', 'CCloudStorageService_Selectel', 'GetObject', '', 1),
(113, '2015-04-22 17:26:28', 1, 'main', 'OnPageStart', 'compression', '', 'CCompress', 'OnPageStart', '', 1),
(114, '2015-04-22 17:26:28', 10000, 'main', 'OnAfterEpilog', 'compression', '', 'CCompress', 'OnAfterEpilog', '', 1),
(115, '2015-04-22 17:26:37', 100, 'main', 'OnGroupDelete', 'fileman', '', 'CFileman', 'OnGroupDelete', '', 1),
(116, '2015-04-22 17:26:37', 100, 'main', 'OnPanelCreate', 'fileman', '', 'CFileman', 'OnPanelCreate', '', 1),
(117, '2015-04-22 17:26:37', 100, 'main', 'OnModuleUpdate', 'fileman', '', 'CFileman', 'OnModuleUpdate', '', 1),
(118, '2015-04-22 17:26:38', 100, 'main', 'OnModuleInstalled', 'fileman', '', 'CFileman', 'ClearComponentsListCache', '', 1),
(119, '2015-04-22 17:26:38', 100, 'iblock', 'OnIBlockPropertyBuildList', 'fileman', '', 'CIBlockPropertyMapGoogle', 'GetUserTypeDescription', '', 1),
(120, '2015-04-22 17:26:38', 100, 'iblock', 'OnIBlockPropertyBuildList', 'fileman', '', 'CIBlockPropertyMapYandex', 'GetUserTypeDescription', '', 1),
(121, '2015-04-22 17:26:38', 100, 'iblock', 'OnIBlockPropertyBuildList', 'fileman', '', 'CIBlockPropertyVideo', 'GetUserTypeDescription', '', 1),
(122, '2015-04-22 17:26:38', 100, 'main', 'OnUserTypeBuildList', 'fileman', '', 'CUserTypeVideo', 'GetUserTypeDescription', '', 1),
(123, '2015-04-22 17:26:38', 100, 'main', 'OnEventLogGetAuditTypes', 'fileman', '', 'CEventFileman', 'GetAuditTypes', '', 1),
(124, '2015-04-22 17:26:38', 100, 'main', 'OnEventLogGetAuditHandlers', 'fileman', '', 'CEventFileman', 'MakeFilemanObject', '', 1),
(125, '2015-04-22 17:26:55', 100, 'main', 'OnGroupDelete', 'forum', '', 'CForumNew', 'OnGroupDelete', '', 1),
(126, '2015-04-22 17:26:55', 100, 'main', 'OnBeforeLangDelete', 'forum', '', 'CForumNew', 'OnBeforeLangDelete', '', 1),
(127, '2015-04-22 17:26:55', 100, 'main', 'OnFileDelete', 'forum', '', 'CForumFiles', 'OnFileDelete', '', 1),
(128, '2015-04-22 17:26:55', 100, 'search', 'OnReindex', 'forum', '', 'CForumNew', 'OnReindex', '', 1),
(129, '2015-04-22 17:26:55', 100, 'main', 'OnUserDelete', 'forum', '', 'CForumUser', 'OnUserDelete', '', 1),
(130, '2015-04-22 17:26:55', 100, 'iblock', 'OnIBlockPropertyBuildList', 'main', '/modules/forum/tools/prop_topicid.php', 'CIBlockPropertyTopicID', 'GetUserTypeDescription', '', 1),
(131, '2015-04-22 17:26:55', 100, 'iblock', 'OnBeforeIBlockElementDelete', 'forum', '', 'CForumTopic', 'OnBeforeIBlockElementDelete', '', 1),
(132, '2015-04-22 17:26:55', 100, 'main', 'OnEventLogGetAuditTypes', 'forum', '', 'CForumEventLog', 'GetAuditTypes', '', 1),
(133, '2015-04-22 17:26:55', 100, 'main', 'OnEventLogGetAuditHandlers', 'forum', '', 'CEventForum', 'MakeForumObject', '', 1),
(134, '2015-04-22 17:26:55', 100, 'socialnetwork', 'OnSocNetGroupDelete', 'forum', '', 'CForumUser', 'OnSocNetGroupDelete', '', 1),
(135, '2015-04-22 17:26:55', 100, 'socialnetwork', 'OnSocNetLogFormatEvent', 'forum', '', 'CForumMessage', 'OnSocNetLogFormatEvent', '', 1),
(136, '2015-04-22 17:26:55', 100, 'mail', 'OnGetFilterList', 'forum', '', 'CForumEMail', 'OnGetSocNetFilterList', '', 1),
(137, '2015-04-22 17:26:55', 100, 'main', 'OnAfterAddRating', 'forum', '', 'CRatingsComponentsForum', 'OnAfterAddRating', '', 1),
(138, '2015-04-22 17:26:55', 100, 'main', 'OnAfterUpdateRating', 'forum', '', 'CRatingsComponentsForum', 'OnAfterUpdateRating', '', 1),
(139, '2015-04-22 17:26:55', 100, 'main', 'OnSetRatingsConfigs', 'forum', '', 'CRatingsComponentsForum', 'OnSetRatingConfigs', '', 1),
(140, '2015-04-22 17:26:55', 100, 'main', 'OnGetRatingsConfigs', 'forum', '', 'CRatingsComponentsForum', 'OnGetRatingConfigs', '', 1),
(141, '2015-04-22 17:26:55', 100, 'main', 'OnGetRatingsObjects', 'forum', '', 'CRatingsComponentsForum', 'OnGetRatingObject', '', 1),
(142, '2015-04-22 17:26:55', 100, 'main', 'OnGetRatingContentOwner', 'forum', '', 'CRatingsComponentsForum', 'OnGetRatingContentOwner', '', 1),
(143, '2015-04-22 17:26:55', 100, 'im', 'OnGetNotifySchema', 'forum', '', 'CForumNotifySchema', 'OnGetNotifySchema', '', 1),
(144, '2015-04-22 17:26:55', 100, 'main', 'OnAfterRegisterModule', 'main', '/modules/forum/install/index.php', 'forum', 'InstallUserFields', '', 1),
(145, '2015-04-22 17:27:07', 100, 'main', 'OnBeforeUserTypeAdd', 'highloadblock', '', '\\Bitrix\\Highloadblock\\HighloadBlockTable', 'OnBeforeUserTypeAdd', '', 1),
(146, '2015-04-22 17:27:07', 100, 'main', 'OnAfterUserTypeAdd', 'highloadblock', '', '\\Bitrix\\Highloadblock\\HighloadBlockTable', 'onAfterUserTypeAdd', '', 1),
(147, '2015-04-22 17:27:07', 100, 'main', 'OnBeforeUserTypeDelete', 'highloadblock', '', '\\Bitrix\\Highloadblock\\HighloadBlockTable', 'OnBeforeUserTypeDelete', '', 1),
(148, '2015-04-22 17:27:07', 100, 'main', 'OnUserTypeBuildList', 'highloadblock', '', 'CUserTypeHlblock', 'GetUserTypeDescription', '', 1),
(149, '2015-04-22 17:27:07', 100, 'iblock', 'OnIBlockPropertyBuildList', 'highloadblock', '', 'CIBlockPropertyDirectory', 'GetUserTypeDescription', '', 1),
(150, '2015-04-22 17:27:19', 100, 'main', 'OnGroupDelete', 'iblock', '', 'CIBlock', 'OnGroupDelete', '', 1),
(151, '2015-04-22 17:27:19', 100, 'main', 'OnBeforeLangDelete', 'iblock', '', 'CIBlock', 'OnBeforeLangDelete', '', 1),
(152, '2015-04-22 17:27:19', 100, 'main', 'OnLangDelete', 'iblock', '', 'CIBlock', 'OnLangDelete', '', 1),
(153, '2015-04-22 17:27:19', 100, 'main', 'OnUserTypeRightsCheck', 'iblock', '', 'CIBlockSection', 'UserTypeRightsCheck', '', 1),
(154, '2015-04-22 17:27:19', 100, 'search', 'OnReindex', 'iblock', '', 'CIBlock', 'OnSearchReindex', '', 1),
(155, '2015-04-22 17:27:19', 100, 'search', 'OnSearchGetURL', 'iblock', '', 'CIBlock', 'OnSearchGetURL', '', 1),
(156, '2015-04-22 17:27:19', 100, 'main', 'OnEventLogGetAuditTypes', 'iblock', '', 'CIBlock', 'GetAuditTypes', '', 1),
(157, '2015-04-22 17:27:19', 100, 'main', 'OnEventLogGetAuditHandlers', 'iblock', '', 'CEventIBlock', 'MakeIBlockObject', '', 1),
(158, '2015-04-22 17:27:19', 200, 'main', 'OnGetRatingContentOwner', 'iblock', '', 'CRatingsComponentsIBlock', 'OnGetRatingContentOwner', '', 1),
(159, '2015-04-22 17:27:19', 100, 'main', 'OnTaskOperationsChanged', 'iblock', '', 'CIBlockRightsStorage', 'OnTaskOperationsChanged', '', 1),
(160, '2015-04-22 17:27:19', 100, 'main', 'OnGroupDelete', 'iblock', '', 'CIBlockRightsStorage', 'OnGroupDelete', '', 1),
(161, '2015-04-22 17:27:19', 100, 'main', 'OnUserDelete', 'iblock', '', 'CIBlockRightsStorage', 'OnUserDelete', '', 1),
(162, '2015-04-22 17:27:19', 100, 'perfmon', 'OnGetTableSchema', 'iblock', '', 'iblock', 'OnGetTableSchema', '', 1),
(163, '2015-04-22 17:27:19', 100, 'sender', 'OnConnectorList', 'iblock', '', '\\Bitrix\\Iblock\\SenderEventHandler', 'onConnectorListIblock', '', 1),
(164, '2015-04-22 17:27:19', 10, 'iblock', 'OnIBlockPropertyBuildList', 'iblock', '', 'CIBlockProperty', '_DateTime_GetUserTypeDescription', '', 1),
(165, '2015-04-22 17:27:19', 20, 'iblock', 'OnIBlockPropertyBuildList', 'iblock', '', 'CIBlockProperty', '_XmlID_GetUserTypeDescription', '', 1),
(166, '2015-04-22 17:27:19', 30, 'iblock', 'OnIBlockPropertyBuildList', 'iblock', '', 'CIBlockProperty', '_FileMan_GetUserTypeDescription', '', 1),
(167, '2015-04-22 17:27:19', 40, 'iblock', 'OnIBlockPropertyBuildList', 'iblock', '', 'CIBlockProperty', '_HTML_GetUserTypeDescription', '', 1),
(168, '2015-04-22 17:27:19', 50, 'iblock', 'OnIBlockPropertyBuildList', 'iblock', '', 'CIBlockProperty', '_ElementList_GetUserTypeDescription', '', 1),
(169, '2015-04-22 17:27:19', 60, 'iblock', 'OnIBlockPropertyBuildList', 'iblock', '', 'CIBlockProperty', '_Sequence_GetUserTypeDescription', '', 1),
(170, '2015-04-22 17:27:19', 70, 'iblock', 'OnIBlockPropertyBuildList', 'iblock', '', 'CIBlockProperty', '_ElementAutoComplete_GetUserTypeDescription', '', 1),
(171, '2015-04-22 17:27:19', 80, 'iblock', 'OnIBlockPropertyBuildList', 'iblock', '', 'CIBlockProperty', '_SKU_GetUserTypeDescription', '', 1),
(172, '2015-04-22 17:27:19', 90, 'iblock', 'OnIBlockPropertyBuildList', 'iblock', '', 'CIBlockProperty', '_SectionAutoComplete_GetUserTypeDescription', '', 1),
(173, '2015-04-22 17:27:39', 100, 'pull', 'OnGetDependentModule', 'mobileapp', '', 'CMobileAppPullSchema', 'OnGetDependentModule', '', 1),
(174, '2015-04-22 17:27:49', 100, 'iblock', 'OnBeforeIBlockElementDelete', 'photogallery', '', 'CPhotogalleryElement', 'OnBeforeIBlockElementDelete', '', 1),
(175, '2015-04-22 17:27:49', 100, 'iblock', 'OnAfterIBlockElementAdd', 'photogallery', '', 'CPhotogalleryElement', 'OnAfterIBlockElementAdd', '', 1),
(176, '2015-04-22 17:27:49', 100, 'search', 'BeforeIndex', 'photogallery', '', 'CRatingsComponentsPhotogallery', 'BeforeIndex', '', 1),
(177, '2015-04-22 17:27:49', 100, 'im', 'OnGetNotifySchema', 'photogallery', '', 'CPhotogalleryNotifySchema', 'OnGetNotifySchema', '', 1),
(178, '2015-04-22 17:28:00', 3, 'main', 'OnProlog', 'main', '/modules/pull/ajax_hit.php', '', '', '', 1),
(179, '2015-04-22 17:28:00', 100, 'main', 'OnEpilog', 'pull', '', 'CPullWatch', 'DeferredSql', '', 1),
(180, '2015-04-22 17:28:00', 100, 'main', 'OnEpilog', 'pull', '', 'CPullOptions', 'OnEpilog', '', 1),
(181, '2015-04-22 17:28:00', 100, 'perfmon', 'OnGetTableSchema', 'pull', '', 'CPullTableSchema', 'OnGetTableSchema', '', 1),
(182, '2015-04-22 17:28:00', 100, 'main', 'OnAfterRegisterModule', 'pull', '', 'CPullOptions', 'ClearCheckCache', '', 1),
(183, '2015-04-22 17:28:00', 100, 'main', 'OnAfterUnRegisterModule', 'pull', '', 'CPullOptions', 'ClearCheckCache', '', 1),
(184, '2015-04-22 17:28:10', 100, 'main', 'OnUserLogin', 'sale', '', 'CSaleUser', 'OnUserLogin', '', 1),
(185, '2015-04-22 17:28:10', 100, 'main', 'OnUserLogout', 'sale', '', 'CSaleUser', 'OnUserLogout', '', 1),
(186, '2015-04-22 17:28:10', 100, 'main', 'OnBeforeLangDelete', 'sale', '', 'CSalePersonType', 'OnBeforeLangDelete', '', 1),
(187, '2015-04-22 17:28:10', 100, 'main', 'OnLanguageDelete', 'sale', '', 'CSaleLocation', 'OnLangDelete', '', 1),
(188, '2015-04-22 17:28:10', 100, 'main', 'OnLanguageDelete', 'sale', '', 'CSaleLocationGroup', 'OnLangDelete', '', 1),
(189, '2015-04-22 17:28:10', 100, 'main', 'OnUserDelete', 'sale', '', 'CSaleOrderUserProps', 'OnUserDelete', '', 1),
(190, '2015-04-22 17:28:10', 100, 'main', 'OnUserDelete', 'sale', '', 'CSaleUserAccount', 'OnUserDelete', '', 1),
(191, '2015-04-22 17:28:10', 100, 'main', 'OnUserDelete', 'sale', '', 'CSaleAuxiliary', 'OnUserDelete', '', 1),
(192, '2015-04-22 17:28:10', 100, 'main', 'OnUserDelete', 'sale', '', 'CSaleUser', 'OnUserDelete', '', 1),
(193, '2015-04-22 17:28:10', 100, 'main', 'OnUserDelete', 'sale', '', 'CSaleRecurring', 'OnUserDelete', '', 1),
(194, '2015-04-22 17:28:10', 100, 'main', 'OnUserDelete', 'sale', '', 'CSaleUserCards', 'OnUserDelete', '', 1),
(195, '2015-04-22 17:28:10', 100, 'main', 'OnBeforeUserDelete', 'sale', '', 'CSaleOrder', 'OnBeforeUserDelete', '', 1),
(196, '2015-04-22 17:28:10', 100, 'main', 'OnBeforeUserDelete', 'sale', '', 'CSaleAffiliate', 'OnBeforeUserDelete', '', 1),
(197, '2015-04-22 17:28:10', 100, 'main', 'OnBeforeUserDelete', 'sale', '', 'CSaleUserAccount', 'OnBeforeUserDelete', '', 1),
(198, '2015-04-22 17:28:10', 100, 'main', 'OnBeforeProlog', 'main', '/modules/sale/affiliate.php', '', '', '', 1),
(199, '2015-04-22 17:28:10', 100, 'main', 'OnEventLogGetAuditTypes', 'sale', '', 'CSaleYMHandler', 'OnEventLogGetAuditTypes', '', 1),
(200, '2015-04-22 17:28:10', 100, 'currency', 'OnBeforeCurrencyDelete', 'sale', '', 'CSaleOrder', 'OnBeforeCurrencyDelete', '', 1),
(201, '2015-04-22 17:28:10', 100, 'currency', 'OnBeforeCurrencyDelete', 'sale', '', 'CSaleLang', 'OnBeforeCurrencyDelete', '', 1),
(202, '2015-04-22 17:28:10', 100, 'currency', 'OnModuleUnInstall', 'sale', '', '', 'CurrencyModuleUnInstallSale', '', 1),
(203, '2015-04-22 17:28:10', 100, 'catalog', 'OnSaleOrderSumm', 'sale', '', 'CSaleOrder', '__SaleOrderCount', '', 1),
(204, '2015-04-22 17:28:10', 100, 'mobileapp', 'OnBeforeAdminMobileMenuBuild', 'sale', '', 'CSaleMobileOrderUtils', 'buildSaleAdminMobileMenu', '', 1),
(205, '2015-04-22 17:28:10', 100, 'sender', 'OnConnectorList', 'sale', '', '\\Bitrix\\Sale\\SenderEventHandler', 'onConnectorListBuyer', '', 1),
(206, '2015-04-22 17:28:10', 100, 'sale', 'OnCondSaleControlBuildList', 'sale', '', 'CSaleCondCtrlGroup', 'GetControlDescr', '', 1),
(207, '2015-04-22 17:28:10', 200, 'sale', 'OnCondSaleControlBuildList', 'sale', '', 'CSaleCondCtrlBasketGroup', 'GetControlDescr', '', 1),
(208, '2015-04-22 17:28:10', 300, 'sale', 'OnCondSaleControlBuildList', 'sale', '', 'CSaleCondCtrlBasketFields', 'GetControlDescr', '', 1),
(209, '2015-04-22 17:28:10', 1000, 'sale', 'OnCondSaleControlBuildList', 'sale', '', 'CSaleCondCtrlOrderFields', 'GetControlDescr', '', 1),
(210, '2015-04-22 17:28:10', 10000, 'sale', 'OnCondSaleControlBuildList', 'sale', '', 'CSaleCondCtrlCommon', 'GetControlDescr', '', 1),
(211, '2015-04-22 17:28:10', 100, 'sale', 'OnCondSaleActionsControlBuildList', 'sale', '', 'CSaleActionCtrlGroup', 'GetControlDescr', '', 1),
(212, '2015-04-22 17:28:10', 200, 'sale', 'OnCondSaleActionsControlBuildList', 'sale', '', 'CSaleActionCtrlDelivery', 'GetControlDescr', '', 1),
(213, '2015-04-22 17:28:10', 300, 'sale', 'OnCondSaleActionsControlBuildList', 'sale', '', 'CSaleActionCtrlBasketGroup', 'GetControlDescr', '', 1),
(214, '2015-04-22 17:28:10', 1000, 'sale', 'OnCondSaleActionsControlBuildList', 'sale', '', 'CSaleActionCtrlSubGroup', 'GetControlDescr', '', 1),
(215, '2015-04-22 17:28:10', 1100, 'sale', 'OnCondSaleActionsControlBuildList', 'sale', '', 'CSaleActionCondCtrlBasketFields', 'GetControlDescr', '', 1),
(216, '2015-04-22 17:28:11', 100, 'sale', 'OnOrderDelete', 'sale', '', 'CSaleMobileOrderPull', 'onOrderDelete', '', 1),
(217, '2015-04-22 17:28:11', 100, 'sale', 'OnOrderAdd', 'sale', '', 'CSaleMobileOrderPull', 'onOrderAdd', '', 1),
(218, '2015-04-22 17:28:11', 100, 'sale', 'OnOrderUpdate', 'sale', '', 'CSaleMobileOrderPull', 'onOrderUpdate', '', 1),
(219, '2015-04-22 17:28:11', 100, 'sale', 'OnBasketOrder', 'sale', '', '\\Bitrix\\Sale\\Product2ProductTable', 'onSaleOrderAdd', '', 1),
(220, '2015-04-22 17:28:11', 100, 'sale', 'OnSaleStatusOrder', 'sale', '', '\\Bitrix\\Sale\\Product2ProductTable', 'onSaleStatusOrderHandler', '', 1),
(221, '2015-04-22 17:28:11', 100, 'sale', 'OnSaleDeliveryOrder', 'sale', '', '\\Bitrix\\Sale\\Product2ProductTable', 'onSaleDeliveryOrderHandler', '', 1),
(222, '2015-04-22 17:28:11', 100, 'sale', 'OnSaleDeductOrder', 'sale', '', '\\Bitrix\\Sale\\Product2ProductTable', 'onSaleDeductOrderHandler', '', 1),
(223, '2015-04-22 17:28:11', 100, 'sale', 'OnSaleCancelOrder', 'sale', '', '\\Bitrix\\Sale\\Product2ProductTable', 'onSaleCancelOrderHandler', '', 1),
(224, '2015-04-22 17:28:11', 100, 'sale', 'OnSalePayOrder', 'sale', '', '\\Bitrix\\Sale\\Product2ProductTable', 'onSalePayOrderHandler', '', 1),
(225, '2015-04-22 17:28:26', 100, 'main', 'OnEventLogGetAuditTypes', 'scale', '', '\\Bitrix\\Scale\\Logger', 'onEventLogGetAuditTypes', '', 1),
(226, '2015-04-22 17:28:29', 100, 'main', 'OnChangePermissions', 'search', '', 'CSearch', 'OnChangeFilePermissions', '', 1),
(227, '2015-04-22 17:28:29', 100, 'main', 'OnChangeFile', 'search', '', 'CSearch', 'OnChangeFile', '', 1),
(228, '2015-04-22 17:28:29', 100, 'main', 'OnGroupDelete', 'search', '', 'CSearch', 'OnGroupDelete', '', 1),
(229, '2015-04-22 17:28:29', 100, 'main', 'OnLangDelete', 'search', '', 'CSearch', 'OnLangDelete', '', 1),
(230, '2015-04-22 17:28:29', 100, 'main', 'OnAfterUserUpdate', 'search', '', 'CSearchUser', 'OnAfterUserUpdate', '', 1),
(231, '2015-04-22 17:28:29', 100, 'main', 'OnUserDelete', 'search', '', 'CSearchUser', 'DeleteByUserID', '', 1),
(232, '2015-04-22 17:28:29', 100, 'cluster', 'OnGetTableList', 'search', '', 'search', 'OnGetTableList', '', 1),
(233, '2015-04-22 17:28:29', 100, 'perfmon', 'OnGetTableSchema', 'search', '', 'search', 'OnGetTableSchema', '', 1),
(234, '2015-04-22 17:28:29', 90, 'main', 'OnEpilog', 'search', '', 'CSearchStatistic', 'OnEpilog', '', 1),
(235, '2015-04-22 17:28:33', 100, 'main', 'OnUserDelete', 'security', '', 'CSecurityUser', 'OnUserDelete', '', 1),
(236, '2015-04-22 17:28:33', 100, 'main', 'OnEventLogGetAuditTypes', 'security', '', 'CSecurityFilter', 'GetAuditTypes', '', 1),
(237, '2015-04-22 17:28:33', 100, 'main', 'OnEventLogGetAuditTypes', 'security', '', 'CSecurityAntiVirus', 'GetAuditTypes', '', 1),
(238, '2015-04-22 17:28:33', 100, 'main', 'OnAdminInformerInsertItems', 'security', '', 'CSecurityFilter', 'OnAdminInformerInsertItems', '', 1),
(239, '2015-04-22 17:28:33', 100, 'main', 'OnAdminInformerInsertItems', 'security', '', 'CSecuritySiteChecker', 'OnAdminInformerInsertItems', '', 1),
(240, '2015-04-22 17:28:33', 5, 'main', 'OnBeforeProlog', 'security', '', 'CSecurityFilter', 'OnBeforeProlog', '', 1),
(241, '2015-04-22 17:28:33', 9999, 'main', 'OnEndBufferContent', 'security', '', 'CSecurityXSSDetect', 'OnEndBufferContent', '', 1),
(242, '2015-04-22 17:28:33', -1, 'main', 'OnPageStart', 'security', '', 'CSecurityAntiVirus', 'OnPageStart', '', 1),
(243, '2015-04-22 17:28:33', 10000, 'main', 'OnEndBufferContent', 'security', '', 'CSecurityAntiVirus', 'OnEndBufferContent', '', 1),
(244, '2015-04-22 17:28:34', 10001, 'main', 'OnAfterEpilog', 'security', '', 'CSecurityAntiVirus', 'OnAfterEpilog', '', 1),
(245, '2015-04-22 17:28:37', 100, 'main', 'OnPanelCreate', 'seo', '', 'CSeoEventHandlers', 'SeoOnPanelCreate', '', 2),
(246, '2015-04-22 17:28:37', 100, 'fileman', 'OnIncludeHTMLEditorScript', 'seo', '', 'CSeoEventHandlers', 'OnIncludeHTMLEditorScript', '', 2),
(247, '2015-04-22 17:28:37', 100, 'fileman', 'OnBeforeHTMLEditorScriptRuns', 'seo', '', 'CSeoEventHandlers', 'OnBeforeHTMLEditorScriptRuns', '', 2),
(248, '2015-04-22 17:28:37', 100, 'iblock', 'OnAfterIBlockSectionAdd', 'seo', '', '\\Bitrix\\Seo\\SitemapIblock', 'addSection', '', 2),
(249, '2015-04-22 17:28:37', 100, 'iblock', 'OnAfterIBlockElementAdd', 'seo', '', '\\Bitrix\\Seo\\SitemapIblock', 'addElement', '', 2),
(250, '2015-04-22 17:28:37', 100, 'iblock', 'OnBeforeIBlockSectionDelete', 'seo', '', '\\Bitrix\\Seo\\SitemapIblock', 'beforeDeleteSection', '', 2),
(251, '2015-04-22 17:28:37', 100, 'iblock', 'OnBeforeIBlockElementDelete', 'seo', '', '\\Bitrix\\Seo\\SitemapIblock', 'beforeDeleteElement', '', 2),
(252, '2015-04-22 17:28:37', 100, 'iblock', 'OnAfterIBlockSectionDelete', 'seo', '', '\\Bitrix\\Seo\\SitemapIblock', 'deleteSection', '', 2),
(253, '2015-04-22 17:28:37', 100, 'iblock', 'OnAfterIBlockElementDelete', 'seo', '', '\\Bitrix\\Seo\\SitemapIblock', 'deleteElement', '', 2),
(254, '2015-04-22 17:28:37', 100, 'iblock', 'OnBeforeIBlockSectionUpdate', 'seo', '', '\\Bitrix\\Seo\\SitemapIblock', 'beforeUpdateSection', '', 2),
(255, '2015-04-22 17:28:37', 100, 'iblock', 'OnBeforeIBlockElementUpdate', 'seo', '', '\\Bitrix\\Seo\\SitemapIblock', 'beforeUpdateElement', '', 2),
(256, '2015-04-22 17:28:37', 100, 'iblock', 'OnAfterIBlockSectionUpdate', 'seo', '', '\\Bitrix\\Seo\\SitemapIblock', 'updateSection', '', 2),
(257, '2015-04-22 17:28:37', 100, 'iblock', 'OnAfterIBlockElementUpdate', 'seo', '', '\\Bitrix\\Seo\\SitemapIblock', 'updateElement', '', 2),
(258, '2015-04-22 17:28:37', 100, 'forum', 'onAfterTopicAdd', 'seo', '', '\\Bitrix\\Seo\\SitemapForum', 'addTopic', '', 2),
(259, '2015-04-22 17:28:37', 100, 'forum', 'onAfterTopicUpdate', 'seo', '', '\\Bitrix\\Seo\\SitemapForum', 'updateTopic', '', 2),
(260, '2015-04-22 17:28:37', 100, 'forum', 'onAfterTopicDelete', 'seo', '', '\\Bitrix\\Seo\\SitemapForum', 'deleteTopic', '', 2),
(261, '2015-04-22 17:28:37', 100, 'main', 'OnAdminIBlockElementEdit', 'seo', '', '\\Bitrix\\Seo\\AdvTabEngine', 'eventHandler', '', 2),
(262, '2015-04-22 17:28:37', 100, 'main', 'OnBeforeProlog', 'seo', '', '\\Bitrix\\Seo\\AdvSession', 'checkSession', '', 2),
(263, '2015-04-22 17:28:39', 100, 'main', 'OnUserDelete', 'socialservices', '', 'CSocServAuthDB', 'OnUserDelete', '', 1),
(264, '2015-04-22 17:28:39', 100, 'timeman', 'OnAfterTMReportDailyAdd', 'socialservices', '', 'CSocServAuthDB', 'OnAfterTMReportDailyAdd', '', 1),
(265, '2015-04-22 17:28:39', 100, 'timeman', 'OnAfterTMDayStart', 'socialservices', '', 'CSocServAuthDB', 'OnAfterTMDayStart', '', 1),
(266, '2015-04-22 17:28:39', 100, 'timeman', 'OnTimeManShow', 'socialservices', '', 'CSocServEventHandlers', 'OnTimeManShow', '', 1),
(267, '2015-04-22 17:28:39', 100, 'main', 'OnFindExternalUser', 'socialservices', '', 'CSocServAuthDB', 'OnFindExternalUser', '', 1),
(268, '2015-04-22 17:28:42', 100, 'main', 'OnPrologAdminTitle', 'storeassist', '', 'CStoreAssist', 'onPrologAdminTitle', '', 1),
(269, '2015-04-22 17:28:42', 100, 'main', 'OnBuildGlobalMenu', 'storeassist', '', 'CStoreAssist', 'onBuildGlobalMenu', '', 1),
(270, '2015-04-22 17:28:44', 100, 'main', 'OnBeforeLangDelete', 'subscribe', '', 'CRubric', 'OnBeforeLangDelete', '', 1),
(271, '2015-04-22 17:28:44', 100, 'main', 'OnUserDelete', 'subscribe', '', 'CSubscription', 'OnUserDelete', '', 1),
(272, '2015-04-22 17:28:44', 100, 'main', 'OnUserLogout', 'subscribe', '', 'CSubscription', 'OnUserLogout', '', 1),
(273, '2015-04-22 17:28:44', 100, 'main', 'OnGroupDelete', 'subscribe', '', 'CPosting', 'OnGroupDelete', '', 1),
(274, '2015-04-22 17:28:47', 100, 'main', 'OnBeforeProlog', 'main', '/modules/vote/keepvoting.php', '', '', '', 1),
(275, '2015-04-22 17:28:47', 200, 'main', 'OnUserTypeBuildList', 'vote', '', 'CUserTypeVote', 'GetUserTypeDescription', '', 1),
(276, '2015-04-22 17:28:47', 200, 'main', 'OnUserLogin', 'vote', '', 'CVoteUser', 'OnUserLogin', '', 1),
(277, '2015-04-22 17:28:47', 100, 'im', 'OnGetNotifySchema', 'vote', '', 'CVoteNotifySchema', 'OnGetNotifySchema', '', 1),
(278, '2015-04-22 17:29:18', 100, 'main', 'OnBeforeProlog', 'main', '/modules/main/install/wizard_sol/panel_button.php', 'CWizardSolPanel', 'ShowPanel', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `b_operation`
--

CREATE TABLE IF NOT EXISTS `b_operation` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BINDING` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'module',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=124 ;

--
-- Dumping data for table `b_operation`
--

INSERT INTO `b_operation` (`ID`, `NAME`, `MODULE_ID`, `DESCRIPTION`, `BINDING`) VALUES
(1, 'edit_php', 'main', NULL, 'module'),
(2, 'view_own_profile', 'main', NULL, 'module'),
(3, 'edit_own_profile', 'main', NULL, 'module'),
(4, 'view_all_users', 'main', NULL, 'module'),
(5, 'view_groups', 'main', NULL, 'module'),
(6, 'view_tasks', 'main', NULL, 'module'),
(7, 'view_other_settings', 'main', NULL, 'module'),
(8, 'view_subordinate_users', 'main', NULL, 'module'),
(9, 'edit_subordinate_users', 'main', NULL, 'module'),
(10, 'edit_all_users', 'main', NULL, 'module'),
(11, 'edit_groups', 'main', NULL, 'module'),
(12, 'edit_tasks', 'main', NULL, 'module'),
(13, 'edit_other_settings', 'main', NULL, 'module'),
(14, 'cache_control', 'main', NULL, 'module'),
(15, 'lpa_template_edit', 'main', NULL, 'module'),
(16, 'view_event_log', 'main', NULL, 'module'),
(17, 'edit_ratings', 'main', NULL, 'module'),
(18, 'manage_short_uri', 'main', NULL, 'module'),
(19, 'fm_view_permission', 'main', NULL, 'file'),
(20, 'fm_view_file', 'main', NULL, 'file'),
(21, 'fm_view_listing', 'main', NULL, 'file'),
(22, 'fm_edit_existent_folder', 'main', NULL, 'file'),
(23, 'fm_create_new_file', 'main', NULL, 'file'),
(24, 'fm_edit_existent_file', 'main', NULL, 'file'),
(25, 'fm_create_new_folder', 'main', NULL, 'file'),
(26, 'fm_delete_file', 'main', NULL, 'file'),
(27, 'fm_delete_folder', 'main', NULL, 'file'),
(28, 'fm_edit_in_workflow', 'main', NULL, 'file'),
(29, 'fm_rename_file', 'main', NULL, 'file'),
(30, 'fm_rename_folder', 'main', NULL, 'file'),
(31, 'fm_upload_file', 'main', NULL, 'file'),
(32, 'fm_add_to_menu', 'main', NULL, 'file'),
(33, 'fm_download_file', 'main', NULL, 'file'),
(34, 'fm_lpa', 'main', NULL, 'file'),
(35, 'fm_edit_permission', 'main', NULL, 'file'),
(36, 'catalog_read', 'catalog', NULL, 'module'),
(37, 'catalog_price', 'catalog', NULL, 'module'),
(38, 'catalog_group', 'catalog', NULL, 'module'),
(39, 'catalog_discount', 'catalog', NULL, 'module'),
(40, 'catalog_vat', 'catalog', NULL, 'module'),
(41, 'catalog_extra', 'catalog', NULL, 'module'),
(42, 'catalog_store', 'catalog', NULL, 'module'),
(43, 'catalog_purchas_info', 'catalog', NULL, 'module'),
(44, 'catalog_export_edit', 'catalog', NULL, 'module'),
(45, 'catalog_export_exec', 'catalog', NULL, 'module'),
(46, 'catalog_import_edit', 'catalog', NULL, 'module'),
(47, 'catalog_import_exec', 'catalog', NULL, 'module'),
(48, 'catalog_measure', 'catalog', NULL, 'module'),
(49, 'catalog_settings', 'catalog', NULL, 'module'),
(50, 'clouds_browse', 'clouds', NULL, 'module'),
(51, 'clouds_upload', 'clouds', NULL, 'module'),
(52, 'clouds_config', 'clouds', NULL, 'module'),
(53, 'fileman_view_all_settings', 'fileman', '', 'module'),
(54, 'fileman_edit_menu_types', 'fileman', '', 'module'),
(55, 'fileman_add_element_to_menu', 'fileman', '', 'module'),
(56, 'fileman_edit_menu_elements', 'fileman', '', 'module'),
(57, 'fileman_edit_existent_files', 'fileman', '', 'module'),
(58, 'fileman_edit_existent_folders', 'fileman', '', 'module'),
(59, 'fileman_admin_files', 'fileman', '', 'module'),
(60, 'fileman_admin_folders', 'fileman', '', 'module'),
(61, 'fileman_view_permissions', 'fileman', '', 'module'),
(62, 'fileman_edit_all_settings', 'fileman', '', 'module'),
(63, 'fileman_upload_files', 'fileman', '', 'module'),
(64, 'fileman_view_file_structure', 'fileman', '', 'module'),
(65, 'fileman_install_control', 'fileman', '', 'module'),
(66, 'medialib_view_collection', 'fileman', '', 'medialib'),
(67, 'medialib_new_collection', 'fileman', '', 'medialib'),
(68, 'medialib_edit_collection', 'fileman', '', 'medialib'),
(69, 'medialib_del_collection', 'fileman', '', 'medialib'),
(70, 'medialib_access', 'fileman', '', 'medialib'),
(71, 'medialib_new_item', 'fileman', '', 'medialib'),
(72, 'medialib_edit_item', 'fileman', '', 'medialib'),
(73, 'medialib_del_item', 'fileman', '', 'medialib'),
(74, 'sticker_view', 'fileman', '', 'stickers'),
(75, 'sticker_edit', 'fileman', '', 'stickers'),
(76, 'sticker_new', 'fileman', '', 'stickers'),
(77, 'sticker_del', 'fileman', '', 'stickers'),
(78, 'section_read', 'iblock', NULL, 'iblock'),
(79, 'element_read', 'iblock', NULL, 'iblock'),
(80, 'section_element_bind', 'iblock', NULL, 'iblock'),
(81, 'iblock_admin_display', 'iblock', NULL, 'iblock'),
(82, 'element_edit', 'iblock', NULL, 'iblock'),
(83, 'element_edit_price', 'iblock', NULL, 'iblock'),
(84, 'element_delete', 'iblock', NULL, 'iblock'),
(85, 'element_bizproc_start', 'iblock', NULL, 'iblock'),
(86, 'section_edit', 'iblock', NULL, 'iblock'),
(87, 'section_delete', 'iblock', NULL, 'iblock'),
(88, 'section_section_bind', 'iblock', NULL, 'iblock'),
(89, 'element_edit_any_wf_status', 'iblock', NULL, 'iblock'),
(90, 'iblock_edit', 'iblock', NULL, 'iblock'),
(91, 'iblock_delete', 'iblock', NULL, 'iblock'),
(92, 'iblock_rights_edit', 'iblock', NULL, 'iblock'),
(93, 'iblock_export', 'iblock', NULL, 'iblock'),
(94, 'section_rights_edit', 'iblock', NULL, 'iblock'),
(95, 'element_rights_edit', 'iblock', NULL, 'iblock'),
(96, 'security_filter_bypass', 'security', NULL, 'module'),
(97, 'security_edit_user_otp', 'security', NULL, 'module'),
(98, 'security_module_settings_read', 'security', NULL, 'module'),
(99, 'security_panel_view', 'security', NULL, 'module'),
(100, 'security_filter_settings_read', 'security', NULL, 'module'),
(101, 'security_otp_settings_read', 'security', NULL, 'module'),
(102, 'security_iprule_admin_settings_read', 'security', NULL, 'module'),
(103, 'security_session_settings_read', 'security', NULL, 'module'),
(104, 'security_redirect_settings_read', 'security', NULL, 'module'),
(105, 'security_stat_activity_settings_read', 'security', NULL, 'module'),
(106, 'security_iprule_settings_read', 'security', NULL, 'module'),
(107, 'security_antivirus_settings_read', 'security', NULL, 'module'),
(108, 'security_frame_settings_read', 'security', NULL, 'module'),
(109, 'security_module_settings_write', 'security', NULL, 'module'),
(110, 'security_filter_settings_write', 'security', NULL, 'module'),
(111, 'security_otp_settings_write', 'security', NULL, 'module'),
(112, 'security_iprule_admin_settings_write', 'security', NULL, 'module'),
(113, 'security_session_settings_write', 'security', NULL, 'module'),
(114, 'security_redirect_settings_write', 'security', NULL, 'module'),
(115, 'security_stat_activity_settings_write', 'security', NULL, 'module'),
(116, 'security_iprule_settings_write', 'security', NULL, 'module'),
(117, 'security_file_verifier_sign', 'security', NULL, 'module'),
(118, 'security_file_verifier_collect', 'security', NULL, 'module'),
(119, 'security_file_verifier_verify', 'security', NULL, 'module'),
(120, 'security_antivirus_settings_write', 'security', NULL, 'module'),
(121, 'security_frame_settings_write', 'security', NULL, 'module'),
(122, 'seo_settings', 'seo', '', 'module'),
(123, 'seo_tools', 'seo', '', 'module');

-- --------------------------------------------------------

--
-- Table structure for table `b_option`
--

CREATE TABLE IF NOT EXISTS `b_option` (
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `ix_option` (`MODULE_ID`,`NAME`,`SITE_ID`),
  KEY `ix_option_name` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_option`
--

INSERT INTO `b_option` (`MODULE_ID`, `NAME`, `VALUE`, `DESCRIPTION`, `SITE_ID`) VALUES
('main', 'rating_authority_rating', '2', NULL, NULL),
('main', 'rating_assign_rating_group_add', '1', NULL, NULL),
('main', 'rating_assign_rating_group_delete', '1', NULL, NULL),
('main', 'rating_assign_rating_group', '3', NULL, NULL),
('main', 'rating_assign_authority_group_add', '2', NULL, NULL),
('main', 'rating_assign_authority_group_delete', '2', NULL, NULL),
('main', 'rating_assign_authority_group', '4', NULL, NULL),
('main', 'rating_community_size', '1', NULL, NULL),
('main', 'rating_community_authority', '30', NULL, NULL),
('main', 'rating_vote_weight', '10', NULL, NULL),
('main', 'rating_normalization_type', 'auto', NULL, NULL),
('main', 'rating_normalization', '10', NULL, NULL),
('main', 'rating_count_vote', '10', NULL, NULL),
('main', 'rating_authority_weight_formula', 'Y', NULL, NULL),
('main', 'rating_community_last_visit', '90', NULL, NULL),
('main', 'rating_text_like_y', 'Нравится', NULL, NULL),
('main', 'rating_text_like_n', 'Не нравится', NULL, NULL),
('main', 'rating_text_like_d', 'Это нравится', NULL, NULL),
('main', 'rating_assign_type', 'auto', NULL, NULL),
('main', 'rating_vote_type', 'like', NULL, NULL),
('main', 'rating_self_vote', 'Y', NULL, NULL),
('main', 'rating_vote_show', 'Y', NULL, NULL),
('main', 'rating_vote_template', 'like', NULL, NULL),
('main', 'rating_start_authority', '3', NULL, NULL),
('main', 'PARAM_MAX_SITES', '2', NULL, NULL),
('main', 'PARAM_MAX_USERS', '0', NULL, NULL),
('main', 'distributive6', 'Y', NULL, NULL),
('main', '~new_license11_sign', 'Y', NULL, NULL),
('main', 'GROUP_DEFAULT_TASK', '1', NULL, NULL),
('main', 'vendor', '1c_bitrix', NULL, NULL),
('main', 'admin_lid', 'ru', NULL, NULL),
('main', 'update_site', 'www.bitrixsoft.com', NULL, NULL),
('main', 'update_site_ns', 'Y', NULL, NULL),
('main', 'optimize_css_files', 'Y', NULL, NULL),
('main', 'optimize_js_files', 'Y', NULL, NULL),
('main', 'admin_passwordh', 'FVoQfWYUBgYtCUVcAhcFCgsTAQ==', NULL, NULL),
('main', 'server_uniq_id', 'bc44c71be8403dcad8fe8008cfeb30ad', NULL, NULL),
('blog', 'socNetNewPerms', 'Y', NULL, NULL),
('currency', 'installed_currencies', 'RUB,USD,EUR,UAH,BYR', NULL, NULL),
('fileman', 'use_editor_3', 'Y', NULL, NULL),
('forum', 'FILTER_DICT_W', '1', NULL, 'ru'),
('forum', 'FILTER_DICT_T', '2', NULL, 'ru'),
('forum', 'FILTER_DICT_W', '3', NULL, 'en'),
('forum', 'FILTER_DICT_T', '4', NULL, 'en'),
('forum', 'FILTER', 'N', NULL, NULL),
('sale', 'viewed_capability', 'N', NULL, NULL),
('sale', 'viewed_count', '10', NULL, NULL),
('sale', 'viewed_time', '5', NULL, NULL),
('sale', 'p2p_status_list', 'a:7:{i:0;s:1:"N";i:1;s:1:"P";i:2;s:1:"F";i:3;s:10:"F_CANCELED";i:4;s:10:"F_DELIVERY";i:5;s:5:"F_PAY";i:6;s:5:"F_OUT";}', NULL, NULL),
('sale', 'product_reserve_clear_period', '3', NULL, NULL),
('sale', 'sale_locationpro_migrated', 'Y', NULL, NULL),
('sale', 'sale_locationpro_enabled', 'Y', NULL, NULL),
('search', 'version', 'v2.0', NULL, NULL),
('search', 'dbnode_id', 'N', NULL, NULL),
('search', 'dbnode_status', 'ok', NULL, NULL),
('security', 'ipcheck_disable_file', '/bitrix/modules/ipcheck_disable_d8c0c8e98882f7c26f3f548248d8c91a', NULL, NULL),
('vote', 'VOTE_DIR', '', NULL, NULL),
('vote', 'VOTE_COMPATIBLE_OLD_TEMPLATE', 'N', NULL, NULL),
('main', 'email_from', 'test@test.ru', NULL, NULL),
('fileman', 'different_set', 'Y', NULL, NULL),
('fileman', 'menutypes', 'a:4:{s:4:\\"left\\";s:19:\\"Левое меню\\";s:3:\\"top\\";s:23:\\"Верхнее меню\\";s:6:\\"bottom\\";s:21:\\"Нижнее меню\\";s:8:\\"undertop\\";s:36:\\"Второе верхнее меню\\";}', NULL, 's1'),
('main', 'wizard_template_id', 'furniture', NULL, 's1'),
('main', 'wizard_site_logo', '0', NULL, 's1'),
('main', 'wizard_furniture_theme_id', 'pale-blue', NULL, 's1'),
('socialnetwork', 'allow_tooltip', 'N', NULL, NULL),
('fileman', 'propstypes', 'a:4:{s:11:\\"description\\";s:33:\\"Описание страницы\\";s:8:\\"keywords\\";s:27:\\"Ключевые слова\\";s:5:\\"title\\";s:44:\\"Заголовок окна браузера\\";s:14:\\"keywords_inner\\";s:35:\\"Продвигаемые слова\\";}', NULL, 's1'),
('search', 'suggest_save_days', '250', NULL, NULL),
('search', 'use_tf_cache', 'Y', NULL, NULL),
('search', 'use_word_distance', 'Y', NULL, NULL),
('search', 'use_social_rating', 'Y', NULL, NULL),
('iblock', 'use_htmledit', 'Y', NULL, NULL),
('socialservices', 'auth_services', 'a:12:{s:9:"VKontakte";s:1:"N";s:8:"MyMailRu";s:1:"N";s:7:"Twitter";s:1:"N";s:8:"Facebook";s:1:"N";s:11:"Livejournal";s:1:"Y";s:12:"YandexOpenID";s:1:"Y";s:7:"Rambler";s:1:"Y";s:12:"MailRuOpenID";s:1:"Y";s:12:"Liveinternet";s:1:"Y";s:7:"Blogger";s:1:"Y";s:6:"OpenID";s:1:"Y";s:6:"LiveID";s:1:"N";}', NULL, NULL),
('main', 'wizard_firstcorp_furniture_s1', 'Y', NULL, NULL),
('main', 'wizard_solution', 'corp_furniture', NULL, 's1'),
('fileman', 'stickers_use_hotkeys', 'N', NULL, NULL),
('storeassist', 'num_orders', 'a:2:{s:6:"newDay";i:0;s:7:"prevDay";i:0;}', NULL, NULL),
('storeassist', 'progress_percent', '0', NULL, NULL),
('fileman', 'use_pspell', 'N', NULL, NULL),
('fileman', 'GROUP_DEFAULT_TASK', '22', NULL, NULL),
('fileman', 'default_edit', 'text', NULL, NULL),
('fileman', 'use_medialib', 'Y', NULL, NULL),
('fileman', 'user_dics_path', '/bitrix/modules/fileman/u_dics', NULL, NULL),
('fileman', 'use_separeted_dics', 'N', NULL, NULL),
('fileman', 'use_custom_spell', 'N', NULL, NULL),
('fileman', 'ar_entities', 'umlya,greek,other', NULL, NULL),
('fileman', 'editor_body_id', '', NULL, NULL),
('fileman', 'editor_body_class', '', NULL, NULL),
('fileman', 'ml_thumb_width', '140', NULL, NULL),
('fileman', 'ml_thumb_height', '105', NULL, NULL),
('fileman', 'ml_media_extentions', 'jpg,jpeg,gif,png,flv,mp4,wmv,wma,mp3,ppt', NULL, NULL),
('fileman', 'ml_max_width', '1024', NULL, NULL),
('fileman', 'ml_max_height', '1024', NULL, NULL),
('fileman', 'ml_media_available_ext', 'jpg,jpeg,gif,png,flv,mp4,wmv,wma,mp3,ppt,aac', NULL, NULL),
('fileman', 'ml_use_default', '1', NULL, NULL),
('fileman', '~script_files', 'php,php3,php4,php5,php6,phtml,pl,asp,aspx,cgi,exe,ico,shtm,shtml', NULL, NULL),
('fileman', '~allowed_components', '', NULL, NULL),
('fileman', 'num_menu_param', '1', NULL, 's1'),
('fileman', 'search_max_open_file_size', '1024', NULL, NULL),
('fileman', 'search_max_res_count', '', NULL, NULL),
('fileman', 'search_time_step', '5', NULL, NULL),
('fileman', 'search_mask', '*.php', NULL, NULL),
('fileman', 'show_inc_icons', 'N', NULL, NULL),
('fileman', 'hide_physical_struc', '', NULL, NULL),
('fileman', 'use_translit', '1', NULL, NULL),
('fileman', 'use_translit_google', '1', NULL, NULL),
('fileman', 'log_menu', 'Y', NULL, NULL),
('fileman', 'log_page', 'Y', NULL, NULL),
('fileman', 'use_code_editor', 'Y', NULL, NULL),
('fileman', 'default_edit_groups', '', NULL, NULL),
('fileman', 'archive_step_time', '30', NULL, NULL),
('fileman', 'GROUP_DEFAULT_RIGHT', 'D', NULL, NULL),
('main', 'rcm_component_usage', '1431071687', NULL, NULL),
('main', 'signer_default_key', '059bed5aa49d3cf61a96ce5bc8b8edce1c81ae78ae8b671be83ea26b80c194bbd58bf610fe22cf4e185be1e2cf1c3f869d3ca623e98bf94ed589dfef1c2358ce', NULL, NULL),
('iblock', 'list_image_size', '50', NULL, NULL),
('iblock', 'detail_image_size', '200', NULL, NULL),
('iblock', 'show_xml_id', '', NULL, NULL),
('iblock', 'path2rss', '/upload/', NULL, NULL),
('iblock', 'combined_list_mode', 'Y', NULL, NULL),
('iblock', 'iblock_menu_max_sections', '50', NULL, NULL),
('iblock', 'event_log_iblock', 'Y', NULL, NULL),
('iblock', 'num_catalog_levels', '3', NULL, NULL),
('perfmon', 'bitrix_optimal', 'N', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_cache`
--

CREATE TABLE IF NOT EXISTS `b_perf_cache` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `HIT_ID` int(18) DEFAULT NULL,
  `COMPONENT_ID` int(18) DEFAULT NULL,
  `NN` int(18) DEFAULT NULL,
  `CACHE_SIZE` float DEFAULT NULL,
  `OP_MODE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MODULE_NAME` text COLLATE utf8_unicode_ci,
  `COMPONENT_NAME` text COLLATE utf8_unicode_ci,
  `BASE_DIR` text COLLATE utf8_unicode_ci,
  `INIT_DIR` text COLLATE utf8_unicode_ci,
  `FILE_NAME` text COLLATE utf8_unicode_ci,
  `FILE_PATH` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_PERF_CACHE_0` (`HIT_ID`,`NN`),
  KEY `IX_B_PERF_CACHE_1` (`COMPONENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_cluster`
--

CREATE TABLE IF NOT EXISTS `b_perf_cluster` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `THREADS` int(11) DEFAULT NULL,
  `HITS` int(11) DEFAULT NULL,
  `ERRORS` int(11) DEFAULT NULL,
  `PAGES_PER_SECOND` float DEFAULT NULL,
  `PAGE_EXEC_TIME` float DEFAULT NULL,
  `PAGE_RESP_TIME` float DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_component`
--

CREATE TABLE IF NOT EXISTS `b_perf_component` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `HIT_ID` int(18) DEFAULT NULL,
  `NN` int(18) DEFAULT NULL,
  `CACHE_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CACHE_SIZE` int(11) DEFAULT NULL,
  `CACHE_COUNT_R` int(11) DEFAULT NULL,
  `CACHE_COUNT_W` int(11) DEFAULT NULL,
  `CACHE_COUNT_C` int(11) DEFAULT NULL,
  `COMPONENT_TIME` float DEFAULT NULL,
  `QUERIES` int(11) DEFAULT NULL,
  `QUERIES_TIME` float DEFAULT NULL,
  `COMPONENT_NAME` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_PERF_COMPONENT_0` (`HIT_ID`,`NN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_error`
--

CREATE TABLE IF NOT EXISTS `b_perf_error` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `HIT_ID` int(18) DEFAULT NULL,
  `ERRNO` int(18) DEFAULT NULL,
  `ERRSTR` text COLLATE utf8_unicode_ci,
  `ERRFILE` text COLLATE utf8_unicode_ci,
  `ERRLINE` int(18) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_PERF_ERROR_0` (`HIT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_history`
--

CREATE TABLE IF NOT EXISTS `b_perf_history` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TOTAL_MARK` float DEFAULT NULL,
  `ACCELERATOR_ENABLED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_hit`
--

CREATE TABLE IF NOT EXISTS `b_perf_hit` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_HIT` datetime DEFAULT NULL,
  `IS_ADMIN` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REQUEST_METHOD` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERVER_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SERVER_PORT` int(11) DEFAULT NULL,
  `SCRIPT_NAME` text COLLATE utf8_unicode_ci,
  `REQUEST_URI` text COLLATE utf8_unicode_ci,
  `INCLUDED_FILES` int(11) DEFAULT NULL,
  `MEMORY_PEAK_USAGE` int(11) DEFAULT NULL,
  `CACHE_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CACHE_SIZE` int(11) DEFAULT NULL,
  `CACHE_COUNT_R` int(11) DEFAULT NULL,
  `CACHE_COUNT_W` int(11) DEFAULT NULL,
  `CACHE_COUNT_C` int(11) DEFAULT NULL,
  `QUERIES` int(11) DEFAULT NULL,
  `QUERIES_TIME` float DEFAULT NULL,
  `COMPONENTS` int(11) DEFAULT NULL,
  `COMPONENTS_TIME` float DEFAULT NULL,
  `SQL_LOG` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAGE_TIME` float DEFAULT NULL,
  `PROLOG_TIME` float DEFAULT NULL,
  `PROLOG_BEFORE_TIME` float DEFAULT NULL,
  `AGENTS_TIME` float DEFAULT NULL,
  `PROLOG_AFTER_TIME` float DEFAULT NULL,
  `WORK_AREA_TIME` float DEFAULT NULL,
  `EPILOG_TIME` float DEFAULT NULL,
  `EPILOG_BEFORE_TIME` float DEFAULT NULL,
  `EVENTS_TIME` float DEFAULT NULL,
  `EPILOG_AFTER_TIME` float DEFAULT NULL,
  `MENU_RECALC` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_PERF_HIT_0` (`DATE_HIT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_index_ban`
--

CREATE TABLE IF NOT EXISTS `b_perf_index_ban` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BAN_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLUMN_NAMES` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_index_complete`
--

CREATE TABLE IF NOT EXISTS `b_perf_index_complete` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BANNED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLUMN_NAMES` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INDEX_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_perf_index_complete_0` (`TABLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_index_suggest`
--

CREATE TABLE IF NOT EXISTS `b_perf_index_suggest` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SQL_MD5` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SQL_COUNT` int(11) DEFAULT NULL,
  `SQL_TIME` float DEFAULT NULL,
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TABLE_ALIAS` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLUMN_NAMES` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SQL_TEXT` text COLLATE utf8_unicode_ci,
  `SQL_EXPLAIN` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_perf_index_suggest_0` (`SQL_MD5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_index_suggest_sql`
--

CREATE TABLE IF NOT EXISTS `b_perf_index_suggest_sql` (
  `SUGGEST_ID` int(11) NOT NULL DEFAULT '0',
  `SQL_ID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SUGGEST_ID`,`SQL_ID`),
  KEY `ix_b_perf_index_suggest_sql_0` (`SQL_ID`,`SUGGEST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_sql`
--

CREATE TABLE IF NOT EXISTS `b_perf_sql` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `HIT_ID` int(18) DEFAULT NULL,
  `COMPONENT_ID` int(18) DEFAULT NULL,
  `NN` int(18) DEFAULT NULL,
  `QUERY_TIME` float DEFAULT NULL,
  `NODE_ID` int(18) DEFAULT NULL,
  `MODULE_NAME` text COLLATE utf8_unicode_ci,
  `COMPONENT_NAME` text COLLATE utf8_unicode_ci,
  `SQL_TEXT` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_PERF_SQL_0` (`HIT_ID`,`NN`),
  KEY `IX_B_PERF_SQL_1` (`COMPONENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_sql_backtrace`
--

CREATE TABLE IF NOT EXISTS `b_perf_sql_backtrace` (
  `SQL_ID` int(18) NOT NULL DEFAULT '0',
  `NN` int(18) NOT NULL DEFAULT '0',
  `FILE_NAME` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LINE_NO` int(18) DEFAULT NULL,
  `CLASS_NAME` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FUNCTION_NAME` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`SQL_ID`,`NN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_tab_column_stat`
--

CREATE TABLE IF NOT EXISTS `b_perf_tab_column_stat` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLUMN_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TABLE_ROWS` float DEFAULT NULL,
  `COLUMN_ROWS` float DEFAULT NULL,
  `VALUE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_perf_tab_column_stat` (`TABLE_NAME`,`COLUMN_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_tab_stat`
--

CREATE TABLE IF NOT EXISTS `b_perf_tab_stat` (
  `TABLE_NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `TABLE_SIZE` float DEFAULT NULL,
  `TABLE_ROWS` float DEFAULT NULL,
  PRIMARY KEY (`TABLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_perf_test`
--

CREATE TABLE IF NOT EXISTS `b_perf_test` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `REFERENCE_ID` int(18) DEFAULT NULL,
  `NAME` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_PERF_TEST_0` (`REFERENCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_posting`
--

CREATE TABLE IF NOT EXISTS `b_posting` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `STATUS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D',
  `VERSION` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_SENT` datetime DEFAULT NULL,
  `SENT_BCC` mediumtext COLLATE utf8_unicode_ci,
  `FROM_FIELD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TO_FIELD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BCC_FIELD` mediumtext COLLATE utf8_unicode_ci,
  `EMAIL_FILTER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUBJECT` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `BODY_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `BODY` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `DIRECT_SEND` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CHARSET` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MSG_CHARSET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUBSCR_FORMAT` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERROR_EMAIL` mediumtext COLLATE utf8_unicode_ci,
  `AUTO_SEND_TIME` datetime DEFAULT NULL,
  `BCC_TO_SEND` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_posting_email`
--

CREATE TABLE IF NOT EXISTS `b_posting_email` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `POSTING_ID` int(11) NOT NULL,
  `STATUS` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SUBSCRIPTION_ID` int(11) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_posting_email_status` (`POSTING_ID`,`STATUS`),
  KEY `ix_posting_email_email` (`POSTING_ID`,`EMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_posting_file`
--

CREATE TABLE IF NOT EXISTS `b_posting_file` (
  `POSTING_ID` int(11) NOT NULL,
  `FILE_ID` int(11) NOT NULL,
  UNIQUE KEY `UK_POSTING_POSTING_FILE` (`POSTING_ID`,`FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_posting_group`
--

CREATE TABLE IF NOT EXISTS `b_posting_group` (
  `POSTING_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  UNIQUE KEY `UK_POSTING_POSTING_GROUP` (`POSTING_ID`,`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_posting_rubric`
--

CREATE TABLE IF NOT EXISTS `b_posting_rubric` (
  `POSTING_ID` int(11) NOT NULL,
  `LIST_RUBRIC_ID` int(11) NOT NULL,
  UNIQUE KEY `UK_POSTING_POSTING_RUBRIC` (`POSTING_ID`,`LIST_RUBRIC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_pull_channel`
--

CREATE TABLE IF NOT EXISTS `b_pull_channel` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `CHANNEL_TYPE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CHANNEL_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `LAST_ID` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_PULL_CN_UID` (`USER_ID`,`CHANNEL_TYPE`),
  KEY `IX_PULL_CN_CID` (`CHANNEL_ID`),
  KEY `IX_PULL_CN_D` (`DATE_CREATE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_pull_push`
--

CREATE TABLE IF NOT EXISTS `b_pull_push` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `DEVICE_TYPE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `APP_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNIQUE_HASH` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEVICE_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEVICE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEVICE_TOKEN` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime NOT NULL,
  `DATE_AUTH` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_PULL_PSH_UID` (`USER_ID`),
  KEY `IX_PULL_PSH_UH` (`UNIQUE_HASH`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_pull_push_queue`
--

CREATE TABLE IF NOT EXISTS `b_pull_push_queue` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `TAG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUB_TAG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MESSAGE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  `BADGE` int(11) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `APP_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_PULL_PSHQ_UT` (`USER_ID`,`TAG`),
  KEY `IX_PULL_PSHQ_UST` (`USER_ID`,`SUB_TAG`),
  KEY `IX_PULL_PSHQ_UID` (`USER_ID`),
  KEY `IX_PULL_PSHQ_DC` (`DATE_CREATE`),
  KEY `IX_PULL_PSHQ_AID` (`APP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_pull_stack`
--

CREATE TABLE IF NOT EXISTS `b_pull_stack` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `CHANNEL_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE` text COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_PULL_STACK_CID` (`CHANNEL_ID`),
  KEY `IX_PULL_STACK_D` (`DATE_CREATE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_pull_watch`
--

CREATE TABLE IF NOT EXISTS `b_pull_watch` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `CHANNEL_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `TAG` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_PULL_W_UT` (`USER_ID`,`TAG`),
  KEY `IX_PULL_W_D` (`DATE_CREATE`),
  KEY `IX_PULL_W_T` (`TAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_rating`
--

CREATE TABLE IF NOT EXISTS `b_rating` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CALCULATION_METHOD` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SUM',
  `CREATED` datetime DEFAULT NULL,
  `LAST_MODIFIED` datetime DEFAULT NULL,
  `LAST_CALCULATED` datetime DEFAULT NULL,
  `POSITION` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `AUTHORITY` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `CALCULATED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CONFIGS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `b_rating`
--

INSERT INTO `b_rating` (`ID`, `ACTIVE`, `NAME`, `ENTITY_ID`, `CALCULATION_METHOD`, `CREATED`, `LAST_MODIFIED`, `LAST_CALCULATED`, `POSITION`, `AUTHORITY`, `CALCULATED`, `CONFIGS`) VALUES
(1, 'N', 'Рейтинг', 'USER', 'SUM', '2015-04-22 23:24:54', NULL, NULL, 'Y', 'N', 'N', 'a:3:{s:4:"MAIN";a:2:{s:4:"VOTE";a:1:{s:4:"USER";a:2:{s:11:"COEFFICIENT";s:1:"1";s:5:"LIMIT";s:2:"30";}}s:6:"RATING";a:1:{s:5:"BONUS";a:2:{s:6:"ACTIVE";s:1:"Y";s:11:"COEFFICIENT";s:1:"1";}}}s:5:"FORUM";a:2:{s:4:"VOTE";a:2:{s:5:"TOPIC";a:3:{s:6:"ACTIVE";s:1:"Y";s:11:"COEFFICIENT";s:3:"0.5";s:5:"LIMIT";s:2:"30";}s:4:"POST";a:3:{s:6:"ACTIVE";s:1:"Y";s:11:"COEFFICIENT";s:3:"0.1";s:5:"LIMIT";s:2:"30";}}s:6:"RATING";a:1:{s:8:"ACTIVITY";a:9:{s:6:"ACTIVE";s:1:"Y";s:16:"TODAY_TOPIC_COEF";s:3:"0.4";s:15:"WEEK_TOPIC_COEF";s:3:"0.2";s:16:"MONTH_TOPIC_COEF";s:3:"0.1";s:14:"ALL_TOPIC_COEF";s:1:"0";s:15:"TODAY_POST_COEF";s:3:"0.2";s:14:"WEEK_POST_COEF";s:3:"0.1";s:15:"MONTH_POST_COEF";s:4:"0.05";s:13:"ALL_POST_COEF";s:1:"0";}}}s:4:"BLOG";a:2:{s:4:"VOTE";a:2:{s:4:"POST";a:3:{s:6:"ACTIVE";s:1:"Y";s:11:"COEFFICIENT";s:3:"0.5";s:5:"LIMIT";s:2:"30";}s:7:"COMMENT";a:3:{s:6:"ACTIVE";s:1:"Y";s:11:"COEFFICIENT";s:3:"0.1";s:5:"LIMIT";s:2:"30";}}s:6:"RATING";a:1:{s:8:"ACTIVITY";a:9:{s:6:"ACTIVE";s:1:"Y";s:15:"TODAY_POST_COEF";s:3:"0.4";s:14:"WEEK_POST_COEF";s:3:"0.2";s:15:"MONTH_POST_COEF";s:3:"0.1";s:13:"ALL_POST_COEF";s:1:"0";s:18:"TODAY_COMMENT_COEF";s:3:"0.2";s:17:"WEEK_COMMENT_COEF";s:3:"0.1";s:18:"MONTH_COMMENT_COEF";s:4:"0.05";s:16:"ALL_COMMENT_COEF";s:1:"0";}}}}'),
(2, 'N', 'Авторитет', 'USER', 'SUM', '2015-04-22 23:24:54', NULL, NULL, 'Y', 'Y', 'N', 'a:3:{s:4:"MAIN";a:2:{s:4:"VOTE";a:1:{s:4:"USER";a:3:{s:6:"ACTIVE";s:1:"Y";s:11:"COEFFICIENT";s:1:"1";s:5:"LIMIT";s:1:"0";}}s:6:"RATING";a:1:{s:5:"BONUS";a:2:{s:6:"ACTIVE";s:1:"Y";s:11:"COEFFICIENT";s:1:"1";}}}s:5:"FORUM";a:2:{s:4:"VOTE";a:2:{s:5:"TOPIC";a:2:{s:11:"COEFFICIENT";s:1:"1";s:5:"LIMIT";s:2:"30";}s:4:"POST";a:2:{s:11:"COEFFICIENT";s:1:"1";s:5:"LIMIT";s:2:"30";}}s:6:"RATING";a:1:{s:8:"ACTIVITY";a:8:{s:16:"TODAY_TOPIC_COEF";s:2:"20";s:15:"WEEK_TOPIC_COEF";s:2:"10";s:16:"MONTH_TOPIC_COEF";s:1:"5";s:14:"ALL_TOPIC_COEF";s:1:"0";s:15:"TODAY_POST_COEF";s:3:"0.4";s:14:"WEEK_POST_COEF";s:3:"0.2";s:15:"MONTH_POST_COEF";s:3:"0.1";s:13:"ALL_POST_COEF";s:1:"0";}}}s:4:"BLOG";a:2:{s:4:"VOTE";a:2:{s:4:"POST";a:2:{s:11:"COEFFICIENT";s:1:"1";s:5:"LIMIT";s:2:"30";}s:7:"COMMENT";a:2:{s:11:"COEFFICIENT";s:1:"1";s:5:"LIMIT";s:2:"30";}}s:6:"RATING";a:1:{s:8:"ACTIVITY";a:8:{s:15:"TODAY_POST_COEF";s:3:"0.4";s:14:"WEEK_POST_COEF";s:3:"0.2";s:15:"MONTH_POST_COEF";s:3:"0.1";s:13:"ALL_POST_COEF";s:1:"0";s:18:"TODAY_COMMENT_COEF";s:3:"0.2";s:17:"WEEK_COMMENT_COEF";s:3:"0.1";s:18:"MONTH_COMMENT_COEF";s:4:"0.05";s:16:"ALL_COMMENT_COEF";s:1:"0";}}}}');

-- --------------------------------------------------------

--
-- Table structure for table `b_rating_component`
--

CREATE TABLE IF NOT EXISTS `b_rating_component` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ENTITY_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `RATING_TYPE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `COMPLEX_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CLASS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CALC_METHOD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `EXCEPTION_METHOD` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_MODIFIED` datetime DEFAULT NULL,
  `LAST_CALCULATED` datetime DEFAULT NULL,
  `NEXT_CALCULATION` datetime DEFAULT NULL,
  `REFRESH_INTERVAL` int(11) NOT NULL,
  `CONFIG` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_RATING_ID_1` (`RATING_ID`,`ACTIVE`,`NEXT_CALCULATION`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_rating_component_results`
--

CREATE TABLE IF NOT EXISTS `b_rating_component_results` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_ID` int(11) NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `RATING_TYPE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `COMPLEX_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CURRENT_VALUE` decimal(18,4) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ENTITY_TYPE_ID` (`ENTITY_TYPE_ID`),
  KEY `IX_COMPLEX_NAME` (`COMPLEX_NAME`),
  KEY `IX_RATING_ID_2` (`RATING_ID`,`COMPLEX_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_rating_prepare`
--

CREATE TABLE IF NOT EXISTS `b_rating_prepare` (
  `ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_rating_results`
--

CREATE TABLE IF NOT EXISTS `b_rating_results` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_ID` int(11) NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `CURRENT_VALUE` decimal(18,4) DEFAULT NULL,
  `PREVIOUS_VALUE` decimal(18,4) DEFAULT NULL,
  `CURRENT_POSITION` int(11) DEFAULT '0',
  `PREVIOUS_POSITION` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_RATING_3` (`RATING_ID`,`ENTITY_TYPE_ID`,`ENTITY_ID`),
  KEY `IX_RATING_4` (`RATING_ID`,`ENTITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_rating_rule`
--

CREATE TABLE IF NOT EXISTS `b_rating_rule` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `NAME` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_MODULE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONDITION_CLASS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_METHOD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION_CONFIG` text COLLATE utf8_unicode_ci NOT NULL,
  `ACTION_NAME` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ACTION_CONFIG` text COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ACTIVATE_CLASS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVATE_METHOD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEACTIVATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DEACTIVATE_CLASS` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEACTIVATE_METHOD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED` datetime DEFAULT NULL,
  `LAST_MODIFIED` datetime DEFAULT NULL,
  `LAST_APPLIED` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `b_rating_rule`
--

INSERT INTO `b_rating_rule` (`ID`, `ACTIVE`, `NAME`, `ENTITY_TYPE_ID`, `CONDITION_NAME`, `CONDITION_MODULE`, `CONDITION_CLASS`, `CONDITION_METHOD`, `CONDITION_CONFIG`, `ACTION_NAME`, `ACTION_CONFIG`, `ACTIVATE`, `ACTIVATE_CLASS`, `ACTIVATE_METHOD`, `DEACTIVATE`, `DEACTIVATE_CLASS`, `DEACTIVATE_METHOD`, `CREATED`, `LAST_MODIFIED`, `LAST_APPLIED`) VALUES
(1, 'N', 'Добавление в группу пользователей, имеющих право голосовать за рейтинг', 'USER', 'AUTHORITY', NULL, 'CRatingRulesMain', 'ratingCheck', 'a:1:{s:9:"AUTHORITY";a:2:{s:16:"RATING_CONDITION";i:1;s:12:"RATING_VALUE";i:1;}}', 'ADD_TO_GROUP', 'a:1:{s:12:"ADD_TO_GROUP";a:1:{s:8:"GROUP_ID";s:1:"3";}}', 'N', 'CRatingRulesMain', 'addToGroup', 'N', 'CRatingRulesMain ', 'addToGroup', '2015-04-22 23:24:54', '2015-04-22 23:24:54', NULL),
(2, 'N', 'Удаление из группы пользователей, не имеющих права голосовать за рейтинг', 'USER', 'AUTHORITY', NULL, 'CRatingRulesMain', 'ratingCheck', 'a:1:{s:9:"AUTHORITY";a:2:{s:16:"RATING_CONDITION";i:2;s:12:"RATING_VALUE";i:1;}}', 'REMOVE_FROM_GROUP', 'a:1:{s:17:"REMOVE_FROM_GROUP";a:1:{s:8:"GROUP_ID";s:1:"3";}}', 'N', 'CRatingRulesMain', 'removeFromGroup', 'N', 'CRatingRulesMain ', 'removeFromGroup', '2015-04-22 23:24:54', '2015-04-22 23:24:54', NULL),
(3, 'N', 'Добавление в группу пользователей, имеющих право голосовать за авторитет', 'USER', 'AUTHORITY', NULL, 'CRatingRulesMain', 'ratingCheck', 'a:1:{s:9:"AUTHORITY";a:2:{s:16:"RATING_CONDITION";i:1;s:12:"RATING_VALUE";i:2;}}', 'ADD_TO_GROUP', 'a:1:{s:12:"ADD_TO_GROUP";a:1:{s:8:"GROUP_ID";s:1:"4";}}', 'N', 'CRatingRulesMain', 'addToGroup', 'N', 'CRatingRulesMain ', 'addToGroup', '2015-04-22 23:24:54', '2015-04-22 23:24:54', NULL),
(4, 'N', 'Удаление из группы пользователей, не имеющих права голосовать за авторитет', 'USER', 'AUTHORITY', NULL, 'CRatingRulesMain', 'ratingCheck', 'a:1:{s:9:"AUTHORITY";a:2:{s:16:"RATING_CONDITION";i:2;s:12:"RATING_VALUE";i:2;}}', 'REMOVE_FROM_GROUP', 'a:1:{s:17:"REMOVE_FROM_GROUP";a:1:{s:8:"GROUP_ID";s:1:"4";}}', 'N', 'CRatingRulesMain', 'removeFromGroup', 'N', 'CRatingRulesMain ', 'removeFromGroup', '2015-04-22 23:24:54', '2015-04-22 23:24:54', NULL),
(5, 'Y', 'Автоматическое голосование за авторитет пользователя', 'USER', 'VOTE', NULL, 'CRatingRulesMain', 'voteCheck', 'a:1:{s:4:"VOTE";a:6:{s:10:"VOTE_LIMIT";i:90;s:11:"VOTE_RESULT";i:10;s:16:"VOTE_FORUM_TOPIC";d:0.5;s:15:"VOTE_FORUM_POST";d:0.10000000000000001;s:14:"VOTE_BLOG_POST";d:0.5;s:17:"VOTE_BLOG_COMMENT";d:0.10000000000000001;}}', 'empty', 'a:0:{}', 'N', 'empty', 'empty', 'N', 'empty ', 'empty', '2015-04-22 23:24:54', '2015-04-22 23:24:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_rating_rule_vetting`
--

CREATE TABLE IF NOT EXISTS `b_rating_rule_vetting` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RULE_ID` int(11) NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `ACTIVATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `APPLIED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `RULE_ID` (`RULE_ID`,`ENTITY_TYPE_ID`,`ENTITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_rating_user`
--

CREATE TABLE IF NOT EXISTS `b_rating_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_ID` int(11) NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `BONUS` decimal(18,4) DEFAULT '0.0000',
  `VOTE_WEIGHT` decimal(18,4) DEFAULT '0.0000',
  `VOTE_COUNT` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RATING_ID` (`RATING_ID`,`ENTITY_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `b_rating_user`
--

INSERT INTO `b_rating_user` (`ID`, `RATING_ID`, `ENTITY_ID`, `BONUS`, `VOTE_WEIGHT`, `VOTE_COUNT`) VALUES
(1, 2, 1, '3.0000', '30.0000', 13);

-- --------------------------------------------------------

--
-- Table structure for table `b_rating_vote`
--

CREATE TABLE IF NOT EXISTS `b_rating_vote` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_VOTING_ID` int(11) NOT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `OWNER_ID` int(11) NOT NULL,
  `VALUE` decimal(18,4) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED` datetime NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `USER_IP` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_RAT_VOTE_ID` (`RATING_VOTING_ID`,`USER_ID`),
  KEY `IX_RAT_VOTE_ID_2` (`ENTITY_TYPE_ID`,`ENTITY_ID`,`USER_ID`),
  KEY `IX_RAT_VOTE_ID_3` (`OWNER_ID`,`CREATED`),
  KEY `IX_RAT_VOTE_ID_4` (`USER_ID`),
  KEY `IX_RAT_VOTE_ID_5` (`CREATED`,`VALUE`),
  KEY `IX_RAT_VOTE_ID_6` (`ACTIVE`),
  KEY `IX_RAT_VOTE_ID_7` (`RATING_VOTING_ID`,`CREATED`),
  KEY `IX_RAT_VOTE_ID_8` (`ENTITY_TYPE_ID`,`CREATED`),
  KEY `IX_RAT_VOTE_ID_9` (`CREATED`,`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_rating_vote_group`
--

CREATE TABLE IF NOT EXISTS `b_rating_vote_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUP_ID` int(11) NOT NULL,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `RATING_ID` (`GROUP_ID`,`TYPE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `b_rating_vote_group`
--

INSERT INTO `b_rating_vote_group` (`ID`, `GROUP_ID`, `TYPE`) VALUES
(5, 1, 'A'),
(1, 1, 'R'),
(3, 1, 'R'),
(2, 3, 'R'),
(4, 3, 'R'),
(6, 4, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `b_rating_voting`
--

CREATE TABLE IF NOT EXISTS `b_rating_voting` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `OWNER_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `CREATED` datetime DEFAULT NULL,
  `LAST_CALCULATED` datetime DEFAULT NULL,
  `TOTAL_VALUE` decimal(18,4) NOT NULL,
  `TOTAL_VOTES` int(11) NOT NULL,
  `TOTAL_POSITIVE_VOTES` int(11) NOT NULL,
  `TOTAL_NEGATIVE_VOTES` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_ENTITY_TYPE_ID_2` (`ENTITY_TYPE_ID`,`ENTITY_ID`,`ACTIVE`),
  KEY `IX_ENTITY_TYPE_ID_4` (`TOTAL_VALUE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_rating_voting_prepare`
--

CREATE TABLE IF NOT EXISTS `b_rating_voting_prepare` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_VOTING_ID` int(11) NOT NULL,
  `TOTAL_VALUE` decimal(18,4) NOT NULL,
  `TOTAL_VOTES` int(11) NOT NULL,
  `TOTAL_POSITIVE_VOTES` int(11) NOT NULL,
  `TOTAL_NEGATIVE_VOTES` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_RATING_VOTING_ID` (`RATING_VOTING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_rating_weight`
--

CREATE TABLE IF NOT EXISTS `b_rating_weight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RATING_FROM` decimal(18,4) NOT NULL,
  `RATING_TO` decimal(18,4) NOT NULL,
  `WEIGHT` decimal(18,4) DEFAULT '0.0000',
  `COUNT` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `b_rating_weight`
--

INSERT INTO `b_rating_weight` (`ID`, `RATING_FROM`, `RATING_TO`, `WEIGHT`, `COUNT`) VALUES
(1, '-1000000.0000', '1000000.0000', '1.0000', 10);

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_affiliate`
--

CREATE TABLE IF NOT EXISTS `b_sale_affiliate` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `AFFILIATE_ID` int(11) DEFAULT NULL,
  `PLAN_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DATE_CREATE` datetime NOT NULL,
  `PAID_SUM` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `APPROVED_SUM` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `PENDING_SUM` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `ITEMS_NUMBER` int(11) NOT NULL DEFAULT '0',
  `ITEMS_SUM` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `LAST_CALCULATE` datetime DEFAULT NULL,
  `AFF_SITE` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AFF_DESCRIPTION` text COLLATE utf8_unicode_ci,
  `FIX_PLAN` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_SAA_USER_ID` (`USER_ID`,`SITE_ID`),
  KEY `IX_SAA_AFFILIATE_ID` (`AFFILIATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_affiliate_plan`
--

CREATE TABLE IF NOT EXISTS `b_sale_affiliate_plan` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `BASE_RATE` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `BASE_RATE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `BASE_RATE_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MIN_PAY` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `MIN_PLAN_VALUE` decimal(18,4) DEFAULT NULL,
  `VALUE_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_affiliate_plan_section`
--

CREATE TABLE IF NOT EXISTS `b_sale_affiliate_plan_section` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PLAN_ID` int(11) NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'catalog',
  `SECTION_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `RATE` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `RATE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `RATE_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_SAP_PLAN_ID` (`PLAN_ID`,`MODULE_ID`,`SECTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_affiliate_tier`
--

CREATE TABLE IF NOT EXISTS `b_sale_affiliate_tier` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `RATE1` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `RATE2` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `RATE3` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `RATE4` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `RATE5` decimal(18,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_SAT_SITE_ID` (`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_affiliate_transact`
--

CREATE TABLE IF NOT EXISTS `b_sale_affiliate_transact` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `AFFILIATE_ID` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TRANSACT_DATE` datetime NOT NULL,
  `AMOUNT` decimal(18,4) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `DEBIT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DESCRIPTION` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `EMPLOYEE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_SAT_AFFILIATE_ID` (`AFFILIATE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_auxiliary`
--

CREATE TABLE IF NOT EXISTS `b_sale_auxiliary` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ITEM` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM_MD5` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `DATE_INSERT` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_STT_USER_ITEM` (`USER_ID`,`ITEM_MD5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_basket`
--

CREATE TABLE IF NOT EXISTS `b_sale_basket` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FUSER_ID` int(11) NOT NULL,
  `ORDER_ID` int(11) DEFAULT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `PRODUCT_PRICE_ID` int(11) DEFAULT NULL,
  `PRICE` decimal(18,2) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_INSERT` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `WEIGHT` double(18,2) DEFAULT NULL,
  `QUANTITY` double(18,2) NOT NULL DEFAULT '0.00',
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `DELAY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CAN_BUY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `MODULE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CALLBACK_FUNC` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NOTES` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORDER_CALLBACK_FUNC` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DETAIL_PAGE_URL` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISCOUNT_PRICE` decimal(18,2) NOT NULL DEFAULT '0.00',
  `CANCEL_CALLBACK_FUNC` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAY_CALLBACK_FUNC` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_PROVIDER_CLASS` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CATALOG_XML_ID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_XML_ID` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISCOUNT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISCOUNT_VALUE` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISCOUNT_COUPON` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VAT_RATE` decimal(18,2) DEFAULT '0.00',
  `SUBSCRIBE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DEDUCTED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `RESERVED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `BARCODE_MULTI` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `RESERVE_QUANTITY` double DEFAULT NULL,
  `CUSTOM_PRICE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DIMENSIONS` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `SET_PARENT_ID` int(11) DEFAULT NULL,
  `MEASURE_CODE` int(11) DEFAULT NULL,
  `MEASURE_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RECOMMENDATION` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_BASKET_LID` (`LID`),
  KEY `IXS_BASKET_USER_ID` (`FUSER_ID`),
  KEY `IXS_BASKET_ORDER_ID` (`ORDER_ID`),
  KEY `IXS_BASKET_PRODUCT_ID` (`PRODUCT_ID`),
  KEY `IXS_BASKET_PRODUCT_PRICE_ID` (`PRODUCT_PRICE_ID`),
  KEY `IXS_SBAS_XML_ID` (`PRODUCT_XML_ID`,`CATALOG_XML_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_basket_props`
--

CREATE TABLE IF NOT EXISTS `b_sale_basket_props` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BASKET_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`),
  KEY `IXS_BASKET_PROPS_BASKET` (`BASKET_ID`),
  KEY `IXS_BASKET_PROPS_CODE` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_delivery`
--

CREATE TABLE IF NOT EXISTS `b_sale_delivery` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `PERIOD_FROM` int(11) DEFAULT NULL,
  `PERIOD_TO` int(11) DEFAULT NULL,
  `PERIOD_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WEIGHT_FROM` int(11) DEFAULT NULL,
  `WEIGHT_TO` int(11) DEFAULT NULL,
  `ORDER_PRICE_FROM` decimal(18,2) DEFAULT NULL,
  `ORDER_PRICE_TO` decimal(18,2) DEFAULT NULL,
  `ORDER_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `PRICE` decimal(18,2) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `LOGOTIP` int(11) DEFAULT NULL,
  `STORE` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IXS_DELIVERY_LID` (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_delivery2location`
--

CREATE TABLE IF NOT EXISTS `b_sale_delivery2location` (
  `DELIVERY_ID` int(11) NOT NULL,
  `LOCATION_CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LOCATION_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'L',
  PRIMARY KEY (`DELIVERY_ID`,`LOCATION_CODE`,`LOCATION_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_delivery2paysystem`
--

CREATE TABLE IF NOT EXISTS `b_sale_delivery2paysystem` (
  `DELIVERY_ID` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `DELIVERY_PROFILE_ID` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAYSYSTEM_ID` int(11) NOT NULL,
  KEY `IX_DELIVERY` (`DELIVERY_ID`),
  KEY `IX_PAYSYSTEM` (`PAYSYSTEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_delivery_handler`
--

CREATE TABLE IF NOT EXISTS `b_sale_delivery_handler` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci DEFAULT '',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `HID` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `HANDLER` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SETTINGS` text COLLATE utf8_unicode_ci,
  `PROFILES` text COLLATE utf8_unicode_ci,
  `TAX_RATE` double DEFAULT '0',
  `LOGOTIP` int(11) DEFAULT NULL,
  `BASE_CURRENCY` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_HID` (`HID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_discount`
--

CREATE TABLE IF NOT EXISTS `b_sale_discount` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRICE_FROM` decimal(18,2) DEFAULT NULL,
  `PRICE_TO` decimal(18,2) DEFAULT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISCOUNT_VALUE` decimal(18,2) NOT NULL,
  `DISCOUNT_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '100',
  `ACTIVE_FROM` datetime DEFAULT NULL,
  `ACTIVE_TO` datetime DEFAULT NULL,
  `TIMESTAMP_X` datetime DEFAULT NULL,
  `MODIFIED_BY` int(18) DEFAULT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `CREATED_BY` int(18) DEFAULT NULL,
  `PRIORITY` int(18) NOT NULL DEFAULT '1',
  `LAST_DISCOUNT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `VERSION` int(11) NOT NULL DEFAULT '1',
  `CONDITIONS` mediumtext COLLATE utf8_unicode_ci,
  `UNPACK` mediumtext COLLATE utf8_unicode_ci,
  `ACTIONS` mediumtext COLLATE utf8_unicode_ci,
  `APPLICATION` mediumtext COLLATE utf8_unicode_ci,
  `USE_COUPONS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `IXS_DISCOUNT_LID` (`LID`),
  KEY `IX_SSD_ACTIVE_DATE` (`ACTIVE_FROM`,`ACTIVE_TO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_discount_group`
--

CREATE TABLE IF NOT EXISTS `b_sale_discount_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_S_DISGRP` (`DISCOUNT_ID`,`GROUP_ID`),
  UNIQUE KEY `IX_S_DISGRP_G` (`GROUP_ID`,`DISCOUNT_ID`),
  KEY `IX_S_DISGRP_D` (`DISCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_discount_module`
--

CREATE TABLE IF NOT EXISTS `b_sale_discount_module` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DISCOUNT_ID` int(11) NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_SALE_DSC_MOD` (`DISCOUNT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_export`
--

CREATE TABLE IF NOT EXISTS `b_sale_export` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `VARS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_fuser`
--

CREATE TABLE IF NOT EXISTS `b_sale_fuser` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_INSERT` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `CODE` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_USER_ID` (`USER_ID`),
  KEY `IX_CODE` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_lang`
--

CREATE TABLE IF NOT EXISTS `b_sale_lang` (
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_location`
--

CREATE TABLE IF NOT EXISTS `b_sale_location` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LEFT_MARGIN` int(11) DEFAULT NULL,
  `RIGHT_MARGIN` int(11) DEFAULT NULL,
  `PARENT_ID` int(11) DEFAULT '0',
  `DEPTH_LEVEL` int(11) DEFAULT '1',
  `TYPE_ID` int(11) DEFAULT NULL,
  `LATITUDE` decimal(8,6) DEFAULT NULL,
  `LONGITUDE` decimal(9,6) DEFAULT NULL,
  `COUNTRY_ID` int(11) DEFAULT NULL,
  `REGION_ID` int(11) DEFAULT NULL,
  `CITY_ID` int(11) DEFAULT NULL,
  `LOC_DEFAULT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_SALE_LOC_CODE` (`CODE`),
  KEY `IX_B_SALE_LOC_MARGINS` (`LEFT_MARGIN`,`RIGHT_MARGIN`),
  KEY `IX_B_SALE_LOC_MARGINS_REV` (`RIGHT_MARGIN`,`LEFT_MARGIN`),
  KEY `IX_B_SALE_LOC_PARENT` (`PARENT_ID`),
  KEY `IX_B_SALE_LOC_DL` (`DEPTH_LEVEL`),
  KEY `IX_B_SALE_LOC_TYPE` (`TYPE_ID`),
  KEY `IXS_LOCATION_COUNTRY_ID` (`COUNTRY_ID`),
  KEY `IXS_LOCATION_REGION_ID` (`REGION_ID`),
  KEY `IXS_LOCATION_CITY_ID` (`CITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_location2location_group`
--

CREATE TABLE IF NOT EXISTS `b_sale_location2location_group` (
  `LOCATION_ID` int(11) NOT NULL,
  `LOCATION_GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`LOCATION_ID`,`LOCATION_GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_location_city`
--

CREATE TABLE IF NOT EXISTS `b_sale_location_city` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REGION_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_LOCAT_REGION_ID` (`REGION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_location_city_lang`
--

CREATE TABLE IF NOT EXISTS `b_sale_location_city_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CITY_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IXS_LOCAT_CITY_LID` (`CITY_ID`,`LID`),
  KEY `IX_NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_location_country`
--

CREATE TABLE IF NOT EXISTS `b_sale_location_country` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_location_country_lang`
--

CREATE TABLE IF NOT EXISTS `b_sale_location_country_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COUNTRY_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IXS_LOCAT_CNTR_LID` (`COUNTRY_ID`,`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_location_group`
--

CREATE TABLE IF NOT EXISTS `b_sale_location_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_SALE_LOC_GROUP_CODE` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_location_group_lang`
--

CREATE TABLE IF NOT EXISTS `b_sale_location_group_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOCATION_GROUP_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_LOCATION_GROUP_LID` (`LOCATION_GROUP_ID`,`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_location_region`
--

CREATE TABLE IF NOT EXISTS `b_sale_location_region` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_location_region_lang`
--

CREATE TABLE IF NOT EXISTS `b_sale_location_region_lang` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REGION_ID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IXS_LOCAT_REGION_LID` (`REGION_ID`,`LID`),
  KEY `IXS_NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_location_zip`
--

CREATE TABLE IF NOT EXISTS `b_sale_location_zip` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOCATION_ID` int(11) NOT NULL DEFAULT '0',
  `ZIP` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `IX_LOCATION_ID` (`LOCATION_ID`),
  KEY `IX_ZIP` (`ZIP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_loc_2site`
--

CREATE TABLE IF NOT EXISTS `b_sale_loc_2site` (
  `LOCATION_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `LOCATION_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'L',
  PRIMARY KEY (`SITE_ID`,`LOCATION_ID`,`LOCATION_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_loc_def2site`
--

CREATE TABLE IF NOT EXISTS `b_sale_loc_def2site` (
  `LOCATION_CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) DEFAULT '100',
  PRIMARY KEY (`LOCATION_CODE`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_loc_ext`
--

CREATE TABLE IF NOT EXISTS `b_sale_loc_ext` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SERVICE_ID` int(11) NOT NULL,
  `LOCATION_ID` int(11) NOT NULL,
  `XML_ID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_loc_ext_srv`
--

CREATE TABLE IF NOT EXISTS `b_sale_loc_ext_srv` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_loc_name`
--

CREATE TABLE IF NOT EXISTS `b_sale_loc_name` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `LOCATION_ID` int(11) NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `NAME_UPPER` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SHORT_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_SALE_LOC_NAME_NAME_U` (`NAME_UPPER`),
  KEY `IX_B_SALE_LOC_NAME_LI_LI` (`LOCATION_ID`,`LANGUAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_loc_type`
--

CREATE TABLE IF NOT EXISTS `b_sale_loc_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) DEFAULT '100',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_loc_type_name`
--

CREATE TABLE IF NOT EXISTS `b_sale_loc_type_name` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_B_SALE_LOC_TYPE_NAME_TI_LI` (`TYPE_ID`,`LANGUAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_order`
--

CREATE TABLE IF NOT EXISTS `b_sale_order` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `PAYED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_PAYED` datetime DEFAULT NULL,
  `EMP_PAYED_ID` int(11) DEFAULT NULL,
  `CANCELED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_CANCELED` datetime DEFAULT NULL,
  `EMP_CANCELED_ID` int(11) DEFAULT NULL,
  `REASON_CANCELED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS_ID` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_STATUS` datetime NOT NULL,
  `EMP_STATUS_ID` int(11) DEFAULT NULL,
  `PRICE_DELIVERY` decimal(18,2) NOT NULL,
  `ALLOW_DELIVERY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_ALLOW_DELIVERY` datetime DEFAULT NULL,
  `EMP_ALLOW_DELIVERY_ID` int(11) DEFAULT NULL,
  `DEDUCTED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_DEDUCTED` datetime DEFAULT NULL,
  `EMP_DEDUCTED_ID` int(11) DEFAULT NULL,
  `REASON_UNDO_DEDUCTED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MARKED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_MARKED` datetime DEFAULT NULL,
  `EMP_MARKED_ID` int(11) DEFAULT NULL,
  `REASON_MARKED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESERVED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PRICE` decimal(18,2) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `DISCOUNT_VALUE` decimal(18,2) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `PAY_SYSTEM_ID` int(11) DEFAULT NULL,
  `DELIVERY_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_INSERT` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `USER_DESCRIPTION` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ADDITIONAL_INFO` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_CODE` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_DESCRIPTION` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_MESSAGE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_SUM` decimal(18,2) DEFAULT NULL,
  `PS_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_RESPONSE_DATE` datetime DEFAULT NULL,
  `COMMENTS` text COLLATE utf8_unicode_ci,
  `TAX_VALUE` decimal(18,2) NOT NULL DEFAULT '0.00',
  `STAT_GID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUM_PAID` decimal(18,2) NOT NULL DEFAULT '0.00',
  `RECURRING_ID` int(11) DEFAULT NULL,
  `PAY_VOUCHER_NUM` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAY_VOUCHER_DATE` date DEFAULT NULL,
  `LOCKED_BY` int(11) DEFAULT NULL,
  `DATE_LOCK` datetime DEFAULT NULL,
  `RECOUNT_FLAG` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `AFFILIATE_ID` int(11) DEFAULT NULL,
  `DELIVERY_DOC_NUM` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DELIVERY_DOC_DATE` date DEFAULT NULL,
  `UPDATED_1C` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `STORE_ID` int(11) DEFAULT NULL,
  `ORDER_TOPIC` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESPONSIBLE_ID` int(11) DEFAULT NULL,
  `DATE_PAY_BEFORE` datetime DEFAULT NULL,
  `DATE_BILL` datetime DEFAULT NULL,
  `ACCOUNT_NUMBER` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TRACKING_NUMBER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ID_1C` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION_1C` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION` int(11) NOT NULL DEFAULT '0',
  `EXTERNAL_ORDER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IXS_ACCOUNT_NUMBER` (`ACCOUNT_NUMBER`),
  KEY `IXS_ORDER_USER_ID` (`USER_ID`),
  KEY `IXS_ORDER_PERSON_TYPE_ID` (`PERSON_TYPE_ID`),
  KEY `IXS_ORDER_PAYED` (`PAYED`),
  KEY `IXS_ORDER_STATUS_ID` (`STATUS_ID`),
  KEY `IXS_ORDER_REC_ID` (`RECURRING_ID`),
  KEY `IX_SOO_AFFILIATE_ID` (`AFFILIATE_ID`),
  KEY `IXS_ORDER_UPDATED_1C` (`UPDATED_1C`),
  KEY `IXS_SALE_COUNT` (`USER_ID`,`LID`,`PAYED`,`CANCELED`),
  KEY `IXS_DATE_UPDATE` (`DATE_UPDATE`),
  KEY `IXS_XML_ID` (`XML_ID`),
  KEY `IXS_ID_1C` (`ID_1C`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_order_change`
--

CREATE TABLE IF NOT EXISTS `b_sale_order_change` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATA` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_CREATE` datetime NOT NULL,
  `DATE_MODIFY` datetime NOT NULL,
  `USER_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_ORDER_ID_CHANGE` (`ORDER_ID`),
  KEY `IXS_TYPE_CHANGE` (`TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_order_delivery`
--

CREATE TABLE IF NOT EXISTS `b_sale_order_delivery` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `DATE_REQUEST` datetime DEFAULT NULL,
  `DELIVERY_LOCATION` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `PARAMS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_ORDER_ID` (`ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_order_flags2group`
--

CREATE TABLE IF NOT EXISTS `b_sale_order_flags2group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUP_ID` int(11) NOT NULL,
  `ORDER_FLAG` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_sale_ordfla2group` (`GROUP_ID`,`ORDER_FLAG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_order_history`
--

CREATE TABLE IF NOT EXISTS `b_sale_order_history` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `H_USER_ID` int(11) unsigned NOT NULL,
  `H_DATE_INSERT` datetime NOT NULL,
  `H_ORDER_ID` int(11) unsigned NOT NULL,
  `H_CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `PERSON_TYPE_ID` int(11) unsigned DEFAULT NULL,
  `PAYED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_PAYED` datetime DEFAULT NULL,
  `EMP_PAYED_ID` int(11) unsigned DEFAULT NULL,
  `CANCELED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_CANCELED` datetime DEFAULT NULL,
  `REASON_CANCELED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STATUS_ID` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_STATUS` datetime DEFAULT NULL,
  `PRICE_DELIVERY` decimal(18,2) DEFAULT NULL,
  `ALLOW_DELIVERY` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_ALLOW_DELIVERY` datetime DEFAULT NULL,
  `RESERVED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEDUCTED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_DEDUCTED` datetime DEFAULT NULL,
  `REASON_UNDO_DEDUCTED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MARKED` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_MARKED` datetime DEFAULT NULL,
  `REASON_MARKED` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRICE` decimal(18,2) DEFAULT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DISCOUNT_VALUE` decimal(18,2) DEFAULT NULL,
  `USER_ID` int(11) unsigned DEFAULT NULL,
  `PAY_SYSTEM_ID` int(11) unsigned DEFAULT NULL,
  `DELIVERY_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_CODE` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_DESCRIPTION` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_STATUS_MESSAGE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_SUM` decimal(18,2) DEFAULT NULL,
  `PS_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PS_RESPONSE_DATE` datetime DEFAULT NULL,
  `TAX_VALUE` decimal(18,2) DEFAULT NULL,
  `STAT_GID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUM_PAID` decimal(18,2) DEFAULT NULL,
  `PAY_VOUCHER_NUM` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAY_VOUCHER_DATE` date DEFAULT NULL,
  `AFFILIATE_ID` int(11) unsigned DEFAULT NULL,
  `DELIVERY_DOC_NUM` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DELIVERY_DOC_DATE` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ixH_ORDER_ID` (`H_ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_order_processing`
--

CREATE TABLE IF NOT EXISTS `b_sale_order_processing` (
  `ORDER_ID` int(11) DEFAULT '0',
  `PRODUCTS_ADDED` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  `PRODUCTS_REMOVED` char(1) COLLATE utf8_unicode_ci DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_order_props`
--

CREATE TABLE IF NOT EXISTS `b_sale_order_props` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TYPE` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `REQUIED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DEFAULT_VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `USER_PROPS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_LOCATION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PROPS_GROUP_ID` int(11) NOT NULL,
  `SIZE1` int(11) NOT NULL DEFAULT '0',
  `SIZE2` int(11) NOT NULL DEFAULT '0',
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_EMAIL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_PROFILE_NAME` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_PAYER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_LOCATION4TAX` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_FILTERED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_ZIP` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IS_PHONE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ACTIVE` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `UTIL` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `INPUT_FIELD_LOCATION` int(11) NOT NULL DEFAULT '0',
  `MULTIPLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `IXS_ORDER_PROPS_PERSON_TYPE_ID` (`PERSON_TYPE_ID`),
  KEY `IXS_CODE_OPP` (`CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_order_props_group`
--

CREATE TABLE IF NOT EXISTS `b_sale_order_props_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`),
  KEY `IXS_ORDER_PROPS_GROUP_PERSON_TYPE_ID` (`PERSON_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_order_props_relation`
--

CREATE TABLE IF NOT EXISTS `b_sale_order_props_relation` (
  `PROPERTY_ID` int(11) NOT NULL,
  `ENTITY_ID` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`PROPERTY_ID`,`ENTITY_ID`,`ENTITY_TYPE`),
  KEY `IX_PROPERTY` (`PROPERTY_ID`),
  KEY `IX_ENTITY_ID` (`ENTITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_order_props_value`
--

CREATE TABLE IF NOT EXISTS `b_sale_order_props_value` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `ORDER_PROPS_ID` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_SOPV_ORD_PROP_UNI` (`ORDER_ID`,`ORDER_PROPS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_order_props_variant`
--

CREATE TABLE IF NOT EXISTS `b_sale_order_props_variant` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_PROPS_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_ORDER_PROPS_VARIANT_ORDER_PROPS_ID` (`ORDER_PROPS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_order_tax`
--

CREATE TABLE IF NOT EXISTS `b_sale_order_tax` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDER_ID` int(11) NOT NULL,
  `TAX_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` decimal(18,2) DEFAULT NULL,
  `VALUE_MONEY` decimal(18,2) NOT NULL,
  `APPLY_ORDER` int(11) NOT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_PERCENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `IS_IN_PRICE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `ixs_sot_order_id` (`ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_pay_system`
--

CREATE TABLE IF NOT EXISTS `b_sale_pay_system` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '100',
  `DESCRIPTION` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_PAY_SYSTEM_LID` (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_pay_system_action`
--

CREATE TABLE IF NOT EXISTS `b_sale_pay_system_action` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PAY_SYSTEM_ID` int(11) NOT NULL,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTION_FILE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESULT_FILE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NEW_WINDOW` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `PARAMS` text COLLATE utf8_unicode_ci,
  `TARIF` text COLLATE utf8_unicode_ci,
  `HAVE_PAYMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `HAVE_ACTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `HAVE_RESULT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `HAVE_PREPAY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `HAVE_RESULT_RECEIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ENCODING` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOGOTIP` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_SPSA_PSPT_UNI` (`PAY_SYSTEM_ID`,`PERSON_TYPE_ID`),
  KEY `IXS_PAY_SYSTEM_ACTION_PERSON_TYPE_ID` (`PERSON_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_person_type`
--

CREATE TABLE IF NOT EXISTS `b_sale_person_type` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '150',
  `ACTIVE` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `IXS_PERSON_TYPE_LID` (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_person_type_site`
--

CREATE TABLE IF NOT EXISTS `b_sale_person_type_site` (
  `PERSON_TYPE_ID` int(18) NOT NULL DEFAULT '0',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`PERSON_TYPE_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_product2product`
--

CREATE TABLE IF NOT EXISTS `b_sale_product2product` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `PARENT_PRODUCT_ID` int(11) NOT NULL,
  `CNT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_PRODUCT2PRODUCT_PRODUCT_ID` (`PRODUCT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_recurring`
--

CREATE TABLE IF NOT EXISTS `b_sale_recurring` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `MODULE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_ID` int(11) DEFAULT NULL,
  `PRODUCT_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_PRICE_ID` int(11) DEFAULT NULL,
  `PRICE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'R',
  `RECUR_SCHEME_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'M',
  `RECUR_SCHEME_LENGTH` int(11) NOT NULL DEFAULT '0',
  `WITHOUT_ORDER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PRICE` decimal(10,0) NOT NULL DEFAULT '0',
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CANCELED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_CANCELED` datetime DEFAULT NULL,
  `PRIOR_DATE` datetime DEFAULT NULL,
  `NEXT_DATE` datetime NOT NULL,
  `CALLBACK_FUNC` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_PROVIDER_CLASS` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CANCELED_REASON` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ORDER_ID` int(11) NOT NULL,
  `REMAINING_ATTEMPTS` int(11) NOT NULL DEFAULT '0',
  `SUCCESS_PAYMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `IX_S_R_USER_ID` (`USER_ID`),
  KEY `IX_S_R_NEXT_DATE` (`NEXT_DATE`,`CANCELED`,`REMAINING_ATTEMPTS`),
  KEY `IX_S_R_PRODUCT_ID` (`MODULE`,`PRODUCT_ID`,`PRODUCT_PRICE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_site2group`
--

CREATE TABLE IF NOT EXISTS `b_sale_site2group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUP_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_sale_site2group` (`GROUP_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_status`
--

CREATE TABLE IF NOT EXISTS `b_sale_status` (
  `ID` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '100',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_sale_status`
--

INSERT INTO `b_sale_status` (`ID`, `SORT`) VALUES
('F', 200),
('N', 100);

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_status2group`
--

CREATE TABLE IF NOT EXISTS `b_sale_status2group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUP_ID` int(11) NOT NULL,
  `STATUS_ID` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `PERM_VIEW` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_CANCEL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_MARK` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_DELIVERY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_DEDUCTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_PAYMENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_STATUS` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_UPDATE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_DELETE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `PERM_STATUS_FROM` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_sale_s2g_ix1` (`GROUP_ID`,`STATUS_ID`),
  KEY `ix_sale_s2g_1` (`STATUS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_status_lang`
--

CREATE TABLE IF NOT EXISTS `b_sale_status_lang` (
  `STATUS_ID` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`STATUS_ID`,`LID`),
  UNIQUE KEY `ixs_status_lang_status_id` (`STATUS_ID`,`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_sale_status_lang`
--

INSERT INTO `b_sale_status_lang` (`STATUS_ID`, `LID`, `NAME`, `DESCRIPTION`) VALUES
('F', 'en', 'Completed', 'Order has been delivered and paid'),
('F', 'ru', 'Выполнен', 'Заказ доставлен и оплачен'),
('N', 'en', 'Accepted', 'Order has been accepted but is not being processed as yet (for example: order may have just been created, or awaiting payment)'),
('N', 'ru', 'Принят', 'Заказ принят, но пока не обрабатывается (например, заказ только что создан или ожидается оплата заказа)');

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_store_barcode`
--

CREATE TABLE IF NOT EXISTS `b_sale_store_barcode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `BASKET_ID` int(11) NOT NULL,
  `BARCODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STORE_ID` int(11) NOT NULL,
  `QUANTITY` double NOT NULL,
  `DATE_CREATE` datetime DEFAULT NULL,
  `DATE_MODIFY` datetime DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `MODIFIED_BY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_tax`
--

CREATE TABLE IF NOT EXISTS `b_sale_tax` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIMESTAMP_X` datetime NOT NULL,
  `CODE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `itax_lid` (`LID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_tax2location`
--

CREATE TABLE IF NOT EXISTS `b_sale_tax2location` (
  `TAX_RATE_ID` int(11) NOT NULL,
  `LOCATION_CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LOCATION_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'L',
  PRIMARY KEY (`TAX_RATE_ID`,`LOCATION_CODE`,`LOCATION_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_tax_exempt2group`
--

CREATE TABLE IF NOT EXISTS `b_sale_tax_exempt2group` (
  `GROUP_ID` int(11) NOT NULL,
  `TAX_ID` int(11) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`TAX_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_tax_rate`
--

CREATE TABLE IF NOT EXISTS `b_sale_tax_rate` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TAX_ID` int(11) NOT NULL,
  `PERSON_TYPE_ID` int(11) DEFAULT NULL,
  `VALUE` decimal(18,4) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IS_PERCENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `IS_IN_PRICE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `APPLY_ORDER` int(11) NOT NULL DEFAULT '100',
  `TIMESTAMP_X` datetime NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `itax_pers_type` (`PERSON_TYPE_ID`),
  KEY `itax_lid` (`TAX_ID`),
  KEY `itax_inprice` (`IS_IN_PRICE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_trading_platform`
--

CREATE TABLE IF NOT EXISTS `b_sale_trading_platform` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SETTINGS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_CODE` (`CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `b_sale_trading_platform`
--

INSERT INTO `b_sale_trading_platform` (`ID`, `CODE`, `ACTIVE`, `NAME`, `DESCRIPTION`, `SETTINGS`) VALUES
(1, 'ymarket', 'N', 'Покупки на Яндекс-Маркете', 'Интеграция магазина с программой Яндекса "Покупка на Маркете"', '');

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_user_account`
--

CREATE TABLE IF NOT EXISTS `b_sale_user_account` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `CURRENT_BUDGET` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `LOCKED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_LOCKED` datetime DEFAULT NULL,
  `NOTES` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_S_U_USER_ID` (`USER_ID`,`CURRENCY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_user_cards`
--

CREATE TABLE IF NOT EXISTS `b_sale_user_cards` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '100',
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `PAY_SYSTEM_ACTION_ID` int(11) NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CARD_TYPE` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CARD_NUM` text COLLATE utf8_unicode_ci NOT NULL,
  `CARD_CODE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CARD_EXP_MONTH` int(11) NOT NULL,
  `CARD_EXP_YEAR` int(11) NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUM_MIN` decimal(18,4) DEFAULT NULL,
  `SUM_MAX` decimal(18,4) DEFAULT NULL,
  `SUM_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_STATUS` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_STATUS_CODE` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_STATUS_DESCRIPTION` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_STATUS_MESSAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_SUM` decimal(18,4) DEFAULT NULL,
  `LAST_CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_S_U_C_USER_ID` (`USER_ID`,`ACTIVE`,`CURRENCY`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_user_props`
--

CREATE TABLE IF NOT EXISTS `b_sale_user_props` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `PERSON_TYPE_ID` int(11) NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `XML_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION_1C` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_USER_PROPS_USER_ID` (`USER_ID`),
  KEY `IXS_USER_PROPS_PERSON_TYPE_ID` (`PERSON_TYPE_ID`),
  KEY `IXS_USER_PROPS_XML_ID` (`XML_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_user_props_value`
--

CREATE TABLE IF NOT EXISTS `b_sale_user_props_value` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_PROPS_ID` int(11) NOT NULL,
  `ORDER_PROPS_ID` int(11) NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IXS_USER_PROPS_VALUE_USER_PROPS_ID` (`USER_PROPS_ID`),
  KEY `IXS_USER_PROPS_VALUE_ORDER_PROPS_ID` (`ORDER_PROPS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_user_transact`
--

CREATE TABLE IF NOT EXISTS `b_sale_user_transact` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TRANSACT_DATE` datetime NOT NULL,
  `AMOUNT` decimal(18,4) NOT NULL DEFAULT '0.0000',
  `CURRENCY` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `DEBIT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ORDER_ID` int(11) DEFAULT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NOTES` text COLLATE utf8_unicode_ci,
  `EMPLOYEE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_S_U_T_USER_ID` (`USER_ID`),
  KEY `IX_S_U_T_USER_ID_CURRENCY` (`USER_ID`,`CURRENCY`),
  KEY `IX_S_U_T_ORDER_ID` (`ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sale_viewed_product`
--

CREATE TABLE IF NOT EXISTS `b_sale_viewed_product` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FUSER_ID` int(11) unsigned NOT NULL DEFAULT '0',
  `DATE_VISIT` datetime NOT NULL,
  `PRODUCT_ID` int(11) unsigned NOT NULL DEFAULT '0',
  `MODULE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DETAIL_PAGE_URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRICE` decimal(18,2) NOT NULL DEFAULT '0.00',
  `NOTES` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREVIEW_PICTURE` int(11) DEFAULT NULL,
  `DETAIL_PICTURE` int(11) DEFAULT NULL,
  `CALLBACK_FUNC` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PRODUCT_PROVIDER_CLASS` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ixLID` (`FUSER_ID`,`LID`),
  KEY `ixPRODUCT_ID` (`PRODUCT_ID`),
  KEY `ixDATE_VISIT` (`DATE_VISIT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_search_content`
--

CREATE TABLE IF NOT EXISTS `b_search_content` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_CHANGE` datetime NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ITEM_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CUSTOM_RANK` int(11) NOT NULL DEFAULT '0',
  `USER_ID` int(11) DEFAULT NULL,
  `ENTITY_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ENTITY_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL` text COLLATE utf8_unicode_ci,
  `TITLE` text COLLATE utf8_unicode_ci,
  `BODY` longtext COLLATE utf8_unicode_ci,
  `TAGS` text COLLATE utf8_unicode_ci,
  `PARAM1` text COLLATE utf8_unicode_ci,
  `PARAM2` text COLLATE utf8_unicode_ci,
  `UPD` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DATE_FROM` datetime DEFAULT NULL,
  `DATE_TO` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_B_SEARCH_CONTENT` (`MODULE_ID`,`ITEM_ID`),
  KEY `IX_B_SEARCH_CONTENT_1` (`MODULE_ID`,`PARAM1`(50),`PARAM2`(50)),
  KEY `IX_B_SEARCH_CONTENT_2` (`ENTITY_ID`(50),`ENTITY_TYPE_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=96 ;

--
-- Dumping data for table `b_search_content`
--

INSERT INTO `b_search_content` (`ID`, `DATE_CHANGE`, `MODULE_ID`, `ITEM_ID`, `CUSTOM_RANK`, `USER_ID`, `ENTITY_TYPE_ID`, `ENTITY_ID`, `URL`, `TITLE`, `BODY`, `TAGS`, `PARAM1`, `PARAM2`, `UPD`, `DATE_FROM`, `DATE_TO`) VALUES
(1, '2015-04-22 17:43:24', 'main', 's1|/company/history.php', 0, NULL, NULL, NULL, '/company/history.php', 'История', '1992г. 								\r&laquo;Мебельная компания&raquo;\rначиналась с изготовления мебели для школ и офисов. Первое 									производство мебели располагалось в арендуемой мастерской, площадью 400 м2 с одним 									деревообрабатывающим станком. В компании работало двадцать человек. Все заработанные 									средства вкладывались в развитие, что позволило молодой Компании расти быстрыми 									темпами. 								\r1993г. 								\rВведен в эксплуатацию новый цех площадью 700 м2, ставший первой собственностью 									\r&laquo;Мебельной компании&raquo;\r. Модернизация производственной базы предприятия стала хорошей 									традицией. Компания постепенно перешла к более совершенному оборудованию, что позволило 									повысить уровень качества выпускаемой продукции и значительно увеличить объемы производства. 								\r1998г. 								\rВ Воронеже открыт первый фирменный магазин-салон \r&laquo;Мебельная компания&raquo;\r. Шаг за шагом 									продукция предприятия завоевывала регионы Сибири и Урала, Москвы и Подмосковья, 									Юга и Северо-Запада России. Производственные площади компании увеличены до 5000 									м2. 								\r1999г. 								\r&laquo;Мебельная компания&raquo;\rстала дипломантом одной из самых престижных международных 									выставок \r&laquo;ЕвроЭкспоМебель-99&raquo;\r- первое официальное признание мебельной продукции 									&laquo;Мебельной компании&raquo;. В этом же году компания выходит на мировой рынок. Был заключен 									ряд контрактов на поставку мебели в страны СНГ и Ближнего Зарубежья. 								\r2000г. 								\rКоллектив компании насчитывает более 500 сотрудников. Заключаются новые контракт 									на поставку мебели в европейские страны. 								\r2002г. 								\r&laquo;Мебельная компания&raquo;\rвошла в десятку лучших производителей мебели по данным ведущих 									мебельных салонов России, а также в число лидеров организационного развития. 								\r2003г. 								\rПриступили к строительству склада материалов. В Москве открыт филиал компании. \nПроведена первая конференция партнеров, результатом которой стало укрепление взаимовыгодных 									отношений и заключение дилерских договоров. 								\r2004г. 								\rЗавершено строительство и оснащение нового производственного корпуса и склада материалов. \nРасширено представительство компании на российском рынке и за рубежом. \nОткрыто 									региональное представительство \r&laquo;Мебельной компании&raquo;\rв Екатеринбурге. 								\r2005г. 								\rКомпания приобретает новое производственное оборудование концерна - угловую линию 									раскроя материалов и линию загрузки выгрузки. \nНачинается выпуск продукции в 									натуральном шпоне. Формируется отдельный склад материалов и комплектующих. \nВ этом же году открывается Фабрика мягкой мебели \r&laquo;МебельПлюс&raquo;\r2006г. 								\rОткрыты региональные представительства \r&laquo;Мебельной компании&raquo;\rв Санкт-Петербурге 									и Нижнем Новгороде. \nРазвивается собственная розничная сеть фирменных магазинов-салонов 									на территории России. 								\r2007г. 								\rЗавершено строительство второй фабрики. Общая площадь производсвенно-складских корпусов 									Компании составляет уже более 30000 м2. \nПроведена вторая конференция партнеров 									компании \r"Технология успешного бизнеса"\r. 								\r2008г. 								\rОткрыто новое предприятие для производства мебели по индивидуальным проектам \r&laquo;Комфорт&raquo;\r. \n&laquo;Мебельная компания&raquo;\rпродолжает обновление оборудования. 								\r2008г. 								\rНовейшим оборудованием укомплектована вторая фабрика. Запущена вторая производственная 									линия. \nПроведена третья конференция \r&laquo;Партнерство - бизнес сегодня&raquo;\rПринято решение о строительстве третьей фабрики. Площадь производственно &mdash; складских 									корпусов составит более 70000м2. \nПо всей стране и зарубежом открыто 37 мебельных 									салонов. \nВ Компании работает более полутора тысяч сотрудников.', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(2, '2015-04-22 17:43:24', 'main', 's1|/company/index.php', 0, NULL, NULL, NULL, '/company/index.php', 'О компании', 'Наша компания существует на Российском рынке с 1992 года. За это время \r&laquo;Мебельная компания&raquo;\rпрошла большой путь от маленькой торговой фирмы до одного из крупнейших производителей корпусной мебели в России. 						 \rГлавное правило - индивидуальный подход к каждому клиенту\rНа сегодняшний день нами разработано более пятисот моделей для офиса и дома. Вместе с тем мы стремимся обеспечить неповторимость своей продукции. Мы изготовим мебель для кухни, детской, гостиной, спальной или ванной комнаты, мебель для офиса особого дизайна и нестандартного размера. \rНаши дизайнеры произведут замеры помещения и вместе с вами разработают дизайн-проект мебели для вашего интерьера, подобрав с высокой точностью размеры, модели, цвета, помогут оптимально расположить мебель. \rВаш проект будет создан с учетом всех нюансов и прорисовкой мельчайших деталей. Результаты совместного творчества вы сможете посмотреть в объемном представлении. Вам наглядно продемонстрируют, как будут выглядеть в жизни выбранные элементы интерьера при разном освещении, в конкретном помещении, сделанные из определенных материалов. В ваше распоряжение будет предоставлено много различных вариантов, из которых Вы сможете выбрать наиболее подходящий именно Вам. 						 					\rЗаказ будет выполнен и доставлен точно в срок. Все работы по сборке и установке мебели осуществляют сотрудники нашей компании. Строгий контроль качества осуществляется на всех этапах работ: от момента оформления заказа до момента приема выполненных работ. \rПередовые технологии и бесценный опыт\rИспользование передовых компьютерных технологий, многолетний опыт наших специалистов позволяют произвести грамотные расчеты и снизить расход материалов и себестоимость продукции, избежать ошибок при проектировании и оптимизировать дизайн комплексных интерьеров. Гарантия на нашу продукцию составляет 18 месяцев, а на отдельную продукцию 36 месяцев. \rМы гордимся нашими сотрудниками прошедшими профессиональное обучение в лучших учебных заведениях России и зарубежья. У нас трудятся высококлассные специалисты разных возрастов. Мы ценим энтузиазм молодежи и бесценный опыт старшего поколения. Все мы разные, но нас объединяет преданность своему делу и вера в идеи нашей компании. \rВысочайшие стандарты качества - залог нашего успеха\r&laquo;Мебельная компания&raquo;\rосуществляет производство мебели на высококлассном оборудовании с применением минимальной доли ручного труда, что позволяет обеспечить высокое качество нашей продукции. Налажен производственный процесс как массового и индивидуального характера, что с одной стороны позволяет обеспечить постоянную номенклатуру изделий и индивидуальный подход &ndash; с другой. \rЕжегодно наша продукция проходит сертификационные испытания в специализированных лабораториях России и имеет сертификаты соответствия продукции нормам безопасности и качества. Кроме того, \r&laquo;Мебельная компания&raquo;\rодной из первых среди мебельных производителей России в 2003 году прошла аудит на соответствие требованиям системы менеджмента качества &laquo;ИСО 9000&raquo; и получила сертификат соответствия системы качества на предприятии нормам международного стандарта.', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(3, '2015-04-22 17:43:24', 'main', 's1|/company/management.php', 0, NULL, NULL, NULL, '/company/management.php', 'Руководство', 'Успешное развитие бизнеса &ndash; во многом результат квалифицированной работы руководства. 							\r&laquo;Мебельная компания&raquo;\rна мебельном рынке уже 18 лет. За это время Компания 							не только сохранила, но и упрочила лидирующие позиции среди ведущих игроков мебельного 							рынка. 						\r&laquo;Мебельная компания&raquo;\rиз года в год расширяет ассортимент выпускаемой продукции, 							наращивает темпы и объемы производства, увеличивает производственные и складские 							площади, развивает отношения с партнерами со всех регионов страны и налаживает связи 							с зарубежными партнерами. В большой степени это заслуга хорошо подготовленного руководящего 							состава и его грамотной политики. 						\rСобственник Компании &laquo;Мебельная компания&raquo;\rКолесников Виктор Федорович 								\rРодился 3 сентября 1964 года.\nОбразование: закончил авиационный факультет Воронежского 									государственного политехнического института. В 1994 году прошел обучение по программе 									&laquo;Подготовка малого и среднего бизнеса&raquo; в США.\nВ настоящее время Виктор Федорович 									возглавляет Управляющую компанию, которая координирует деятельность предприятий, 									входящих в Группу Компаний \r&laquo;Мебельная компания&raquo;\r. 								\rГенеральный директор &laquo;Мебельной компании&raquo;\rРатченко Александр Петрович 								\rРодился 5 июня 1962 года.\nОбразование: Воронежский политехнический институт 									по специальности инженер-технолог; программа &laquo;Эффективное развитие производства&raquo; 									(США).\nВ \r&laquo;Мебельной компании&raquo;\rСергей Фомич с 1994 года. За это время прошел 									путь от начальника цеха до генерального директора предприятия. 								\rЗаместитель генерального директора Управляющей компании\rРоговой Андрей Владимирович 								\rОбразование: факультет радиотехники Воронежского государственного технического университета.\nВ Компании с 1 июня 2000 года.', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(4, '2015-04-22 17:43:24', 'main', 's1|/company/mission.php', 0, NULL, NULL, NULL, '/company/mission.php', 'Миссия и стратегия', '&laquo;Мебельная компания&raquo;\r- динамично развивающееся производственное 							предприятие, которое имеет перед собой \nясно выраженные цели и инструменты для 							их достижени.Мы предоставляем каждому лучшую возможность обустроить свое жизненное \nи рабочее пространство.Мы работаем на долгосрочную перспективу и предлагаем оптимальные 							решения. Компания \r&laquo;Мебельная компания&raquo;\r- \nнадежный, технологичный, гибкий поставщик 							с большими мощностями. 						\rЦели и задачи\rОправдывать ожидания заказчика: &ldquo;Клиент всегда прав&rdquo;. Только Потребитель формирует 									единую систему взглядов на качество \nвыпускаемой продукции и работ.\rДобиться от работников компании понимания их личной ответственности за качество 									работ.\rПутем повышения качества продукции и работ постоянно увеличивать объемы производства 									с целью последующего реинвестирования \nприбыли в развитие компании.\rОбеспечивать строгое соответствие производимой продукции требованиям потребителей, 									нормам и правилам \nбезопасности, требованиям защиты окружающей среды.\rПолитика компании\rПостоянное совершенствование системы качества. Своевременное и эффективное принятие 									корректирующих мер .\rЗабота о работниках компании. Создание условий труда и оснащение рабочих мест, соответствующих 									всем санитарным \nи гигиеническим нормам.\rПовышение благосостояния сотрудников. Обеспечение морального и материального удовлетворения 									работников компании.\rСистематическое обучение работников всех уровней с целью постоянного повышения их 									профессионального мастерства.\rВнедрение высокопроизводительного оборудования и новейших технологий для повышения 									производительности труда, \nоптимизации затрат и, как результат, снижения цен 									на выпускаемую продукцию.\rСоздание новых рабочих мест. Привлечение на работу специалистов высокой квалификации.\rВыход на международный рынок.\rМы развиваем доверительные взаимовыгодные отношения со своими партнерами в долгосрочных 							интересах нашего бизнеса. \n&laquo;Мебельная компания&raquo;\rответственно относится 							к выполнению взятых на себя обязательств и ждет такого же \nподхода к делу от 							своих партнеров по бизнесу. Эта требовательность &ndash; залог нашей долгосрочной прибыльности. 						\rСо дня своего основания \r&laquo;Мебельная компания&raquo;\rсодействует росту благосостояния 							регионов России. Мы понимаем важность \nсоциальной ответственности нашей Компании 							и останемся примером в вопросах социальной защищенности наших сотрудников.', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(5, '2015-04-22 17:43:24', 'main', 's1|/company/vacancies.php', 0, NULL, NULL, NULL, '/company/vacancies.php', 'Вакансии', '', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(6, '2015-04-22 17:43:24', 'main', 's1|/contacts/index.php', 0, NULL, NULL, NULL, '/contacts/index.php', 'Контакты', 'Обратитесь к нашим специалистам и получите профессиональную консультацию по вопросам создания и покупки мебели (от дизайна, разработки технического задания до доставки мебели к Вам домой).\rВы можете обратиться к нам по телефону, по электронной почте или договориться о встрече в нашем офисе. Будем рады помочь вам и ответить на все ваши вопросы. \rТелефоны\rТелефон/факс:\n(495) 212-85-06\rТелефоны:\n(495) 212-85-07\r(495) 212-85-08\rEmail\rinfo@example.ru\r&mdash; общие вопросы\rsales@example.ru\r&mdash; приобретение продукции\rmarketing@example.ru\r&mdash; маркетинг/мероприятия/PR\rОфис в Москве', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(7, '2015-04-22 17:43:24', 'main', 's1|/login/index.php', 0, NULL, NULL, NULL, '/login/index.php', 'Вход на сайт', 'Вы зарегистрированы и успешно авторизовались.\rВернуться на главную страницу', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(8, '2015-04-22 17:43:24', 'main', 's1|/news/index.php', 0, NULL, NULL, NULL, '/news/index.php', 'Новости', '', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(9, '2015-04-22 17:43:24', 'main', 's1|/products/index.php', 0, NULL, NULL, NULL, '/products/index.php', 'Продукция', '', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(10, '2015-04-22 17:43:24', 'main', 's1|/search/index.php', 0, NULL, NULL, NULL, '/search/index.php', 'Поиск', '', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(11, '2015-04-22 17:43:24', 'main', 's1|/search/map.php', 0, NULL, NULL, NULL, '/search/map.php', 'Карта сайта', '', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(12, '2015-04-22 17:43:24', 'main', 's1|/services/index.php', 0, NULL, NULL, NULL, '/services/index.php', 'Услуги', '', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(13, '2015-04-22 17:43:24', 'main', 's1|/_index.php', 0, NULL, NULL, NULL, '/_index.php', 'Мебельная компания', 'Наша компания существует на Российском рынке с 1992 года. За это время «Мебельная компания» прошла большой путь от маленькой торговой фирмы до одного из крупнейших производителей корпусной мебели в России.\n«Мебельная компания» осуществляет производство мебели на высококлассном оборудовании с применением минимальной доли ручного труда, что позволяет обеспечить высокое качество нашей продукции. Налажен производственный процесс как массового и индивидуального характера, что с одной стороны позволяет обеспечить постоянную номенклатуру изделий и индивидуальный подход – с другой.\nНаша продукция\rНаши услуги', '', '', '', 'b8d31621d3b74434d2a59abb265200bc', NULL, NULL),
(45, '2015-04-22 23:43:48', 'iblock', '28', 0, NULL, NULL, NULL, '=ID=28&EXTERNAL_ID=5&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=3&IBLOCK_CODE=furniture_services&IBLOCK_EXTERNAL_ID=furniture_services&CODE=', 'Мебель на заказ', 'Наша компания занимается разработкой мебели на заказ.\r\n \rНаша компания занимается разработкой мебели на заказ по индивидуальным проектам: встроенные и корпусные шкафы купе,\rгардеробные комнаты, прихожие, библиотеки, платяные шкафы, комоды и другие сложные конструкции.\rМы создаем мебель идеально подходящую именно к вашему дому и офису, интерьеры, максимально отображающие вашу индивидуальность.\rПо Вашей заявке наш специалист приезжает со всеми образцами материалов, с которыми мы работаем, в любое удобное для Вас время и\rпроизведет все необходимые замеры. Учитывая все Ваши пожелания и особенности помещения, составляется эскизный проект.\rПосле заключения договора, в котором оговариваются сроки доставки и монтажа мебели, эскизный проект передается на производство,\rгде опытными специалистами производятся расчеты в программе трехмерного моделирования. После всех расчетов готовый проект поступает\rнепосредственно на производство, где производят раскрой деталей, их обработку, и сборку некоторых элементов. Накануне дня доставки\rсотрудники отдела транспортных услуг свяжутся с Вами и более конкретно оговорят время доставки. После заключения договора вами\rвносится предоплата в размере 30% от суммы заказанной Вами мебели. Остальная сумма оплачивается Вами по доставке.', '', 'products', '3', NULL, NULL, NULL),
(46, '2015-04-22 23:43:48', 'iblock', '29', 0, NULL, NULL, NULL, '=ID=29&EXTERNAL_ID=6&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=3&IBLOCK_CODE=furniture_services&IBLOCK_EXTERNAL_ID=furniture_services&CODE=', 'Услуги дизайнера', 'Мы предлагаем широкий спектр услуг по дизайну мебели.\r\n \rДиагностика возможностей преобразования помещений – определение вариантов перепланировки, отделки, разработка новых решений колористки, освещения, перестановки мебели и декора, разработка специальных функциональных зон для переключения в различные режимы жизни.\rРазработка Идеи-Образа, проведение предварительных расчётов и создание 3D-модели: изображение передает цвет и фактуру, предлагая клиенту экспериментировать и подбирать оптимальный вариант.\rРазработка компьютерных цветных трехмерных моделей мебели. Натуралистичность изображений, их высокая схожесть с оригиналом позволяют представить, как будет выглядеть готовое изделие, рассмотреть его в деталях.\rПодбор и расстановка мебели.\rДекорирование - подбор декора и аксессуаров интерьера в соответствии с образом и стилем помещения. Возможно создание по индивидуальному запросу эксклюзивных, авторских коллекций.', '', 'products', '3', NULL, NULL, NULL),
(50, '2015-04-23 21:32:19', 'iblock', 'S5', 0, NULL, NULL, NULL, '=ID=5&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=furniture_products_s1&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=vyduv-pet-tary', 'Выдув ПЭТ-тары', '', NULL, 'products', '2', NULL, NULL, NULL),
(51, '2015-04-23 21:32:24', 'iblock', 'S6', 0, NULL, NULL, NULL, '=ID=6&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=furniture_products_s1&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=oborudovanie-rozliva-', 'Оборудование розлива ', '', NULL, 'products', '2', NULL, NULL, NULL),
(52, '2015-04-23 21:32:48', 'iblock', 'S7', 0, NULL, NULL, NULL, '=ID=7&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=furniture_products_s1&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=ukuporochnoe-oborudovanie', 'Укупорочное оборудование', '', NULL, 'products', '2', NULL, NULL, NULL),
(53, '2015-04-23 21:32:55', 'iblock', 'S8', 0, NULL, NULL, NULL, '=ID=8&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=furniture_products_s1&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=etikerovshchiki-', 'Этикеровщики ', '', NULL, 'products', '2', NULL, NULL, NULL),
(54, '2015-04-23 21:33:00', 'iblock', 'S9', 0, NULL, NULL, NULL, '=ID=9&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=furniture_products_s1&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=printery-datirovshchiki-', 'Принтеры-датировщики ', '', NULL, 'products', '2', NULL, NULL, NULL),
(55, '2015-04-23 21:33:09', 'iblock', 'S10', 0, NULL, NULL, NULL, '=ID=10&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=furniture_products_s1&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=upakovochnoe-oborudovanie', 'Упаковочное оборудование', '', NULL, 'products', '2', NULL, NULL, NULL),
(56, '2015-04-23 21:33:16', 'iblock', 'S11', 0, NULL, NULL, NULL, '=ID=11&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=furniture_products_s1&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=palletoobmotchiki-', 'Паллетообмотчики ', '', NULL, 'products', '2', NULL, NULL, NULL),
(57, '2015-04-23 21:33:21', 'iblock', 'S12', 0, NULL, NULL, NULL, '=ID=12&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=furniture_products_s1&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=konveyernye-sistemy-', 'Конвейерные системы ', '', NULL, 'products', '2', NULL, NULL, NULL),
(58, '2015-04-23 21:33:26', 'iblock', 'S13', 0, NULL, NULL, NULL, '=ID=13&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=furniture_products_s1&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=kompressory-', 'Компрессоры ', '', NULL, 'products', '2', NULL, NULL, NULL),
(59, '2015-04-23 21:33:31', 'iblock', 'S14', 0, NULL, NULL, NULL, '=ID=14&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=furniture_products_s1&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=saturatory-', 'Сатураторы ', '', NULL, 'products', '2', NULL, NULL, NULL),
(60, '2015-04-23 21:33:37', 'iblock', 'S15', 0, NULL, NULL, NULL, '=ID=15&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=furniture_products_s1&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=resivery', 'Ресиверы', '', NULL, 'products', '2', NULL, NULL, NULL),
(61, '2015-05-10 20:39:18', 'main', 's1|/produktsiya/index.php', 0, NULL, NULL, NULL, '/produktsiya/index.php', 'Продукция', '', '', '', '', NULL, NULL, NULL),
(62, '2015-04-23 15:53:30', 'main', 's1|/kompleksnye-linii/index.php', 0, NULL, NULL, NULL, '/kompleksnye-linii/index.php', 'Комплексные линии', 'Text here....', '', '', '', NULL, NULL, NULL),
(63, '2015-04-23 15:53:52', 'main', 's1|/uslugi/index.php', 0, NULL, NULL, NULL, '/uslugi/index.php', 'Услуги', 'Text here....', '', '', '', NULL, NULL, NULL),
(64, '2015-04-25 17:40:43', 'main', 's1|/proekty/index.php', 0, NULL, NULL, NULL, '/proekty/index.php', 'Проекты', '', '', '', '', NULL, NULL, NULL),
(65, '2015-04-24 02:40:17', 'main', 's1|/kompaniya/index.php', 0, NULL, NULL, NULL, '/kompaniya/index.php', 'Компания', 'ООО «Аквакультура»\rПродаём оборудование трёх крупнейших китайских производителей, и оборудование водоподготовки из США, Германии и Канады.\rУмеем запустить завод с нуля или модернизировать готовую линию.\rНа складе храним оборудование и запасные части. Контролируем качество, обучаем персонал.\rРаботаем в Москве, регионах РФ и странах СНГ. С корпорациями и частными заказчиками.\rНовости\rСтатьи', '', '', '', NULL, NULL, NULL),
(66, '2015-04-23 15:54:58', 'main', 's1|/kontakty/index.php', 0, NULL, NULL, NULL, '/kontakty/index.php', 'Контакты', 'Text here....', '', '', '', NULL, NULL, NULL),
(67, '2015-04-24 02:07:58', 'main', 's1|/kompaniya/o-kompanii/index.php', 0, NULL, NULL, NULL, '/kompaniya/o-kompanii/index.php', 'Росупак-2014', 'Text here....', '', '', '', NULL, NULL, NULL),
(68, '2015-04-24 00:00:00', 'iblock', '33', 0, NULL, NULL, NULL, '=ID=33&EXTERNAL_ID=33&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=news&IBLOCK_ID=1&IBLOCK_CODE=furniture_news_s1&IBLOCK_EXTERNAL_ID=furniture_news_s1&CODE=otchyet-o-vystavke-rosupak-2012', 'Отчёт о выставке Росупак 2012', 'В 2012 году выставка Росупак открыла посетителям свои двери на новой площадке. В 75-ом павильоне ВВЦ собрались ведущие производители и поставщики оборудования, всего 700 компаний из 31 страны мира. Компания Аквакультура, как один из ведущих поставщиков упаковочного оборудования в России и СНГ, традиционно приняла участие в выставке. На стенде компании была представлена линия по выдуву ПЭТ бутылок производительностью 6000 бутылок в час.', '', 'news', '1', NULL, '2015-04-24 00:00:00', NULL),
(69, '2015-04-24 00:00:00', 'iblock', '34', 0, NULL, NULL, NULL, '=ID=34&EXTERNAL_ID=34&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=news&IBLOCK_ID=1&IBLOCK_CODE=furniture_news_s1&IBLOCK_EXTERNAL_ID=furniture_news_s1&CODE=vystavka-rosupack-2013', 'Выставка Rosupack 2013', 'Уважаемые коллеги и друзья! Приглашаем Вас посетить наш стенд А 215 в зале 1, первого павильона в МВЦ "Крокус Экспо" во время проведения выставки RosUpack 2013 с 18 по 21 июня 2013 года!', '', 'news', '1', NULL, '2015-04-24 00:00:00', NULL),
(70, '2015-04-24 00:00:00', 'iblock', '35', 0, NULL, NULL, NULL, '=ID=35&EXTERNAL_ID=35&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=news&IBLOCK_ID=1&IBLOCK_CODE=furniture_news_s1&IBLOCK_EXTERNAL_ID=furniture_news_s1&CODE=watershow-2014', 'Watershow 2014', 'Уважаемые коллеги! Приглашаем Вас посетить самые ожидаемые и посещаемые события водной отрасли Тринадцатый Форум производителей бутилированной воды России «WaterShow» и Второй Международный Конгресс производителей минеральной, питьевой воды и безалкогольных напитков «Beverages Industry Conference», мероприятия пройдут 18-20 марта 2014 года, в Москве, отель Милан.', '', 'news', '1', NULL, '2015-04-24 00:00:00', NULL),
(71, '2015-04-24 00:00:00', 'iblock', '36', 0, NULL, NULL, NULL, '=ID=36&EXTERNAL_ID=36&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=news&IBLOCK_ID=1&IBLOCK_CODE=furniture_news_s1&IBLOCK_EXTERNAL_ID=furniture_news_s1&CODE=otchyet-o-watershow-2014', 'Отчёт о Watershow 2014', 'Как всегда очень успешно прошел Тринадцатый Форум производителей бутилированной воды России Watershow 2014. Многие участники уже назвали его одним из лучших за необыкновенно приятную атмосферу и отличную организацию мероприятия. На Watershow приехало около 250 человек из 128 компаний России, Казахстана, Украины, Узбекистана, Италии, Польши, Китая, Армении, Белоруси.', '', 'news', '1', NULL, '2015-04-24 00:00:00', NULL),
(72, '2015-04-24 02:36:43', 'iblock', '37', 0, NULL, NULL, NULL, '=ID=37&EXTERNAL_ID=37&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=news&IBLOCK_ID=6&IBLOCK_CODE=articles&IBLOCK_EXTERNAL_ID=&CODE=sistemy-i-filtry-ochistki-vody-dlya-doma', 'Системы и фильтры очистки воды для дома', 'Когда-то, чтобы утолить жажду, для наших предков было совершенно естественным спуститься к реке, озеру или ручью, зачерпнуть ладонями и выпить свежую воду. С тех пор прошли века...', '', 'news', '6', NULL, '2015-04-24 02:36:43', NULL),
(73, '2015-04-24 02:37:24', 'iblock', '38', 0, NULL, NULL, NULL, '=ID=38&EXTERNAL_ID=38&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=news&IBLOCK_ID=6&IBLOCK_CODE=articles&IBLOCK_EXTERNAL_ID=&CODE=dezinfektsiya-pitevoy-vody-s-pomoshchyu-uf-izlucheniya', 'Дезинфекция питьевой воды с помощью УФ излучения', 'Вода имеете очень важное значение в жизни человека. Без воды человек может прожить не более 3 суток. Вода оставляет ориентировочно две трети массы тела взрослого человека.', '', 'news', '6', NULL, '2015-04-24 02:37:24', NULL),
(74, '2015-04-24 02:37:39', 'iblock', '39', 0, NULL, NULL, NULL, '=ID=39&EXTERNAL_ID=39&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=news&IBLOCK_ID=6&IBLOCK_CODE=articles&IBLOCK_EXTERNAL_ID=&CODE=ocherednoy-seminar-organizovannyy-soyuzom-proizvoditeley-butilirovannykh-vod-rossii', 'Очередной семинар, организованный Союзом производителей бутилированных вод России', 'Семинар прошел 2-3 июня в Подмосковном отеле «Фореста Фестиваль Парк». В качестве приглашенного специалиста семинар «Влияние финансового кризиса на водные компании» провел независимый бизнес-консультант Константин Раков.', '', 'news', '6', NULL, '2015-04-24 02:37:39', NULL),
(75, '2015-04-24 02:37:55', 'iblock', '40', 0, NULL, NULL, NULL, '=ID=40&EXTERNAL_ID=40&IBLOCK_SECTION_ID=&IBLOCK_TYPE_ID=news&IBLOCK_ID=6&IBLOCK_CODE=articles&IBLOCK_EXTERNAL_ID=&CODE=tekhnologii-ochistki-vody', 'Технологии очистки воды', 'Вода имеете очень важное значение в жизни человека. Без воды человек может прожить не более 3 суток. Вода оставляет ориентировочно две трети массы тела взрослого человека.', '', 'news', '6', NULL, '2015-04-24 02:37:55', NULL),
(76, '2015-04-24 04:19:50', 'main', 's1|/kontakty/novosti/index.php', 0, NULL, NULL, NULL, '/kontakty/novosti/index.php', 'Новости', 'Text here....', '', '', '', NULL, NULL, NULL),
(77, '2015-04-24 04:24:34', 'main', 's1|/kompaniya/novosti/index.php', 0, NULL, NULL, NULL, '/kompaniya/novosti/index.php', 'Новости', '', '', '', '', NULL, NULL, NULL),
(78, '2015-04-24 04:24:55', 'main', 's1|/kompaniya/stati/index.php', 0, NULL, NULL, NULL, '/kompaniya/stati/index.php', 'Статьи', 'Text here....', '', '', '', NULL, NULL, NULL),
(79, '2015-04-27 21:31:27', 'iblock', 'S20', 0, NULL, NULL, NULL, '=ID=20&EXTERNAL_ID=&IBLOCK_TYPE_ID=projects&IBLOCK_ID=7&IBLOCK_CODE=projects&IBLOCK_EXTERNAL_ID=&CODE=voda', 'Вода', '', NULL, 'projects', '7', NULL, NULL, NULL),
(80, '2015-04-27 21:31:17', 'iblock', 'S21', 0, NULL, NULL, NULL, '=ID=21&EXTERNAL_ID=&IBLOCK_TYPE_ID=projects&IBLOCK_ID=7&IBLOCK_CODE=projects&IBLOCK_EXTERNAL_ID=&CODE=gazirovannye-napitki', 'Газированные напитки', '', NULL, 'projects', '7', NULL, NULL, NULL),
(81, '2015-04-27 21:31:05', 'iblock', 'S22', 0, NULL, NULL, NULL, '=ID=22&EXTERNAL_ID=&IBLOCK_TYPE_ID=projects&IBLOCK_ID=7&IBLOCK_CODE=projects&IBLOCK_EXTERNAL_ID=&CODE=bytovaya-khimiya-i-tekhzhidkosti', 'Бытовая химия и техжидкости', '', NULL, 'projects', '7', NULL, NULL, NULL),
(82, '2015-04-27 21:30:52', 'iblock', 'S23', 0, NULL, NULL, NULL, '=ID=23&EXTERNAL_ID=&IBLOCK_TYPE_ID=projects&IBLOCK_ID=7&IBLOCK_CODE=projects&IBLOCK_EXTERNAL_ID=&CODE=maslo', 'Масло', '', NULL, 'projects', '7', NULL, NULL, NULL),
(83, '2015-04-27 21:44:07', 'iblock', '41', 0, NULL, NULL, NULL, '=ID=41&EXTERNAL_ID=41&IBLOCK_SECTION_ID=22&IBLOCK_TYPE_ID=projects&IBLOCK_ID=7&IBLOCK_CODE=projects&IBLOCK_EXTERNAL_ID=&CODE=zavod-ooo-koka-kola-eychbisi-evraziya', 'Завод ООО «Кока-Кола ЭйчБиСи Евразия»', 'Задача:\rЗапустить производство лимонада с нуля в сжатые сроки.\rРезультат:\rСейчас компания выпускает 700 бутылок лимонада и 400 бутылей чистой воды в час. Заказчик планирует расширять производство и ассортимент в сотрудничестве с нами.', '', 'projects', '7', NULL, NULL, NULL),
(84, '2015-04-27 21:44:51', 'iblock', '42', 0, NULL, NULL, NULL, '=ID=42&EXTERNAL_ID=42&IBLOCK_SECTION_ID=21&IBLOCK_TYPE_ID=projects&IBLOCK_ID=7&IBLOCK_CODE=projects&IBLOCK_EXTERNAL_ID=&CODE=tsekh-rozliva-mpbk-ochakovo', 'Цех розлива МПБК «Очаково»', 'Задача:\rЗапустить производство лимонада с нуля в сжатые сроки.\rРезультат:\rСейчас компания выпускает 700 бутылок лимонада и 400 бутылей чистой воды в час. Заказчик планирует расширять производство и ассортимент в сотрудничестве с нами.', '', 'projects', '7', NULL, NULL, NULL),
(85, '2015-04-27 21:47:16', 'iblock', '43', 0, NULL, NULL, NULL, '=ID=43&EXTERNAL_ID=43&IBLOCK_SECTION_ID=23&IBLOCK_TYPE_ID=projects&IBLOCK_ID=7&IBLOCK_CODE=projects&IBLOCK_EXTERNAL_ID=&CODE=zavod-oao-bryanskpivo', 'Завод ОАО «Брянскпиво»', 'Задача:\rЗапустить производство лимонада с нуля в сжатые сроки.\rРезультат:\rСейчас компания выпускает 700 бутылок лимонада и 400 бутылей чистой воды в час. Заказчик планирует расширять производство и ассортимент в сотрудничестве с нами.', '', 'projects', '7', NULL, NULL, NULL),
(86, '2015-05-10 09:51:36', 'iblock', 'S24', 0, NULL, NULL, NULL, '=ID=24&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=products&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=tara-do-2-litrov', 'Тара до 2 литров', '', NULL, 'products', '2', NULL, NULL, NULL),
(87, '2015-05-10 09:51:36', 'iblock', 'S25', 0, NULL, NULL, NULL, '=ID=25&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=products&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=tara-bolee-2-litrov', 'Тара более 2 литров', '', NULL, 'products', '2', NULL, NULL, NULL),
(88, '2015-05-10 09:51:36', 'iblock', 'S26', 0, NULL, NULL, NULL, '=ID=26&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=products&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=kosmetika-tolstostennaya-tara', 'Косметика, толстостенная тара', '', NULL, 'products', '2', NULL, NULL, NULL),
(89, '2015-05-10 09:51:36', 'iblock', 'S27', 0, NULL, NULL, NULL, '=ID=27&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=products&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=banki-uvelichennyy-diametr-gorla', 'Банки, увеличенный диаметр горла', '', NULL, 'products', '2', NULL, NULL, NULL),
(90, '2015-05-10 09:51:36', 'iblock', 'S28', 0, NULL, NULL, NULL, '=ID=28&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=products&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=s-avtoustanovkoy-ruchki', 'С автоустановкой ручки', '', NULL, 'products', '2', NULL, NULL, NULL),
(91, '2015-04-28 14:55:06', 'iblock', '44', 0, NULL, NULL, NULL, '=ID=44&EXTERNAL_ID=44&IBLOCK_SECTION_ID=24&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=products&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=sn-csss2000', 'SN-CSSS2000', 'Автоматическая выдувная машина РN-PP 2000E предназначена для производства ПП (Полипропиленовых) бутылок объёмом от 0,1 до 0.6 литра, путём двухстадийного пневматического формования из предварительно разогретых заготовок (преформ). Для каждого типоразмера бутылки изготавливается своя пресс-форма, точно повторяющая внешний вид и геометрию бутылки, заданные заказчиком.\rМоноблоки серии DGF предназначены для розлива и укупоривания ПЭТ-бутылок емкостью от 0,25 до 2 л газированными жидкостями производительностью до 24000 бут/час.\rКонструктивно моноблок состоит из:\rGX-200 встраиваются в линию, путём стыковки конвейеров с двух сторон автомата\rвсе детали и механизмы контактирующие с продукцией изготовлены из нержавеющей стали\rМашина полностью автоматизирована после загрузки преформ в приемный бункер до подачи готовой продукции на воздушный транспортер и не требует постоянного присутствия обслуживающего персонала.\rОсновным преимуществом данной модели является простота наладки, стабильность параметров выдува и качества продукции, её компактность и удобство в обслуживании. Уже через 15 минут после запуска машина готова выдавать продукцию.', '', 'products', '2', NULL, NULL, NULL),
(92, '2015-04-28 14:57:49', 'iblock', '45', 0, NULL, NULL, NULL, '=ID=45&EXTERNAL_ID=45&IBLOCK_SECTION_ID=24&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=products&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=sn-csss2001', 'SN-CSSS2001', 'Автоматическая выдувная машина РN-PP 2000E предназначена для производства ПП (Полипропиленовых) бутылок объёмом от 0,1 до 0.6 литра, путём двухстадийного пневматического формования из предварительно разогретых заготовок (преформ). Для каждого типоразмера бутылки изготавливается своя пресс-форма, точно повторяющая внешний вид и геометрию бутылки, заданные заказчиком.\rМоноблоки серии DGF предназначены для розлива и укупоривания ПЭТ-бутылок емкостью от 0,25 до 2 л газированными жидкостями производительностью до 24000 бут/час.\rКонструктивно моноблок состоит из:\rGX-200 встраиваются в линию, путём стыковки конвейеров с двух сторон автомата\rвсе детали и механизмы контактирующие с продукцией изготовлены из нержавеющей стали\rМашина полностью автоматизирована после загрузки преформ в приемный бункер до подачи готовой продукции на воздушный транспортер и не требует постоянного присутствия обслуживающего персонала.\rОсновным преимуществом данной модели является простота наладки, стабильность параметров выдува и качества продукции, её компактность и удобство в обслуживании. Уже через 15 минут после запуска машина готова выдавать продукцию.', '', 'products', '2', NULL, NULL, NULL),
(93, '2015-05-10 19:52:14', 'iblock', '46', 0, NULL, NULL, NULL, '=ID=46&EXTERNAL_ID=46&IBLOCK_SECTION_ID=24&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=products&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=sn-csss2002', 'SN-CSSS2002', 'Автоматическая выдувная машина РN-PP 2000E предназначена для производства ПП (Полипропиленовых) бутылок объёмом от 0,1 до 0.6 литра, путём двухстадийного пневматического формования из предварительно разогретых заготовок (преформ). Для каждого типоразмера бутылки изготавливается своя пресс-форма, точно повторяющая внешний вид и геометрию бутылки, заданные заказчиком.\rМоноблоки серии DGF предназначены для розлива и укупоривания ПЭТ-бутылок емкостью от 0,25 до 2 л газированными жидкостями производительностью до 24000 бут/час.\rКонструктивно моноблок состоит из:\rGX-200 встраиваются в линию, путём стыковки конвейеров с двух сторон автомата\rвсе детали и механизмы контактирующие с продукцией изготовлены из нержавеющей стали\rМашина полностью автоматизирована после загрузки преформ в приемный бункер до подачи готовой продукции на воздушный транспортер и не требует постоянного присутствия обслуживающего персонала.\rОсновным преимуществом данной модели является простота наладки, стабильность параметров выдува и качества продукции, её компактность и удобство в обслуживании. Уже через 15 минут после запуска машина готова выдавать продукцию.', '', 'products', '2', NULL, NULL, NULL),
(94, '2015-05-10 09:48:31', 'iblock', 'S29', 0, NULL, NULL, NULL, '=ID=29&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=products&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=avtomaticheskiy-vyduv', 'Автоматический выдув', '', NULL, 'products', '2', NULL, NULL, NULL),
(95, '2015-05-10 09:48:45', 'iblock', 'S30', 0, NULL, NULL, NULL, '=ID=30&EXTERNAL_ID=&IBLOCK_TYPE_ID=products&IBLOCK_ID=2&IBLOCK_CODE=products&IBLOCK_EXTERNAL_ID=furniture_products_s1&CODE=poluavtomaticheskiy-vyvod', 'Полуавтоматический вывод', '', NULL, 'products', '2', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_search_content_freq`
--

CREATE TABLE IF NOT EXISTS `b_search_content_freq` (
  `STEM` int(11) NOT NULL DEFAULT '0',
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FREQ` float DEFAULT NULL,
  `TF` float DEFAULT NULL,
  UNIQUE KEY `UX_B_SEARCH_CONTENT_FREQ` (`STEM`,`LANGUAGE_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_search_content_param`
--

CREATE TABLE IF NOT EXISTS `b_search_content_param` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `PARAM_NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `PARAM_VALUE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  KEY `IX_B_SEARCH_CONTENT_PARAM` (`SEARCH_CONTENT_ID`,`PARAM_NAME`),
  KEY `IX_B_SEARCH_CONTENT_PARAM_1` (`PARAM_NAME`,`PARAM_VALUE`(50),`SEARCH_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_search_content_right`
--

CREATE TABLE IF NOT EXISTS `b_search_content_right` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `GROUP_CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `UX_B_SEARCH_CONTENT_RIGHT` (`SEARCH_CONTENT_ID`,`GROUP_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_search_content_right`
--

INSERT INTO `b_search_content_right` (`SEARCH_CONTENT_ID`, `GROUP_CODE`) VALUES
(1, 'G2'),
(2, 'G2'),
(3, 'G2'),
(4, 'G2'),
(5, 'G2'),
(6, 'G2'),
(7, 'G2'),
(8, 'G2'),
(9, 'G2'),
(10, 'G2'),
(11, 'G2'),
(12, 'G2'),
(13, 'G2'),
(45, 'G2'),
(46, 'G2'),
(50, 'G1'),
(50, 'G2'),
(51, 'G1'),
(51, 'G2'),
(52, 'G1'),
(52, 'G2'),
(53, 'G1'),
(53, 'G2'),
(54, 'G1'),
(54, 'G2'),
(55, 'G1'),
(55, 'G2'),
(56, 'G1'),
(56, 'G2'),
(57, 'G1'),
(57, 'G2'),
(58, 'G1'),
(58, 'G2'),
(59, 'G1'),
(59, 'G2'),
(60, 'G1'),
(60, 'G2'),
(61, 'G2'),
(62, 'G2'),
(63, 'G2'),
(64, 'G2'),
(65, 'G2'),
(66, 'G2'),
(67, 'G2'),
(68, 'G1'),
(68, 'G2'),
(69, 'G1'),
(69, 'G2'),
(70, 'G1'),
(70, 'G2'),
(71, 'G1'),
(71, 'G2'),
(72, 'G1'),
(72, 'G2'),
(73, 'G1'),
(73, 'G2'),
(74, 'G1'),
(74, 'G2'),
(75, 'G1'),
(75, 'G2'),
(76, 'G2'),
(77, 'G2'),
(78, 'G2'),
(79, 'G1'),
(79, 'G2'),
(80, 'G1'),
(80, 'G2'),
(81, 'G1'),
(81, 'G2'),
(82, 'G1'),
(82, 'G2'),
(83, 'G1'),
(83, 'G2'),
(84, 'G1'),
(84, 'G2'),
(85, 'G1'),
(85, 'G2'),
(86, 'G1'),
(86, 'G2'),
(87, 'G1'),
(87, 'G2'),
(88, 'G1'),
(88, 'G2'),
(89, 'G1'),
(89, 'G2'),
(90, 'G1'),
(90, 'G2'),
(91, 'G1'),
(91, 'G2'),
(92, 'G1'),
(92, 'G2'),
(93, 'G1'),
(93, 'G2'),
(94, 'G1'),
(94, 'G2'),
(95, 'G1'),
(95, 'G2');

-- --------------------------------------------------------

--
-- Table structure for table `b_search_content_site`
--

CREATE TABLE IF NOT EXISTS `b_search_content_site` (
  `SEARCH_CONTENT_ID` int(18) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `URL` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`SEARCH_CONTENT_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_search_content_site`
--

INSERT INTO `b_search_content_site` (`SEARCH_CONTENT_ID`, `SITE_ID`, `URL`) VALUES
(1, 's1', ''),
(2, 's1', ''),
(3, 's1', ''),
(4, 's1', ''),
(5, 's1', ''),
(6, 's1', ''),
(7, 's1', ''),
(8, 's1', ''),
(9, 's1', ''),
(10, 's1', ''),
(11, 's1', ''),
(12, 's1', ''),
(13, 's1', ''),
(45, 's1', ''),
(46, 's1', ''),
(50, 's1', ''),
(51, 's1', ''),
(52, 's1', ''),
(53, 's1', ''),
(54, 's1', ''),
(55, 's1', ''),
(56, 's1', ''),
(57, 's1', ''),
(58, 's1', ''),
(59, 's1', ''),
(60, 's1', ''),
(61, 's1', ''),
(62, 's1', ''),
(63, 's1', ''),
(64, 's1', ''),
(65, 's1', ''),
(66, 's1', ''),
(67, 's1', ''),
(68, 's1', ''),
(69, 's1', ''),
(70, 's1', ''),
(71, 's1', ''),
(72, 's1', ''),
(73, 's1', ''),
(74, 's1', ''),
(75, 's1', ''),
(76, 's1', ''),
(77, 's1', ''),
(78, 's1', ''),
(79, 's1', ''),
(80, 's1', ''),
(81, 's1', ''),
(82, 's1', ''),
(83, 's1', ''),
(84, 's1', ''),
(85, 's1', ''),
(86, 's1', ''),
(87, 's1', ''),
(88, 's1', ''),
(89, 's1', ''),
(90, 's1', ''),
(91, 's1', ''),
(92, 's1', ''),
(93, 's1', ''),
(94, 's1', ''),
(95, 's1', '');

-- --------------------------------------------------------

--
-- Table structure for table `b_search_content_stem`
--

CREATE TABLE IF NOT EXISTS `b_search_content_stem` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `STEM` int(11) NOT NULL,
  `TF` float NOT NULL,
  `PS` float NOT NULL,
  UNIQUE KEY `UX_B_SEARCH_CONTENT_STEM` (`STEM`,`LANGUAGE_ID`,`TF`,`PS`,`SEARCH_CONTENT_ID`),
  KEY `IND_B_SEARCH_CONTENT_STEM` (`SEARCH_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci DELAY_KEY_WRITE=1;

--
-- Dumping data for table `b_search_content_stem`
--

INSERT INTO `b_search_content_stem` (`SEARCH_CONTENT_ID`, `LANGUAGE_ID`, `STEM`, `TF`, `PS`) VALUES
(1, 'ru', 1, 0.1187, 1),
(1, 'ru', 2, 0.1187, 2),
(1, 'ru', 3, 0.4392, 287.667),
(1, 'ru', 4, 0.5292, 284.333),
(1, 'ru', 5, 0.1881, 218),
(1, 'ru', 6, 0.1187, 12),
(1, 'ru', 7, 0.356, 261.143),
(1, 'ru', 8, 0.1187, 15),
(1, 'ru', 9, 0.1187, 17),
(1, 'ru', 10, 0.1187, 17),
(1, 'ru', 11, 0.3068, 156.6),
(1, 'ru', 12, 0.2374, 231),
(1, 'ru', 13, 0.1187, 26),
(1, 'ru', 14, 0.1187, 28),
(1, 'ru', 15, 0.1187, 29),
(1, 'ru', 16, 0.3068, 280),
(1, 'ru', 17, 0.1187, 31),
(1, 'ru', 18, 0.2756, 202),
(1, 'ru', 19, 0.1881, 113.5),
(1, 'ru', 20, 0.1187, 35),
(1, 'ru', 21, 0.1187, 36),
(1, 'ru', 22, 0.1881, 340.5),
(1, 'ru', 23, 0.1187, 45),
(1, 'ru', 24, 0.1187, 46),
(1, 'ru', 25, 0.1187, 53),
(1, 'ru', 26, 0.1187, 54),
(1, 'ru', 27, 0.1187, 55),
(1, 'ru', 28, 0.1881, 179.5),
(1, 'ru', 29, 0.1881, 86.5),
(1, 'ru', 30, 0.1187, 60),
(1, 'ru', 31, 0.1187, 62),
(1, 'ru', 32, 0.1187, 63),
(1, 'ru', 33, 0.1187, 64),
(1, 'ru', 34, 0.1187, 70),
(1, 'ru', 35, 0.1187, 76),
(1, 'ru', 36, 0.1187, 78),
(1, 'ru', 37, 0.3332, 371.167),
(1, 'ru', 38, 0.1187, 80),
(1, 'ru', 39, 0.1187, 82),
(1, 'ru', 40, 0.1187, 84),
(1, 'ru', 41, 0.1881, 282.5),
(1, 'ru', 42, 0.1187, 94),
(1, 'ru', 43, 0.3332, 372.167),
(1, 'ru', 44, 0.1187, 96),
(1, 'ru', 45, 0.2374, 264.333),
(1, 'ru', 46, 0.2374, 209.667),
(1, 'ru', 47, 0.1187, 99),
(1, 'ru', 48, 0.1187, 100),
(1, 'ru', 49, 0.1187, 107),
(1, 'ru', 50, 0.1187, 108),
(1, 'ru', 51, 0.3068, 426),
(1, 'ru', 52, 0.1187, 111),
(1, 'ru', 53, 0.2756, 413.75),
(1, 'ru', 54, 0.1187, 115),
(1, 'ru', 55, 0.1187, 116),
(1, 'ru', 56, 0.1187, 117),
(1, 'ru', 57, 0.1187, 118),
(1, 'ru', 58, 0.2756, 226),
(1, 'ru', 59, 0.1187, 121),
(1, 'ru', 60, 0.1881, 148),
(1, 'ru', 61, 0.1187, 123),
(1, 'ru', 62, 0.1187, 130),
(1, 'ru', 63, 0.1187, 137),
(1, 'ru', 64, 0.1881, 232),
(1, 'ru', 65, 0.1881, 311),
(1, 'ru', 66, 0.1187, 141),
(1, 'ru', 67, 0.1881, 150),
(1, 'ru', 68, 0.1187, 154),
(1, 'ru', 69, 0.1187, 155),
(1, 'ru', 70, 0.1187, 156),
(1, 'ru', 71, 0.1187, 158),
(1, 'ru', 72, 0.1881, 242),
(1, 'ru', 73, 0.1187, 161),
(1, 'ru', 74, 0.1187, 162),
(1, 'ru', 75, 0.1187, 164),
(1, 'ru', 76, 0.2374, 315.333),
(1, 'ru', 77, 0.1187, 176),
(1, 'ru', 78, 0.1187, 183),
(1, 'ru', 79, 0.1187, 192),
(1, 'ru', 80, 0.1187, 196),
(1, 'ru', 81, 0.1187, 197),
(1, 'ru', 82, 0.1187, 198),
(1, 'ru', 83, 0.1187, 199),
(1, 'ru', 84, 0.1187, 202),
(1, 'ru', 85, 0.1187, 203),
(1, 'ru', 86, 0.1881, 333.5),
(1, 'ru', 87, 0.1187, 218),
(1, 'ru', 88, 0.1187, 220),
(1, 'ru', 89, 0.1187, 221),
(1, 'ru', 90, 0.1187, 228),
(1, 'ru', 91, 0.1187, 229),
(1, 'ru', 92, 0.1187, 230),
(1, 'ru', 93, 0.1881, 247),
(1, 'ru', 94, 0.1881, 249),
(1, 'ru', 95, 0.2374, 376),
(1, 'ru', 96, 0.1187, 236),
(1, 'ru', 97, 0.1187, 238),
(1, 'ru', 98, 0.1881, 432),
(1, 'ru', 99, 0.1187, 245),
(1, 'ru', 100, 0.1187, 251),
(1, 'ru', 101, 0.1187, 253),
(1, 'ru', 102, 0.1187, 255),
(1, 'ru', 103, 0.1881, 448.5),
(1, 'ru', 104, 0.1881, 448.5),
(1, 'ru', 105, 0.1187, 262),
(1, 'ru', 106, 0.1187, 269),
(1, 'ru', 107, 0.1187, 276),
(1, 'ru', 108, 0.1187, 284),
(1, 'ru', 109, 0.1187, 286),
(1, 'ru', 110, 0.1187, 287),
(1, 'ru', 111, 0.1187, 288),
(1, 'ru', 112, 0.1187, 291),
(1, 'ru', 113, 0.1187, 292),
(1, 'ru', 114, 0.1881, 461.5),
(1, 'ru', 115, 0.1881, 461.5),
(1, 'ru', 116, 0.1187, 297),
(1, 'ru', 117, 0.1187, 299),
(1, 'ru', 118, 0.1187, 300),
(1, 'ru', 119, 0.1187, 300),
(1, 'ru', 120, 0.1187, 301),
(1, 'ru', 121, 0.1187, 308),
(1, 'ru', 122, 0.1187, 314),
(1, 'ru', 123, 0.2756, 444),
(1, 'ru', 124, 0.2374, 374.333),
(1, 'ru', 125, 0.2756, 385.5),
(1, 'ru', 126, 0.2756, 385.5),
(1, 'ru', 127, 0.1187, 327),
(1, 'ru', 128, 0.2374, 482.333),
(1, 'ru', 129, 0.2374, 484.333),
(1, 'ru', 130, 0.1881, 431),
(1, 'ru', 131, 0.1881, 431),
(1, 'ru', 132, 0.1187, 338),
(1, 'ru', 133, 0.1187, 339),
(1, 'ru', 134, 0.1187, 341),
(1, 'ru', 135, 0.1187, 342),
(1, 'ru', 136, 0.1187, 343),
(1, 'ru', 137, 0.1187, 345),
(1, 'ru', 138, 0.1187, 346),
(1, 'ru', 139, 0.1187, 347),
(1, 'ru', 140, 0.1187, 347),
(1, 'ru', 141, 0.1187, 353),
(1, 'ru', 142, 0.1881, 428.5),
(1, 'ru', 143, 0.1187, 362),
(1, 'ru', 144, 0.2374, 495.667),
(1, 'ru', 145, 0.1187, 374),
(1, 'ru', 146, 0.2374, 410),
(1, 'ru', 147, 0.1187, 378),
(1, 'ru', 148, 0.1187, 379),
(1, 'ru', 149, 0.1187, 382),
(1, 'ru', 150, 0.2756, 504.5),
(1, 'ru', 151, 0.1881, 426.5),
(1, 'ru', 152, 0.1187, 394),
(1, 'ru', 153, 0.1187, 400),
(1, 'ru', 154, 0.1187, 407),
(1, 'ru', 155, 0.1187, 411),
(1, 'ru', 156, 0.1187, 413),
(1, 'ru', 157, 0.2374, 472.333),
(1, 'ru', 158, 0.1187, 415),
(1, 'ru', 159, 0.1187, 419),
(1, 'ru', 160, 0.1187, 420),
(1, 'ru', 161, 0.1187, 427),
(1, 'ru', 162, 0.1187, 430),
(1, 'ru', 163, 0.1187, 431),
(1, 'ru', 164, 0.1187, 437),
(1, 'ru', 165, 0.1187, 438),
(1, 'ru', 166, 0.1187, 442),
(1, 'ru', 167, 0.1187, 452),
(1, 'ru', 168, 0.2756, 533.25),
(1, 'ru', 169, 0.1187, 454),
(1, 'ru', 170, 0.1187, 456),
(1, 'ru', 171, 0.1187, 457),
(1, 'ru', 172, 0.1187, 469),
(1, 'ru', 173, 0.1187, 471),
(1, 'ru', 174, 0.1187, 472),
(1, 'ru', 175, 0.1187, 478),
(1, 'ru', 176, 0.1187, 480),
(1, 'ru', 177, 0.1187, 481),
(1, 'ru', 178, 0.1187, 483),
(1, 'ru', 179, 0.1187, 483),
(1, 'ru', 180, 0.1187, 485),
(1, 'ru', 181, 0.1187, 492),
(1, 'ru', 182, 0.2756, 545.25),
(1, 'ru', 183, 0.1187, 507),
(1, 'ru', 184, 0.1187, 509),
(1, 'ru', 185, 0.1881, 561),
(1, 'ru', 186, 0.1187, 512),
(1, 'ru', 187, 0.1187, 515),
(1, 'ru', 188, 0.1187, 527),
(1, 'ru', 189, 0.1187, 528),
(1, 'ru', 190, 0.1881, 562.5),
(1, 'ru', 191, 0.1881, 550.5),
(1, 'ru', 192, 0.1187, 548),
(1, 'ru', 193, 0.1187, 549),
(1, 'ru', 194, 0.1187, 550),
(1, 'ru', 195, 0.1187, 558),
(1, 'ru', 196, 0.1187, 559),
(1, 'ru', 197, 0.1187, 574),
(1, 'ru', 198, 0.1187, 582),
(1, 'ru', 199, 0.1881, 597),
(1, 'ru', 200, 0.1187, 594),
(1, 'ru', 201, 0.1187, 597),
(1, 'ru', 202, 0.1187, 598),
(1, 'ru', 203, 0.1187, 599),
(1, 'ru', 204, 0.1187, 611),
(1, 'ru', 205, 0.1187, 613),
(1, 'ru', 206, 0.1187, 615),
(1, 'ru', 207, 0.1187, 627),
(1, 'ru', 208, 0.1187, 639),
(1, 'ru', 209, 0.1187, 640),
(2, 'ru', 3, 0.2798, 342.25),
(2, 'ru', 4, 0.3615, 217),
(2, 'ru', 7, 0.3615, 156.286),
(2, 'ru', 10, 0.191, 77.5),
(2, 'ru', 11, 0.1205, 478),
(2, 'ru', 12, 0.1205, 398),
(2, 'ru', 19, 0.241, 312.333),
(2, 'ru', 43, 0.1205, 422),
(2, 'ru', 45, 0.1205, 503),
(2, 'ru', 51, 0.1205, 56),
(2, 'ru', 53, 0.1205, 402),
(2, 'ru', 56, 0.3383, 418.333),
(2, 'ru', 58, 0.3615, 330.857),
(2, 'ru', 76, 0.2798, 327.5),
(2, 'ru', 81, 0.1205, 505),
(2, 'ru', 86, 0.191, 248),
(2, 'ru', 98, 0.1205, 336),
(2, 'ru', 104, 0.191, 281.5),
(2, 'ru', 110, 0.1205, 331),
(2, 'ru', 111, 0.191, 257),
(2, 'ru', 125, 0.191, 237.5),
(2, 'ru', 126, 0.191, 237.5),
(2, 'ru', 132, 0.1205, 154),
(2, 'ru', 133, 0.1205, 203),
(2, 'ru', 147, 0.1205, 7),
(2, 'ru', 148, 0.1205, 8),
(2, 'ru', 165, 0.1205, 314),
(2, 'ru', 186, 0.1205, 309),
(2, 'ru', 188, 0.191, 271.5),
(2, 'ru', 192, 0.241, 304),
(2, 'ru', 193, 0.1205, 138),
(2, 'ru', 210, 0.4168, 289.4),
(2, 'ru', 211, 0.1205, 5),
(2, 'ru', 212, 0.1205, 10),
(2, 'ru', 213, 0.1205, 19),
(2, 'ru', 214, 0.191, 254),
(2, 'ru', 215, 0.1205, 23),
(2, 'ru', 216, 0.1205, 24),
(2, 'ru', 217, 0.1205, 26),
(2, 'ru', 218, 0.1205, 27),
(2, 'ru', 219, 0.1205, 28),
(2, 'ru', 220, 0.1205, 32),
(2, 'ru', 221, 0.1205, 34),
(2, 'ru', 222, 0.1205, 43),
(2, 'ru', 223, 0.1205, 44),
(2, 'ru', 224, 0.191, 243.5),
(2, 'ru', 225, 0.1205, 49),
(2, 'ru', 226, 0.1205, 50),
(2, 'ru', 227, 0.1205, 52),
(2, 'ru', 228, 0.1205, 53),
(2, 'ru', 229, 0.1205, 54),
(2, 'ru', 230, 0.191, 85),
(2, 'ru', 231, 0.1205, 57),
(2, 'ru', 232, 0.191, 92),
(2, 'ru', 233, 0.1205, 62),
(2, 'ru', 234, 0.191, 90),
(2, 'ru', 235, 0.1205, 70),
(2, 'ru', 236, 0.1205, 72),
(2, 'ru', 237, 0.241, 306),
(2, 'ru', 238, 0.1205, 74),
(2, 'ru', 239, 0.1205, 83),
(2, 'ru', 240, 0.1205, 86),
(2, 'ru', 241, 0.1205, 87),
(2, 'ru', 242, 0.1205, 88),
(2, 'ru', 243, 0.1205, 89),
(2, 'ru', 244, 0.1205, 91),
(2, 'ru', 245, 0.1205, 92),
(2, 'ru', 246, 0.1205, 96),
(2, 'ru', 247, 0.191, 197),
(2, 'ru', 248, 0.1205, 99),
(2, 'ru', 249, 0.191, 112.5),
(2, 'ru', 250, 0.1205, 107),
(2, 'ru', 251, 0.1205, 108),
(2, 'ru', 252, 0.1205, 109),
(2, 'ru', 253, 0.191, 147),
(2, 'ru', 254, 0.1205, 116),
(2, 'ru', 255, 0.241, 199),
(2, 'ru', 256, 0.1205, 121),
(2, 'ru', 257, 0.191, 267.5),
(2, 'ru', 258, 0.1205, 124),
(2, 'ru', 259, 0.1205, 127),
(2, 'ru', 260, 0.1205, 128),
(2, 'ru', 261, 0.1205, 129),
(2, 'ru', 262, 0.1205, 130),
(2, 'ru', 263, 0.241, 184.333),
(2, 'ru', 264, 0.1205, 140),
(2, 'ru', 265, 0.1205, 142),
(2, 'ru', 266, 0.191, 196.5),
(2, 'ru', 267, 0.1205, 144),
(2, 'ru', 268, 0.1205, 144),
(2, 'ru', 269, 0.1205, 146),
(2, 'ru', 270, 0.1205, 147),
(2, 'ru', 271, 0.1205, 148),
(2, 'ru', 272, 0.1205, 155),
(2, 'ru', 273, 0.1205, 156),
(2, 'ru', 274, 0.191, 181.5),
(2, 'ru', 275, 0.1205, 159),
(2, 'ru', 276, 0.1205, 161),
(2, 'ru', 277, 0.1205, 162),
(2, 'ru', 278, 0.1205, 169),
(2, 'ru', 279, 0.1205, 170),
(2, 'ru', 280, 0.1205, 172),
(2, 'ru', 281, 0.1205, 173),
(2, 'ru', 282, 0.1205, 175),
(2, 'ru', 283, 0.191, 191),
(2, 'ru', 284, 0.1205, 177),
(2, 'ru', 285, 0.241, 299),
(2, 'ru', 286, 0.1205, 181),
(2, 'ru', 287, 0.1205, 183),
(2, 'ru', 288, 0.1205, 185),
(2, 'ru', 289, 0.1205, 187),
(2, 'ru', 290, 0.1205, 196),
(2, 'ru', 291, 0.1205, 198),
(2, 'ru', 292, 0.1205, 199),
(2, 'ru', 293, 0.1205, 200),
(2, 'ru', 294, 0.1205, 201),
(2, 'ru', 295, 0.1205, 201),
(2, 'ru', 296, 0.1205, 207),
(2, 'ru', 297, 0.1205, 208),
(2, 'ru', 298, 0.1205, 209),
(2, 'ru', 299, 0.191, 236),
(2, 'ru', 300, 0.1205, 218),
(2, 'ru', 301, 0.1205, 220),
(2, 'ru', 302, 0.1205, 221),
(2, 'ru', 303, 0.1205, 223),
(2, 'ru', 304, 0.241, 247.667),
(2, 'ru', 305, 0.1205, 232),
(2, 'ru', 306, 0.1205, 234),
(2, 'ru', 307, 0.241, 293.667),
(2, 'ru', 308, 0.1205, 245),
(2, 'ru', 309, 0.1205, 246),
(2, 'ru', 310, 0.1205, 251),
(2, 'ru', 311, 0.191, 256),
(2, 'ru', 312, 0.1205, 255),
(2, 'ru', 313, 0.1205, 259),
(2, 'ru', 314, 0.1205, 260),
(2, 'ru', 315, 0.191, 270),
(2, 'ru', 316, 0.191, 314.5),
(2, 'ru', 317, 0.241, 302.667),
(2, 'ru', 318, 0.1205, 272),
(2, 'ru', 319, 0.1205, 274),
(2, 'ru', 320, 0.1205, 276),
(2, 'ru', 321, 0.1205, 279),
(2, 'ru', 322, 0.191, 312.5),
(2, 'ru', 323, 0.241, 374.333),
(2, 'ru', 324, 0.1205, 281),
(2, 'ru', 325, 0.1205, 282),
(2, 'ru', 326, 0.1205, 283),
(2, 'ru', 327, 0.1205, 285),
(2, 'ru', 328, 0.1205, 286),
(2, 'ru', 329, 0.1205, 289),
(2, 'ru', 330, 0.1205, 291),
(2, 'ru', 331, 0.1205, 292),
(2, 'ru', 332, 0.1205, 294),
(2, 'ru', 333, 0.1205, 296),
(2, 'ru', 334, 0.1205, 298),
(2, 'ru', 335, 0.1205, 299),
(2, 'ru', 336, 0.1205, 305),
(2, 'ru', 337, 0.1205, 310),
(2, 'ru', 338, 0.191, 314),
(2, 'ru', 339, 0.191, 314),
(2, 'ru', 340, 0.1205, 316),
(2, 'ru', 341, 0.1205, 324),
(2, 'ru', 342, 0.1205, 327),
(2, 'ru', 343, 0.1205, 328),
(2, 'ru', 344, 0.1205, 329),
(2, 'ru', 345, 0.1205, 332),
(2, 'ru', 346, 0.1205, 333),
(2, 'ru', 347, 0.191, 357.5),
(2, 'ru', 348, 0.191, 376),
(2, 'ru', 349, 0.191, 373),
(2, 'ru', 350, 0.1205, 348),
(2, 'ru', 351, 0.1205, 348),
(2, 'ru', 352, 0.1205, 355),
(2, 'ru', 353, 0.1205, 356),
(2, 'ru', 354, 0.1205, 357),
(2, 'ru', 355, 0.1205, 361),
(2, 'ru', 356, 0.1205, 362),
(2, 'ru', 357, 0.1205, 373),
(2, 'ru', 358, 0.1205, 374),
(2, 'ru', 359, 0.1205, 376),
(2, 'ru', 360, 0.1205, 378),
(2, 'ru', 361, 0.1205, 380),
(2, 'ru', 362, 0.1205, 388),
(2, 'ru', 363, 0.191, 447.5),
(2, 'ru', 364, 0.1205, 392),
(2, 'ru', 365, 0.1205, 394),
(2, 'ru', 366, 0.1205, 404),
(2, 'ru', 367, 0.1205, 405),
(2, 'ru', 368, 0.1205, 406),
(2, 'ru', 369, 0.1205, 407),
(2, 'ru', 370, 0.1205, 421),
(2, 'ru', 371, 0.1205, 423),
(2, 'ru', 372, 0.1205, 425),
(2, 'ru', 373, 0.1205, 428),
(2, 'ru', 374, 0.1205, 432),
(2, 'ru', 375, 0.1205, 435),
(2, 'ru', 376, 0.1205, 436),
(2, 'ru', 377, 0.1205, 437),
(2, 'ru', 378, 0.1205, 442),
(2, 'ru', 379, 0.1205, 448),
(2, 'ru', 380, 0.1205, 451),
(2, 'ru', 381, 0.1205, 452),
(2, 'ru', 382, 0.1205, 453),
(2, 'ru', 383, 0.1205, 455),
(2, 'ru', 384, 0.1205, 456),
(2, 'ru', 385, 0.1205, 459),
(2, 'ru', 386, 0.191, 479),
(2, 'ru', 387, 0.241, 483),
(2, 'ru', 388, 0.191, 483.5),
(2, 'ru', 389, 0.1205, 464),
(2, 'ru', 390, 0.1205, 472),
(2, 'ru', 391, 0.1205, 473),
(2, 'ru', 392, 0.1205, 479),
(2, 'ru', 393, 0.1205, 484),
(2, 'ru', 394, 0.1205, 487),
(2, 'ru', 395, 0.1205, 490),
(2, 'ru', 396, 0.191, 495.5),
(2, 'ru', 397, 0.1205, 492),
(2, 'ru', 398, 0.1205, 494),
(2, 'ru', 399, 0.1205, 495),
(2, 'ru', 400, 0.1205, 497),
(3, 'ru', 3, 0.4303, 101.25),
(3, 'ru', 4, 0.5023, 145.25),
(3, 'ru', 12, 0.2152, 136),
(3, 'ru', 16, 0.1358, 70),
(3, 'ru', 28, 0.2152, 104.5),
(3, 'ru', 33, 0.1358, 62),
(3, 'ru', 38, 0.1358, 234),
(3, 'ru', 43, 0.1358, 67),
(3, 'ru', 45, 0.2152, 203),
(3, 'ru', 47, 0.1358, 95),
(3, 'ru', 57, 0.1358, 59),
(3, 'ru', 58, 0.1358, 60),
(3, 'ru', 61, 0.1358, 64),
(3, 'ru', 69, 0.1358, 77),
(3, 'ru', 86, 0.4073, 150.143),
(3, 'ru', 95, 0.1358, 78),
(3, 'ru', 113, 0.1358, 42),
(3, 'ru', 131, 0.2152, 79),
(3, 'ru', 132, 0.1358, 7),
(3, 'ru', 133, 0.1358, 165),
(3, 'ru', 136, 0.1358, 72),
(3, 'ru', 148, 0.2152, 32.5),
(3, 'ru', 175, 0.1358, 71),
(3, 'ru', 189, 0.1358, 2),
(3, 'ru', 190, 0.2152, 76.5),
(3, 'ru', 204, 0.1358, 69),
(3, 'ru', 205, 0.1358, 98),
(3, 'ru', 213, 0.2715, 139.667),
(3, 'ru', 215, 0.1358, 91),
(3, 'ru', 216, 0.1358, 231),
(3, 'ru', 266, 0.1358, 76),
(3, 'ru', 292, 0.1358, 6),
(3, 'ru', 304, 0.1358, 9),
(3, 'ru', 325, 0.1358, 101),
(3, 'ru', 337, 0.1358, 22),
(3, 'ru', 344, 0.1358, 142),
(3, 'ru', 392, 0.1358, 41),
(3, 'ru', 401, 0.2152, 5.5),
(3, 'ru', 402, 0.1358, 8),
(3, 'ru', 403, 0.1358, 23),
(3, 'ru', 404, 0.1358, 35),
(3, 'ru', 405, 0.1358, 38),
(3, 'ru', 406, 0.1358, 39),
(3, 'ru', 407, 0.1358, 40),
(3, 'ru', 408, 0.1358, 43),
(3, 'ru', 409, 0.1358, 43),
(3, 'ru', 410, 0.1358, 57),
(3, 'ru', 411, 0.1358, 58),
(3, 'ru', 412, 0.1358, 61),
(3, 'ru', 413, 0.1358, 66),
(3, 'ru', 414, 0.1358, 75),
(3, 'ru', 415, 0.1358, 77),
(3, 'ru', 416, 0.1358, 80),
(3, 'ru', 417, 0.1358, 81),
(3, 'ru', 418, 0.1358, 83),
(3, 'ru', 419, 0.1358, 92),
(3, 'ru', 420, 0.1358, 94),
(3, 'ru', 421, 0.1358, 96),
(3, 'ru', 422, 0.1358, 97),
(3, 'ru', 423, 0.1358, 102),
(3, 'ru', 424, 0.1358, 108),
(3, 'ru', 425, 0.1358, 112),
(3, 'ru', 426, 0.1358, 112),
(3, 'ru', 427, 0.2152, 136.5),
(3, 'ru', 428, 0.2152, 137.5),
(3, 'ru', 429, 0.2152, 151),
(3, 'ru', 430, 0.1358, 117),
(3, 'ru', 431, 0.1358, 118),
(3, 'ru', 432, 0.2715, 191.333),
(3, 'ru', 433, 0.1358, 126),
(3, 'ru', 434, 0.1358, 127),
(3, 'ru', 435, 0.2152, 190.5),
(3, 'ru', 436, 0.2715, 194),
(3, 'ru', 437, 0.2152, 193),
(3, 'ru', 438, 0.2152, 165),
(3, 'ru', 439, 0.2152, 166),
(3, 'ru', 440, 0.2152, 179.5),
(3, 'ru', 441, 0.2152, 185.5),
(3, 'ru', 442, 0.2152, 174),
(3, 'ru', 443, 0.1358, 145),
(3, 'ru', 444, 0.1358, 146),
(3, 'ru', 445, 0.1358, 148),
(3, 'ru', 446, 0.2152, 179.5),
(3, 'ru', 447, 0.1358, 158),
(3, 'ru', 448, 0.1358, 162),
(3, 'ru', 449, 0.2152, 205),
(3, 'ru', 450, 0.1358, 166),
(3, 'ru', 451, 0.1358, 167),
(3, 'ru', 452, 0.1358, 169),
(3, 'ru', 453, 0.1358, 171),
(3, 'ru', 454, 0.2715, 220.333),
(3, 'ru', 455, 0.2715, 221.333),
(3, 'ru', 456, 0.1358, 184),
(3, 'ru', 457, 0.1358, 185),
(3, 'ru', 458, 0.1358, 186),
(3, 'ru', 459, 0.2152, 228.5),
(3, 'ru', 460, 0.1358, 190),
(3, 'ru', 461, 0.1358, 202),
(3, 'ru', 462, 0.1358, 203),
(3, 'ru', 463, 0.1358, 205),
(3, 'ru', 464, 0.1358, 217),
(3, 'ru', 465, 0.1358, 218),
(3, 'ru', 466, 0.1358, 233),
(3, 'ru', 467, 0.1358, 244),
(3, 'ru', 468, 0.1358, 249),
(3, 'ru', 469, 0.1358, 250),
(3, 'ru', 470, 0.1358, 251),
(3, 'ru', 471, 0.1358, 254),
(3, 'ru', 472, 0.1358, 257),
(3, 'ru', 473, 0.1358, 258),
(3, 'ru', 474, 0.1358, 269),
(4, 'ru', 3, 0.2974, 187.25),
(4, 'ru', 4, 0.4592, 183.636),
(4, 'ru', 12, 0.1281, 130),
(4, 'ru', 22, 0.1281, 45),
(4, 'ru', 28, 0.1281, 137),
(4, 'ru', 37, 0.203, 265),
(4, 'ru', 43, 0.1281, 9),
(4, 'ru', 45, 0.1281, 10),
(4, 'ru', 53, 0.1281, 252),
(4, 'ru', 56, 0.2974, 125.25),
(4, 'ru', 57, 0.203, 182),
(4, 'ru', 58, 0.2974, 159.5),
(4, 'ru', 61, 0.1281, 129),
(4, 'ru', 69, 0.1281, 369),
(4, 'ru', 76, 0.1281, 370),
(4, 'ru', 81, 0.1281, 297),
(4, 'ru', 87, 0.1281, 295),
(4, 'ru', 89, 0.1281, 298),
(4, 'ru', 103, 0.203, 303),
(4, 'ru', 104, 0.203, 303),
(4, 'ru', 110, 0.1281, 31),
(4, 'ru', 130, 0.1281, 341),
(4, 'ru', 131, 0.203, 326),
(4, 'ru', 132, 0.1281, 264),
(4, 'ru', 133, 0.1281, 11),
(4, 'ru', 135, 0.1281, 307),
(4, 'ru', 136, 0.1281, 308),
(4, 'ru', 143, 0.1281, 199),
(4, 'ru', 164, 0.1281, 90),
(4, 'ru', 175, 0.203, 156.5),
(4, 'ru', 188, 0.1281, 255),
(4, 'ru', 190, 0.203, 329.5),
(4, 'ru', 202, 0.1281, 178),
(4, 'ru', 203, 0.1281, 52),
(4, 'ru', 210, 0.2974, 359.5),
(4, 'ru', 215, 0.1281, 67),
(4, 'ru', 216, 0.1281, 121),
(4, 'ru', 223, 0.1281, 82),
(4, 'ru', 224, 0.1281, 336),
(4, 'ru', 225, 0.1281, 30),
(4, 'ru', 226, 0.1281, 80),
(4, 'ru', 257, 0.1281, 288),
(4, 'ru', 261, 0.1281, 51),
(4, 'ru', 266, 0.1281, 236),
(4, 'ru', 304, 0.2974, 156.5),
(4, 'ru', 308, 0.1281, 145),
(4, 'ru', 314, 0.1281, 327),
(4, 'ru', 321, 0.1281, 287),
(4, 'ru', 322, 0.1281, 287),
(4, 'ru', 343, 0.1281, 243),
(4, 'ru', 344, 0.1281, 234),
(4, 'ru', 348, 0.203, 228),
(4, 'ru', 352, 0.1281, 266),
(4, 'ru', 359, 0.1281, 338),
(4, 'ru', 364, 0.1281, 351),
(4, 'ru', 375, 0.2562, 177.667),
(4, 'ru', 385, 0.1281, 12),
(4, 'ru', 387, 0.203, 174),
(4, 'ru', 388, 0.203, 179),
(4, 'ru', 389, 0.1281, 154),
(4, 'ru', 392, 0.1281, 158),
(4, 'ru', 395, 0.203, 152),
(4, 'ru', 396, 0.1281, 168),
(4, 'ru', 413, 0.1281, 128),
(4, 'ru', 414, 0.203, 334.5),
(4, 'ru', 415, 0.1281, 369),
(4, 'ru', 423, 0.1281, 164),
(4, 'ru', 463, 0.1281, 177),
(4, 'ru', 475, 0.1281, 1),
(4, 'ru', 476, 0.1281, 3),
(4, 'ru', 477, 0.1281, 7),
(4, 'ru', 478, 0.1281, 13),
(4, 'ru', 479, 0.1281, 14),
(4, 'ru', 480, 0.1281, 15),
(4, 'ru', 481, 0.1281, 16),
(4, 'ru', 482, 0.2974, 115.5),
(4, 'ru', 483, 0.1281, 19),
(4, 'ru', 484, 0.1281, 22),
(4, 'ru', 485, 0.1281, 29),
(4, 'ru', 486, 0.1281, 32),
(4, 'ru', 487, 0.1281, 33),
(4, 'ru', 488, 0.1281, 35),
(4, 'ru', 489, 0.2562, 171.333),
(4, 'ru', 490, 0.1281, 38),
(4, 'ru', 491, 0.2562, 237.667),
(4, 'ru', 492, 0.1281, 48),
(4, 'ru', 493, 0.1281, 50),
(4, 'ru', 494, 0.1281, 62),
(4, 'ru', 495, 0.1281, 63),
(4, 'ru', 496, 0.1281, 64),
(4, 'ru', 497, 0.1281, 65),
(4, 'ru', 498, 0.1281, 68),
(4, 'ru', 499, 0.1281, 76),
(4, 'ru', 500, 0.1281, 77),
(4, 'ru', 501, 0.1281, 78),
(4, 'ru', 502, 0.1281, 79),
(4, 'ru', 503, 0.1281, 81),
(4, 'ru', 504, 0.203, 119.5),
(4, 'ru', 505, 0.1281, 91),
(4, 'ru', 506, 0.1281, 92),
(4, 'ru', 507, 0.1281, 93),
(4, 'ru', 508, 0.1281, 93),
(4, 'ru', 509, 0.1281, 105),
(4, 'ru', 510, 0.2562, 189.333),
(4, 'ru', 511, 0.2974, 189),
(4, 'ru', 512, 0.1281, 109),
(4, 'ru', 513, 0.1281, 111),
(4, 'ru', 514, 0.2562, 272),
(4, 'ru', 515, 0.2974, 208.25),
(4, 'ru', 516, 0.1281, 133),
(4, 'ru', 517, 0.1281, 134),
(4, 'ru', 518, 0.1281, 135),
(4, 'ru', 519, 0.1281, 144),
(4, 'ru', 520, 0.1281, 147),
(4, 'ru', 521, 0.1281, 153),
(4, 'ru', 522, 0.1281, 156),
(4, 'ru', 523, 0.1281, 157),
(4, 'ru', 524, 0.1281, 167),
(4, 'ru', 525, 0.1281, 175),
(4, 'ru', 526, 0.1281, 179),
(4, 'ru', 527, 0.1281, 180),
(4, 'ru', 528, 0.1281, 186),
(4, 'ru', 529, 0.203, 235),
(4, 'ru', 530, 0.1281, 196),
(4, 'ru', 531, 0.203, 239.5),
(4, 'ru', 532, 0.1281, 203),
(4, 'ru', 533, 0.1281, 204),
(4, 'ru', 534, 0.1281, 206),
(4, 'ru', 535, 0.203, 291),
(4, 'ru', 536, 0.1281, 221),
(4, 'ru', 537, 0.1281, 222),
(4, 'ru', 538, 0.1281, 224),
(4, 'ru', 539, 0.1281, 225),
(4, 'ru', 540, 0.1281, 233),
(4, 'ru', 541, 0.1281, 237),
(4, 'ru', 542, 0.1281, 244),
(4, 'ru', 543, 0.1281, 250),
(4, 'ru', 544, 0.1281, 251),
(4, 'ru', 545, 0.1281, 258),
(4, 'ru', 546, 0.1281, 260),
(4, 'ru', 547, 0.1281, 261),
(4, 'ru', 548, 0.1281, 265),
(4, 'ru', 549, 0.1281, 284),
(4, 'ru', 550, 0.1281, 289),
(4, 'ru', 551, 0.1281, 306),
(4, 'ru', 552, 0.1281, 314),
(4, 'ru', 553, 0.1281, 325),
(4, 'ru', 554, 0.1281, 328),
(4, 'ru', 555, 0.1281, 331),
(4, 'ru', 556, 0.1281, 333),
(4, 'ru', 557, 0.1281, 350),
(4, 'ru', 558, 0.1281, 354),
(4, 'ru', 559, 0.1281, 361),
(4, 'ru', 560, 0.1281, 363),
(4, 'ru', 561, 0.1281, 366),
(4, 'ru', 562, 0.1281, 367),
(4, 'ru', 563, 0.1281, 377),
(4, 'ru', 564, 0.1281, 378),
(4, 'ru', 565, 0.203, 383.5),
(4, 'ru', 566, 0.1281, 384),
(4, 'ru', 567, 0.1281, 385),
(4, 'ru', 568, 0.1281, 387),
(4, 'ru', 569, 0.1281, 389),
(5, 'ru', 570, 0.2314, 1),
(6, 'ru', 7, 0.2694, 19),
(6, 'ru', 10, 0.2694, 79.5),
(6, 'ru', 58, 0.17, 99),
(6, 'ru', 72, 0.17, 113),
(6, 'ru', 140, 0.17, 43),
(6, 'ru', 183, 0.17, 88),
(6, 'ru', 210, 0.2694, 25.5),
(6, 'ru', 229, 0.17, 36),
(6, 'ru', 233, 0.17, 26),
(6, 'ru', 247, 0.17, 17),
(6, 'ru', 322, 0.17, 5),
(6, 'ru', 343, 0.17, 8),
(6, 'ru', 400, 0.17, 7),
(6, 'ru', 472, 0.17, 19),
(6, 'ru', 529, 0.17, 12),
(6, 'ru', 568, 0.34, 54.3333),
(6, 'ru', 571, 0.17, 1),
(6, 'ru', 572, 0.2694, 18),
(6, 'ru', 573, 0.17, 9),
(6, 'ru', 574, 0.17, 14),
(6, 'ru', 575, 0.17, 18),
(6, 'ru', 576, 0.17, 20),
(6, 'ru', 577, 0.17, 22),
(6, 'ru', 578, 0.17, 33),
(6, 'ru', 579, 0.3947, 62.75),
(6, 'ru', 580, 0.17, 40),
(6, 'ru', 581, 0.17, 41),
(6, 'ru', 582, 0.17, 45),
(6, 'ru', 583, 0.17, 54),
(6, 'ru', 584, 0.17, 55),
(6, 'ru', 585, 0.17, 56),
(6, 'ru', 586, 0.17, 59),
(6, 'ru', 587, 0.17, 71),
(6, 'ru', 588, 0.34, 74.6667),
(6, 'ru', 589, 0.17, 73),
(6, 'ru', 590, 0.17, 76),
(6, 'ru', 591, 0.17, 78),
(6, 'ru', 592, 0.17, 79),
(6, 'ru', 593, 0.17, 80),
(6, 'ru', 594, 0.34, 91),
(6, 'ru', 595, 0.17, 90),
(6, 'ru', 596, 0.17, 98),
(6, 'ru', 597, 0.17, 100),
(6, 'ru', 598, 0.17, 108),
(6, 'ru', 599, 0.17, 109),
(6, 'ru', 600, 0.17, 110),
(7, 'ru', 189, 0.2314, 7),
(7, 'ru', 222, 0.2314, 16),
(7, 'ru', 601, 0.2314, 1),
(7, 'ru', 602, 0.2314, 3),
(7, 'ru', 603, 0.2314, 5),
(7, 'ru', 604, 0.2314, 8),
(7, 'ru', 605, 0.2314, 14),
(7, 'ru', 606, 0.2314, 17),
(8, 'ru', 607, 0.2314, 1),
(9, 'ru', 58, 0.2314, 1),
(10, 'ru', 608, 0.2314, 1),
(11, 'ru', 602, 0.2314, 2),
(11, 'ru', 609, 0.2314, 1),
(12, 'ru', 610, 0.2314, 1),
(13, 'ru', 3, 0.3359, 21.3333),
(13, 'ru', 4, 0.39, 17.75),
(13, 'ru', 7, 0.2662, 41),
(13, 'ru', 12, 0.1679, 46),
(13, 'ru', 19, 0.2662, 54.5),
(13, 'ru', 43, 0.1679, 70),
(13, 'ru', 53, 0.1679, 50),
(13, 'ru', 56, 0.1679, 61),
(13, 'ru', 58, 0.2662, 80),
(13, 'ru', 76, 0.1679, 37),
(13, 'ru', 86, 0.1679, 11),
(13, 'ru', 111, 0.1679, 33),
(13, 'ru', 147, 0.1679, 7),
(13, 'ru', 148, 0.1679, 8),
(13, 'ru', 192, 0.2662, 81),
(13, 'ru', 210, 0.39, 64.75),
(13, 'ru', 211, 0.1679, 5),
(13, 'ru', 212, 0.1679, 10),
(13, 'ru', 213, 0.1679, 19),
(13, 'ru', 214, 0.1679, 22),
(13, 'ru', 215, 0.1679, 23),
(13, 'ru', 216, 0.1679, 24),
(13, 'ru', 217, 0.1679, 26),
(13, 'ru', 218, 0.1679, 27),
(13, 'ru', 219, 0.1679, 28),
(13, 'ru', 220, 0.1679, 32),
(13, 'ru', 221, 0.1679, 34),
(13, 'ru', 224, 0.1679, 88),
(13, 'ru', 237, 0.2662, 70.5),
(13, 'ru', 257, 0.1679, 60),
(13, 'ru', 307, 0.1679, 45),
(13, 'ru', 323, 0.2662, 69.5),
(13, 'ru', 348, 0.1679, 56),
(13, 'ru', 349, 0.1679, 49),
(13, 'ru', 366, 0.1679, 52),
(13, 'ru', 367, 0.1679, 53),
(13, 'ru', 368, 0.1679, 54),
(13, 'ru', 369, 0.1679, 55),
(13, 'ru', 370, 0.1679, 69),
(13, 'ru', 371, 0.1679, 71),
(13, 'ru', 372, 0.1679, 73),
(13, 'ru', 373, 0.1679, 76),
(13, 'ru', 374, 0.1679, 80),
(13, 'ru', 375, 0.1679, 83),
(13, 'ru', 376, 0.1679, 84),
(13, 'ru', 377, 0.1679, 85),
(13, 'ru', 378, 0.1679, 90),
(13, 'ru', 610, 0.1679, 99),
(45, 'ru', 4, 0.2257, 11),
(45, 'ru', 7, 0.3998, 67),
(45, 'ru', 10, 0.1424, 57),
(45, 'ru', 12, 0.2257, 139.5),
(45, 'ru', 22, 0.1424, 81),
(45, 'ru', 51, 0.1424, 178),
(45, 'ru', 104, 0.1424, 170),
(45, 'ru', 125, 0.1424, 77),
(45, 'ru', 126, 0.1424, 77),
(45, 'ru', 133, 0.2257, 98),
(45, 'ru', 137, 0.2257, 151.5),
(45, 'ru', 140, 0.2257, 152.5),
(45, 'ru', 186, 0.1424, 105),
(45, 'ru', 192, 0.2257, 43),
(45, 'ru', 193, 0.3306, 101),
(45, 'ru', 210, 0.2848, 30.3333),
(45, 'ru', 213, 0.2257, 134),
(45, 'ru', 221, 0.1424, 28),
(45, 'ru', 233, 0.1424, 55),
(45, 'ru', 245, 0.1424, 32),
(45, 'ru', 249, 0.1424, 195),
(45, 'ru', 252, 0.1424, 92),
(45, 'ru', 253, 0.1424, 104),
(45, 'ru', 255, 0.1424, 58),
(45, 'ru', 264, 0.1424, 48),
(45, 'ru', 266, 0.1424, 144),
(45, 'ru', 271, 0.1424, 155),
(45, 'ru', 284, 0.1424, 161),
(45, 'ru', 287, 0.1424, 179),
(45, 'ru', 297, 0.1424, 51),
(45, 'ru', 298, 0.1424, 52),
(45, 'ru', 299, 0.2848, 11.6667),
(45, 'ru', 303, 0.1424, 119),
(45, 'ru', 305, 0.1424, 159),
(45, 'ru', 322, 0.2257, 101.5),
(45, 'ru', 326, 0.2257, 139),
(45, 'ru', 378, 0.1424, 39),
(45, 'ru', 414, 0.1424, 74),
(45, 'ru', 442, 0.1424, 135),
(45, 'ru', 532, 0.1424, 75),
(45, 'ru', 559, 0.1424, 168),
(45, 'ru', 575, 0.2257, 13),
(45, 'ru', 577, 0.3306, 170.75),
(45, 'ru', 610, 0.1424, 173),
(45, 'ru', 641, 0.1424, 146),
(45, 'ru', 653, 0.1424, 161),
(45, 'ru', 676, 0.1424, 76),
(45, 'ru', 707, 0.1424, 70),
(45, 'ru', 912, 0.1424, 91),
(45, 'ru', 947, 0.2257, 32.5),
(45, 'ru', 1004, 0.1424, 84),
(45, 'ru', 1018, 0.1424, 83),
(45, 'ru', 1027, 0.2257, 142.5),
(45, 'ru', 1033, 0.1424, 103),
(45, 'ru', 1106, 0.2257, 12),
(45, 'ru', 1107, 0.1424, 26),
(45, 'ru', 1108, 0.1424, 30),
(45, 'ru', 1109, 0.1424, 31),
(45, 'ru', 1110, 0.1424, 33),
(45, 'ru', 1111, 0.1424, 34),
(45, 'ru', 1112, 0.1424, 35),
(45, 'ru', 1113, 0.1424, 37),
(45, 'ru', 1114, 0.1424, 40),
(45, 'ru', 1115, 0.1424, 41),
(45, 'ru', 1116, 0.1424, 50),
(45, 'ru', 1117, 0.1424, 59),
(45, 'ru', 1118, 0.1424, 60),
(45, 'ru', 1119, 0.1424, 73),
(45, 'ru', 1120, 0.1424, 86),
(45, 'ru', 1121, 0.1424, 89),
(45, 'ru', 1122, 0.1424, 98),
(45, 'ru', 1123, 0.1424, 101),
(45, 'ru', 1124, 0.2257, 115),
(45, 'ru', 1125, 0.2848, 148),
(45, 'ru', 1126, 0.1424, 118),
(45, 'ru', 1127, 0.1424, 122),
(45, 'ru', 1128, 0.1424, 126),
(45, 'ru', 1129, 0.1424, 130),
(45, 'ru', 1130, 0.1424, 136),
(45, 'ru', 1131, 0.1424, 137),
(45, 'ru', 1132, 0.1424, 145),
(45, 'ru', 1133, 0.1424, 148),
(45, 'ru', 1134, 0.1424, 149),
(45, 'ru', 1135, 0.1424, 154),
(45, 'ru', 1136, 0.1424, 157),
(45, 'ru', 1137, 0.1424, 160),
(45, 'ru', 1138, 0.1424, 167),
(45, 'ru', 1139, 0.1424, 171),
(45, 'ru', 1140, 0.1424, 172),
(45, 'ru', 1141, 0.1424, 174),
(45, 'ru', 1142, 0.1424, 180),
(45, 'ru', 1143, 0.1424, 192),
(45, 'ru', 1144, 0.1424, 193),
(45, 'ru', 1145, 0.1424, 196),
(45, 'ru', 1146, 0.2257, 203),
(45, 'ru', 1147, 0.1424, 199),
(45, 'ru', 1148, 0.1424, 207),
(45, 'ru', 1149, 0.1424, 209),
(46, 'ru', 7, 0.3577, 57),
(46, 'ru', 37, 0.154, 25),
(46, 'ru', 192, 0.154, 138),
(46, 'ru', 203, 0.154, 26),
(46, 'ru', 226, 0.154, 62),
(46, 'ru', 232, 0.154, 77),
(46, 'ru', 247, 0.154, 9),
(46, 'ru', 250, 0.154, 2),
(46, 'ru', 253, 0.2441, 74),
(46, 'ru', 255, 0.154, 122),
(46, 'ru', 257, 0.154, 87),
(46, 'ru', 259, 0.154, 58),
(46, 'ru', 261, 0.154, 66),
(46, 'ru', 263, 0.154, 94),
(46, 'ru', 271, 0.154, 101),
(46, 'ru', 281, 0.154, 95),
(46, 'ru', 282, 0.154, 42),
(46, 'ru', 286, 0.154, 28),
(46, 'ru', 289, 0.154, 20),
(46, 'ru', 293, 0.154, 40),
(46, 'ru', 294, 0.154, 21),
(46, 'ru', 295, 0.2441, 44),
(46, 'ru', 319, 0.154, 74),
(46, 'ru', 323, 0.154, 91),
(46, 'ru', 326, 0.154, 52),
(46, 'ru', 377, 0.154, 97),
(46, 'ru', 387, 0.154, 124),
(46, 'ru', 461, 0.154, 34),
(46, 'ru', 486, 0.2441, 76),
(46, 'ru', 493, 0.154, 4),
(46, 'ru', 529, 0.2441, 95),
(46, 'ru', 575, 0.3577, 44.5),
(46, 'ru', 610, 0.2441, 4),
(46, 'ru', 641, 0.154, 96),
(46, 'ru', 654, 0.154, 121),
(46, 'ru', 655, 0.154, 121),
(46, 'ru', 712, 0.154, 50),
(46, 'ru', 812, 0.2441, 75.5),
(46, 'ru', 857, 0.154, 128),
(46, 'ru', 940, 0.154, 36),
(46, 'ru', 1128, 0.154, 57),
(46, 'ru', 1130, 0.154, 76),
(46, 'ru', 1132, 0.154, 52),
(46, 'ru', 1150, 0.154, 5),
(46, 'ru', 1151, 0.154, 6),
(46, 'ru', 1152, 0.154, 16),
(46, 'ru', 1153, 0.154, 18),
(46, 'ru', 1154, 0.154, 22),
(46, 'ru', 1155, 0.154, 23),
(46, 'ru', 1156, 0.154, 27),
(46, 'ru', 1157, 0.154, 29),
(46, 'ru', 1158, 0.154, 35),
(46, 'ru', 1159, 0.154, 38),
(46, 'ru', 1160, 0.154, 41),
(46, 'ru', 1161, 0.154, 49),
(46, 'ru', 1162, 0.154, 51),
(46, 'ru', 1163, 0.154, 55),
(46, 'ru', 1164, 0.2441, 70.5),
(46, 'ru', 1165, 0.154, 60),
(46, 'ru', 1166, 0.154, 61),
(46, 'ru', 1167, 0.154, 63),
(46, 'ru', 1168, 0.154, 65),
(46, 'ru', 1169, 0.154, 75),
(46, 'ru', 1170, 0.154, 84),
(46, 'ru', 1171, 0.154, 88),
(46, 'ru', 1172, 0.154, 90),
(46, 'ru', 1173, 0.154, 92),
(46, 'ru', 1174, 0.154, 98),
(46, 'ru', 1175, 0.2441, 112.5),
(46, 'ru', 1176, 0.154, 109),
(46, 'ru', 1177, 0.154, 116),
(46, 'ru', 1178, 0.154, 126),
(46, 'ru', 1179, 0.154, 139),
(46, 'ru', 1180, 0.154, 140),
(46, 'ru', 1181, 0.154, 141),
(46, 'ru', 1182, 0.154, 142),
(50, 'ru', 1263, 0.2314, 1),
(50, 'ru', 1264, 0.2314, 2),
(51, 'ru', 53, 0.2314, 1),
(51, 'ru', 1265, 0.2314, 2),
(52, 'ru', 53, 0.2314, 2),
(52, 'ru', 1266, 0.2314, 1),
(53, 'ru', 1267, 0.2314, 1),
(54, 'ru', 1268, 0.2314, 1),
(55, 'ru', 53, 0.2314, 2),
(55, 'ru', 1269, 0.2314, 1),
(56, 'ru', 1270, 0.2314, 1),
(57, 'ru', 396, 0.2314, 2),
(57, 'ru', 1271, 0.2314, 1),
(58, 'ru', 1272, 0.2314, 1),
(59, 'ru', 1273, 0.2314, 1),
(60, 'ru', 1274, 0.2314, 1),
(61, 'ru', 58, 0.2314, 1),
(62, 'ru', 157, 0.2314, 2),
(62, 'ru', 334, 0.2314, 1),
(62, 'ru', 1275, 0.2314, 3),
(62, 'ru', 1276, 0.2314, 4),
(63, 'ru', 610, 0.2314, 1),
(63, 'ru', 1275, 0.2314, 2),
(63, 'ru', 1276, 0.2314, 3),
(64, 'ru', 193, 0.2314, 1),
(65, 'ru', 4, 0.1867, 1),
(65, 'ru', 22, 0.1867, 58),
(65, 'ru', 53, 0.3733, 18.6667),
(65, 'ru', 56, 0.1867, 50),
(65, 'ru', 69, 0.1867, 61),
(65, 'ru', 72, 0.1867, 60),
(65, 'ru', 95, 0.1867, 64),
(65, 'ru', 96, 0.1867, 65),
(65, 'ru', 111, 0.1867, 9),
(65, 'ru', 124, 0.1867, 38),
(65, 'ru', 157, 0.1867, 31),
(65, 'ru', 220, 0.1867, 7),
(65, 'ru', 446, 0.1867, 14),
(65, 'ru', 502, 0.1867, 75),
(65, 'ru', 607, 0.1867, 81),
(65, 'ru', 641, 0.1867, 30),
(65, 'ru', 669, 0.1867, 82),
(65, 'ru', 929, 0.1867, 52),
(65, 'ru', 1105, 0.1867, 39),
(65, 'ru', 1212, 0.1867, 62),
(65, 'ru', 1287, 0.1867, 3),
(65, 'ru', 1387, 0.1867, 2),
(65, 'ru', 1388, 0.1867, 4),
(65, 'ru', 1389, 0.1867, 6),
(65, 'ru', 1390, 0.1867, 8),
(65, 'ru', 1391, 0.1867, 12),
(65, 'ru', 1392, 0.1867, 15),
(65, 'ru', 1393, 0.1867, 17),
(65, 'ru', 1394, 0.1867, 23),
(65, 'ru', 1395, 0.1867, 24),
(65, 'ru', 1396, 0.1867, 25),
(65, 'ru', 1397, 0.1867, 27),
(65, 'ru', 1398, 0.1867, 29),
(65, 'ru', 1399, 0.1867, 42),
(65, 'ru', 1400, 0.1867, 43),
(65, 'ru', 1401, 0.1867, 49),
(65, 'ru', 1402, 0.1867, 51),
(65, 'ru', 1403, 0.1867, 72),
(65, 'ru', 1404, 0.1867, 74),
(66, 'ru', 571, 0.2314, 1),
(66, 'ru', 1275, 0.2314, 2),
(66, 'ru', 1276, 0.2314, 3),
(67, 'ru', 1275, 0.2314, 2),
(67, 'ru', 1276, 0.2314, 3),
(67, 'ru', 1277, 0.2314, 1),
(68, 'ru', 4, 0.3509, 50),
(68, 'ru', 37, 0.1754, 16),
(68, 'ru', 39, 0.1754, 34),
(68, 'ru', 53, 0.278, 42.5),
(68, 'ru', 64, 0.1754, 11),
(68, 'ru', 76, 0.1754, 55),
(68, 'ru', 86, 0.1754, 8),
(68, 'ru', 95, 0.1754, 38),
(68, 'ru', 96, 0.1754, 57),
(68, 'ru', 111, 0.1754, 29),
(68, 'ru', 113, 0.278, 39),
(68, 'ru', 157, 0.1754, 73),
(68, 'ru', 497, 0.278, 41),
(68, 'ru', 545, 0.1754, 78),
(68, 'ru', 623, 0.1754, 60),
(68, 'ru', 642, 0.1754, 72),
(68, 'ru', 663, 0.3509, 24.6667),
(68, 'ru', 692, 0.1754, 1),
(68, 'ru', 694, 0.1754, 12),
(68, 'ru', 709, 0.1754, 33),
(68, 'ru', 710, 0.1754, 39),
(68, 'ru', 1231, 0.1754, 82),
(68, 'ru', 1263, 0.1754, 75),
(68, 'ru', 1269, 0.1754, 52),
(68, 'ru', 1278, 0.278, 7),
(68, 'ru', 1279, 0.278, 6),
(68, 'ru', 1280, 0.1754, 14),
(68, 'ru', 1281, 0.1754, 17),
(68, 'ru', 1282, 0.1754, 24),
(68, 'ru', 1283, 0.1754, 25),
(68, 'ru', 1284, 0.1754, 26),
(68, 'ru', 1285, 0.1754, 27),
(68, 'ru', 1286, 0.1754, 37),
(68, 'ru', 1287, 0.1754, 46),
(68, 'ru', 1288, 0.1754, 48),
(68, 'ru', 1289, 0.1754, 51),
(68, 'ru', 1290, 0.1754, 58),
(68, 'ru', 1291, 0.1754, 59),
(68, 'ru', 1292, 0.1754, 69),
(68, 'ru', 1293, 0.1754, 76),
(68, 'ru', 1294, 0.278, 78.5),
(68, 'ru', 1295, 0.1754, 79),
(69, 'ru', 11, 0.208, 23),
(69, 'ru', 86, 0.208, 41),
(69, 'ru', 210, 0.208, 16),
(69, 'ru', 213, 0.208, 30),
(69, 'ru', 337, 0.208, 36),
(69, 'ru', 459, 0.208, 39),
(69, 'ru', 663, 0.3297, 16.5),
(69, 'ru', 712, 0.208, 31),
(69, 'ru', 713, 0.208, 15),
(69, 'ru', 934, 0.208, 5),
(69, 'ru', 1120, 0.208, 14),
(69, 'ru', 1283, 0.208, 24),
(69, 'ru', 1292, 0.208, 17),
(69, 'ru', 1296, 0.3297, 17.5),
(69, 'ru', 1297, 0.416, 25.6667),
(69, 'ru', 1300, 0.208, 4),
(69, 'ru', 1301, 0.208, 13),
(69, 'ru', 1335, 0.208, 7),
(69, 'ru', 1336, 0.208, 19),
(69, 'ru', 1337, 0.208, 21),
(69, 'ru', 1338, 0.208, 26),
(69, 'ru', 1339, 0.208, 27),
(69, 'ru', 1340, 0.208, 28),
(69, 'ru', 1341, 0.208, 38),
(70, 'ru', 72, 0.1867, 48),
(70, 'ru', 76, 0.1867, 25),
(70, 'ru', 81, 0.1867, 29),
(70, 'ru', 86, 0.1867, 46),
(70, 'ru', 111, 0.2958, 26.5),
(70, 'ru', 182, 0.1867, 28),
(70, 'ru', 599, 0.1867, 41),
(70, 'ru', 611, 0.1867, 21),
(70, 'ru', 619, 0.1867, 19),
(70, 'ru', 713, 0.1867, 12),
(70, 'ru', 934, 0.1867, 4),
(70, 'ru', 1120, 0.1867, 11),
(70, 'ru', 1298, 0.2958, 13.5),
(70, 'ru', 1299, 0.2958, 23.5),
(70, 'ru', 1300, 0.1867, 3),
(70, 'ru', 1301, 0.1867, 10),
(70, 'ru', 1302, 0.1867, 14),
(70, 'ru', 1303, 0.1867, 16),
(70, 'ru', 1304, 0.1867, 17),
(70, 'ru', 1305, 0.1867, 18),
(70, 'ru', 1306, 0.1867, 20),
(70, 'ru', 1307, 0.1867, 23),
(70, 'ru', 1308, 0.2958, 29),
(70, 'ru', 1309, 0.1867, 30),
(70, 'ru', 1310, 0.1867, 32),
(70, 'ru', 1311, 0.1867, 33),
(70, 'ru', 1312, 0.1867, 36),
(70, 'ru', 1313, 0.1867, 37),
(70, 'ru', 1314, 0.1867, 37),
(70, 'ru', 1315, 0.1867, 38),
(70, 'ru', 1316, 0.1867, 39),
(70, 'ru', 1317, 0.1867, 40),
(70, 'ru', 1318, 0.1867, 42),
(70, 'ru', 1319, 0.1867, 43),
(70, 'ru', 1320, 0.1867, 44),
(70, 'ru', 1321, 0.1867, 49),
(70, 'ru', 1322, 0.1867, 50),
(71, 'ru', 4, 0.1867, 52),
(71, 'ru', 19, 0.1867, 28),
(71, 'ru', 24, 0.1867, 49),
(71, 'ru', 76, 0.2958, 34),
(71, 'ru', 110, 0.1867, 30),
(71, 'ru', 111, 0.1867, 12),
(71, 'ru', 189, 0.1867, 8),
(71, 'ru', 292, 0.1867, 23),
(71, 'ru', 441, 0.1867, 9),
(71, 'ru', 503, 0.1867, 6),
(71, 'ru', 599, 0.1867, 38),
(71, 'ru', 611, 0.1867, 11),
(71, 'ru', 629, 0.1867, 58),
(71, 'ru', 630, 0.1867, 55),
(71, 'ru', 689, 0.1867, 24),
(71, 'ru', 692, 0.1867, 1),
(71, 'ru', 858, 0.1867, 34),
(71, 'ru', 867, 0.1867, 32),
(71, 'ru', 914, 0.1867, 37),
(71, 'ru', 1093, 0.1867, 36),
(71, 'ru', 1298, 0.3733, 21.3333),
(71, 'ru', 1299, 0.2958, 10.5),
(71, 'ru', 1306, 0.1867, 10),
(71, 'ru', 1307, 0.1867, 13),
(71, 'ru', 1308, 0.1867, 14),
(71, 'ru', 1323, 0.1867, 26),
(71, 'ru', 1324, 0.1867, 33),
(71, 'ru', 1325, 0.1867, 46),
(71, 'ru', 1326, 0.1867, 47),
(71, 'ru', 1327, 0.1867, 48),
(71, 'ru', 1328, 0.1867, 51),
(71, 'ru', 1329, 0.1867, 54),
(71, 'ru', 1330, 0.1867, 56),
(71, 'ru', 1331, 0.1867, 57),
(71, 'ru', 1332, 0.1867, 59),
(71, 'ru', 1333, 0.1867, 60),
(71, 'ru', 1334, 0.1867, 61),
(72, 'ru', 52, 0.2127, 16),
(72, 'ru', 210, 0.2127, 13),
(72, 'ru', 214, 0.2127, 38),
(72, 'ru', 233, 0.2127, 7),
(72, 'ru', 396, 0.2127, 1),
(72, 'ru', 913, 0.2127, 28),
(72, 'ru', 1037, 0.2127, 36),
(72, 'ru', 1308, 0.3372, 17),
(72, 'ru', 1342, 0.2127, 3),
(72, 'ru', 1343, 0.2127, 4),
(72, 'ru', 1344, 0.2127, 8),
(72, 'ru', 1345, 0.2127, 10),
(72, 'ru', 1346, 0.2127, 11),
(72, 'ru', 1347, 0.2127, 14),
(72, 'ru', 1348, 0.2127, 14),
(72, 'ru', 1349, 0.2127, 17),
(72, 'ru', 1350, 0.2127, 18),
(72, 'ru', 1351, 0.2127, 20),
(72, 'ru', 1352, 0.2127, 21),
(72, 'ru', 1353, 0.2127, 23),
(72, 'ru', 1354, 0.2127, 24),
(72, 'ru', 1355, 0.2127, 25),
(72, 'ru', 1356, 0.2127, 27),
(72, 'ru', 1357, 0.2127, 37),
(72, 'ru', 1358, 0.2127, 39),
(73, 'ru', 24, 0.4206, 27),
(73, 'ru', 51, 0.2103, 27),
(73, 'ru', 199, 0.2103, 39),
(73, 'ru', 282, 0.2103, 14),
(73, 'ru', 385, 0.2103, 9),
(73, 'ru', 578, 0.2103, 24),
(73, 'ru', 618, 0.2103, 11),
(73, 'ru', 830, 0.2103, 5),
(73, 'ru', 938, 0.2103, 40),
(73, 'ru', 1308, 0.4883, 17),
(73, 'ru', 1311, 0.2103, 2),
(73, 'ru', 1359, 0.2103, 1),
(73, 'ru', 1360, 0.2103, 6),
(73, 'ru', 1361, 0.2103, 7),
(73, 'ru', 1362, 0.2103, 12),
(73, 'ru', 1363, 0.2103, 25),
(73, 'ru', 1364, 0.2103, 29),
(73, 'ru', 1365, 0.2103, 36),
(73, 'ru', 1366, 0.2103, 37),
(73, 'ru', 1367, 0.2103, 38),
(73, 'ru', 1368, 0.2103, 41),
(73, 'ru', 1369, 0.2103, 42),
(74, 'ru', 4, 0.2, 34),
(74, 'ru', 56, 0.2, 25),
(74, 'ru', 76, 0.2, 8),
(74, 'ru', 111, 0.2, 5),
(74, 'ru', 322, 0.2, 27),
(74, 'ru', 441, 0.2, 10),
(74, 'ru', 459, 0.2, 12),
(74, 'ru', 664, 0.4, 13),
(74, 'ru', 932, 0.2, 3),
(74, 'ru', 1305, 0.2, 33),
(74, 'ru', 1307, 0.2, 6),
(74, 'ru', 1308, 0.2, 7),
(74, 'ru', 1321, 0.2, 15),
(74, 'ru', 1370, 0.2, 1),
(74, 'ru', 1371, 0.2, 4),
(74, 'ru', 1372, 0.2, 11),
(74, 'ru', 1373, 0.2, 14),
(74, 'ru', 1374, 0.2, 16),
(74, 'ru', 1375, 0.2, 17),
(74, 'ru', 1376, 0.2, 18),
(74, 'ru', 1377, 0.2, 26),
(74, 'ru', 1378, 0.2, 29),
(74, 'ru', 1379, 0.2, 30),
(74, 'ru', 1380, 0.2, 31),
(74, 'ru', 1381, 0.2, 35),
(74, 'ru', 1382, 0.2, 36),
(74, 'ru', 1383, 0.2, 37),
(74, 'ru', 1384, 0.2, 38),
(74, 'ru', 1385, 0.2, 39),
(74, 'ru', 1386, 0.2, 39),
(75, 'ru', 24, 0.4362, 23),
(75, 'ru', 51, 0.2181, 23),
(75, 'ru', 188, 0.2181, 1),
(75, 'ru', 199, 0.2181, 35),
(75, 'ru', 282, 0.2181, 10),
(75, 'ru', 385, 0.2181, 5),
(75, 'ru', 578, 0.2181, 20),
(75, 'ru', 618, 0.2181, 7),
(75, 'ru', 938, 0.2181, 36),
(75, 'ru', 1308, 0.5064, 14),
(75, 'ru', 1343, 0.2181, 2),
(75, 'ru', 1362, 0.2181, 8),
(75, 'ru', 1363, 0.2181, 21),
(75, 'ru', 1364, 0.2181, 25),
(75, 'ru', 1365, 0.2181, 32),
(75, 'ru', 1366, 0.2181, 33),
(75, 'ru', 1367, 0.2181, 34),
(75, 'ru', 1368, 0.2181, 37),
(75, 'ru', 1369, 0.2181, 38),
(76, 'ru', 607, 0.2314, 1),
(76, 'ru', 1275, 0.2314, 2),
(76, 'ru', 1276, 0.2314, 3),
(77, 'ru', 607, 0.2314, 1),
(78, 'ru', 669, 0.2314, 1),
(78, 'ru', 1275, 0.2314, 2),
(78, 'ru', 1276, 0.2314, 3),
(79, 'ru', 1308, 0.2314, 1),
(80, 'ru', 1314, 0.2314, 2),
(80, 'ru', 1405, 0.2314, 1),
(81, 'ru', 1406, 0.2314, 1),
(81, 'ru', 1407, 0.2314, 2),
(81, 'ru', 1408, 0.2314, 4),
(82, 'ru', 1409, 0.2314, 1),
(83, 'ru', 4, 0.2018, 22),
(83, 'ru', 12, 0.3199, 25),
(83, 'ru', 17, 0.2018, 28),
(83, 'ru', 39, 0.2018, 24),
(83, 'ru', 57, 0.2018, 23),
(83, 'ru', 132, 0.2018, 20),
(83, 'ru', 229, 0.2018, 48),
(83, 'ru', 303, 0.2018, 14),
(83, 'ru', 410, 0.2018, 41),
(83, 'ru', 411, 0.2018, 44),
(83, 'ru', 499, 0.2018, 6),
(83, 'ru', 502, 0.2018, 39),
(83, 'ru', 1044, 0.2018, 30),
(83, 'ru', 1231, 0.2018, 33),
(83, 'ru', 1294, 0.2018, 25),
(83, 'ru', 1308, 0.2018, 31),
(83, 'ru', 1387, 0.2018, 2),
(83, 'ru', 1395, 0.2018, 7),
(83, 'ru', 1396, 0.2018, 1),
(83, 'ru', 1397, 0.2018, 11),
(83, 'ru', 1410, 0.2018, 3),
(83, 'ru', 1411, 0.2018, 4),
(83, 'ru', 1412, 0.2018, 5),
(83, 'ru', 1413, 0.3199, 17.5),
(83, 'ru', 1414, 0.2018, 13),
(83, 'ru', 1415, 0.2018, 21),
(83, 'ru', 1416, 0.2018, 29),
(83, 'ru', 1417, 0.2018, 40),
(83, 'ru', 1418, 0.2018, 46),
(84, 'ru', 4, 0.2038, 21),
(84, 'ru', 12, 0.323, 24),
(84, 'ru', 17, 0.2038, 27),
(84, 'ru', 38, 0.2038, 1),
(84, 'ru', 39, 0.2038, 23),
(84, 'ru', 57, 0.2038, 22),
(84, 'ru', 132, 0.2038, 19),
(84, 'ru', 229, 0.2038, 47),
(84, 'ru', 303, 0.2038, 13),
(84, 'ru', 410, 0.2038, 40),
(84, 'ru', 411, 0.2038, 43),
(84, 'ru', 499, 0.2038, 5),
(84, 'ru', 502, 0.2038, 38),
(84, 'ru', 1044, 0.2038, 29),
(84, 'ru', 1231, 0.2038, 32),
(84, 'ru', 1265, 0.2038, 2),
(84, 'ru', 1294, 0.2038, 24),
(84, 'ru', 1308, 0.2038, 30),
(84, 'ru', 1395, 0.2038, 6),
(84, 'ru', 1397, 0.2038, 10),
(84, 'ru', 1413, 0.323, 16.5),
(84, 'ru', 1414, 0.2038, 12),
(84, 'ru', 1415, 0.2038, 20),
(84, 'ru', 1416, 0.2038, 28),
(84, 'ru', 1417, 0.2038, 39),
(84, 'ru', 1418, 0.2038, 45),
(84, 'ru', 1419, 0.2038, 3),
(84, 'ru', 1420, 0.2038, 4),
(85, 'ru', 4, 0.2058, 20),
(85, 'ru', 12, 0.3263, 23),
(85, 'ru', 17, 0.2058, 26),
(85, 'ru', 39, 0.2058, 22),
(85, 'ru', 57, 0.2058, 21),
(85, 'ru', 132, 0.2058, 18),
(85, 'ru', 229, 0.2058, 46),
(85, 'ru', 303, 0.2058, 12),
(85, 'ru', 410, 0.2058, 39),
(85, 'ru', 411, 0.2058, 42),
(85, 'ru', 499, 0.2058, 4),
(85, 'ru', 502, 0.2058, 37),
(85, 'ru', 1044, 0.2058, 28),
(85, 'ru', 1231, 0.2058, 31),
(85, 'ru', 1294, 0.2058, 23),
(85, 'ru', 1308, 0.2058, 29),
(85, 'ru', 1395, 0.2058, 5),
(85, 'ru', 1396, 0.2058, 1),
(85, 'ru', 1397, 0.2058, 9),
(85, 'ru', 1413, 0.3263, 15.5),
(85, 'ru', 1414, 0.2058, 11),
(85, 'ru', 1415, 0.2058, 19),
(85, 'ru', 1416, 0.2058, 27),
(85, 'ru', 1417, 0.2058, 38),
(85, 'ru', 1418, 0.2058, 44),
(85, 'ru', 1421, 0.2058, 2),
(85, 'ru', 1422, 0.2058, 3),
(86, 'ru', 1423, 0.2314, 1),
(86, 'ru', 1424, 0.2314, 4),
(86, 'ru', 1425, 0.2314, 4),
(87, 'ru', 51, 0.2314, 2),
(87, 'ru', 1423, 0.2314, 1),
(87, 'ru', 1424, 0.2314, 4),
(87, 'ru', 1425, 0.2314, 4),
(88, 'ru', 1423, 0.2314, 3),
(88, 'ru', 1426, 0.2314, 1),
(88, 'ru', 1427, 0.2314, 2),
(89, 'ru', 1428, 0.2314, 1),
(89, 'ru', 1429, 0.2314, 2),
(89, 'ru', 1430, 0.2314, 3),
(89, 'ru', 1431, 0.2314, 4),
(90, 'ru', 1432, 0.2314, 2),
(90, 'ru', 1433, 0.2314, 3),
(91, 'ru', 12, 0.1466, 9),
(91, 'ru', 46, 0.1466, 114),
(91, 'ru', 56, 0.1466, 154),
(91, 'ru', 58, 0.3404, 142),
(91, 'ru', 61, 0.1466, 13),
(91, 'ru', 112, 0.1466, 145),
(91, 'ru', 157, 0.1466, 96),
(91, 'ru', 159, 0.1466, 119),
(91, 'ru', 216, 0.2324, 61.5),
(91, 'ru', 225, 0.1466, 41),
(91, 'ru', 232, 0.1466, 146),
(91, 'ru', 302, 0.1466, 47),
(91, 'ru', 374, 0.1466, 102),
(91, 'ru', 375, 0.1466, 134),
(91, 'ru', 502, 0.1466, 55),
(91, 'ru', 545, 0.1466, 79),
(91, 'ru', 617, 0.1466, 91),
(91, 'ru', 637, 0.1466, 101),
(91, 'ru', 641, 0.2324, 150),
(91, 'ru', 686, 0.1466, 144),
(91, 'ru', 737, 0.2324, 35.5),
(91, 'ru', 756, 0.1466, 147),
(91, 'ru', 839, 0.1466, 49),
(91, 'ru', 840, 0.1466, 50),
(91, 'ru', 872, 0.1466, 107),
(91, 'ru', 901, 0.1466, 143),
(91, 'ru', 903, 0.1466, 62),
(91, 'ru', 929, 0.1466, 137),
(91, 'ru', 1088, 0.1466, 157),
(91, 'ru', 1125, 0.2324, 144.5),
(91, 'ru', 1162, 0.1466, 31),
(91, 'ru', 1231, 0.1466, 83),
(91, 'ru', 1263, 0.1466, 152),
(91, 'ru', 1265, 0.1466, 66),
(91, 'ru', 1294, 0.1466, 12),
(91, 'ru', 1405, 0.1466, 77),
(91, 'ru', 1425, 0.1466, 25),
(91, 'ru', 1434, 0.1466, 1),
(91, 'ru', 1435, 0.1466, 2),
(91, 'ru', 1436, 0.1466, 3),
(91, 'ru', 1437, 0.2932, 97.3333),
(91, 'ru', 1438, 0.1466, 5),
(91, 'ru', 1439, 0.1466, 6),
(91, 'ru', 1440, 0.1466, 10),
(91, 'ru', 1441, 0.1466, 11),
(91, 'ru', 1442, 0.1466, 27),
(91, 'ru', 1443, 0.1466, 28),
(91, 'ru', 1444, 0.1466, 29),
(91, 'ru', 1445, 0.1466, 32),
(91, 'ru', 1446, 0.1466, 33),
(91, 'ru', 1447, 0.2324, 77),
(91, 'ru', 1448, 0.1466, 42),
(91, 'ru', 1449, 0.2324, 48),
(91, 'ru', 1450, 0.1466, 44),
(91, 'ru', 1451, 0.1466, 46),
(91, 'ru', 1452, 0.1466, 48),
(91, 'ru', 1453, 0.1466, 52),
(91, 'ru', 1454, 0.1466, 54),
(91, 'ru', 1455, 0.2324, 75.5),
(91, 'ru', 1456, 0.1466, 63),
(91, 'ru', 1457, 0.1466, 68),
(91, 'ru', 1458, 0.1466, 69),
(91, 'ru', 1459, 0.1466, 70),
(91, 'ru', 1460, 0.1466, 73),
(91, 'ru', 1461, 0.1466, 78),
(91, 'ru', 1462, 0.1466, 81),
(91, 'ru', 1463, 0.1466, 82),
(91, 'ru', 1464, 0.1466, 89),
(91, 'ru', 1465, 0.1466, 93),
(91, 'ru', 1466, 0.1466, 94),
(91, 'ru', 1467, 0.1466, 98),
(91, 'ru', 1468, 0.1466, 99),
(91, 'ru', 1469, 0.1466, 99),
(91, 'ru', 1470, 0.1466, 103),
(91, 'ru', 1471, 0.1466, 105),
(91, 'ru', 1472, 0.1466, 108),
(91, 'ru', 1473, 0.1466, 111),
(91, 'ru', 1474, 0.1466, 113),
(91, 'ru', 1475, 0.1466, 116),
(91, 'ru', 1476, 0.1466, 117),
(91, 'ru', 1477, 0.1466, 122),
(91, 'ru', 1478, 0.1466, 123),
(91, 'ru', 1479, 0.1466, 125),
(91, 'ru', 1480, 0.1466, 129),
(91, 'ru', 1481, 0.1466, 130),
(91, 'ru', 1482, 0.1466, 133),
(91, 'ru', 1483, 0.1466, 135),
(91, 'ru', 1484, 0.1466, 136),
(91, 'ru', 1485, 0.1466, 148),
(91, 'ru', 1486, 0.1466, 149),
(91, 'ru', 1487, 0.1466, 150),
(91, 'ru', 1488, 0.1466, 151),
(91, 'ru', 1489, 0.1466, 151),
(91, 'ru', 1490, 0.1466, 159),
(91, 'ru', 1491, 0.1466, 161),
(91, 'ru', 1492, 0.1466, 168),
(91, 'ru', 1493, 0.1466, 169),
(91, 'ru', 1494, 0.1466, 170),
(91, 'ru', 1495, 0.1466, 172),
(91, 'ru', 1496, 0.1466, 174),
(91, 'ru', 1497, 0.1466, 175),
(92, 'ru', 12, 0.1466, 9),
(92, 'ru', 46, 0.1466, 114),
(92, 'ru', 56, 0.1466, 154),
(92, 'ru', 58, 0.3404, 142),
(92, 'ru', 61, 0.1466, 13),
(92, 'ru', 112, 0.1466, 145),
(92, 'ru', 157, 0.1466, 96),
(92, 'ru', 159, 0.1466, 119),
(92, 'ru', 216, 0.2324, 61.5),
(92, 'ru', 225, 0.1466, 41),
(92, 'ru', 232, 0.1466, 146),
(92, 'ru', 302, 0.1466, 47),
(92, 'ru', 374, 0.1466, 102),
(92, 'ru', 375, 0.1466, 134),
(92, 'ru', 502, 0.1466, 55),
(92, 'ru', 545, 0.1466, 79),
(92, 'ru', 617, 0.1466, 91),
(92, 'ru', 637, 0.1466, 101),
(92, 'ru', 641, 0.2324, 150),
(92, 'ru', 686, 0.1466, 144),
(92, 'ru', 737, 0.2324, 35.5),
(92, 'ru', 756, 0.1466, 147),
(92, 'ru', 839, 0.1466, 49),
(92, 'ru', 840, 0.1466, 50),
(92, 'ru', 872, 0.1466, 107),
(92, 'ru', 901, 0.1466, 143),
(92, 'ru', 903, 0.1466, 62),
(92, 'ru', 929, 0.1466, 137),
(92, 'ru', 1088, 0.1466, 157),
(92, 'ru', 1125, 0.2324, 144.5),
(92, 'ru', 1162, 0.1466, 31),
(92, 'ru', 1231, 0.1466, 83),
(92, 'ru', 1263, 0.1466, 152),
(92, 'ru', 1265, 0.1466, 66),
(92, 'ru', 1294, 0.1466, 12),
(92, 'ru', 1405, 0.1466, 77),
(92, 'ru', 1425, 0.1466, 25),
(92, 'ru', 1435, 0.1466, 2),
(92, 'ru', 1436, 0.1466, 3),
(92, 'ru', 1437, 0.2932, 97.3333),
(92, 'ru', 1438, 0.1466, 5),
(92, 'ru', 1439, 0.1466, 6),
(92, 'ru', 1440, 0.1466, 10),
(92, 'ru', 1441, 0.1466, 11),
(92, 'ru', 1442, 0.1466, 27),
(92, 'ru', 1443, 0.1466, 28),
(92, 'ru', 1444, 0.1466, 29),
(92, 'ru', 1445, 0.1466, 32),
(92, 'ru', 1446, 0.1466, 33),
(92, 'ru', 1447, 0.2324, 77),
(92, 'ru', 1448, 0.1466, 42),
(92, 'ru', 1449, 0.2324, 48),
(92, 'ru', 1450, 0.1466, 44),
(92, 'ru', 1451, 0.1466, 46),
(92, 'ru', 1452, 0.1466, 48),
(92, 'ru', 1453, 0.1466, 52),
(92, 'ru', 1454, 0.1466, 54),
(92, 'ru', 1455, 0.2324, 75.5),
(92, 'ru', 1456, 0.1466, 63),
(92, 'ru', 1457, 0.1466, 68),
(92, 'ru', 1458, 0.1466, 69),
(92, 'ru', 1459, 0.1466, 70),
(92, 'ru', 1460, 0.1466, 73),
(92, 'ru', 1461, 0.1466, 78),
(92, 'ru', 1462, 0.1466, 81),
(92, 'ru', 1463, 0.1466, 82),
(92, 'ru', 1464, 0.1466, 89),
(92, 'ru', 1465, 0.1466, 93),
(92, 'ru', 1466, 0.1466, 94),
(92, 'ru', 1467, 0.1466, 98),
(92, 'ru', 1468, 0.1466, 99),
(92, 'ru', 1469, 0.1466, 99),
(92, 'ru', 1470, 0.1466, 103),
(92, 'ru', 1471, 0.1466, 105),
(92, 'ru', 1472, 0.1466, 108),
(92, 'ru', 1473, 0.1466, 111),
(92, 'ru', 1474, 0.1466, 113),
(92, 'ru', 1475, 0.1466, 116),
(92, 'ru', 1476, 0.1466, 117),
(92, 'ru', 1477, 0.1466, 122),
(92, 'ru', 1478, 0.1466, 123),
(92, 'ru', 1479, 0.1466, 125),
(92, 'ru', 1480, 0.1466, 129),
(92, 'ru', 1481, 0.1466, 130),
(92, 'ru', 1482, 0.1466, 133),
(92, 'ru', 1483, 0.1466, 135),
(92, 'ru', 1484, 0.1466, 136),
(92, 'ru', 1485, 0.1466, 148),
(92, 'ru', 1486, 0.1466, 149),
(92, 'ru', 1487, 0.1466, 150),
(92, 'ru', 1488, 0.1466, 151),
(92, 'ru', 1489, 0.1466, 151),
(92, 'ru', 1490, 0.1466, 159),
(92, 'ru', 1491, 0.1466, 161),
(92, 'ru', 1492, 0.1466, 168),
(92, 'ru', 1493, 0.1466, 169),
(92, 'ru', 1494, 0.1466, 170),
(92, 'ru', 1495, 0.1466, 172),
(92, 'ru', 1496, 0.1466, 174),
(92, 'ru', 1497, 0.1466, 175),
(92, 'ru', 1498, 0.1466, 1),
(93, 'ru', 12, 0.1466, 9),
(93, 'ru', 46, 0.1466, 114),
(93, 'ru', 56, 0.1466, 154),
(93, 'ru', 58, 0.3404, 142),
(93, 'ru', 61, 0.1466, 13),
(93, 'ru', 112, 0.1466, 145),
(93, 'ru', 157, 0.1466, 96),
(93, 'ru', 159, 0.1466, 119),
(93, 'ru', 216, 0.2324, 61.5),
(93, 'ru', 225, 0.1466, 41),
(93, 'ru', 232, 0.1466, 146),
(93, 'ru', 302, 0.1466, 47),
(93, 'ru', 374, 0.1466, 102),
(93, 'ru', 375, 0.1466, 134),
(93, 'ru', 502, 0.1466, 55),
(93, 'ru', 545, 0.1466, 79),
(93, 'ru', 617, 0.1466, 91),
(93, 'ru', 637, 0.1466, 101),
(93, 'ru', 641, 0.2324, 150),
(93, 'ru', 686, 0.1466, 144),
(93, 'ru', 737, 0.2324, 35.5),
(93, 'ru', 756, 0.1466, 147),
(93, 'ru', 839, 0.1466, 49),
(93, 'ru', 840, 0.1466, 50),
(93, 'ru', 872, 0.1466, 107),
(93, 'ru', 901, 0.1466, 143),
(93, 'ru', 903, 0.1466, 62),
(93, 'ru', 929, 0.1466, 137),
(93, 'ru', 1088, 0.1466, 157),
(93, 'ru', 1125, 0.2324, 144.5),
(93, 'ru', 1162, 0.1466, 31),
(93, 'ru', 1231, 0.1466, 83),
(93, 'ru', 1263, 0.1466, 152),
(93, 'ru', 1265, 0.1466, 66),
(93, 'ru', 1294, 0.1466, 12),
(93, 'ru', 1405, 0.1466, 77),
(93, 'ru', 1425, 0.1466, 25),
(93, 'ru', 1435, 0.1466, 2),
(93, 'ru', 1436, 0.1466, 3),
(93, 'ru', 1437, 0.2932, 97.3333),
(93, 'ru', 1438, 0.1466, 5),
(93, 'ru', 1439, 0.1466, 6),
(93, 'ru', 1440, 0.1466, 10),
(93, 'ru', 1441, 0.1466, 11),
(93, 'ru', 1442, 0.1466, 27),
(93, 'ru', 1443, 0.1466, 28),
(93, 'ru', 1444, 0.1466, 29),
(93, 'ru', 1445, 0.1466, 32),
(93, 'ru', 1446, 0.1466, 33),
(93, 'ru', 1447, 0.2324, 77),
(93, 'ru', 1448, 0.1466, 42),
(93, 'ru', 1449, 0.2324, 48),
(93, 'ru', 1450, 0.1466, 44),
(93, 'ru', 1451, 0.1466, 46),
(93, 'ru', 1452, 0.1466, 48),
(93, 'ru', 1453, 0.1466, 52),
(93, 'ru', 1454, 0.1466, 54),
(93, 'ru', 1455, 0.2324, 75.5),
(93, 'ru', 1456, 0.1466, 63),
(93, 'ru', 1457, 0.1466, 68),
(93, 'ru', 1458, 0.1466, 69),
(93, 'ru', 1459, 0.1466, 70),
(93, 'ru', 1460, 0.1466, 73),
(93, 'ru', 1461, 0.1466, 78),
(93, 'ru', 1462, 0.1466, 81),
(93, 'ru', 1463, 0.1466, 82),
(93, 'ru', 1464, 0.1466, 89),
(93, 'ru', 1465, 0.1466, 93),
(93, 'ru', 1466, 0.1466, 94),
(93, 'ru', 1467, 0.1466, 98),
(93, 'ru', 1468, 0.1466, 99),
(93, 'ru', 1469, 0.1466, 99),
(93, 'ru', 1470, 0.1466, 103),
(93, 'ru', 1471, 0.1466, 105),
(93, 'ru', 1472, 0.1466, 108),
(93, 'ru', 1473, 0.1466, 111),
(93, 'ru', 1474, 0.1466, 113),
(93, 'ru', 1475, 0.1466, 116),
(93, 'ru', 1476, 0.1466, 117),
(93, 'ru', 1477, 0.1466, 122),
(93, 'ru', 1478, 0.1466, 123),
(93, 'ru', 1479, 0.1466, 125),
(93, 'ru', 1480, 0.1466, 129),
(93, 'ru', 1481, 0.1466, 130),
(93, 'ru', 1482, 0.1466, 133),
(93, 'ru', 1483, 0.1466, 135),
(93, 'ru', 1484, 0.1466, 136),
(93, 'ru', 1485, 0.1466, 148),
(93, 'ru', 1486, 0.1466, 149),
(93, 'ru', 1487, 0.1466, 150),
(93, 'ru', 1488, 0.1466, 151),
(93, 'ru', 1489, 0.1466, 151),
(93, 'ru', 1490, 0.1466, 159),
(93, 'ru', 1491, 0.1466, 161),
(93, 'ru', 1492, 0.1466, 168),
(93, 'ru', 1493, 0.1466, 169),
(93, 'ru', 1494, 0.1466, 170),
(93, 'ru', 1495, 0.1466, 172),
(93, 'ru', 1496, 0.1466, 174),
(93, 'ru', 1497, 0.1466, 175),
(93, 'ru', 1499, 0.1466, 1),
(94, 'ru', 1263, 0.2314, 2),
(94, 'ru', 1435, 0.2314, 1),
(95, 'ru', 1500, 0.2314, 1),
(95, 'ru', 1501, 0.2314, 2);

-- --------------------------------------------------------

--
-- Table structure for table `b_search_content_text`
--

CREATE TABLE IF NOT EXISTS `b_search_content_text` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `SEARCH_CONTENT_MD5` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `SEARCHABLE_CONTENT` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`SEARCH_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_search_content_text`
--

INSERT INTO `b_search_content_text` (`SEARCH_CONTENT_ID`, `SEARCH_CONTENT_MD5`, `SEARCHABLE_CONTENT`) VALUES
(1, '55171aa121806afa21b564e0af9ed46f', 'ИСТОРИЯ\r\n1992Г. 								\rМЕБЕЛЬНАЯ КОМПАНИЯ\rНАЧИНАЛАСЬ С ИЗГОТОВЛЕНИЯ МЕБЕЛИ ДЛЯ ШКОЛ И ОФИСОВ. ПЕРВОЕ 									ПРОИЗВОДСТВО МЕБЕЛИ РАСПОЛАГАЛОСЬ В АРЕНДУЕМОЙ МАСТЕРСКОЙ, ПЛОЩАДЬЮ 400 М2 С ОДНИМ 									ДЕРЕВООБРАБАТЫВАЮЩИМ СТАНКОМ. В КОМПАНИИ РАБОТАЛО ДВАДЦАТЬ ЧЕЛОВЕК. ВСЕ ЗАРАБОТАННЫЕ 									СРЕДСТВА ВКЛАДЫВАЛИСЬ В РАЗВИТИЕ, ЧТО ПОЗВОЛИЛО МОЛОДОЙ КОМПАНИИ РАСТИ БЫСТРЫМИ 									ТЕМПАМИ. 								\r1993Г. 								\rВВЕДЕН В ЭКСПЛУАТАЦИЮ НОВЫЙ ЦЕХ ПЛОЩАДЬЮ 700 М2, СТАВШИЙ ПЕРВОЙ СОБСТВЕННОСТЬЮ 									\rМЕБЕЛЬНОЙ КОМПАНИИ\r. МОДЕРНИЗАЦИЯ ПРОИЗВОДСТВЕННОЙ БАЗЫ ПРЕДПРИЯТИЯ СТАЛА ХОРОШЕЙ 									ТРАДИЦИЕЙ. КОМПАНИЯ ПОСТЕПЕННО ПЕРЕШЛА К БОЛЕЕ СОВЕРШЕННОМУ ОБОРУДОВАНИЮ, ЧТО ПОЗВОЛИЛО 									ПОВЫСИТЬ УРОВЕНЬ КАЧЕСТВА ВЫПУСКАЕМОЙ ПРОДУКЦИИ И ЗНАЧИТЕЛЬНО УВЕЛИЧИТЬ ОБЪЕМЫ ПРОИЗВОДСТВА. 								\r1998Г. 								\rВ ВОРОНЕЖЕ ОТКРЫТ ПЕРВЫЙ ФИРМЕННЫЙ МАГАЗИН-САЛОН \rМЕБЕЛЬНАЯ КОМПАНИЯ\r. ШАГ ЗА ШАГОМ 									ПРОДУКЦИЯ ПРЕДПРИЯТИЯ ЗАВОЕВЫВАЛА РЕГИОНЫ СИБИРИ И УРАЛА, МОСКВЫ И ПОДМОСКОВЬЯ, 									ЮГА И СЕВЕРО-ЗАПАДА РОССИИ. ПРОИЗВОДСТВЕННЫЕ ПЛОЩАДИ КОМПАНИИ УВЕЛИЧЕНЫ ДО 5000 									М2. 								\r1999Г. 								\rМЕБЕЛЬНАЯ КОМПАНИЯ\rСТАЛА ДИПЛОМАНТОМ ОДНОЙ ИЗ САМЫХ ПРЕСТИЖНЫХ МЕЖДУНАРОДНЫХ 									ВЫСТАВОК \rЕВРОЭКСПОМЕБЕЛЬ-99\r- ПЕРВОЕ ОФИЦИАЛЬНОЕ ПРИЗНАНИЕ МЕБЕЛЬНОЙ ПРОДУКЦИИ 									МЕБЕЛЬНОЙ КОМПАНИИ. В ЭТОМ ЖЕ ГОДУ КОМПАНИЯ ВЫХОДИТ НА МИРОВОЙ РЫНОК. БЫЛ ЗАКЛЮЧЕН 									РЯД КОНТРАКТОВ НА ПОСТАВКУ МЕБЕЛИ В СТРАНЫ СНГ И БЛИЖНЕГО ЗАРУБЕЖЬЯ. 								\r2000Г. 								\rКОЛЛЕКТИВ КОМПАНИИ НАСЧИТЫВАЕТ БОЛЕЕ 500 СОТРУДНИКОВ. ЗАКЛЮЧАЮТСЯ НОВЫЕ КОНТРАКТ 									НА ПОСТАВКУ МЕБЕЛИ В ЕВРОПЕЙСКИЕ СТРАНЫ. 								\r2002Г. 								\rМЕБЕЛЬНАЯ КОМПАНИЯ\rВОШЛА В ДЕСЯТКУ ЛУЧШИХ ПРОИЗВОДИТЕЛЕЙ МЕБЕЛИ ПО ДАННЫМ ВЕДУЩИХ 									МЕБЕЛЬНЫХ САЛОНОВ РОССИИ, А ТАКЖЕ В ЧИСЛО ЛИДЕРОВ ОРГАНИЗАЦИОННОГО РАЗВИТИЯ. 								\r2003Г. 								\rПРИСТУПИЛИ К СТРОИТЕЛЬСТВУ СКЛАДА МАТЕРИАЛОВ. В МОСКВЕ ОТКРЫТ ФИЛИАЛ КОМПАНИИ. \nПРОВЕДЕНА ПЕРВАЯ КОНФЕРЕНЦИЯ ПАРТНЕРОВ, РЕЗУЛЬТАТОМ КОТОРОЙ СТАЛО УКРЕПЛЕНИЕ ВЗАИМОВЫГОДНЫХ 									ОТНОШЕНИЙ И ЗАКЛЮЧЕНИЕ ДИЛЕРСКИХ ДОГОВОРОВ. 								\r2004Г. 								\rЗАВЕРШЕНО СТРОИТЕЛЬСТВО И ОСНАЩЕНИЕ НОВОГО ПРОИЗВОДСТВЕННОГО КОРПУСА И СКЛАДА МАТЕРИАЛОВ. \nРАСШИРЕНО ПРЕДСТАВИТЕЛЬСТВО КОМПАНИИ НА РОССИЙСКОМ РЫНКЕ И ЗА РУБЕЖОМ. \nОТКРЫТО 									РЕГИОНАЛЬНОЕ ПРЕДСТАВИТЕЛЬСТВО \rМЕБЕЛЬНОЙ КОМПАНИИ\rВ ЕКАТЕРИНБУРГЕ. 								\r2005Г. 								\rКОМПАНИЯ ПРИОБРЕТАЕТ НОВОЕ ПРОИЗВОДСТВЕННОЕ ОБОРУДОВАНИЕ КОНЦЕРНА - УГЛОВУЮ ЛИНИЮ 									РАСКРОЯ МАТЕРИАЛОВ И ЛИНИЮ ЗАГРУЗКИ ВЫГРУЗКИ. \nНАЧИНАЕТСЯ ВЫПУСК ПРОДУКЦИИ В 									НАТУРАЛЬНОМ ШПОНЕ. ФОРМИРУЕТСЯ ОТДЕЛЬНЫЙ СКЛАД МАТЕРИАЛОВ И КОМПЛЕКТУЮЩИХ. \nВ ЭТОМ ЖЕ ГОДУ ОТКРЫВАЕТСЯ ФАБРИКА МЯГКОЙ МЕБЕЛИ \rМЕБЕЛЬПЛЮС\r2006Г. 								\rОТКРЫТЫ РЕГИОНАЛЬНЫЕ ПРЕДСТАВИТЕЛЬСТВА \rМЕБЕЛЬНОЙ КОМПАНИИ\rВ САНКТ-ПЕТЕРБУРГЕ 									И НИЖНЕМ НОВГОРОДЕ. \nРАЗВИВАЕТСЯ СОБСТВЕННАЯ РОЗНИЧНАЯ СЕТЬ ФИРМЕННЫХ МАГАЗИНОВ-САЛОНОВ 									НА ТЕРРИТОРИИ РОССИИ. 								\r2007Г. 								\rЗАВЕРШЕНО СТРОИТЕЛЬСТВО ВТОРОЙ ФАБРИКИ. ОБЩАЯ ПЛОЩАДЬ ПРОИЗВОДСВЕННО-СКЛАДСКИХ КОРПУСОВ 									КОМПАНИИ СОСТАВЛЯЕТ УЖЕ БОЛЕЕ 30000 М2. \nПРОВЕДЕНА ВТОРАЯ КОНФЕРЕНЦИЯ ПАРТНЕРОВ 									КОМПАНИИ \r"ТЕХНОЛОГИЯ УСПЕШНОГО БИЗНЕСА"\r. 								\r2008Г. 								\rОТКРЫТО НОВОЕ ПРЕДПРИЯТИЕ ДЛЯ ПРОИЗВОДСТВА МЕБЕЛИ ПО ИНДИВИДУАЛЬНЫМ ПРОЕКТАМ \rКОМФОРТ\r. \nМЕБЕЛЬНАЯ КОМПАНИЯ\rПРОДОЛЖАЕТ ОБНОВЛЕНИЕ ОБОРУДОВАНИЯ. 								\r2008Г. 								\rНОВЕЙШИМ ОБОРУДОВАНИЕМ УКОМПЛЕКТОВАНА ВТОРАЯ ФАБРИКА. ЗАПУЩЕНА ВТОРАЯ ПРОИЗВОДСТВЕННАЯ 									ЛИНИЯ. \nПРОВЕДЕНА ТРЕТЬЯ КОНФЕРЕНЦИЯ \rПАРТНЕРСТВО - БИЗНЕС СЕГОДНЯ\rПРИНЯТО РЕШЕНИЕ О СТРОИТЕЛЬСТВЕ ТРЕТЬЕЙ ФАБРИКИ. ПЛОЩАДЬ ПРОИЗВОДСТВЕННО  СКЛАДСКИХ 									КОРПУСОВ СОСТАВИТ БОЛЕЕ 70000М2. \nПО ВСЕЙ СТРАНЕ И ЗАРУБЕЖОМ ОТКРЫТО 37 МЕБЕЛЬНЫХ 									САЛОНОВ. \nВ КОМПАНИИ РАБОТАЕТ БОЛЕЕ ПОЛУТОРА ТЫСЯЧ СОТРУДНИКОВ.\r\n'),
(2, 'b535d9cb75c64e695d6ebad33b22d99c', 'О КОМПАНИИ\r\nНАША КОМПАНИЯ СУЩЕСТВУЕТ НА РОССИЙСКОМ РЫНКЕ С 1992 ГОДА. ЗА ЭТО ВРЕМЯ \rМЕБЕЛЬНАЯ КОМПАНИЯ\rПРОШЛА БОЛЬШОЙ ПУТЬ ОТ МАЛЕНЬКОЙ ТОРГОВОЙ ФИРМЫ ДО ОДНОГО ИЗ КРУПНЕЙШИХ ПРОИЗВОДИТЕЛЕЙ КОРПУСНОЙ МЕБЕЛИ В РОССИИ. 						 \rГЛАВНОЕ ПРАВИЛО - ИНДИВИДУАЛЬНЫЙ ПОДХОД К КАЖДОМУ КЛИЕНТУ\rНА СЕГОДНЯШНИЙ ДЕНЬ НАМИ РАЗРАБОТАНО БОЛЕЕ ПЯТИСОТ МОДЕЛЕЙ ДЛЯ ОФИСА И ДОМА. ВМЕСТЕ С ТЕМ МЫ СТРЕМИМСЯ ОБЕСПЕЧИТЬ НЕПОВТОРИМОСТЬ СВОЕЙ ПРОДУКЦИИ. МЫ ИЗГОТОВИМ МЕБЕЛЬ ДЛЯ КУХНИ, ДЕТСКОЙ, ГОСТИНОЙ, СПАЛЬНОЙ ИЛИ ВАННОЙ КОМНАТЫ, МЕБЕЛЬ ДЛЯ ОФИСА ОСОБОГО ДИЗАЙНА И НЕСТАНДАРТНОГО РАЗМЕРА. \rНАШИ ДИЗАЙНЕРЫ ПРОИЗВЕДУТ ЗАМЕРЫ ПОМЕЩЕНИЯ И ВМЕСТЕ С ВАМИ РАЗРАБОТАЮТ ДИЗАЙН-ПРОЕКТ МЕБЕЛИ ДЛЯ ВАШЕГО ИНТЕРЬЕРА, ПОДОБРАВ С ВЫСОКОЙ ТОЧНОСТЬЮ РАЗМЕРЫ, МОДЕЛИ, ЦВЕТА, ПОМОГУТ ОПТИМАЛЬНО РАСПОЛОЖИТЬ МЕБЕЛЬ. \rВАШ ПРОЕКТ БУДЕТ СОЗДАН С УЧЕТОМ ВСЕХ НЮАНСОВ И ПРОРИСОВКОЙ МЕЛЬЧАЙШИХ ДЕТАЛЕЙ. РЕЗУЛЬТАТЫ СОВМЕСТНОГО ТВОРЧЕСТВА ВЫ СМОЖЕТЕ ПОСМОТРЕТЬ В ОБЪЕМНОМ ПРЕДСТАВЛЕНИИ. ВАМ НАГЛЯДНО ПРОДЕМОНСТРИРУЮТ, КАК БУДУТ ВЫГЛЯДЕТЬ В ЖИЗНИ ВЫБРАННЫЕ ЭЛЕМЕНТЫ ИНТЕРЬЕРА ПРИ РАЗНОМ ОСВЕЩЕНИИ, В КОНКРЕТНОМ ПОМЕЩЕНИИ, СДЕЛАННЫЕ ИЗ ОПРЕДЕЛЕННЫХ МАТЕРИАЛОВ. В ВАШЕ РАСПОРЯЖЕНИЕ БУДЕТ ПРЕДОСТАВЛЕНО МНОГО РАЗЛИЧНЫХ ВАРИАНТОВ, ИЗ КОТОРЫХ ВЫ СМОЖЕТЕ ВЫБРАТЬ НАИБОЛЕЕ ПОДХОДЯЩИЙ ИМЕННО ВАМ. 						 					\rЗАКАЗ БУДЕТ ВЫПОЛНЕН И ДОСТАВЛЕН ТОЧНО В СРОК. ВСЕ РАБОТЫ ПО СБОРКЕ И УСТАНОВКЕ МЕБЕЛИ ОСУЩЕСТВЛЯЮТ СОТРУДНИКИ НАШЕЙ КОМПАНИИ. СТРОГИЙ КОНТРОЛЬ КАЧЕСТВА ОСУЩЕСТВЛЯЕТСЯ НА ВСЕХ ЭТАПАХ РАБОТ: ОТ МОМЕНТА ОФОРМЛЕНИЯ ЗАКАЗА ДО МОМЕНТА ПРИЕМА ВЫПОЛНЕННЫХ РАБОТ. \rПЕРЕДОВЫЕ ТЕХНОЛОГИИ И БЕСЦЕННЫЙ ОПЫТ\rИСПОЛЬЗОВАНИЕ ПЕРЕДОВЫХ КОМПЬЮТЕРНЫХ ТЕХНОЛОГИЙ, МНОГОЛЕТНИЙ ОПЫТ НАШИХ СПЕЦИАЛИСТОВ ПОЗВОЛЯЮТ ПРОИЗВЕСТИ ГРАМОТНЫЕ РАСЧЕТЫ И СНИЗИТЬ РАСХОД МАТЕРИАЛОВ И СЕБЕСТОИМОСТЬ ПРОДУКЦИИ, ИЗБЕЖАТЬ ОШИБОК ПРИ ПРОЕКТИРОВАНИИ И ОПТИМИЗИРОВАТЬ ДИЗАЙН КОМПЛЕКСНЫХ ИНТЕРЬЕРОВ. ГАРАНТИЯ НА НАШУ ПРОДУКЦИЮ СОСТАВЛЯЕТ 18 МЕСЯЦЕВ, А НА ОТДЕЛЬНУЮ ПРОДУКЦИЮ 36 МЕСЯЦЕВ. \rМЫ ГОРДИМСЯ НАШИМИ СОТРУДНИКАМИ ПРОШЕДШИМИ ПРОФЕССИОНАЛЬНОЕ ОБУЧЕНИЕ В ЛУЧШИХ УЧЕБНЫХ ЗАВЕДЕНИЯХ РОССИИ И ЗАРУБЕЖЬЯ. У НАС ТРУДЯТСЯ ВЫСОКОКЛАССНЫЕ СПЕЦИАЛИСТЫ РАЗНЫХ ВОЗРАСТОВ. МЫ ЦЕНИМ ЭНТУЗИАЗМ МОЛОДЕЖИ И БЕСЦЕННЫЙ ОПЫТ СТАРШЕГО ПОКОЛЕНИЯ. ВСЕ МЫ РАЗНЫЕ, НО НАС ОБЪЕДИНЯЕТ ПРЕДАННОСТЬ СВОЕМУ ДЕЛУ И ВЕРА В ИДЕИ НАШЕЙ КОМПАНИИ. \rВЫСОЧАЙШИЕ СТАНДАРТЫ КАЧЕСТВА - ЗАЛОГ НАШЕГО УСПЕХА\rМЕБЕЛЬНАЯ КОМПАНИЯ\rОСУЩЕСТВЛЯЕТ ПРОИЗВОДСТВО МЕБЕЛИ НА ВЫСОКОКЛАССНОМ ОБОРУДОВАНИИ С ПРИМЕНЕНИЕМ МИНИМАЛЬНОЙ ДОЛИ РУЧНОГО ТРУДА, ЧТО ПОЗВОЛЯЕТ ОБЕСПЕЧИТЬ ВЫСОКОЕ КАЧЕСТВО НАШЕЙ ПРОДУКЦИИ. НАЛАЖЕН ПРОИЗВОДСТВЕННЫЙ ПРОЦЕСС КАК МАССОВОГО И ИНДИВИДУАЛЬНОГО ХАРАКТЕРА, ЧТО С ОДНОЙ СТОРОНЫ ПОЗВОЛЯЕТ ОБЕСПЕЧИТЬ ПОСТОЯННУЮ НОМЕНКЛАТУРУ ИЗДЕЛИЙ И ИНДИВИДУАЛЬНЫЙ ПОДХОД  С ДРУГОЙ. \rЕЖЕГОДНО НАША ПРОДУКЦИЯ ПРОХОДИТ СЕРТИФИКАЦИОННЫЕ ИСПЫТАНИЯ В СПЕЦИАЛИЗИРОВАННЫХ ЛАБОРАТОРИЯХ РОССИИ И ИМЕЕТ СЕРТИФИКАТЫ СООТВЕТСТВИЯ ПРОДУКЦИИ НОРМАМ БЕЗОПАСНОСТИ И КАЧЕСТВА. КРОМЕ ТОГО, \rМЕБЕЛЬНАЯ КОМПАНИЯ\rОДНОЙ ИЗ ПЕРВЫХ СРЕДИ МЕБЕЛЬНЫХ ПРОИЗВОДИТЕЛЕЙ РОССИИ В 2003 ГОДУ ПРОШЛА АУДИТ НА СООТВЕТСТВИЕ ТРЕБОВАНИЯМ СИСТЕМЫ МЕНЕДЖМЕНТА КАЧЕСТВА ИСО 9000 И ПОЛУЧИЛА СЕРТИФИКАТ СООТВЕТСТВИЯ СИСТЕМЫ КАЧЕСТВА НА ПРЕДПРИЯТИИ НОРМАМ МЕЖДУНАРОДНОГО СТАНДАРТА.\r\n'),
(3, 'c501a4fb29fd5bc92bc4babebf160ed9', 'РУКОВОДСТВО\r\nУСПЕШНОЕ РАЗВИТИЕ БИЗНЕСА  ВО МНОГОМ РЕЗУЛЬТАТ КВАЛИФИЦИРОВАННОЙ РАБОТЫ РУКОВОДСТВА. 							\rМЕБЕЛЬНАЯ КОМПАНИЯ\rНА МЕБЕЛЬНОМ РЫНКЕ УЖЕ 18 ЛЕТ. ЗА ЭТО ВРЕМЯ КОМПАНИЯ 							НЕ ТОЛЬКО СОХРАНИЛА, НО И УПРОЧИЛА ЛИДИРУЮЩИЕ ПОЗИЦИИ СРЕДИ ВЕДУЩИХ ИГРОКОВ МЕБЕЛЬНОГО 							РЫНКА. 						\rМЕБЕЛЬНАЯ КОМПАНИЯ\rИЗ ГОДА В ГОД РАСШИРЯЕТ АССОРТИМЕНТ ВЫПУСКАЕМОЙ ПРОДУКЦИИ, 							НАРАЩИВАЕТ ТЕМПЫ И ОБЪЕМЫ ПРОИЗВОДСТВА, УВЕЛИЧИВАЕТ ПРОИЗВОДСТВЕННЫЕ И СКЛАДСКИЕ 							ПЛОЩАДИ, РАЗВИВАЕТ ОТНОШЕНИЯ С ПАРТНЕРАМИ СО ВСЕХ РЕГИОНОВ СТРАНЫ И НАЛАЖИВАЕТ СВЯЗИ 							С ЗАРУБЕЖНЫМИ ПАРТНЕРАМИ. В БОЛЬШОЙ СТЕПЕНИ ЭТО ЗАСЛУГА ХОРОШО ПОДГОТОВЛЕННОГО РУКОВОДЯЩЕГО 							СОСТАВА И ЕГО ГРАМОТНОЙ ПОЛИТИКИ. 						\rСОБСТВЕННИК КОМПАНИИ МЕБЕЛЬНАЯ КОМПАНИЯ\rКОЛЕСНИКОВ ВИКТОР ФЕДОРОВИЧ 								\rРОДИЛСЯ 3 СЕНТЯБРЯ 1964 ГОДА.\nОБРАЗОВАНИЕ: ЗАКОНЧИЛ АВИАЦИОННЫЙ ФАКУЛЬТЕТ ВОРОНЕЖСКОГО 									ГОСУДАРСТВЕННОГО ПОЛИТЕХНИЧЕСКОГО ИНСТИТУТА. В 1994 ГОДУ ПРОШЕЛ ОБУЧЕНИЕ ПО ПРОГРАММЕ 									ПОДГОТОВКА МАЛОГО И СРЕДНЕГО БИЗНЕСА В США.\nВ НАСТОЯЩЕЕ ВРЕМЯ ВИКТОР ФЕДОРОВИЧ 									ВОЗГЛАВЛЯЕТ УПРАВЛЯЮЩУЮ КОМПАНИЮ, КОТОРАЯ КООРДИНИРУЕТ ДЕЯТЕЛЬНОСТЬ ПРЕДПРИЯТИЙ, 									ВХОДЯЩИХ В ГРУППУ КОМПАНИЙ \rМЕБЕЛЬНАЯ КОМПАНИЯ\r. 								\rГЕНЕРАЛЬНЫЙ ДИРЕКТОР МЕБЕЛЬНОЙ КОМПАНИИ\rРАТЧЕНКО АЛЕКСАНДР ПЕТРОВИЧ 								\rРОДИЛСЯ 5 ИЮНЯ 1962 ГОДА.\nОБРАЗОВАНИЕ: ВОРОНЕЖСКИЙ ПОЛИТЕХНИЧЕСКИЙ ИНСТИТУТ 									ПО СПЕЦИАЛЬНОСТИ ИНЖЕНЕР-ТЕХНОЛОГ; ПРОГРАММА ЭФФЕКТИВНОЕ РАЗВИТИЕ ПРОИЗВОДСТВА 									(США).\nВ \rМЕБЕЛЬНОЙ КОМПАНИИ\rСЕРГЕЙ ФОМИЧ С 1994 ГОДА. ЗА ЭТО ВРЕМЯ ПРОШЕЛ 									ПУТЬ ОТ НАЧАЛЬНИКА ЦЕХА ДО ГЕНЕРАЛЬНОГО ДИРЕКТОРА ПРЕДПРИЯТИЯ. 								\rЗАМЕСТИТЕЛЬ ГЕНЕРАЛЬНОГО ДИРЕКТОРА УПРАВЛЯЮЩЕЙ КОМПАНИИ\rРОГОВОЙ АНДРЕЙ ВЛАДИМИРОВИЧ 								\rОБРАЗОВАНИЕ: ФАКУЛЬТЕТ РАДИОТЕХНИКИ ВОРОНЕЖСКОГО ГОСУДАРСТВЕННОГО ТЕХНИЧЕСКОГО УНИВЕРСИТЕТА.\nВ КОМПАНИИ С 1 ИЮНЯ 2000 ГОДА.\r\n'),
(4, '17e08217818f62561b1d2f0f3a016a1f', 'МИССИЯ И СТРАТЕГИЯ\r\nМЕБЕЛЬНАЯ КОМПАНИЯ\r- ДИНАМИЧНО РАЗВИВАЮЩЕЕСЯ ПРОИЗВОДСТВЕННОЕ 							ПРЕДПРИЯТИЕ, КОТОРОЕ ИМЕЕТ ПЕРЕД СОБОЙ \nЯСНО ВЫРАЖЕННЫЕ ЦЕЛИ И ИНСТРУМЕНТЫ ДЛЯ 							ИХ ДОСТИЖЕНИ.МЫ ПРЕДОСТАВЛЯЕМ КАЖДОМУ ЛУЧШУЮ ВОЗМОЖНОСТЬ ОБУСТРОИТЬ СВОЕ ЖИЗНЕННОЕ \nИ РАБОЧЕЕ ПРОСТРАНСТВО.МЫ РАБОТАЕМ НА ДОЛГОСРОЧНУЮ ПЕРСПЕКТИВУ И ПРЕДЛАГАЕМ ОПТИМАЛЬНЫЕ 							РЕШЕНИЯ. КОМПАНИЯ \rМЕБЕЛЬНАЯ КОМПАНИЯ\r- \nНАДЕЖНЫЙ, ТЕХНОЛОГИЧНЫЙ, ГИБКИЙ ПОСТАВЩИК 							С БОЛЬШИМИ МОЩНОСТЯМИ. 						\rЦЕЛИ И ЗАДАЧИ\rОПРАВДЫВАТЬ ОЖИДАНИЯ ЗАКАЗЧИКА: КЛИЕНТ ВСЕГДА ПРАВ. ТОЛЬКО ПОТРЕБИТЕЛЬ ФОРМИРУЕТ 									ЕДИНУЮ СИСТЕМУ ВЗГЛЯДОВ НА КАЧЕСТВО \nВЫПУСКАЕМОЙ ПРОДУКЦИИ И РАБОТ.\rДОБИТЬСЯ ОТ РАБОТНИКОВ КОМПАНИИ ПОНИМАНИЯ ИХ ЛИЧНОЙ ОТВЕТСТВЕННОСТИ ЗА КАЧЕСТВО 									РАБОТ.\rПУТЕМ ПОВЫШЕНИЯ КАЧЕСТВА ПРОДУКЦИИ И РАБОТ ПОСТОЯННО УВЕЛИЧИВАТЬ ОБЪЕМЫ ПРОИЗВОДСТВА 									С ЦЕЛЬЮ ПОСЛЕДУЮЩЕГО РЕИНВЕСТИРОВАНИЯ \nПРИБЫЛИ В РАЗВИТИЕ КОМПАНИИ.\rОБЕСПЕЧИВАТЬ СТРОГОЕ СООТВЕТСТВИЕ ПРОИЗВОДИМОЙ ПРОДУКЦИИ ТРЕБОВАНИЯМ ПОТРЕБИТЕЛЕЙ, 									НОРМАМ И ПРАВИЛАМ \nБЕЗОПАСНОСТИ, ТРЕБОВАНИЯМ ЗАЩИТЫ ОКРУЖАЮЩЕЙ СРЕДЫ.\rПОЛИТИКА КОМПАНИИ\rПОСТОЯННОЕ СОВЕРШЕНСТВОВАНИЕ СИСТЕМЫ КАЧЕСТВА. СВОЕВРЕМЕННОЕ И ЭФФЕКТИВНОЕ ПРИНЯТИЕ 									КОРРЕКТИРУЮЩИХ МЕР .\rЗАБОТА О РАБОТНИКАХ КОМПАНИИ. СОЗДАНИЕ УСЛОВИЙ ТРУДА И ОСНАЩЕНИЕ РАБОЧИХ МЕСТ, СООТВЕТСТВУЮЩИХ 									ВСЕМ САНИТАРНЫМ \nИ ГИГИЕНИЧЕСКИМ НОРМАМ.\rПОВЫШЕНИЕ БЛАГОСОСТОЯНИЯ СОТРУДНИКОВ. ОБЕСПЕЧЕНИЕ МОРАЛЬНОГО И МАТЕРИАЛЬНОГО УДОВЛЕТВОРЕНИЯ 									РАБОТНИКОВ КОМПАНИИ.\rСИСТЕМАТИЧЕСКОЕ ОБУЧЕНИЕ РАБОТНИКОВ ВСЕХ УРОВНЕЙ С ЦЕЛЬЮ ПОСТОЯННОГО ПОВЫШЕНИЯ ИХ 									ПРОФЕССИОНАЛЬНОГО МАСТЕРСТВА.\rВНЕДРЕНИЕ ВЫСОКОПРОИЗВОДИТЕЛЬНОГО ОБОРУДОВАНИЯ И НОВЕЙШИХ ТЕХНОЛОГИЙ ДЛЯ ПОВЫШЕНИЯ 									ПРОИЗВОДИТЕЛЬНОСТИ ТРУДА, \nОПТИМИЗАЦИИ ЗАТРАТ И, КАК РЕЗУЛЬТАТ, СНИЖЕНИЯ ЦЕН 									НА ВЫПУСКАЕМУЮ ПРОДУКЦИЮ.\rСОЗДАНИЕ НОВЫХ РАБОЧИХ МЕСТ. ПРИВЛЕЧЕНИЕ НА РАБОТУ СПЕЦИАЛИСТОВ ВЫСОКОЙ КВАЛИФИКАЦИИ.\rВЫХОД НА МЕЖДУНАРОДНЫЙ РЫНОК.\rМЫ РАЗВИВАЕМ ДОВЕРИТЕЛЬНЫЕ ВЗАИМОВЫГОДНЫЕ ОТНОШЕНИЯ СО СВОИМИ ПАРТНЕРАМИ В ДОЛГОСРОЧНЫХ 							ИНТЕРЕСАХ НАШЕГО БИЗНЕСА. \nМЕБЕЛЬНАЯ КОМПАНИЯ\rОТВЕТСТВЕННО ОТНОСИТСЯ 							К ВЫПОЛНЕНИЮ ВЗЯТЫХ НА СЕБЯ ОБЯЗАТЕЛЬСТВ И ЖДЕТ ТАКОГО ЖЕ \nПОДХОДА К ДЕЛУ ОТ 							СВОИХ ПАРТНЕРОВ ПО БИЗНЕСУ. ЭТА ТРЕБОВАТЕЛЬНОСТЬ  ЗАЛОГ НАШЕЙ ДОЛГОСРОЧНОЙ ПРИБЫЛЬНОСТИ. 						\rСО ДНЯ СВОЕГО ОСНОВАНИЯ \rМЕБЕЛЬНАЯ КОМПАНИЯ\rСОДЕЙСТВУЕТ РОСТУ БЛАГОСОСТОЯНИЯ 							РЕГИОНОВ РОССИИ. МЫ ПОНИМАЕМ ВАЖНОСТЬ \nСОЦИАЛЬНОЙ ОТВЕТСТВЕННОСТИ НАШЕЙ КОМПАНИИ 							И ОСТАНЕМСЯ ПРИМЕРОМ В ВОПРОСАХ СОЦИАЛЬНОЙ ЗАЩИЩЕННОСТИ НАШИХ СОТРУДНИКОВ.\r\n'),
(5, 'd6ae18283686e0f65091531174c8b418', 'ВАКАНСИИ\r\n\r\n'),
(6, 'd63c4b77cf485102f232c0d0d27f8bc3', 'КОНТАКТЫ\r\nОБРАТИТЕСЬ К НАШИМ СПЕЦИАЛИСТАМ И ПОЛУЧИТЕ ПРОФЕССИОНАЛЬНУЮ КОНСУЛЬТАЦИЮ ПО ВОПРОСАМ СОЗДАНИЯ И ПОКУПКИ МЕБЕЛИ (ОТ ДИЗАЙНА, РАЗРАБОТКИ ТЕХНИЧЕСКОГО ЗАДАНИЯ ДО ДОСТАВКИ МЕБЕЛИ К ВАМ ДОМОЙ).\rВЫ МОЖЕТЕ ОБРАТИТЬСЯ К НАМ ПО ТЕЛЕФОНУ, ПО ЭЛЕКТРОННОЙ ПОЧТЕ ИЛИ ДОГОВОРИТЬСЯ О ВСТРЕЧЕ В НАШЕМ ОФИСЕ. БУДЕМ РАДЫ ПОМОЧЬ ВАМ И ОТВЕТИТЬ НА ВСЕ ВАШИ ВОПРОСЫ. \rТЕЛЕФОНЫ\rТЕЛЕФОН/ФАКС:\n(495) 212-85-06\rТЕЛЕФОНЫ:\n(495) 212-85-07\r(495) 212-85-08\rEMAIL\rINFO@EXAMPLE.RU\r ОБЩИЕ ВОПРОСЫ\rSALES@EXAMPLE.RU\r ПРИОБРЕТЕНИЕ ПРОДУКЦИИ\rMARKETING@EXAMPLE.RU\r МАРКЕТИНГ/МЕРОПРИЯТИЯ/PR\rОФИС В МОСКВЕ\r\n'),
(7, '7c285f6a61d238a340b43f0ef846a536', 'ВХОД НА САЙТ\r\nВЫ ЗАРЕГИСТРИРОВАНЫ И УСПЕШНО АВТОРИЗОВАЛИСЬ.\rВЕРНУТЬСЯ НА ГЛАВНУЮ СТРАНИЦУ\r\n'),
(8, '626a5d1bd18fcc622fbe4e4e3619a88a', 'НОВОСТИ\r\n\r\n'),
(9, '43974ce3161b1536a23aab70dfdf083c', 'ПРОДУКЦИЯ\r\n\r\n'),
(10, 'f58e028735b5d07233c4a161c9231405', 'ПОИСК\r\n\r\n'),
(11, 'be3fe2336ca014ea48484837a6379cf7', 'КАРТА САЙТА\r\n\r\n'),
(12, '3140fa939e82d14de3829b731863cd80', 'УСЛУГИ\r\n\r\n'),
(13, '9dc81310e5f6940cd74425623ed50b52', 'МЕБЕЛЬНАЯ КОМПАНИЯ\r\nНАША КОМПАНИЯ СУЩЕСТВУЕТ НА РОССИЙСКОМ РЫНКЕ С 1992 ГОДА. ЗА ЭТО ВРЕМЯ «МЕБЕЛЬНАЯ КОМПАНИЯ» ПРОШЛА БОЛЬШОЙ ПУТЬ ОТ МАЛЕНЬКОЙ ТОРГОВОЙ ФИРМЫ ДО ОДНОГО ИЗ КРУПНЕЙШИХ ПРОИЗВОДИТЕЛЕЙ КОРПУСНОЙ МЕБЕЛИ В РОССИИ.\n«МЕБЕЛЬНАЯ КОМПАНИЯ» ОСУЩЕСТВЛЯЕТ ПРОИЗВОДСТВО МЕБЕЛИ НА ВЫСОКОКЛАССНОМ ОБОРУДОВАНИИ С ПРИМЕНЕНИЕМ МИНИМАЛЬНОЙ ДОЛИ РУЧНОГО ТРУДА, ЧТО ПОЗВОЛЯЕТ ОБЕСПЕЧИТЬ ВЫСОКОЕ КАЧЕСТВО НАШЕЙ ПРОДУКЦИИ. НАЛАЖЕН ПРОИЗВОДСТВЕННЫЙ ПРОЦЕСС КАК МАССОВОГО И ИНДИВИДУАЛЬНОГО ХАРАКТЕРА, ЧТО С ОДНОЙ СТОРОНЫ ПОЗВОЛЯЕТ ОБЕСПЕЧИТЬ ПОСТОЯННУЮ НОМЕНКЛАТУРУ ИЗДЕЛИЙ И ИНДИВИДУАЛЬНЫЙ ПОДХОД – С ДРУГОЙ.\nНАША ПРОДУКЦИЯ\rНАШИ УСЛУГИ\r\n'),
(45, 'da430218f51fdea19711a3806fb7a3c4', 'МЕБЕЛЬ НА ЗАКАЗ\r\nНАША КОМПАНИЯ ЗАНИМАЕТСЯ РАЗРАБОТКОЙ МЕБЕЛИ НА ЗАКАЗ.\r\n \rНАША КОМПАНИЯ ЗАНИМАЕТСЯ РАЗРАБОТКОЙ МЕБЕЛИ НА ЗАКАЗ ПО ИНДИВИДУАЛЬНЫМ ПРОЕКТАМ: ВСТРОЕННЫЕ И КОРПУСНЫЕ ШКАФЫ КУПЕ,\rГАРДЕРОБНЫЕ КОМНАТЫ, ПРИХОЖИЕ, БИБЛИОТЕКИ, ПЛАТЯНЫЕ ШКАФЫ, КОМОДЫ И ДРУГИЕ СЛОЖНЫЕ КОНСТРУКЦИИ.\rМЫ СОЗДАЕМ МЕБЕЛЬ ИДЕАЛЬНО ПОДХОДЯЩУЮ ИМЕННО К ВАШЕМУ ДОМУ И ОФИСУ, ИНТЕРЬЕРЫ, МАКСИМАЛЬНО ОТОБРАЖАЮЩИЕ ВАШУ ИНДИВИДУАЛЬНОСТЬ.\rПО ВАШЕЙ ЗАЯВКЕ НАШ СПЕЦИАЛИСТ ПРИЕЗЖАЕТ СО ВСЕМИ ОБРАЗЦАМИ МАТЕРИАЛОВ, С КОТОРЫМИ МЫ РАБОТАЕМ, В ЛЮБОЕ УДОБНОЕ ДЛЯ ВАС ВРЕМЯ И\rПРОИЗВЕДЕТ ВСЕ НЕОБХОДИМЫЕ ЗАМЕРЫ. УЧИТЫВАЯ ВСЕ ВАШИ ПОЖЕЛАНИЯ И ОСОБЕННОСТИ ПОМЕЩЕНИЯ, СОСТАВЛЯЕТСЯ ЭСКИЗНЫЙ ПРОЕКТ.\rПОСЛЕ ЗАКЛЮЧЕНИЯ ДОГОВОРА, В КОТОРОМ ОГОВАРИВАЮТСЯ СРОКИ ДОСТАВКИ И МОНТАЖА МЕБЕЛИ, ЭСКИЗНЫЙ ПРОЕКТ ПЕРЕДАЕТСЯ НА ПРОИЗВОДСТВО,\rГДЕ ОПЫТНЫМИ СПЕЦИАЛИСТАМИ ПРОИЗВОДЯТСЯ РАСЧЕТЫ В ПРОГРАММЕ ТРЕХМЕРНОГО МОДЕЛИРОВАНИЯ. ПОСЛЕ ВСЕХ РАСЧЕТОВ ГОТОВЫЙ ПРОЕКТ ПОСТУПАЕТ\rНЕПОСРЕДСТВЕННО НА ПРОИЗВОДСТВО, ГДЕ ПРОИЗВОДЯТ РАСКРОЙ ДЕТАЛЕЙ, ИХ ОБРАБОТКУ, И СБОРКУ НЕКОТОРЫХ ЭЛЕМЕНТОВ. НАКАНУНЕ ДНЯ ДОСТАВКИ\rСОТРУДНИКИ ОТДЕЛА ТРАНСПОРТНЫХ УСЛУГ СВЯЖУТСЯ С ВАМИ И БОЛЕЕ КОНКРЕТНО ОГОВОРЯТ ВРЕМЯ ДОСТАВКИ. ПОСЛЕ ЗАКЛЮЧЕНИЯ ДОГОВОРА ВАМИ\rВНОСИТСЯ ПРЕДОПЛАТА В РАЗМЕРЕ 30% ОТ СУММЫ ЗАКАЗАННОЙ ВАМИ МЕБЕЛИ. ОСТАЛЬНАЯ СУММА ОПЛАЧИВАЕТСЯ ВАМИ ПО ДОСТАВКЕ.\r\n'),
(46, '6ae9d8e3d86f6edc36947e4e39db948b', 'УСЛУГИ ДИЗАЙНЕРА\r\nМЫ ПРЕДЛАГАЕМ ШИРОКИЙ СПЕКТР УСЛУГ ПО ДИЗАЙНУ МЕБЕЛИ.\r\n \rДИАГНОСТИКА ВОЗМОЖНОСТЕЙ ПРЕОБРАЗОВАНИЯ ПОМЕЩЕНИЙ – ОПРЕДЕЛЕНИЕ ВАРИАНТОВ ПЕРЕПЛАНИРОВКИ, ОТДЕЛКИ, РАЗРАБОТКА НОВЫХ РЕШЕНИЙ КОЛОРИСТКИ, ОСВЕЩЕНИЯ, ПЕРЕСТАНОВКИ МЕБЕЛИ И ДЕКОРА, РАЗРАБОТКА СПЕЦИАЛЬНЫХ ФУНКЦИОНАЛЬНЫХ ЗОН ДЛЯ ПЕРЕКЛЮЧЕНИЯ В РАЗЛИЧНЫЕ РЕЖИМЫ ЖИЗНИ.\rРАЗРАБОТКА ИДЕИ-ОБРАЗА, ПРОВЕДЕНИЕ ПРЕДВАРИТЕЛЬНЫХ РАСЧЁТОВ И СОЗДАНИЕ 3D-МОДЕЛИ: ИЗОБРАЖЕНИЕ ПЕРЕДАЕТ ЦВЕТ И ФАКТУРУ, ПРЕДЛАГАЯ КЛИЕНТУ ЭКСПЕРИМЕНТИРОВАТЬ И ПОДБИРАТЬ ОПТИМАЛЬНЫЙ ВАРИАНТ.\rРАЗРАБОТКА КОМПЬЮТЕРНЫХ ЦВЕТНЫХ ТРЕХМЕРНЫХ МОДЕЛЕЙ МЕБЕЛИ. НАТУРАЛИСТИЧНОСТЬ ИЗОБРАЖЕНИЙ, ИХ ВЫСОКАЯ СХОЖЕСТЬ С ОРИГИНАЛОМ ПОЗВОЛЯЮТ ПРЕДСТАВИТЬ, КАК БУДЕТ ВЫГЛЯДЕТЬ ГОТОВОЕ ИЗДЕЛИЕ, РАССМОТРЕТЬ ЕГО В ДЕТАЛЯХ.\rПОДБОР И РАССТАНОВКА МЕБЕЛИ.\rДЕКОРИРОВАНИЕ - ПОДБОР ДЕКОРА И АКСЕССУАРОВ ИНТЕРЬЕРА В СООТВЕТСТВИИ С ОБРАЗОМ И СТИЛЕМ ПОМЕЩЕНИЯ. ВОЗМОЖНО СОЗДАНИЕ ПО ИНДИВИДУАЛЬНОМУ ЗАПРОСУ ЭКСКЛЮЗИВНЫХ, АВТОРСКИХ КОЛЛЕКЦИЙ.\r\n'),
(50, '44278a93ef5e6c742624edf99a812289', 'ВЫДУВ ПЭТ-ТАРЫ\r\n\r\n'),
(51, 'c92b7fe2501a94d5ffbdf47d2fb3f4a8', 'ОБОРУДОВАНИЕ РОЗЛИВА \r\n\r\n'),
(52, '1594992b01fe5f2e7befd83e447de224', 'УКУПОРОЧНОЕ ОБОРУДОВАНИЕ\r\n\r\n'),
(53, '76d4fb079f45836780a546f842770e29', 'ЭТИКЕРОВЩИКИ \r\n\r\n'),
(54, '513756a4ece1b0b8b359cfe9dbd6c784', 'ПРИНТЕРЫ-ДАТИРОВЩИКИ \r\n\r\n'),
(55, 'dab7a96b39011ca230f62e333258d98a', 'УПАКОВОЧНОЕ ОБОРУДОВАНИЕ\r\n\r\n'),
(56, '0bbf704ac1994a595d97794c3e989133', 'ПАЛЛЕТООБМОТЧИКИ \r\n\r\n'),
(57, '1946de6abd1c8ae99bebdb8af0062837', 'КОНВЕЙЕРНЫЕ СИСТЕМЫ \r\n\r\n'),
(58, 'd8f47b60a636bfb17b3fefcf4270b80f', 'КОМПРЕССОРЫ \r\n\r\n'),
(59, '6b8f0e6fd30ec1fcdfa487d5d4b52afb', 'САТУРАТОРЫ \r\n\r\n'),
(60, '1cf0f58cf622e828b8b0657b7c9231e2', 'РЕСИВЕРЫ\r\n\r\n'),
(61, '43974ce3161b1536a23aab70dfdf083c', 'ПРОДУКЦИЯ\r\n\r\n'),
(62, '90bd144749b3a86dcf2e197ed2db4e3e', 'КОМПЛЕКСНЫЕ ЛИНИИ\r\nTEXT HERE....\r\n'),
(63, '68a1de2cfb8d3e0e5c151fbf999be2ca', 'УСЛУГИ\r\nTEXT HERE....\r\n'),
(64, 'd704d2e495eaa2b44c29c3501f3700ba', 'ПРОЕКТЫ\r\n\r\n'),
(65, '5bb5df722cc9b78d74f1bd0c6af553f6', 'КОМПАНИЯ\r\nООО «АКВАКУЛЬТУРА»\rПРОДАЁМ ОБОРУДОВАНИЕ ТРЁХ КРУПНЕЙШИХ КИТАЙСКИХ ПРОИЗВОДИТЕЛЕЙ, И ОБОРУДОВАНИЕ ВОДОПОДГОТОВКИ ИЗ США, ГЕРМАНИИ И КАНАДЫ.\rУМЕЕМ ЗАПУСТИТЬ ЗАВОД С НУЛЯ ИЛИ МОДЕРНИЗИРОВАТЬ ГОТОВУЮ ЛИНИЮ.\rНА СКЛАДЕ ХРАНИМ ОБОРУДОВАНИЕ И ЗАПАСНЫЕ ЧАСТИ. КОНТРОЛИРУЕМ КАЧЕСТВО, ОБУЧАЕМ ПЕРСОНАЛ.\rРАБОТАЕМ В МОСКВЕ, РЕГИОНАХ РФ И СТРАНАХ СНГ. С КОРПОРАЦИЯМИ И ЧАСТНЫМИ ЗАКАЗЧИКАМИ.\rНОВОСТИ\rСТАТЬИ\r\n'),
(66, 'd7566702b67f4856d170f1a929784c95', 'КОНТАКТЫ\r\nTEXT HERE....\r\n'),
(67, 'cb8ae7da48332922faa145c93711173b', 'РОСУПАК-2014\r\nTEXT HERE....\r\n'),
(68, '2a0c46b3a4d20e789adb6cb173057507', 'ОТЧЁТ О ВЫСТАВКЕ РОСУПАК 2012\r\nВ 2012 ГОДУ ВЫСТАВКА РОСУПАК ОТКРЫЛА ПОСЕТИТЕЛЯМ СВОИ ДВЕРИ НА НОВОЙ ПЛОЩАДКЕ. В 75-ОМ ПАВИЛЬОНЕ ВВЦ СОБРАЛИСЬ ВЕДУЩИЕ ПРОИЗВОДИТЕЛИ И ПОСТАВЩИКИ ОБОРУДОВАНИЯ, ВСЕГО 700 КОМПАНИЙ ИЗ 31 СТРАНЫ МИРА. КОМПАНИЯ АКВАКУЛЬТУРА, КАК ОДИН ИЗ ВЕДУЩИХ ПОСТАВЩИКОВ УПАКОВОЧНОГО ОБОРУДОВАНИЯ В РОССИИ И СНГ, ТРАДИЦИОННО ПРИНЯЛА УЧАСТИЕ В ВЫСТАВКЕ. НА СТЕНДЕ КОМПАНИИ БЫЛА ПРЕДСТАВЛЕНА ЛИНИЯ ПО ВЫДУВУ ПЭТ БУТЫЛОК ПРОИЗВОДИТЕЛЬНОСТЬЮ 6000 БУТЫЛОК В ЧАС.\r\n'),
(69, 'ed71ee094fd09137dd998224cce10512', 'ВЫСТАВКА ROSUPACK 2013\r\nУВАЖАЕМЫЕ КОЛЛЕГИ И ДРУЗЬЯ! ПРИГЛАШАЕМ ВАС ПОСЕТИТЬ НАШ СТЕНД А 215 В ЗАЛЕ 1, ПЕРВОГО ПАВИЛЬОНА В МВЦ "КРОКУС ЭКСПО" ВО ВРЕМЯ ПРОВЕДЕНИЯ ВЫСТАВКИ ROSUPACK 2013 С 18 ПО 21 ИЮНЯ 2013 ГОДА!\r\n'),
(70, '50ccd5c03dcdc58c59f86e0d0240beaf', 'WATERSHOW 2014\r\nУВАЖАЕМЫЕ КОЛЛЕГИ! ПРИГЛАШАЕМ ВАС ПОСЕТИТЬ САМЫЕ ОЖИДАЕМЫЕ И ПОСЕЩАЕМЫЕ СОБЫТИЯ ВОДНОЙ ОТРАСЛИ ТРИНАДЦАТЫЙ ФОРУМ ПРОИЗВОДИТЕЛЕЙ БУТИЛИРОВАННОЙ ВОДЫ РОССИИ «WATERSHOW» И ВТОРОЙ МЕЖДУНАРОДНЫЙ КОНГРЕСС ПРОИЗВОДИТЕЛЕЙ МИНЕРАЛЬНОЙ, ПИТЬЕВОЙ ВОДЫ И БЕЗАЛКОГОЛЬНЫХ НАПИТКОВ «BEVERAGES INDUSTRY CONFERENCE», МЕРОПРИЯТИЯ ПРОЙДУТ 18-20 МАРТА 2014 ГОДА, В МОСКВЕ, ОТЕЛЬ МИЛАН.\r\n'),
(71, '8a5dc5dd144d41bc894cc1173e612282', 'ОТЧЁТ О WATERSHOW 2014\r\nКАК ВСЕГДА ОЧЕНЬ УСПЕШНО ПРОШЕЛ ТРИНАДЦАТЫЙ ФОРУМ ПРОИЗВОДИТЕЛЕЙ БУТИЛИРОВАННОЙ ВОДЫ РОССИИ WATERSHOW 2014. МНОГИЕ УЧАСТНИКИ УЖЕ НАЗВАЛИ ЕГО ОДНИМ ИЗ ЛУЧШИХ ЗА НЕОБЫКНОВЕННО ПРИЯТНУЮ АТМОСФЕРУ И ОТЛИЧНУЮ ОРГАНИЗАЦИЮ МЕРОПРИЯТИЯ. НА WATERSHOW ПРИЕХАЛО ОКОЛО 250 ЧЕЛОВЕК ИЗ 128 КОМПАНИЙ РОССИИ, КАЗАХСТАНА, УКРАИНЫ, УЗБЕКИСТАНА, ИТАЛИИ, ПОЛЬШИ, КИТАЯ, АРМЕНИИ, БЕЛОРУСИ.\r\n'),
(72, 'b366fd44e568717e60585f9f4c78b5e9', 'СИСТЕМЫ И ФИЛЬТРЫ ОЧИСТКИ ВОДЫ ДЛЯ ДОМА\r\nКОГДА-ТО, ЧТОБЫ УТОЛИТЬ ЖАЖДУ, ДЛЯ НАШИХ ПРЕДКОВ БЫЛО СОВЕРШЕННО ЕСТЕСТВЕННЫМ СПУСТИТЬСЯ К РЕКЕ, ОЗЕРУ ИЛИ РУЧЬЮ, ЗАЧЕРПНУТЬ ЛАДОНЯМИ И ВЫПИТЬ СВЕЖУЮ ВОДУ. С ТЕХ ПОР ПРОШЛИ ВЕКА...\r\n'),
(73, '8caea6ea76d5ae3fb8989a43b9f48301', 'ДЕЗИНФЕКЦИЯ ПИТЬЕВОЙ ВОДЫ С ПОМОЩЬЮ УФ ИЗЛУЧЕНИЯ\r\nВОДА ИМЕЕТЕ ОЧЕНЬ ВАЖНОЕ ЗНАЧЕНИЕ В ЖИЗНИ ЧЕЛОВЕКА. БЕЗ ВОДЫ ЧЕЛОВЕК МОЖЕТ ПРОЖИТЬ НЕ БОЛЕЕ 3 СУТОК. ВОДА ОСТАВЛЯЕТ ОРИЕНТИРОВОЧНО ДВЕ ТРЕТИ МАССЫ ТЕЛА ВЗРОСЛОГО ЧЕЛОВЕКА.\r\n'),
(74, 'f945551905189752781f7b6b1b26431d', 'ОЧЕРЕДНОЙ СЕМИНАР, ОРГАНИЗОВАННЫЙ СОЮЗОМ ПРОИЗВОДИТЕЛЕЙ БУТИЛИРОВАННЫХ ВОД РОССИИ\r\nСЕМИНАР ПРОШЕЛ 2-3 ИЮНЯ В ПОДМОСКОВНОМ ОТЕЛЕ «ФОРЕСТА ФЕСТИВАЛЬ ПАРК». В КАЧЕСТВЕ ПРИГЛАШЕННОГО СПЕЦИАЛИСТА СЕМИНАР «ВЛИЯНИЕ ФИНАНСОВОГО КРИЗИСА НА ВОДНЫЕ КОМПАНИИ» ПРОВЕЛ НЕЗАВИСИМЫЙ БИЗНЕС-КОНСУЛЬТАНТ КОНСТАНТИН РАКОВ.\r\n'),
(75, 'e10000c15f88722608cdba8993204254', 'ТЕХНОЛОГИИ ОЧИСТКИ ВОДЫ\r\nВОДА ИМЕЕТЕ ОЧЕНЬ ВАЖНОЕ ЗНАЧЕНИЕ В ЖИЗНИ ЧЕЛОВЕКА. БЕЗ ВОДЫ ЧЕЛОВЕК МОЖЕТ ПРОЖИТЬ НЕ БОЛЕЕ 3 СУТОК. ВОДА ОСТАВЛЯЕТ ОРИЕНТИРОВОЧНО ДВЕ ТРЕТИ МАССЫ ТЕЛА ВЗРОСЛОГО ЧЕЛОВЕКА.\r\n'),
(76, '2a3d9268a9ede5c88629dea7b348e6b6', 'НОВОСТИ\r\nTEXT HERE....\r\n'),
(77, '626a5d1bd18fcc622fbe4e4e3619a88a', 'НОВОСТИ\r\n\r\n'),
(78, 'e3a13a12458142c89a7f1a0530fe1760', 'СТАТЬИ\r\nTEXT HERE....\r\n'),
(79, 'd71e8840cc9701f5a6e3e5ccc0a37414', 'ВОДА\r\n\r\n'),
(80, 'accfb9d302209acea3927b3246d3bdb2', 'ГАЗИРОВАННЫЕ НАПИТКИ\r\n\r\n'),
(81, 'a9e0ce105c11022e494d4b5c23b8a658', 'БЫТОВАЯ ХИМИЯ И ТЕХЖИДКОСТИ\r\n\r\n'),
(82, '89e27925f16fb1e9aaccd70b16016d45', 'МАСЛО\r\n\r\n'),
(83, '89e01d57b3d3444e663deb5bb3e1f947', 'ЗАВОД ООО «КОКА-КОЛА ЭЙЧБИСИ ЕВРАЗИЯ»\r\nЗАДАЧА:\rЗАПУСТИТЬ ПРОИЗВОДСТВО ЛИМОНАДА С НУЛЯ В СЖАТЫЕ СРОКИ.\rРЕЗУЛЬТАТ:\rСЕЙЧАС КОМПАНИЯ ВЫПУСКАЕТ 700 БУТЫЛОК ЛИМОНАДА И 400 БУТЫЛЕЙ ЧИСТОЙ ВОДЫ В ЧАС. ЗАКАЗЧИК ПЛАНИРУЕТ РАСШИРЯТЬ ПРОИЗВОДСТВО И АССОРТИМЕНТ В СОТРУДНИЧЕСТВЕ С НАМИ.\r\n'),
(84, 'a0a7dd762e85cb92503be21291d27d17', 'ЦЕХ РОЗЛИВА МПБК «ОЧАКОВО»\r\nЗАДАЧА:\rЗАПУСТИТЬ ПРОИЗВОДСТВО ЛИМОНАДА С НУЛЯ В СЖАТЫЕ СРОКИ.\rРЕЗУЛЬТАТ:\rСЕЙЧАС КОМПАНИЯ ВЫПУСКАЕТ 700 БУТЫЛОК ЛИМОНАДА И 400 БУТЫЛЕЙ ЧИСТОЙ ВОДЫ В ЧАС. ЗАКАЗЧИК ПЛАНИРУЕТ РАСШИРЯТЬ ПРОИЗВОДСТВО И АССОРТИМЕНТ В СОТРУДНИЧЕСТВЕ С НАМИ.\r\n'),
(85, '53a3045f64d042c0e278da4c82323419', 'ЗАВОД ОАО «БРЯНСКПИВО»\r\nЗАДАЧА:\rЗАПУСТИТЬ ПРОИЗВОДСТВО ЛИМОНАДА С НУЛЯ В СЖАТЫЕ СРОКИ.\rРЕЗУЛЬТАТ:\rСЕЙЧАС КОМПАНИЯ ВЫПУСКАЕТ 700 БУТЫЛОК ЛИМОНАДА И 400 БУТЫЛЕЙ ЧИСТОЙ ВОДЫ В ЧАС. ЗАКАЗЧИК ПЛАНИРУЕТ РАСШИРЯТЬ ПРОИЗВОДСТВО И АССОРТИМЕНТ В СОТРУДНИЧЕСТВЕ С НАМИ.\r\n'),
(86, 'a384b9f452ef4ef1dc7f61f757fe13cc', 'ТАРА ДО 2 ЛИТРОВ\r\n\r\n'),
(87, '09b0429c58c5cd153d9f9f1af2497c44', 'ТАРА БОЛЕЕ 2 ЛИТРОВ\r\n\r\n'),
(88, '4879a3c3ce9e0e0ff2d9468fc3fd58e8', 'КОСМЕТИКА, ТОЛСТОСТЕННАЯ ТАРА\r\n\r\n'),
(89, '57bb6fcaccee0080a306f68196177718', 'БАНКИ, УВЕЛИЧЕННЫЙ ДИАМЕТР ГОРЛА\r\n\r\n'),
(90, 'c8d4a996336162bc09a2a4ea6c9a38c9', 'С АВТОУСТАНОВКОЙ РУЧКИ\r\n\r\n'),
(91, '1ffd007b5312c3d9f62778ad49659f8a', 'SN-CSSS2000\r\nАВТОМАТИЧЕСКАЯ ВЫДУВНАЯ МАШИНА РN-PP 2000E ПРЕДНАЗНАЧЕНА ДЛЯ ПРОИЗВОДСТВА ПП (ПОЛИПРОПИЛЕНОВЫХ) БУТЫЛОК ОБЪЁМОМ ОТ 0,1 ДО 0.6 ЛИТРА, ПУТЁМ ДВУХСТАДИЙНОГО ПНЕВМАТИЧЕСКОГО ФОРМОВАНИЯ ИЗ ПРЕДВАРИТЕЛЬНО РАЗОГРЕТЫХ ЗАГОТОВОК (ПРЕФОРМ). ДЛЯ КАЖДОГО ТИПОРАЗМЕРА БУТЫЛКИ ИЗГОТАВЛИВАЕТСЯ СВОЯ ПРЕСС-ФОРМА, ТОЧНО ПОВТОРЯЮЩАЯ ВНЕШНИЙ ВИД И ГЕОМЕТРИЮ БУТЫЛКИ, ЗАДАННЫЕ ЗАКАЗЧИКОМ.\rМОНОБЛОКИ СЕРИИ DGF ПРЕДНАЗНАЧЕНЫ ДЛЯ РОЗЛИВА И УКУПОРИВАНИЯ ПЭТ-БУТЫЛОК ЕМКОСТЬЮ ОТ 0,25 ДО 2 Л ГАЗИРОВАННЫМИ ЖИДКОСТЯМИ ПРОИЗВОДИТЕЛЬНОСТЬЮ ДО 24000 БУТ/ЧАС.\rКОНСТРУКТИВНО МОНОБЛОК СОСТОИТ ИЗ:\rGX-200 ВСТРАИВАЮТСЯ В ЛИНИЮ, ПУТЁМ СТЫКОВКИ КОНВЕЙЕРОВ С ДВУХ СТОРОН АВТОМАТА\rВСЕ ДЕТАЛИ И МЕХАНИЗМЫ КОНТАКТИРУЮЩИЕ С ПРОДУКЦИЕЙ ИЗГОТОВЛЕНЫ ИЗ НЕРЖАВЕЮЩЕЙ СТАЛИ\rМАШИНА ПОЛНОСТЬЮ АВТОМАТИЗИРОВАНА ПОСЛЕ ЗАГРУЗКИ ПРЕФОРМ В ПРИЕМНЫЙ БУНКЕР ДО ПОДАЧИ ГОТОВОЙ ПРОДУКЦИИ НА ВОЗДУШНЫЙ ТРАНСПОРТЕР И НЕ ТРЕБУЕТ ПОСТОЯННОГО ПРИСУТСТВИЯ ОБСЛУЖИВАЮЩЕГО ПЕРСОНАЛА.\rОСНОВНЫМ ПРЕИМУЩЕСТВОМ ДАННОЙ МОДЕЛИ ЯВЛЯЕТСЯ ПРОСТОТА НАЛАДКИ, СТАБИЛЬНОСТЬ ПАРАМЕТРОВ ВЫДУВА И КАЧЕСТВА ПРОДУКЦИИ, ЕЁ КОМПАКТНОСТЬ И УДОБСТВО В ОБСЛУЖИВАНИИ. УЖЕ ЧЕРЕЗ 15 МИНУТ ПОСЛЕ ЗАПУСКА МАШИНА ГОТОВА ВЫДАВАТЬ ПРОДУКЦИЮ.\r\n'),
(92, 'a10b1c37bf434157c1cc2eff5d6a0020', 'SN-CSSS2001\r\nАВТОМАТИЧЕСКАЯ ВЫДУВНАЯ МАШИНА РN-PP 2000E ПРЕДНАЗНАЧЕНА ДЛЯ ПРОИЗВОДСТВА ПП (ПОЛИПРОПИЛЕНОВЫХ) БУТЫЛОК ОБЪЁМОМ ОТ 0,1 ДО 0.6 ЛИТРА, ПУТЁМ ДВУХСТАДИЙНОГО ПНЕВМАТИЧЕСКОГО ФОРМОВАНИЯ ИЗ ПРЕДВАРИТЕЛЬНО РАЗОГРЕТЫХ ЗАГОТОВОК (ПРЕФОРМ). ДЛЯ КАЖДОГО ТИПОРАЗМЕРА БУТЫЛКИ ИЗГОТАВЛИВАЕТСЯ СВОЯ ПРЕСС-ФОРМА, ТОЧНО ПОВТОРЯЮЩАЯ ВНЕШНИЙ ВИД И ГЕОМЕТРИЮ БУТЫЛКИ, ЗАДАННЫЕ ЗАКАЗЧИКОМ.\rМОНОБЛОКИ СЕРИИ DGF ПРЕДНАЗНАЧЕНЫ ДЛЯ РОЗЛИВА И УКУПОРИВАНИЯ ПЭТ-БУТЫЛОК ЕМКОСТЬЮ ОТ 0,25 ДО 2 Л ГАЗИРОВАННЫМИ ЖИДКОСТЯМИ ПРОИЗВОДИТЕЛЬНОСТЬЮ ДО 24000 БУТ/ЧАС.\rКОНСТРУКТИВНО МОНОБЛОК СОСТОИТ ИЗ:\rGX-200 ВСТРАИВАЮТСЯ В ЛИНИЮ, ПУТЁМ СТЫКОВКИ КОНВЕЙЕРОВ С ДВУХ СТОРОН АВТОМАТА\rВСЕ ДЕТАЛИ И МЕХАНИЗМЫ КОНТАКТИРУЮЩИЕ С ПРОДУКЦИЕЙ ИЗГОТОВЛЕНЫ ИЗ НЕРЖАВЕЮЩЕЙ СТАЛИ\rМАШИНА ПОЛНОСТЬЮ АВТОМАТИЗИРОВАНА ПОСЛЕ ЗАГРУЗКИ ПРЕФОРМ В ПРИЕМНЫЙ БУНКЕР ДО ПОДАЧИ ГОТОВОЙ ПРОДУКЦИИ НА ВОЗДУШНЫЙ ТРАНСПОРТЕР И НЕ ТРЕБУЕТ ПОСТОЯННОГО ПРИСУТСТВИЯ ОБСЛУЖИВАЮЩЕГО ПЕРСОНАЛА.\rОСНОВНЫМ ПРЕИМУЩЕСТВОМ ДАННОЙ МОДЕЛИ ЯВЛЯЕТСЯ ПРОСТОТА НАЛАДКИ, СТАБИЛЬНОСТЬ ПАРАМЕТРОВ ВЫДУВА И КАЧЕСТВА ПРОДУКЦИИ, ЕЁ КОМПАКТНОСТЬ И УДОБСТВО В ОБСЛУЖИВАНИИ. УЖЕ ЧЕРЕЗ 15 МИНУТ ПОСЛЕ ЗАПУСКА МАШИНА ГОТОВА ВЫДАВАТЬ ПРОДУКЦИЮ.\r\n'),
(93, '8aa91e4ad6a1ff5081799cb45eed9f71', 'SN-CSSS2002\r\nАВТОМАТИЧЕСКАЯ ВЫДУВНАЯ МАШИНА РN-PP 2000E ПРЕДНАЗНАЧЕНА ДЛЯ ПРОИЗВОДСТВА ПП (ПОЛИПРОПИЛЕНОВЫХ) БУТЫЛОК ОБЪЁМОМ ОТ 0,1 ДО 0.6 ЛИТРА, ПУТЁМ ДВУХСТАДИЙНОГО ПНЕВМАТИЧЕСКОГО ФОРМОВАНИЯ ИЗ ПРЕДВАРИТЕЛЬНО РАЗОГРЕТЫХ ЗАГОТОВОК (ПРЕФОРМ). ДЛЯ КАЖДОГО ТИПОРАЗМЕРА БУТЫЛКИ ИЗГОТАВЛИВАЕТСЯ СВОЯ ПРЕСС-ФОРМА, ТОЧНО ПОВТОРЯЮЩАЯ ВНЕШНИЙ ВИД И ГЕОМЕТРИЮ БУТЫЛКИ, ЗАДАННЫЕ ЗАКАЗЧИКОМ.\rМОНОБЛОКИ СЕРИИ DGF ПРЕДНАЗНАЧЕНЫ ДЛЯ РОЗЛИВА И УКУПОРИВАНИЯ ПЭТ-БУТЫЛОК ЕМКОСТЬЮ ОТ 0,25 ДО 2 Л ГАЗИРОВАННЫМИ ЖИДКОСТЯМИ ПРОИЗВОДИТЕЛЬНОСТЬЮ ДО 24000 БУТ/ЧАС.\rКОНСТРУКТИВНО МОНОБЛОК СОСТОИТ ИЗ:\rGX-200 ВСТРАИВАЮТСЯ В ЛИНИЮ, ПУТЁМ СТЫКОВКИ КОНВЕЙЕРОВ С ДВУХ СТОРОН АВТОМАТА\rВСЕ ДЕТАЛИ И МЕХАНИЗМЫ КОНТАКТИРУЮЩИЕ С ПРОДУКЦИЕЙ ИЗГОТОВЛЕНЫ ИЗ НЕРЖАВЕЮЩЕЙ СТАЛИ\rМАШИНА ПОЛНОСТЬЮ АВТОМАТИЗИРОВАНА ПОСЛЕ ЗАГРУЗКИ ПРЕФОРМ В ПРИЕМНЫЙ БУНКЕР ДО ПОДАЧИ ГОТОВОЙ ПРОДУКЦИИ НА ВОЗДУШНЫЙ ТРАНСПОРТЕР И НЕ ТРЕБУЕТ ПОСТОЯННОГО ПРИСУТСТВИЯ ОБСЛУЖИВАЮЩЕГО ПЕРСОНАЛА.\rОСНОВНЫМ ПРЕИМУЩЕСТВОМ ДАННОЙ МОДЕЛИ ЯВЛЯЕТСЯ ПРОСТОТА НАЛАДКИ, СТАБИЛЬНОСТЬ ПАРАМЕТРОВ ВЫДУВА И КАЧЕСТВА ПРОДУКЦИИ, ЕЁ КОМПАКТНОСТЬ И УДОБСТВО В ОБСЛУЖИВАНИИ. УЖЕ ЧЕРЕЗ 15 МИНУТ ПОСЛЕ ЗАПУСКА МАШИНА ГОТОВА ВЫДАВАТЬ ПРОДУКЦИЮ.\r\n'),
(94, '569e5c744709c9a72b6ac76d446aa412', 'АВТОМАТИЧЕСКИЙ ВЫДУВ\r\n\r\n'),
(95, '39c50744a6575df051eacfd8959d0dc1', 'ПОЛУАВТОМАТИЧЕСКИЙ ВЫВОД\r\n\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `b_search_content_title`
--

CREATE TABLE IF NOT EXISTS `b_search_content_title` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `POS` int(11) NOT NULL,
  `WORD` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `UX_B_SEARCH_CONTENT_TITLE` (`SITE_ID`,`WORD`,`SEARCH_CONTENT_ID`,`POS`),
  KEY `IND_B_SEARCH_CONTENT_TITLE` (`SEARCH_CONTENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci DELAY_KEY_WRITE=1;

--
-- Dumping data for table `b_search_content_title`
--

INSERT INTO `b_search_content_title` (`SEARCH_CONTENT_ID`, `SITE_ID`, `POS`, `WORD`) VALUES
(1, 's1', 0, 'ИСТОРИЯ'),
(2, 's1', 2, 'КОМПАНИИ'),
(2, 's1', 0, 'О'),
(3, 's1', 0, 'РУКОВОДСТВО'),
(4, 's1', 1, 'И'),
(4, 's1', 0, 'МИССИЯ'),
(4, 's1', 9, 'СТРАТЕГИЯ'),
(5, 's1', 0, 'ВАКАНСИИ'),
(6, 's1', 0, 'КОНТАКТЫ'),
(7, 's1', 0, 'ВХОД'),
(7, 's1', 5, 'НА'),
(7, 's1', 8, 'САЙТ'),
(8, 's1', 0, 'НОВОСТИ'),
(9, 's1', 0, 'ПРОДУКЦИЯ'),
(10, 's1', 0, 'ПОИСК'),
(11, 's1', 0, 'КАРТА'),
(11, 's1', 6, 'САЙТА'),
(12, 's1', 0, 'УСЛУГИ'),
(13, 's1', 10, 'КОМПАНИЯ'),
(13, 's1', 0, 'МЕБЕЛЬНАЯ'),
(45, 's1', 10, 'ЗАКАЗ'),
(45, 's1', 0, 'МЕБЕЛЬ'),
(45, 's1', 7, 'НА'),
(46, 's1', 7, 'ДИЗАЙНЕРА'),
(46, 's1', 0, 'УСЛУГИ'),
(50, 's1', 0, 'ВЫДУВ'),
(50, 's1', 6, 'ПЭТ-ТАРЫ'),
(51, 's1', 0, 'ОБОРУДОВАНИЕ'),
(51, 's1', 13, 'РОЗЛИВА'),
(52, 's1', 12, 'ОБОРУДОВАНИЕ'),
(52, 's1', 0, 'УКУПОРОЧНОЕ'),
(53, 's1', 0, 'ЭТИКЕРОВЩИКИ'),
(54, 's1', 0, 'ПРИНТЕРЫ-ДАТИРОВЩИКИ'),
(55, 's1', 12, 'ОБОРУДОВАНИЕ'),
(55, 's1', 0, 'УПАКОВОЧНОЕ'),
(56, 's1', 0, 'ПАЛЛЕТООБМОТЧИКИ'),
(57, 's1', 0, 'КОНВЕЙЕРНЫЕ'),
(57, 's1', 12, 'СИСТЕМЫ'),
(58, 's1', 0, 'КОМПРЕССОРЫ'),
(59, 's1', 0, 'САТУРАТОРЫ'),
(60, 's1', 0, 'РЕСИВЕРЫ'),
(61, 's1', 0, 'ПРОДУКЦИЯ'),
(62, 's1', 0, 'КОМПЛЕКСНЫЕ'),
(62, 's1', 12, 'ЛИНИИ'),
(63, 's1', 0, 'УСЛУГИ'),
(64, 's1', 0, 'ПРОЕКТЫ'),
(65, 's1', 0, 'КОМПАНИЯ'),
(66, 's1', 0, 'КОНТАКТЫ'),
(67, 's1', 0, 'РОСУПАК-2014'),
(68, 's1', 25, '2012'),
(68, 's1', 8, 'ВЫСТАВКЕ'),
(68, 's1', 0, 'О'),
(68, 's1', 0, 'ОТЧЁТ'),
(68, 's1', 17, 'РОСУПАК'),
(69, 's1', 18, '2013'),
(69, 's1', 9, 'ROSUPACK'),
(69, 's1', 0, 'ВЫСТАВКА'),
(70, 's1', 10, '2014'),
(70, 's1', 0, 'WATERSHOW'),
(71, 's1', 18, '2014'),
(71, 's1', 8, 'WATERSHOW'),
(71, 's1', 0, 'О'),
(71, 's1', 0, 'ОТЧЁТ'),
(72, 's1', 26, 'ВОДЫ'),
(72, 's1', 31, 'ДЛЯ'),
(72, 's1', 35, 'ДОМА'),
(72, 's1', 1, 'И'),
(72, 's1', 18, 'ОЧИСТКИ'),
(72, 's1', 0, 'СИСТЕМЫ'),
(72, 's1', 10, 'ФИЛЬТРЫ'),
(73, 's1', 21, 'ВОДЫ'),
(73, 's1', 0, 'ДЕЗИНФЕКЦИЯ'),
(73, 's1', 39, 'ИЗЛУЧЕНИЯ'),
(73, 's1', 12, 'ПИТЬЕВОЙ'),
(73, 's1', 28, 'ПОМОЩЬЮ'),
(73, 's1', 26, 'С'),
(73, 's1', 36, 'УФ'),
(74, 's1', 56, 'БУТИЛИРОВАННЫХ'),
(74, 's1', 46, 'ВОД'),
(74, 's1', 19, 'ОРГАНИЗОВАННЫЙ'),
(74, 's1', 0, 'ОЧЕРЕДНОЙ'),
(74, 's1', 41, 'ПРОИЗВОДИТЕЛЕЙ'),
(74, 's1', 75, 'РОССИИ'),
(74, 's1', 10, 'СЕМИНАР'),
(74, 's1', 34, 'СОЮЗОМ'),
(75, 's1', 19, 'ВОДЫ'),
(75, 's1', 11, 'ОЧИСТКИ'),
(75, 's1', 0, 'ТЕХНОЛОГИИ'),
(76, 's1', 0, 'НОВОСТИ'),
(77, 's1', 0, 'НОВОСТИ'),
(78, 's1', 0, 'СТАТЬИ'),
(79, 's1', 0, 'ВОДА'),
(80, 's1', 0, 'ГАЗИРОВАННЫЕ'),
(80, 's1', 13, 'НАПИТКИ'),
(81, 's1', 0, 'БЫТОВАЯ'),
(81, 's1', 9, 'И'),
(81, 's1', 16, 'ТЕХЖИДКОСТИ'),
(81, 's1', 8, 'ХИМИЯ'),
(82, 's1', 0, 'МАСЛО'),
(83, 's1', 29, 'ЕВРАЗИЯ'),
(83, 's1', 0, 'ЗАВОД'),
(83, 's1', 11, 'КОКА-КОЛА'),
(83, 's1', 6, 'ООО'),
(83, 's1', 21, 'ЭЙЧБИСИ'),
(84, 's1', 12, 'МПБК'),
(84, 's1', 18, 'ОЧАКОВО'),
(84, 's1', 4, 'РОЗЛИВА'),
(84, 's1', 0, 'ЦЕХ'),
(85, 's1', 11, 'БРЯНСКПИВО'),
(85, 's1', 0, 'ЗАВОД'),
(85, 's1', 6, 'ОАО'),
(86, 's1', 8, '2'),
(86, 's1', 5, 'ДО'),
(86, 's1', 10, 'ЛИТРОВ'),
(86, 's1', 0, 'ТАРА'),
(87, 's1', 11, '2'),
(87, 's1', 5, 'БОЛЕЕ'),
(87, 's1', 13, 'ЛИТРОВ'),
(87, 's1', 0, 'ТАРА'),
(88, 's1', 0, 'КОСМЕТИКА'),
(88, 's1', 25, 'ТАРА'),
(88, 's1', 11, 'ТОЛСТОСТЕННАЯ'),
(89, 's1', 0, 'БАНКИ'),
(89, 's1', 27, 'ГОРЛА'),
(89, 's1', 19, 'ДИАМЕТР'),
(89, 's1', 7, 'УВЕЛИЧЕННЫЙ'),
(90, 's1', 2, 'АВТОУСТАНОВКОЙ'),
(90, 's1', 17, 'РУЧКИ'),
(90, 's1', 0, 'С'),
(91, 's1', 0, 'SN-CSSS2000'),
(92, 's1', 0, 'SN-CSSS2001'),
(93, 's1', 0, 'SN-CSSS2002'),
(94, 's1', 0, 'АВТОМАТИЧЕСКИЙ'),
(94, 's1', 15, 'ВЫДУВ'),
(95, 's1', 19, 'ВЫВОД'),
(95, 's1', 0, 'ПОЛУАВТОМАТИЧЕСКИЙ');

-- --------------------------------------------------------

--
-- Table structure for table `b_search_custom_rank`
--

CREATE TABLE IF NOT EXISTS `b_search_custom_rank` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `APPLIED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `RANK` int(11) NOT NULL DEFAULT '0',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `PARAM1` text COLLATE utf8_unicode_ci,
  `PARAM2` text COLLATE utf8_unicode_ci,
  `ITEM_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IND_B_SEARCH_CUSTOM_RANK` (`SITE_ID`,`MODULE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_search_phrase`
--

CREATE TABLE IF NOT EXISTS `b_search_phrase` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` datetime NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `RESULT_COUNT` int(11) NOT NULL,
  `PAGES` int(11) NOT NULL,
  `SESSION_ID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `PHRASE` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TAGS` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL_TO` text COLLATE utf8_unicode_ci,
  `URL_TO_404` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `URL_TO_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `STAT_SESS_ID` int(18) DEFAULT NULL,
  `EVENT1` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IND_PK_B_SEARCH_PHRASE_SESS_PH` (`SESSION_ID`,`PHRASE`(50)),
  KEY `IND_PK_B_SEARCH_PHRASE_SESS_TG` (`SESSION_ID`,`TAGS`(50)),
  KEY `IND_PK_B_SEARCH_PHRASE_TIME` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_search_stem`
--

CREATE TABLE IF NOT EXISTS `b_search_stem` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `STEM` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_B_SEARCH_STEM` (`STEM`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1502 ;

--
-- Dumping data for table `b_search_stem`
--

INSERT INTO `b_search_stem` (`ID`, `STEM`) VALUES
(781, '-НЫ'),
(794, '0-15'),
(715, '000'),
(780, '10'),
(722, '10-Й'),
(696, '12'),
(1328, '128'),
(793, '13'),
(1201, '13500'),
(717, '149'),
(1493, '15'),
(625, '160'),
(1230, '168'),
(337, '18'),
(1319, '18-20'),
(460, '1962'),
(431, '1964'),
(212, '1992'),
(2, '1992Г'),
(34, '1993Г'),
(440, '1994'),
(62, '1998Г'),
(78, '1999Г'),
(1372, '2-3'),
(613, '20'),
(474, '2000'),
(1207, '20000'),
(1439, '2000E'),
(99, '2000Г'),
(107, '2002Г'),
(393, '2003'),
(121, '2003Г'),
(141, '2004Г'),
(153, '2005Г'),
(171, '2006Г'),
(181, '2007Г'),
(191, '2008Г'),
(616, '2010'),
(1279, '2012'),
(1297, '2013'),
(1299, '2014'),
(1341, '21'),
(589, '212-85-06'),
(590, '212-85-07'),
(591, '212-85-08'),
(1336, '215'),
(1238, '22-45'),
(614, '23'),
(1462, '24000'),
(1460, '25'),
(1327, '250'),
(779, '2542'),
(1233, '28000'),
(1145, '30'),
(714, '300'),
(187, '30000'),
(1286, '31'),
(1235, '35000'),
(340, '36'),
(207, '37'),
(1163, '3D-МОДЕЛ'),
(791, '4-20'),
(1262, '40'),
(17, '400'),
(1091, '45'),
(588, '495'),
(790, '5-79'),
(102, '500'),
(77, '5000'),
(792, '6-2'),
(1295, '6000'),
(789, '64'),
(39, '700'),
(206, '70000М2'),
(1282, '75-ОМ'),
(1261, '800'),
(718, '871'),
(399, '9000'),
(1315, 'BEVERAG'),
(1317, 'CONFER'),
(698, 'DEL'),
(1456, 'DGF'),
(592, 'EMAIL'),
(594, 'EXAMPL'),
(1465, 'GX-200'),
(1276, 'HERE'),
(1316, 'INDUSTRI'),
(593, 'INFO'),
(597, 'MARKET'),
(699, 'MOBIL'),
(763, 'N-ВИНИЛКАПРОЛАКТАМ'),
(600, 'PR'),
(1296, 'ROSUPACK'),
(753, 'RU2277566'),
(595, 'SALE'),
(697, 'SALON'),
(1434, 'SN-CSSS2000'),
(1498, 'SN-CSSS2001'),
(1499, 'SN-CSSS2002'),
(1275, 'TEXT'),
(704, 'VIP-КАБИНЕТ'),
(703, 'VIP-КАБИНЕТОВ'),
(1298, 'WATERSHOW'),
(992, 'АБСОЛЮТН'),
(434, 'АВИАЦИОН'),
(626, 'АВСТР'),
(1470, 'АВТОМАТ'),
(1476, 'АВТОМАТИЗИРОВА'),
(1435, 'АВТОМАТИЧЕСК'),
(742, 'АВТОР'),
(604, 'АВТОРИЗОВА'),
(1181, 'АВТОРСК'),
(1432, 'АВТОУСТАНОВК'),
(646, 'АДМИНИСТРАТИВН'),
(1257, 'АДРЕС'),
(1287, 'АКВАКУЛЬТУР'),
(767, 'АКРИЛОВ'),
(762, 'АКРИЛОНИТР'),
(655, 'АКСЕССУАР'),
(654, 'АКСЕССУАРОВ'),
(1223, 'АКТИВН'),
(861, 'АЛАДДИН'),
(457, 'АЛЕКСАНДР'),
(766, 'АММОН'),
(749, 'АН'),
(750, 'АНАТОЛЬЕВН'),
(856, 'АНГЛИЙСК'),
(469, 'АНДР'),
(615, 'АПРЕЛ'),
(14, 'АРЕНДУЕМ'),
(1333, 'АРМЕН'),
(411, 'АССОРТИМЕНТ'),
(858, 'АТМОСФЕР'),
(394, 'АУД'),
(1103, 'АУДИТОРН'),
(44, 'БАЗ'),
(1241, 'БАЗОВ'),
(1428, 'БАНК'),
(1096, 'БАР'),
(1095, 'БАРОВ'),
(1312, 'БЕЗАЛКОГОЛЬН'),
(389, 'БЕЗОПАСН'),
(612, 'БЕЛАРУС'),
(1334, 'БЕЛОРУС'),
(316, 'БЕСЦЕН'),
(1111, 'БИБЛИОТЕК'),
(190, 'БИЗНЕС'),
(1383, 'БИЗНЕС-КОНСУЛЬТАНТ'),
(921, 'БИЗНЕСМЕН'),
(920, 'БИЗНЕСМЕНОВ'),
(535, 'БЛАГОСОСТОЯН'),
(97, 'БЛИЖН'),
(1000, 'БЛОК'),
(811, 'БОГАТ'),
(814, 'БОГАТСТВ'),
(51, 'БОЛ'),
(215, 'БОЛЬШ'),
(1259, 'БОТИНСК'),
(1422, 'БРЯНСКПИВ'),
(583, 'БУД'),
(263, 'БУДЕТ'),
(280, 'БУДУТ'),
(1049, 'БУК'),
(1478, 'БУНКЕР'),
(1463, 'БУТ'),
(1307, 'БУТИЛИРОВА'),
(1416, 'БУТЫЛ'),
(1449, 'БУТЫЛК'),
(1294, 'БУТЫЛОК'),
(1236, 'БУХГАЛТЕР'),
(1240, 'БУХГАЛТЕРСК'),
(32, 'БЫСТР'),
(1406, 'БЫТОВ'),
(618, 'ВАЖН'),
(564, 'ВАЖНОСТ'),
(570, 'ВАКАНС'),
(1064, 'ВАЛЕНС'),
(244, 'ВАН'),
(295, 'ВАРИАНТ'),
(294, 'ВАРИАНТОВ'),
(1120, 'ВАС'),
(35, 'ВВЕД'),
(1284, 'ВВЦ'),
(1014, 'ВЕД'),
(1248, 'ВЕДЕН'),
(113, 'ВЕДУЩ'),
(1358, 'ВЕК'),
(989, 'ВЕЛИКОЛЕПН'),
(360, 'ВЕР'),
(605, 'ВЕРНУТ'),
(135, 'ВЗАИМОВЫГОДН'),
(508, 'ВЗГЛЯД'),
(507, 'ВЗГЛЯДОВ'),
(1369, 'ВЗРОСЛ'),
(554, 'ВЗЯТ'),
(840, 'ВИД'),
(427, 'ВИКТОР'),
(27, 'ВКЛАДЫВА'),
(705, 'ВКЛЮЧ'),
(771, 'ВКЛЮЧА'),
(470, 'ВЛАДИМИРОВИЧ'),
(1378, 'ВЛИЯН'),
(234, 'ВМЕСТ'),
(1005, 'ВМЕСТИТЕЛЬН'),
(543, 'ВНЕДРЕН'),
(1040, 'ВНЕСУТ'),
(839, 'ВНЕШН'),
(1067, 'ВНИМАН'),
(1143, 'ВНОС'),
(1308, 'ВОД'),
(1305, 'ВОДН'),
(1391, 'ВОДОПОДГОТОВК'),
(725, 'ВОДОСТОЙК'),
(448, 'ВОЗГЛАВЛЯ'),
(1480, 'ВОЗДУШН'),
(486, 'ВОЗМОЖН'),
(960, 'ВОЗНИКА'),
(351, 'ВОЗРАСТ'),
(350, 'ВОЗРАСТОВ'),
(968, 'ВОЙДЕТ'),
(907, 'ВОПЛОЩ'),
(568, 'ВОПРОС'),
(974, 'ВОПРОСОВ'),
(63, 'ВОРОНЕЖ'),
(436, 'ВОРОНЕЖСК'),
(967, 'ВОТ-ВОТ'),
(108, 'ВОШЛ'),
(687, 'ВПЕРВ'),
(923, 'ВПЕРЕД'),
(828, 'ВПЕЧАТЛЯЮЩ'),
(1101, 'ВПИСЫВА'),
(213, 'ВРЕМ'),
(926, 'ВРЕМЕН'),
(1025, 'ВРУЧН'),
(709, 'ВСЕГ'),
(503, 'ВСЕГД'),
(532, 'ВСЕМ'),
(266, 'ВСЕХ'),
(1466, 'ВСТРАИВА'),
(582, 'ВСТРЕЧ'),
(1107, 'ВСТРОЕН'),
(182, 'ВТОР'),
(601, 'ВХОД'),
(452, 'ВХОДЯ'),
(928, 'ВЫБОР'),
(283, 'ВЫБРА'),
(1501, 'ВЫВОД'),
(829, 'ВЫГЛЯД'),
(281, 'ВЫГЛЯДЕТ'),
(160, 'ВЫГРУЗК'),
(1497, 'ВЫДАВА'),
(1263, 'ВЫДУВ'),
(1436, 'ВЫДУВН'),
(1356, 'ВЫП'),
(300, 'ВЫПОЛН'),
(314, 'ВЫПОЛНЕН'),
(935, 'ВЫПОЛНЯ'),
(161, 'ВЫПУСК'),
(57, 'ВЫПУСКА'),
(816, 'ВЫРАЖ'),
(481, 'ВЫРАЖЕН'),
(257, 'ВЫСОК'),
(349, 'ВЫСОКОКЛАССН'),
(969, 'ВЫСОКОПОСТАВЛЕН'),
(544, 'ВЫСОКОПРОИЗВОДИТЕЛЬН'),
(362, 'ВЫСОЧАЙШ'),
(663, 'ВЫСТАВК'),
(82, 'ВЫСТАВОК'),
(1214, 'ВЫСШ'),
(1024, 'ВЫТОЧ'),
(87, 'ВЫХОД'),
(1405, 'ГАЗИРОВА'),
(336, 'ГАРАНТ'),
(1109, 'ГАРДЕРОБН'),
(454, 'ГЕНЕРАЛЬН'),
(1453, 'ГЕОМЕТР'),
(1392, 'ГЕРМАН'),
(804, 'ГЕРЦОГ'),
(496, 'ГИБК'),
(534, 'ГИГИЕНИЧЕСК'),
(1008, 'ГЛАВ'),
(222, 'ГЛАВН'),
(973, 'ГЛОБАЛЬН'),
(86, 'ГОД'),
(341, 'ГОРД'),
(1431, 'ГОРЛ'),
(738, 'ГОРЯЧ'),
(875, 'ГОСТЕВ'),
(242, 'ГОСТИН'),
(437, 'ГОСУДАРСТВЕН'),
(1496, 'ГОТ'),
(641, 'ГОТОВ'),
(325, 'ГРАМОТН'),
(720, 'ГРАНДИОЗН'),
(1228, 'ГРАФИК'),
(453, 'ГРУПП'),
(112, 'ДАН'),
(1084, 'ДАЧ'),
(23, 'ДВАДЦА'),
(1367, 'ДВЕ'),
(1280, 'ДВЕР'),
(987, 'ДВП'),
(637, 'ДВУХ'),
(890, 'ДВУХМЕСТН'),
(1442, 'ДВУХСТАДИЙН'),
(1359, 'ДЕЗИНФЕКЦ'),
(812, 'ДЕКОР'),
(1177, 'ДЕКОРИРОВАН'),
(359, 'ДЕЛ'),
(1069, 'ДЕЛА'),
(661, 'ДЕЛОВ'),
(648, 'ДЕМОНСТРИРОВА'),
(228, 'ДЕН'),
(883, 'ДЕР'),
(882, 'ДЕРЕВ'),
(20, 'ДЕРЕВООБРАБАТЫВА'),
(854, 'ДЕРЕВЯ'),
(109, 'ДЕСЯТК'),
(643, 'ДЕТ'),
(1471, 'ДЕТА'),
(271, 'ДЕТАЛ'),
(241, 'ДЕТСК'),
(983, 'ДЕШЕВ'),
(451, 'ДЕЯТЕЛЬН'),
(877, 'ДЖОКОНД'),
(1152, 'ДИАГНОСТИК'),
(1430, 'ДИАМЕТР'),
(795, 'ДИВА'),
(247, 'ДИЗАЙН'),
(254, 'ДИЗАЙН-ПРОЕКТ'),
(250, 'ДИЗАЙНЕР'),
(905, 'ДИЗАЙНЕРСК'),
(138, 'ДИЛЕРСК'),
(477, 'ДИНАМИЧН'),
(79, 'ДИПЛОМАНТ'),
(455, 'ДИРЕКТОР'),
(1255, 'ДНЕ'),
(559, 'ДНЯ'),
(509, 'ДОБ'),
(1060, 'ДОБАВ'),
(551, 'ДОВЕРИТЕЛЬН'),
(140, 'ДОГОВОР'),
(139, 'ДОГОВОРОВ'),
(1246, 'ДОКУМЕНТ'),
(368, 'ДОЛ'),
(1102, 'ДОЛГОВЕЧН'),
(491, 'ДОЛГОСРОЧН'),
(942, 'ДОЛЖНОСТН'),
(233, 'ДОМ'),
(1059, 'ДОМАШН'),
(634, 'ДОМОТЕХ'),
(774, 'ДОПОЛНИТЕЛЬН'),
(577, 'ДОСТАВК'),
(301, 'ДОСТАВЛ'),
(770, 'ДОСТИГА'),
(841, 'ДОСТИГНУТ'),
(484, 'ДОСТИЖЕН'),
(1007, 'ДОСТОИНСТВ'),
(1206, 'ДОХОД'),
(1045, 'ДРЕВЕСН'),
(378, 'ДРУГ'),
(1335, 'ДРУЗ'),
(986, 'ДСП'),
(1412, 'ЕВРАЗ'),
(106, 'ЕВРОПЕЙСК'),
(83, 'ЕВРОЭКСПОМЕБЕЛЬ-99'),
(745, 'ЕГОР'),
(505, 'ЕДИН'),
(379, 'ЕЖЕГОДН'),
(874, 'ЕЖЕДНЕВН'),
(152, 'ЕКАТЕРИНБУРГ'),
(994, 'ЕМ'),
(1459, 'ЕМКОСТ'),
(1349, 'ЕСТЕСТВЕН'),
(1346, 'ЖАЖД'),
(556, 'ЖДЕТ'),
(819, 'ЖЕЛАН'),
(1081, 'ЖЕЛЕЗ'),
(1237, 'ЖЕН'),
(850, 'ЖЕСТК'),
(1461, 'ЖИДКОСТ'),
(282, 'ЖИЗН'),
(488, 'ЖИЗНЕН'),
(1250, 'ЖУРНАЛОВ-ОРДЕР'),
(1249, 'ЖУРНАЛОВ-ОРДЕРОВ'),
(528, 'ЗАБОТ'),
(346, 'ЗАВЕДЕН'),
(142, 'ЗАВЕРШ'),
(1396, 'ЗАВОД'),
(68, 'ЗАВОЕВЫВА'),
(1446, 'ЗАГОТОВОК'),
(159, 'ЗАГРУЗК'),
(1454, 'ЗАДА'),
(576, 'ЗАДАН'),
(499, 'ЗАДАЧ'),
(299, 'ЗАКАЗ'),
(1147, 'ЗАКАЗА'),
(1199, 'ЗАКАЗОВ'),
(502, 'ЗАКАЗЧИК'),
(90, 'ЗАКЛЮЧ'),
(105, 'ЗАКЛЮЧА'),
(137, 'ЗАКЛЮЧЕН'),
(1221, 'ЗАКОНОДАТЕЛЬСТВ'),
(433, 'ЗАКОНЧ'),
(1337, 'ЗАЛ'),
(364, 'ЗАЛОГ'),
(252, 'ЗАМЕР'),
(467, 'ЗАМЕСТИТЕЛ'),
(1106, 'ЗАНИМА'),
(1399, 'ЗАПАСН'),
(1179, 'ЗАПРОС'),
(1495, 'ЗАПУСК'),
(1395, 'ЗАПУСТ'),
(198, 'ЗАПУЩ'),
(25, 'ЗАРАБОТА'),
(603, 'ЗАРЕГИСТРИРОВА'),
(1200, 'ЗАРПЛАТ'),
(98, 'ЗАРУБЕЖ'),
(418, 'ЗАРУБЕЖН'),
(420, 'ЗАСЛУГ'),
(547, 'ЗАТРАТ'),
(958, 'ЗАХОД'),
(1354, 'ЗАЧЕРПНУТ'),
(522, 'ЗАЩИТ'),
(569, 'ЗАЩИЩЕН'),
(707, 'ЗАЯВК'),
(695, 'ЗАЯВОК'),
(647, 'ЗДАН'),
(1035, 'ЗДОРОВ'),
(1078, 'ЗИМ'),
(1216, 'ЗНАН'),
(933, 'ЗНАЧ'),
(1362, 'ЗНАЧЕН'),
(59, 'ЗНАЧИТЕЛЬН'),
(818, 'ЗОЛОТ'),
(940, 'ЗОН'),
(409, 'ИГРОК'),
(408, 'ИГРОКОВ'),
(361, 'ИД'),
(1116, 'ИДЕАЛЬН'),
(1161, 'ИДЕИ-ОБРАЗ'),
(859, 'ИЗАБЕЛЛ'),
(330, 'ИЗБЕЖА'),
(1450, 'ИЗГОТАВЛИВА'),
(239, 'ИЗГОТОВ'),
(1473, 'ИЗГОТОВЛ'),
(6, 'ИЗГОТОВЛЕН'),
(377, 'ИЗДЕЛ'),
(1361, 'ИЗЛУЧЕН'),
(1164, 'ИЗОБРАЖЕН'),
(728, 'ИЗОБРЕТЕН'),
(1098, 'ИЗОПЛАСТ'),
(815, 'ИЗЫСК'),
(976, 'ИЗЫСКА'),
(805, 'ИЗЯЩН'),
(385, 'ИМЕЕТ'),
(298, 'ИМЕН'),
(192, 'ИНДИВИДУАЛЬН'),
(462, 'ИНЖЕНЕР-ТЕХНОЛОГ'),
(1222, 'ИНИЦИАТИВН'),
(439, 'ИНСТИТУТ'),
(483, 'ИНСТРУМЕНТ'),
(552, 'ИНТЕРЕС'),
(700, 'ИНТЕРЕСН'),
(255, 'ИНТЕРЬЕР'),
(955, 'ИНТЕРЬЕРН'),
(335, 'ИНТЕРЬЕРОВ'),
(398, 'ИС'),
(1050, 'ИСКЛЮЧЕН'),
(886, 'ИСПОЛНЕН'),
(950, 'ИСПОЛЬЗОВА'),
(318, 'ИСПОЛЬЗОВАН'),
(733, 'ИСПОЛЬЗУЕМ'),
(382, 'ИСПЫТАН'),
(1, 'ИСТОР'),
(1331, 'ИТАЛ'),
(459, 'ИЮН'),
(963, 'КАБИНЕТ'),
(225, 'КАЖД'),
(1329, 'КАЗАХСТА'),
(1393, 'КАНАД'),
(778, 'КАРБАМИДОМЕЛАМИНОФОРМАЛЬДЕГИДН'),
(759, 'КАРБАМИДОФОРМАЛЬДЕГИДН'),
(806, 'КАРКАС'),
(609, 'КАРТ'),
(944, 'КАТЕГОР'),
(1058, 'КАФ'),
(56, 'КАЧЕСТВ'),
(719, 'КВ'),
(550, 'КВАЛИФИКАЦ'),
(402, 'КВАЛИФИЦИРОВА'),
(768, 'КИСЛОТ'),
(1332, 'КИТ'),
(1390, 'КИТАЙСК'),
(855, 'КЛАССИЧЕСК'),
(757, 'КЛЕ'),
(727, 'КЛЕЕВ'),
(731, 'КЛЕЯ'),
(226, 'КЛИЕНТ'),
(1195, 'КЛИЕНТОВ'),
(1344, 'КОГДА-Т'),
(860, 'КОЖ'),
(1410, 'КОКА-КОЛ'),
(426, 'КОЛЕСНИК'),
(425, 'КОЛЕСНИКОВ'),
(934, 'КОЛЛЕГ'),
(100, 'КОЛЛЕКТ'),
(1182, 'КОЛЛЕКЦ'),
(1156, 'КОЛОРИСТК'),
(880, 'КОЛОРИТН'),
(896, 'КОМБИНАЦ'),
(1243, 'КОММУНИКАБЕЛЬН'),
(245, 'КОМНАТ'),
(1113, 'КОМОД'),
(1088, 'КОМПАКТН'),
(4, 'КОМПАН'),
(334, 'КОМПЛЕКСН'),
(166, 'КОМПЛЕКТ'),
(736, 'КОМПОЗИЦ'),
(787, 'КОМПОНЕНТ'),
(786, 'КОМПОНЕНТОВ'),
(909, 'КОМПОНОВК'),
(1272, 'КОМПРЕССОР'),
(1038, 'КОМПЬЮТЕР'),
(319, 'КОМПЬЮТЕРН'),
(194, 'КОМФОРТ'),
(862, 'КОМФОРТН'),
(1469, 'КОНВЕЙЕР'),
(1271, 'КОНВЕЙЕРН'),
(1468, 'КОНВЕЙЕРОВ'),
(1309, 'КОНГРЕСС'),
(287, 'КОНКРЕТН'),
(925, 'КОНКУРЕНТ'),
(685, 'КОНКУРЕНТН'),
(924, 'КОНКУРЕНТОВ'),
(671, 'КОНКУРС'),
(1384, 'КОНСТАНТИН'),
(1464, 'КОНСТРУКТИВН'),
(1190, 'КОНСТРУКТОР'),
(1115, 'КОНСТРУКЦ'),
(573, 'КОНСУЛЬТАЦ'),
(1194, 'КОНСУЛЬТИРОВАН'),
(571, 'КОНТАКТ'),
(1472, 'КОНТАКТИР'),
(93, 'КОНТРАКТ'),
(92, 'КОНТРАКТОВ'),
(309, 'КОНТРОЛ'),
(1401, 'КОНТРОЛИРУ'),
(894, 'КОНУСН'),
(129, 'КОНФЕРЕНЦ'),
(1104, 'КОНФЕРЕНЦ-КРЕСЕЛ'),
(155, 'КОНЦЕРН'),
(450, 'КООРДИНИР'),
(1403, 'КОРПОРАЦ'),
(144, 'КОРПУС'),
(221, 'КОРПУСН'),
(185, 'КОРПУСОВ'),
(526, 'КОРРЕКТИР'),
(1426, 'КОСМЕТИК'),
(133, 'КОТОР'),
(977, 'КРАСОТ'),
(1030, 'КРЕСЕЛ'),
(796, 'КРЕСЛ'),
(844, 'КРЕСТОВИН'),
(1380, 'КРИЗИС'),
(803, 'КРОВАТ'),
(1339, 'КРОКУС'),
(390, 'КРОМ'),
(1009, 'КРУГЛ'),
(220, 'КРУПН'),
(1108, 'КУП'),
(240, 'КУХН'),
(645, 'КУХОН'),
(384, 'ЛАБОРАТОР'),
(1355, 'ЛАДОН'),
(627, 'ЛАТВ'),
(889, 'ЛАУР'),
(1012, 'ЛЕГЕНДАРН'),
(1092, 'ЛЕГК'),
(403, 'ЛЕТ'),
(853, 'ЛИВЕРПУЛ'),
(119, 'ЛИДЕР'),
(118, 'ЛИДЕРОВ'),
(406, 'ЛИДИР'),
(1413, 'ЛИМОНАД'),
(157, 'ЛИН'),
(628, 'ЛИТВ'),
(1425, 'ЛИТР'),
(1424, 'ЛИТРОВ'),
(513, 'ЛИЧН'),
(1051, 'ЛИШ'),
(110, 'ЛУЧШ'),
(1018, 'ЛЮБ'),
(18, 'М2'),
(1213, 'МАГАЗИН'),
(66, 'МАГАЗИН-САЛОН'),
(179, 'МАГАЗИНОВ-САЛОН'),
(178, 'МАГАЗИНОВ-САЛОНОВ'),
(1117, 'МАКСИМАЛЬН'),
(444, 'МАЛ'),
(217, 'МАЛЕНЬК'),
(598, 'МАРКЕТИНГ'),
(691, 'МАРКЕТИНГОВ'),
(1320, 'МАРТ'),
(788, 'МАС'),
(1409, 'МАСЛ'),
(938, 'МАСС'),
(1048, 'МАССИВ'),
(863, 'МАССИВН'),
(372, 'МАССОВ'),
(665, 'МАСТЕР-КЛАСС'),
(15, 'МАСТЕРСК'),
(542, 'МАСТЕРСТВ'),
(667, 'МАСШТАБН'),
(1046, 'МАТЕРИА'),
(126, 'МАТЕРИАЛ'),
(125, 'МАТЕРИАЛОВ'),
(538, 'МАТЕРИАЛЬН'),
(1437, 'МАШИН'),
(1338, 'МВЦ'),
(7, 'МЕБЕЛ'),
(3, 'МЕБЕЛЬН'),
(170, 'МЕБЕЛЬПЛЮС'),
(81, 'МЕЖДУНАРОДН'),
(270, 'МЕЛЬЧАЙШ'),
(397, 'МЕНЕДЖМЕНТ'),
(527, 'МЕР'),
(599, 'МЕРОПРИЯТ'),
(1232, 'МЕС'),
(531, 'МЕСТ'),
(339, 'МЕСЯЦ'),
(338, 'МЕСЯЦЕВ'),
(1086, 'МЕТАЛЛИЧЕСК'),
(1218, 'МЕТОД'),
(1217, 'МЕТОДОВ'),
(872, 'МЕХАНИЗМ'),
(748, 'МЕЩЕРЯК'),
(747, 'МЕЩЕРЯКОВ'),
(1322, 'МИЛА'),
(1310, 'МИНЕРАЛЬН'),
(367, 'МИНИМАЛЬН'),
(966, 'МИНИМУМ'),
(823, 'МИНИСТР'),
(632, 'МИНСК'),
(1494, 'МИНУТ'),
(710, 'МИР'),
(88, 'МИРОВ'),
(475, 'МИСС'),
(746, 'МИХАЙЛОВИЧ'),
(292, 'МНОГ'),
(740, 'МНОГОКОМПОНЕНТН'),
(320, 'МНОГОЛЕТН'),
(679, 'МОД'),
(232, 'МОДЕЛ'),
(1131, 'МОДЕЛИРОВАН'),
(900, 'МОДЕРН'),
(42, 'МОДЕРНИЗАЦ'),
(1398, 'МОДЕРНИЗИРОВА'),
(776, 'МОДИФИКАТОР'),
(578, 'МОЖЕТ'),
(831, 'МОЖН'),
(30, 'МОЛОД'),
(354, 'МОЛОДЕЖ'),
(311, 'МОМЕНТ'),
(813, 'МОНАЛИЗ'),
(1455, 'МОНОБЛОК'),
(1127, 'МОНТАЖ'),
(537, 'МОРАЛЬН'),
(72, 'МОСКВ'),
(498, 'МОЩНОСТ'),
(1419, 'МПБК'),
(1247, 'МПЗ'),
(906, 'МЫСЛ'),
(169, 'МЯГК'),
(911, 'НАБОР'),
(1187, 'НАВЫК'),
(278, 'НАГЛЯДН'),
(494, 'НАДЕЖН'),
(1323, 'НАЗВА'),
(296, 'НАИБОЛ'),
(919, 'НАИЛУЧШ'),
(937, 'НАЙТ'),
(1138, 'НАКАНУН'),
(881, 'НАКЛАДК'),
(1486, 'НАЛАДК'),
(370, 'НАЛАЖ'),
(416, 'НАЛАЖИВА'),
(229, 'НАМ'),
(1314, 'НАПИТК'),
(1313, 'НАПИТКОВ'),
(412, 'НАРАЩИВА'),
(672, 'НАРОДН'),
(347, 'НАС'),
(447, 'НАСТОЯ'),
(101, 'НАСЧИТЫВА'),
(1170, 'НАТУРАЛИСТИЧН'),
(162, 'НАТУРАЛЬН'),
(887, 'НАХОД'),
(466, 'НАЧАЛЬНИК'),
(5, 'НАЧИНА'),
(971, 'НАЧНЕТ'),
(210, 'НАШ'),
(1256, 'НЕДЕЛ'),
(1382, 'НЕЗАВИСИМ'),
(1036, 'НЕЗАМЕНИМ'),
(1137, 'НЕКОТОР'),
(833, 'НЕМ'),
(912, 'НЕОБХОДИМ'),
(867, 'НЕОБЫКНОВЕН'),
(1070, 'НЕОБЫЧАЙН'),
(845, 'НЕОБЫЧН'),
(238, 'НЕПОВТОРИМ'),
(1134, 'НЕПОСРЕДСТВЕН'),
(1474, 'НЕРЖАВЕЮЩ'),
(825, 'НЕСМОТР'),
(248, 'НЕСТАНДАРТН'),
(173, 'НИЖН'),
(826, 'НИЗК'),
(1080, 'НИХ'),
(37, 'НОВ'),
(174, 'НОВГОРОД'),
(904, 'НОВИЗН'),
(649, 'НОВИНК'),
(607, 'НОВОСТ'),
(1001, 'НОГ'),
(996, 'НОЖК'),
(376, 'НОМЕНКЛАТУР'),
(388, 'НОРМ'),
(1397, 'НУЛ'),
(268, 'НЮАНС'),
(267, 'НЮАНСОВ'),
(1421, 'ОА'),
(1042, 'ОБАЯН'),
(237, 'ОБЕСПЕЧ'),
(536, 'ОБЕСПЕЧЕН'),
(519, 'ОБЕСПЕЧИВА'),
(668, 'ОБЕЩА'),
(809, 'ОБИВК'),
(651, 'ОБИВОЧН'),
(729, 'ОБЛАСТ'),
(908, 'ОБЛИК'),
(196, 'ОБНОВЛЕН'),
(1234, 'ОБОРОТ'),
(53, 'ОБОРУДОВАН'),
(1136, 'ОБРАБОТК'),
(1178, 'ОБРАЗ'),
(432, 'ОБРАЗОВАН'),
(676, 'ОБРАЗЦ'),
(675, 'ОБРАЗЦОВ'),
(572, 'ОБРАТ'),
(1484, 'ОБСЛУЖИВА'),
(1491, 'ОБСЛУЖИВАН'),
(1011, 'ОБСТАНОВК'),
(972, 'ОБСУЖДЕН'),
(487, 'ОБУСТРО'),
(939, 'ОБУСТРОЙСТВ'),
(1402, 'ОБУЧА'),
(1244, 'ОБУЧАЕМ'),
(344, 'ОБУЧЕН'),
(183, 'ОБЩ'),
(1193, 'ОБЩЕН'),
(357, 'ОБЪЕДИНЯ'),
(61, 'ОБЪЕМ'),
(276, 'ОБЪЕМН'),
(943, 'ОБЯЗАН'),
(555, 'ОБЯЗАТЕЛЬСТВ'),
(1126, 'ОГОВАРИВА'),
(1142, 'ОГОВОР'),
(1028, 'ОГРАНИЧЕН'),
(1288, 'ОДИН'),
(19, 'ОДН'),
(1302, 'ОЖИДА'),
(501, 'ОЖИДАН'),
(1352, 'ОЗЕР'),
(1023, 'ОКАНТОВК'),
(1202, 'ОКЛАД'),
(1326, 'ОКОЛ'),
(523, 'ОКРУЖА'),
(1387, 'ОО'),
(317, 'ОП'),
(995, 'ОПИРА'),
(754, 'ОПИСАН'),
(1149, 'ОПЛАЧИВА'),
(500, 'ОПРАВДЫВА'),
(289, 'ОПРЕДЕЛЕН'),
(930, 'ОПРЕДЕЛЯ'),
(980, 'ОПРОВЕРГА'),
(693, 'ОПРОС'),
(927, 'ОПТИМ'),
(261, 'ОПТИМАЛЬН'),
(546, 'ОПТИМИЗАЦ'),
(333, 'ОПТИМИЗИРОВА'),
(1129, 'ОПЫТН'),
(914, 'ОРГАНИЗАЦ'),
(120, 'ОРГАНИЗАЦИОН'),
(932, 'ОРГАНИЗОВА'),
(1062, 'ОРГАНИЧН'),
(1172, 'ОРИГИНАЛ'),
(838, 'ОРИГИНАЛЬН'),
(1366, 'ОРИЕНТИРОВОЧН'),
(286, 'ОСВЕЩЕН'),
(997, 'ОСВОБОЖД'),
(893, 'ОСН'),
(143, 'ОСНАЩЕН'),
(892, 'ОСНОВ'),
(560, 'ОСНОВАН'),
(901, 'ОСНОВН'),
(246, 'ОСОБ'),
(1033, 'ОСОБЕН'),
(1196, 'ОСТАВЛЕН'),
(1365, 'ОСТАВЛЯ'),
(1148, 'ОСТАЛЬН'),
(566, 'ОСТАН'),
(307, 'ОСУЩЕСТВЛЯ'),
(741, 'ОТВЕРДИТЕЛ'),
(586, 'ОТВЕТ'),
(514, 'ОТВЕТСТВЕН'),
(1139, 'ОТДЕЛ'),
(1155, 'ОТДЕЛК'),
(165, 'ОТДЕЛЬН'),
(1321, 'ОТЕЛ'),
(64, 'ОТКР'),
(167, 'ОТКРЫВА'),
(150, 'ОТКРЫТ'),
(1093, 'ОТЛИЧН'),
(553, 'ОТНОС'),
(136, 'ОТНОШЕН'),
(1118, 'ОТОБРАЖА'),
(619, 'ОТРАСЛ'),
(692, 'ОТЧЕТ'),
(1225, 'ОТЧЕТН'),
(1252, 'ОТЧЕТОВ'),
(10, 'ОФИС'),
(633, 'ОФИСКОМФОРТ'),
(798, 'ОФИСН'),
(9, 'ОФИСОВ'),
(84, 'ОФИЦИАЛЬН'),
(312, 'ОФОРМЛЕН'),
(1420, 'ОЧАКОВ'),
(1370, 'ОЧЕРЕДН'),
(1343, 'ОЧИСТК'),
(331, 'ОШИБОК'),
(962, 'ОЩУЩЕН'),
(1283, 'ПАВИЛЬОН'),
(1210, 'ПАКЕТ'),
(1055, 'ПАЛЕРМ'),
(1054, 'ПАЛИТР'),
(1270, 'ПАЛЛЕТООБМОТЧИК'),
(1489, 'ПАРАМЕТР'),
(1488, 'ПАРАМЕТРОВ'),
(1376, 'ПАРК'),
(1039, 'ПАРМ'),
(1029, 'ПАРТ'),
(131, 'ПАРТНЕР'),
(130, 'ПАРТНЕРОВ'),
(200, 'ПАРТНЕРСТВ'),
(751, 'ПАТЕНТ'),
(11, 'ПЕРВ'),
(1245, 'ПЕРВИЧН'),
(681, 'ПЕРЕГОВОР'),
(959, 'ПЕРЕГОВОРОВ'),
(946, 'ПЕРЕГОРОДК'),
(478, 'ПЕРЕД'),
(1128, 'ПЕРЕДА'),
(315, 'ПЕРЕДОВ'),
(1159, 'ПЕРЕКЛЮЧЕН'),
(1154, 'ПЕРЕПЛАНИРОВК'),
(1157, 'ПЕРЕСТАНОВК'),
(50, 'ПЕРЕШЛ'),
(929, 'ПЕРСОНА'),
(492, 'ПЕРСПЕКТИВ'),
(458, 'ПЕТРОВИЧ'),
(1311, 'ПИТЬЕВ'),
(1186, 'ПК'),
(1006, 'ПЛАВН'),
(1226, 'ПЛАН'),
(1417, 'ПЛАНИР'),
(1082, 'ПЛАСТИК'),
(1100, 'ПЛАСТИКОВ'),
(1112, 'ПЛАТЯН'),
(897, 'ПЛОТН'),
(16, 'ПЛОЩАД'),
(1281, 'ПЛОЩАДК'),
(1031, 'ПЛУТОН'),
(1443, 'ПНЕВМАТИЧЕСК'),
(1452, 'ПОВТОРЯ'),
(54, 'ПОВЫС'),
(515, 'ПОВЫШЕН'),
(708, 'ПОДА'),
(1479, 'ПОДАЧ'),
(1168, 'ПОДБИРА'),
(1175, 'ПОДБОР'),
(659, 'ПОДГОТОВ'),
(443, 'ПОДГОТОВК'),
(690, 'ПОДГОТОВЛ'),
(421, 'ПОДГОТОВЛЕН'),
(851, 'ПОДДЕРЖИВА'),
(822, 'ПОДЛОКОТНИК'),
(846, 'ПОДЛОКОТНИКОВ'),
(73, 'ПОДМОСКОВ'),
(1373, 'ПОДМОСКОВН'),
(256, 'ПОДОБРА'),
(1094, 'ПОДОЙДЕТ'),
(1071, 'ПОДОЙДУТ'),
(1253, 'ПОДРАЗДЕЛЕН'),
(224, 'ПОДХОД'),
(297, 'ПОДХОДЯ'),
(1123, 'ПОЖЕЛАН'),
(29, 'ПОЗВОЛ'),
(323, 'ПОЗВОЛЯ'),
(852, 'ПОЗВОНОЧНИК'),
(407, 'ПОЗИЦ'),
(677, 'ПОЗНАКОМ'),
(608, 'ПОИСК'),
(666, 'ПОИСТИН'),
(356, 'ПОКОЛЕН'),
(574, 'ПОКУПК'),
(998, 'ПОЛЕЗН'),
(1441, 'ПОЛИПРОПИЛЕНОВ'),
(438, 'ПОЛИТЕХНИЧЕСК'),
(423, 'ПОЛИТИК'),
(801, 'ПОЛК'),
(954, 'ПОЛН'),
(1475, 'ПОЛНОСТ'),
(1500, 'ПОЛУАВТОМАТИЧЕСК'),
(208, 'ПОЛУТОР'),
(400, 'ПОЛУЧ'),
(730, 'ПОЛУЧЕН'),
(1185, 'ПОЛЬЗОВАТЕЛ'),
(629, 'ПОЛЬШ'),
(253, 'ПОМЕЩЕН'),
(656, 'ПОМИМ'),
(260, 'ПОМОГУТ'),
(1010, 'ПОМОЖЕТ'),
(585, 'ПОМОЧ'),
(830, 'ПОМОЩ'),
(563, 'ПОНИМА'),
(512, 'ПОНИМАН'),
(1020, 'ПОНЯТ'),
(1099, 'ПОПУЛЯРН'),
(1357, 'ПОР'),
(899, 'ПОРОЛОН'),
(898, 'ПОРОЛОНОВ'),
(713, 'ПОСЕТ'),
(694, 'ПОСЕТИТЕЛ'),
(1303, 'ПОСЕЩА'),
(1125, 'ПОСЛ'),
(516, 'ПОСЛЕД'),
(275, 'ПОСМОТРЕТ'),
(94, 'ПОСТАВК'),
(497, 'ПОСТАВЩИК'),
(1289, 'ПОСТАВЩИКОВ'),
(49, 'ПОСТЕПЕН'),
(375, 'ПОСТОЯ'),
(1133, 'ПОСТУПА'),
(1026, 'ПОТ'),
(504, 'ПОТРЕБИТЕЛ'),
(953, 'ПОТРЕБН'),
(847, 'ПОТРЯСА'),
(581, 'ПОЧТ'),
(1440, 'ПП'),
(1260, 'ПР-Д'),
(223, 'ПРАВ'),
(521, 'ПРАВИЛ'),
(931, 'ПРАВИЛЬН'),
(682, 'ПРАКТИЧЕСК'),
(358, 'ПРЕДАН'),
(1162, 'ПРЕДВАРИТЕЛЬН'),
(1348, 'ПРЕДК'),
(1347, 'ПРЕДКОВ'),
(1166, 'ПРЕДЛАГ'),
(493, 'ПРЕДЛАГА'),
(683, 'ПРЕДЛОЖЕН'),
(644, 'ПРЕДМЕТ'),
(737, 'ПРЕДНАЗНАЧ'),
(1144, 'ПРЕДОПЛАТ'),
(291, 'ПРЕДОСТАВЛ'),
(485, 'ПРЕДОСТАВЛЯ'),
(711, 'ПРЕДПОЛАГА'),
(45, 'ПРЕДПРИЯТ'),
(1173, 'ПРЕДСТАВ'),
(146, 'ПРЕДСТАВИТЕЛЬСТВ'),
(642, 'ПРЕДСТАВЛ'),
(277, 'ПРЕДСТАВЛЕН'),
(964, 'ПРЕЗИДЕНТ'),
(686, 'ПРЕИМУЩЕСТВ'),
(1083, 'ПРЕКРАСН'),
(1204, 'ПРЕМ'),
(965, 'ПРЕМЬЕР-МИНИСТР'),
(1153, 'ПРЕОБРАЗОВАН'),
(1451, 'ПРЕСС-ФОРМ'),
(739, 'ПРЕССОВАН'),
(884, 'ПРЕСТИЖ'),
(80, 'ПРЕСТИЖН'),
(1447, 'ПРЕФОРМ'),
(518, 'ПРИБ'),
(558, 'ПРИБЫЛЬН'),
(1239, 'ПРИВЕТСВ'),
(1066, 'ПРИВЛЕКА'),
(979, 'ПРИВЛЕКАТЕЛЬН'),
(549, 'ПРИВЛЕЧЕН'),
(922, 'ПРИВЫКЛ'),
(1301, 'ПРИГЛАША'),
(1377, 'ПРИГЛАШЕН'),
(866, 'ПРИДА'),
(1119, 'ПРИЕЗЖА'),
(313, 'ПРИЕМ'),
(1477, 'ПРИЕМН'),
(1325, 'ПРИЕХА'),
(85, 'ПРИЗНАН'),
(366, 'ПРИМЕНЕН'),
(764, 'ПРИМЕНЯ'),
(567, 'ПРИМЕР'),
(837, 'ПРИМЕЧАТЕЛЬН'),
(622, 'ПРИНИМА'),
(1268, 'ПРИНТЕРЫ-ДАТИРОВЩИК'),
(636, 'ПРИНЦИП'),
(1291, 'ПРИНЯ'),
(202, 'ПРИНЯТ'),
(154, 'ПРИОБРЕТА'),
(596, 'ПРИОБРЕТЕН'),
(1075, 'ПРИРОДН'),
(941, 'ПРИСПОСОБ'),
(952, 'ПРИСПОСОБЛЕН'),
(122, 'ПРИСТУП'),
(1483, 'ПРИСУТСТВ'),
(1110, 'ПРИХОЖ'),
(777, 'ПРИЧ'),
(1324, 'ПРИЯТН'),
(128, 'ПРОВЕД'),
(712, 'ПРОВЕДЕН'),
(1381, 'ПРОВЕЛ'),
(680, 'ПРОВЕСТ'),
(721, 'ПРОВОД'),
(442, 'ПРОГРАММ'),
(1388, 'ПРОДА'),
(1183, 'ПРОДАВЕЦ-ДИЗАЙНЕР'),
(1203, 'ПРОДАЖ'),
(279, 'ПРОДЕМОНСТРИР'),
(195, 'ПРОДОЛЖА'),
(58, 'ПРОДУКЦ'),
(193, 'ПРОЕКТ'),
(332, 'ПРОЕКТИРОВАН'),
(1198, 'ПРОЕКТОВ'),
(1363, 'ПРОЖ'),
(1121, 'ПРОИЗВЕДЕТ'),
(251, 'ПРОИЗВЕДУТ'),
(324, 'ПРОИЗВЕСТ'),
(1027, 'ПРОИЗВОД'),
(520, 'ПРОИЗВОДИМ'),
(111, 'ПРОИЗВОДИТЕЛ'),
(545, 'ПРОИЗВОДИТЕЛЬН'),
(184, 'ПРОИЗВОДСВЕННО-СКЛАДСК'),
(12, 'ПРОИЗВОДСТВ'),
(43, 'ПРОИЗВОДСТВЕН'),
(985, 'ПРОИСХОД'),
(1318, 'ПРОЙДУТ'),
(735, 'ПРОМЫШЛЕН'),
(269, 'ПРОРИСОВК'),
(1015, 'ПРОСТ'),
(820, 'ПРОСТЕЖК'),
(1485, 'ПРОСТОТ'),
(490, 'ПРОСТРАНСТВ'),
(755, 'ПРОТОТИП'),
(343, 'ПРОФЕССИОНАЛЬН'),
(380, 'ПРОХОД'),
(371, 'ПРОЦЕСС'),
(797, 'ПРОЧ'),
(724, 'ПРОЧН'),
(342, 'ПРОШЕДШ'),
(441, 'ПРОШЕЛ'),
(214, 'ПРОШЛ'),
(895, 'ПРУЖИН'),
(216, 'ПУТ'),
(1293, 'ПЭТ'),
(1458, 'ПЭТ-БУТЫЛОК'),
(1264, 'ПЭТ-ТАР'),
(631, 'ПЯТ'),
(231, 'ПЯТИСОТ'),
(1438, 'РN-PP'),
(304, 'РАБОТ'),
(22, 'РАБОТА'),
(511, 'РАБОТНИК'),
(510, 'РАБОТНИКОВ'),
(1034, 'РАБОТОСПОСОБН'),
(489, 'РАБОЧ'),
(584, 'РАД'),
(471, 'РАДИОТЕХНИК'),
(723, 'РАЗ'),
(175, 'РАЗВИВА'),
(28, 'РАЗВИТ'),
(744, 'РАЗИНЬК'),
(743, 'РАЗИНЬКОВ'),
(293, 'РАЗЛИЧН'),
(249, 'РАЗМЕР'),
(716, 'РАЗМЕСТ'),
(285, 'РАЗН'),
(660, 'РАЗНООБРАЗН'),
(1445, 'РАЗОГРЕТ'),
(230, 'РАЗРАБОТА'),
(575, 'РАЗРАБОТК'),
(1386, 'РАК'),
(1385, 'РАКОВ'),
(662, 'РАМК'),
(876, 'РАСКЛАДУШК'),
(1135, 'РАСКР'),
(158, 'РАСКРО'),
(817, 'РАСПИСЫВА'),
(13, 'РАСПОЛАГА'),
(262, 'РАСПОЛОЖ'),
(290, 'РАСПОРЯЖЕН'),
(1174, 'РАССМОТРЕТ'),
(1176, 'РАССТАНОВК'),
(31, 'РАСТ'),
(782, 'РАСТВОР'),
(328, 'РАСХОД'),
(326, 'РАСЧЕТ'),
(1132, 'РАСЧЕТОВ'),
(145, 'РАСШИР'),
(657, 'РАСШИРЕН'),
(410, 'РАСШИРЯ'),
(456, 'РАТЧЕНК'),
(949, 'РАЦИОНАЛЬН'),
(69, 'РЕГИОН'),
(151, 'РЕГИОНАЛЬН'),
(415, 'РЕГИОНОВ'),
(1160, 'РЕЖИМ'),
(132, 'РЕЗУЛЬТАТ'),
(517, 'РЕИНВЕСТИРОВАН'),
(1351, 'РЕК'),
(1274, 'РЕСИВЕР'),
(670, 'РЕСПУБЛИКАНСК'),
(769, 'РЕША'),
(203, 'РЕШЕН'),
(468, 'РОГОВ'),
(429, 'РОД'),
(1265, 'РОЗЛИВ'),
(1215, 'РОЗНИЦ'),
(176, 'РОЗНИЧН'),
(870, 'РОСКОШ'),
(808, 'РОСКОШН'),
(807, 'РОСПИС'),
(76, 'РОСС'),
(147, 'РОССИЙСК'),
(562, 'РОСТ'),
(1278, 'РОСУПАК'),
(1277, 'РОСУПАК-2014'),
(1208, 'РУБ'),
(149, 'РУБЕЖ'),
(1189, 'РУК'),
(824, 'РУКОВОДИТЕЛ'),
(401, 'РУКОВОДСТВ'),
(422, 'РУКОВОДЯ'),
(1353, 'РУЧ'),
(1433, 'РУЧК'),
(369, 'РУЧН'),
(1212, 'РФ'),
(148, 'РЫНК'),
(89, 'РЫНОК'),
(91, 'РЯД'),
(706, 'САД'),
(602, 'САЙТ'),
(115, 'САЛОН'),
(114, 'САЛОНОВ'),
(1242, 'САМОСТОЯТЕЛЬН'),
(533, 'САНИТАРН'),
(172, 'САНКТ-ПЕТЕРБУРГ'),
(1273, 'САТУРАТОР'),
(305, 'СБОРК'),
(913, 'СВЕЖ'),
(525, 'СВОЕВРЕМЕН'),
(624, 'СВЫШ'),
(1141, 'СВЯЖУТ'),
(417, 'СВЯЗ'),
(288, 'СДЕЛА'),
(329, 'СЕБЕСТОИМ'),
(75, 'СЕВЕРО-ЗАПАД'),
(201, 'СЕГОДН'),
(227, 'СЕГОДНЯШН'),
(1415, 'СЕЙЧАС'),
(640, 'СЕКТОР'),
(639, 'СЕКТОРОВ'),
(664, 'СЕМИНАР'),
(430, 'СЕНТЯБР'),
(903, 'СЕР'),
(464, 'СЕРГ'),
(386, 'СЕРТИФИКАТ'),
(381, 'СЕРТИФИКАЦИОН'),
(177, 'СЕТ'),
(1414, 'СЖАТ'),
(70, 'СИБИР'),
(1052, 'СИДЕН'),
(1079, 'СИДЕТ'),
(993, 'СИММЕТРИЧ'),
(772, 'СИНТЕТИЧЕСК'),
(506, 'СИСТ'),
(396, 'СИСТЕМ'),
(540, 'СИСТЕМАТИЧЕСК'),
(999, 'СИСТЕМН'),
(1097, 'СИТ'),
(124, 'СКЛАД'),
(1073, 'СКЛАДН'),
(204, 'СКЛАДСК'),
(888, 'СЛЕВ'),
(784, 'СЛЕД'),
(1089, 'СЛОЖ'),
(1114, 'СЛОЖН'),
(1003, 'СЛУЖ'),
(274, 'СМОЖЕТ'),
(760, 'СМОЛ'),
(674, 'СМОТР'),
(96, 'СНГ'),
(548, 'СНИЖЕН'),
(327, 'СНИЗ'),
(414, 'СО'),
(479, 'СОБ'),
(1285, 'СОБРА'),
(957, 'СОБРАН'),
(41, 'СОБСТВЕН'),
(424, 'СОБСТВЕННИК'),
(1304, 'СОБЫТ'),
(52, 'СОВЕРШЕН'),
(524, 'СОВЕРШЕНСТВОВАН'),
(1022, 'СОВЕСТ'),
(1013, 'СОВЕЩАТЕЛЬН'),
(272, 'СОВМЕСТН'),
(1205, 'СОВОКУПН'),
(956, 'СОВРЕМЕН'),
(773, 'СОГЛАСН'),
(1019, 'СОГЛАСУ'),
(561, 'СОДЕЙСТВ'),
(775, 'СОДЕРЖ'),
(726, 'СОЕДИНЕН'),
(264, 'СОЗДА'),
(529, 'СОЗДАН'),
(918, 'СОЗДАСТ'),
(1063, 'СОЛЬЮТ'),
(387, 'СООТВЕТСТВ'),
(785, 'СООТНОШЕН'),
(761, 'СОПОЛИМЕР'),
(205, 'СОСТАВ'),
(186, 'СОСТАВЛЯ'),
(732, 'СОСТАВОВ'),
(617, 'СОСТО'),
(758, 'СОСТОЯ'),
(104, 'СОТРУДНИК'),
(103, 'СОТРУДНИКОВ'),
(1418, 'СОТРУДНИЧЕСТВ'),
(891, 'СОФ'),
(404, 'СОХРАН'),
(988, 'СОХРАНЯ'),
(1209, 'СОЦ'),
(565, 'СОЦИАЛЬН'),
(910, 'СОЧЕТА'),
(915, 'СОЧЕТАН'),
(1371, 'СОЮЗ'),
(702, 'СПАЛ'),
(243, 'СПАЛЬН'),
(1151, 'СПЕКТР'),
(383, 'СПЕЦИАЛИЗИРОВА'),
(322, 'СПЕЦИАЛИСТ'),
(321, 'СПЕЦИАЛИСТОВ'),
(461, 'СПЕЦИАЛЬН'),
(821, 'СПИНК'),
(1002, 'СПРАВ'),
(1350, 'СПУСТ'),
(1065, 'СРАЗ'),
(392, 'СРЕД'),
(445, 'СРЕДН'),
(26, 'СРЕДСТВ'),
(303, 'СРОК'),
(1487, 'СТАБИЛЬН'),
(40, 'СТАВШ'),
(46, 'СТАЛ'),
(363, 'СТАНДАРТ'),
(21, 'СТАНК'),
(673, 'СТАНОВ'),
(355, 'СТАРШ'),
(669, 'СТАТ'),
(1292, 'СТЕНД'),
(419, 'СТЕПЕН'),
(857, 'СТИЛ'),
(991, 'СТИЛЬН'),
(827, 'СТОИМОСТ'),
(961, 'СТОЙК'),
(799, 'СТОЛ'),
(978, 'СТОЛОВ'),
(374, 'СТОРОН'),
(95, 'СТРАН'),
(606, 'СТРАНИЦ'),
(476, 'СТРАТЕГ'),
(236, 'СТРЕМ'),
(635, 'СТРО'),
(308, 'СТРОГ'),
(734, 'СТРОИТЕЛЬН'),
(123, 'СТРОИТЕЛЬСТВ'),
(865, 'СТРОЧК'),
(800, 'СТУЛ'),
(1057, 'СТУЛЬЕВ'),
(1467, 'СТЫКОВК'),
(1146, 'СУММ'),
(1364, 'СУТОК'),
(211, 'СУЩЕСТВ'),
(1191, 'СФЕР'),
(1171, 'СХОЖЕСТ'),
(842, 'СЧЕТ'),
(1076, 'СЧИТА'),
(446, 'США'),
(116, 'ТАКЖ'),
(1423, 'ТАР'),
(885, 'ТАХТ'),
(849, 'ТВЕРД'),
(273, 'ТВОРЧЕСТВ'),
(620, 'ТЕКУЩ'),
(1368, 'ТЕЛ'),
(579, 'ТЕЛЕФОН'),
(235, 'ТЕМ'),
(638, 'ТЕМАТИЧЕСК'),
(33, 'ТЕМП'),
(678, 'ТЕНДЕНЦ'),
(1077, 'ТЕПЛ'),
(180, 'ТЕРРИТОР'),
(1037, 'ТЕХ'),
(1408, 'ТЕХЖИДК'),
(472, 'ТЕХНИЧЕСК'),
(188, 'ТЕХНОЛОГ'),
(495, 'ТЕХНОЛОГИЧН'),
(701, 'ТИП'),
(1448, 'ТИПОРАЗМЕР'),
(1211, 'ТК'),
(652, 'ТКАН'),
(391, 'ТОГ'),
(1427, 'ТОЛСТОСТЕН'),
(832, 'ТОМ'),
(1053, 'ТОНИРОВК'),
(218, 'ТОРГОВ'),
(302, 'ТОЧН'),
(258, 'ТОЧНОСТ'),
(48, 'ТРАДИЦ'),
(1290, 'ТРАДИЦИОН'),
(1481, 'ТРАНСПОРТЕР'),
(1140, 'ТРАНСПОРТН'),
(873, 'ТРАНСФОРМАЦ'),
(1482, 'ТРЕБ'),
(395, 'ТРЕБОВАН'),
(557, 'ТРЕБОВАТЕЛЬН'),
(199, 'ТРЕТ'),
(1389, 'ТРЕХ'),
(1130, 'ТРЕХМЕРН'),
(1306, 'ТРИНАДЦАТ'),
(1016, 'ТРОН'),
(348, 'ТРУД'),
(1220, 'ТРУДОВ'),
(1227, 'ТРУДОУСТРОЙСТВ'),
(948, 'ТУМБ'),
(209, 'ТЫСЯЧ'),
(1300, 'УВАЖА'),
(60, 'УВЕЛИЧ'),
(1429, 'УВЕЛИЧЕН'),
(413, 'УВЕЛИЧИВА'),
(1184, 'УВЕРЕН'),
(684, 'УВИДЕТ'),
(156, 'УГЛОВ'),
(916, 'УДАЧН'),
(975, 'УДИВИТЕЛЬН'),
(1087, 'УДОБ'),
(1004, 'УДОБН'),
(1490, 'УДОБСТВ'),
(539, 'УДОВЛЕТВОРЕН'),
(1330, 'УЗБЕКИСТА'),
(1254, 'УКАЗАН'),
(197, 'УКОМПЛЕКТОВА'),
(630, 'УКРАИН'),
(879, 'УКРАШЕН'),
(134, 'УКРЕПЛЕН'),
(1457, 'УКУПОРИВАН'),
(1266, 'УКУПОРОЧН'),
(1394, 'УМЕ'),
(984, 'УМЕНЬШЕН'),
(1056, 'УНИВЕРСАЛЬН'),
(473, 'УНИВЕРСИТЕТ'),
(951, 'УНИКАЛЬН'),
(1269, 'УПАКОВОЧН'),
(1219, 'УПРАВЛЕН'),
(1017, 'УПРАВЛЕНЦ'),
(449, 'УПРАВЛЯ'),
(405, 'УПРОЧ'),
(71, 'УРА'),
(55, 'УРОВЕН'),
(541, 'УРОВН'),
(530, 'УСЛОВ'),
(610, 'УСЛУГ'),
(365, 'УСПЕХ'),
(189, 'УСПЕШН'),
(835, 'УСТАЛ'),
(871, 'УСТАНОВ'),
(306, 'УСТАНОВК'),
(981, 'УСТОЯ'),
(982, 'УТВЕРЖДЕН'),
(1345, 'УТОЛ'),
(1360, 'УФ'),
(623, 'УЧАСТ'),
(689, 'УЧАСТНИК'),
(688, 'УЧАСТНИКОВ'),
(345, 'УЧЕБН'),
(265, 'УЧЕТ'),
(1122, 'УЧИТЫВ'),
(1061, 'УЮТ'),
(1041, 'УЮТН'),
(168, 'ФАБРИК'),
(1072, 'ФАЗЕНД'),
(587, 'ФАКС'),
(1165, 'ФАКТУР'),
(435, 'ФАКУЛЬТЕТ'),
(752, 'ФЕДЕРАЦ'),
(428, 'ФЕДОРОВИЧ'),
(1375, 'ФЕСТИВАЛ'),
(1032, 'ФИЗИОЛОГИЧЕСК'),
(127, 'ФИЛИА'),
(1342, 'ФИЛЬТР'),
(1379, 'ФИНАНСОВ'),
(1224, 'ФИНАНСОВО-ХОЗЯЙСТВЕН'),
(219, 'ФИРМ'),
(65, 'ФИРМЕН'),
(878, 'ФЛОР'),
(465, 'ФОМИЧ'),
(1374, 'ФОРЕСТ'),
(1068, 'ФОРМ'),
(164, 'ФОРМИР'),
(1251, 'ФОРМИРОВАН'),
(1444, 'ФОРМОВАН'),
(611, 'ФОРУМ'),
(936, 'ФУНКЦ'),
(1158, 'ФУНКЦИОНАЛЬН'),
(650, 'ФУРНИТУР'),
(373, 'ХАРАКТЕР'),
(902, 'ХАРАКТЕРИСТИК'),
(1407, 'ХИМ'),
(1074, 'ХЛОПК'),
(765, 'ХЛОРИСТ'),
(47, 'ХОРОШ'),
(1105, 'ХРАН'),
(1090, 'ХРАНЕН'),
(836, 'ХРОМ'),
(843, 'ХРОМИРОВА'),
(1197, 'ХУДОЖЕСТВЕННО-КОНСТРУКТОРСК'),
(259, 'ЦВЕТ'),
(1169, 'ЦВЕТН'),
(917, 'ЦВЕТОВ'),
(482, 'ЦЕЛ'),
(1047, 'ЦЕЛИК'),
(352, 'ЦЕН'),
(38, 'ЦЕХ'),
(1231, 'ЧАС'),
(1400, 'ЧАСТ'),
(1404, 'ЧАСТН'),
(24, 'ЧЕЛОВЕК'),
(1229, 'ЧЕР'),
(1492, 'ЧЕРЕЗ'),
(1188, 'ЧЕРЧЕН'),
(1021, 'ЧЕСТ'),
(970, 'ЧИНОВНИК'),
(117, 'ЧИСЛ'),
(1044, 'ЧИСТ'),
(834, 'ЧУВСТВУ'),
(67, 'ШАГ'),
(868, 'ШАРМ'),
(1258, 'ШАТУР'),
(1150, 'ШИРОК'),
(947, 'ШКАФ'),
(8, 'ШКОЛ'),
(163, 'ШПОН'),
(783, 'ЩАВЕЛЕВ'),
(1411, 'ЭЙЧБИС'),
(1043, 'ЭКОЛОГИЧЕСК'),
(1180, 'ЭКСКЛЮЗИВН'),
(1340, 'ЭКСП'),
(1167, 'ЭКСПЕРИМЕНТИРОВА'),
(36, 'ЭКСПЛУАТАЦ'),
(990, 'ЭКСПЛУАТАЦИОН'),
(621, 'ЭКСПОЗИЦ'),
(658, 'ЭКСПОФОРУМ'),
(869, 'ЭЛЕГАНТН'),
(580, 'ЭЛЕКТРОН'),
(284, 'ЭЛЕМЕНТ'),
(653, 'ЭЛЕМЕНТОВ'),
(864, 'ЭЛИТН'),
(353, 'ЭНТУЗИАЗМ'),
(1085, 'ЭР'),
(848, 'ЭРГОНОМИК'),
(945, 'ЭРГОНОМИЧН'),
(1124, 'ЭСКИЗН'),
(310, 'ЭТАП'),
(1192, 'ЭТИК'),
(1267, 'ЭТИКЕРОВЩИК'),
(463, 'ЭФФЕКТИВН'),
(74, 'ЮГ'),
(756, 'ЯВЛЯ'),
(810, 'ЯРК'),
(480, 'ЯСН'),
(802, 'ЯЩИК');

-- --------------------------------------------------------

--
-- Table structure for table `b_search_suggest`
--

CREATE TABLE IF NOT EXISTS `b_search_suggest` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `FILTER_MD5` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `PHRASE` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `RATE` float NOT NULL,
  `TIMESTAMP_X` datetime NOT NULL,
  `RESULT_COUNT` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IND_B_SEARCH_SUGGEST` (`FILTER_MD5`,`PHRASE`(50),`RATE`),
  KEY `IND_B_SEARCH_SUGGEST_PHRASE` (`PHRASE`(50),`RATE`),
  KEY `IND_B_SEARCH_SUGGEST_TIME` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_search_tags`
--

CREATE TABLE IF NOT EXISTS `b_search_tags` (
  `SEARCH_CONTENT_ID` int(11) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`SEARCH_CONTENT_ID`,`SITE_ID`,`NAME`),
  KEY `IX_B_SEARCH_TAGS_0` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci DELAY_KEY_WRITE=1;

-- --------------------------------------------------------

--
-- Table structure for table `b_search_user_right`
--

CREATE TABLE IF NOT EXISTS `b_search_user_right` (
  `USER_ID` int(11) NOT NULL,
  `GROUP_CODE` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `UX_B_SEARCH_USER_RIGHT` (`USER_ID`,`GROUP_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_security_sitecheck`
--

CREATE TABLE IF NOT EXISTS `b_security_sitecheck` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TEST_DATE` datetime DEFAULT NULL,
  `RESULTS` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_filter_mask`
--

CREATE TABLE IF NOT EXISTS `b_sec_filter_mask` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SORT` int(11) NOT NULL DEFAULT '10',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILTER_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIKE_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREG_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_frame_mask`
--

CREATE TABLE IF NOT EXISTS `b_sec_frame_mask` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SORT` int(11) NOT NULL DEFAULT '10',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FRAME_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIKE_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREG_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_iprule`
--

CREATE TABLE IF NOT EXISTS `b_sec_iprule` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RULE_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'M',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `ADMIN_SECTION` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '500',
  `ACTIVE_FROM` datetime DEFAULT NULL,
  `ACTIVE_FROM_TIMESTAMP` int(11) DEFAULT NULL,
  `ACTIVE_TO` datetime DEFAULT NULL,
  `ACTIVE_TO_TIMESTAMP` int(11) DEFAULT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_sec_iprule_active_to` (`ACTIVE_TO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_iprule_excl_ip`
--

CREATE TABLE IF NOT EXISTS `b_sec_iprule_excl_ip` (
  `IPRULE_ID` int(11) NOT NULL,
  `RULE_IP` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '500',
  `IP_START` bigint(18) DEFAULT NULL,
  `IP_END` bigint(18) DEFAULT NULL,
  PRIMARY KEY (`IPRULE_ID`,`RULE_IP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_iprule_excl_mask`
--

CREATE TABLE IF NOT EXISTS `b_sec_iprule_excl_mask` (
  `IPRULE_ID` int(11) NOT NULL,
  `RULE_MASK` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `LIKE_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREG_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IPRULE_ID`,`RULE_MASK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_iprule_incl_ip`
--

CREATE TABLE IF NOT EXISTS `b_sec_iprule_incl_ip` (
  `IPRULE_ID` int(11) NOT NULL,
  `RULE_IP` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SORT` int(11) NOT NULL DEFAULT '500',
  `IP_START` bigint(18) DEFAULT NULL,
  `IP_END` bigint(18) DEFAULT NULL,
  PRIMARY KEY (`IPRULE_ID`,`RULE_IP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_iprule_incl_mask`
--

CREATE TABLE IF NOT EXISTS `b_sec_iprule_incl_mask` (
  `IPRULE_ID` int(11) NOT NULL,
  `RULE_MASK` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `LIKE_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PREG_MASK` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`IPRULE_ID`,`RULE_MASK`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_recovery_codes`
--

CREATE TABLE IF NOT EXISTS `b_sec_recovery_codes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `CODE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `USED` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `USING_DATE` datetime DEFAULT NULL,
  `USING_IP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_sec_recovery_codes_user_id` (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_redirect_url`
--

CREATE TABLE IF NOT EXISTS `b_sec_redirect_url` (
  `IS_SYSTEM` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `URL` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `PARAMETER_NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_sec_redirect_url`
--

INSERT INTO `b_sec_redirect_url` (`IS_SYSTEM`, `SORT`, `URL`, `PARAMETER_NAME`) VALUES
('Y', 10, '/bitrix/redirect.php', 'goto'),
('Y', 20, '/bitrix/rk.php', 'goto'),
('Y', 30, '/bitrix/click.php', 'goto');

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_session`
--

CREATE TABLE IF NOT EXISTS `b_sec_session` (
  `SESSION_ID` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SESSION_DATA` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`SESSION_ID`),
  KEY `ix_b_sec_session_time` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_user`
--

CREATE TABLE IF NOT EXISTS `b_sec_user` (
  `USER_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SECRET` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TYPE` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  `ATTEMPTS` int(18) DEFAULT NULL,
  `INITIAL_DATE` datetime DEFAULT NULL,
  `SKIP_MANDATORY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DEACTIVATE_UNTIL` datetime DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_virus`
--

CREATE TABLE IF NOT EXISTS `b_sec_virus` (
  `ID` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `TIMESTAMP_X` datetime NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `INFO` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_sec_white_list`
--

CREATE TABLE IF NOT EXISTS `b_sec_white_list` (
  `ID` int(11) NOT NULL,
  `WHITE_SUBSTR` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_adv_banner`
--

CREATE TABLE IF NOT EXISTS `b_seo_adv_banner` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENGINE_ID` int(11) NOT NULL,
  `OWNER_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `OWNER_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LAST_UPDATE` timestamp NULL DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SETTINGS` text COLLATE utf8_unicode_ci,
  `CAMPAIGN_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_seo_adv_banner` (`ENGINE_ID`,`XML_ID`),
  KEY `ix_b_seo_adv_banner1` (`CAMPAIGN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_adv_campaign`
--

CREATE TABLE IF NOT EXISTS `b_seo_adv_campaign` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENGINE_ID` int(11) NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `OWNER_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `OWNER_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LAST_UPDATE` timestamp NULL DEFAULT NULL,
  `SETTINGS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_seo_adv_campaign` (`ENGINE_ID`,`XML_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_adv_group`
--

CREATE TABLE IF NOT EXISTS `b_seo_adv_group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENGINE_ID` int(11) NOT NULL,
  `OWNER_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `OWNER_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LAST_UPDATE` timestamp NULL DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SETTINGS` text COLLATE utf8_unicode_ci,
  `CAMPAIGN_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_seo_adv_group` (`ENGINE_ID`,`XML_ID`),
  KEY `ix_b_seo_adv_group1` (`CAMPAIGN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_adv_link`
--

CREATE TABLE IF NOT EXISTS `b_seo_adv_link` (
  `LINK_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `LINK_ID` int(18) NOT NULL,
  `BANNER_ID` int(11) NOT NULL,
  PRIMARY KEY (`LINK_TYPE`,`LINK_ID`,`BANNER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_adv_log`
--

CREATE TABLE IF NOT EXISTS `b_seo_adv_log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENGINE_ID` int(11) NOT NULL,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `REQUEST_URI` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `REQUEST_DATA` text COLLATE utf8_unicode_ci,
  `RESPONSE_TIME` float NOT NULL,
  `RESPONSE_STATUS` int(5) DEFAULT NULL,
  `RESPONSE_DATA` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_seo_adv_log1` (`ENGINE_ID`),
  KEY `ix_b_seo_adv_log2` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_adv_region`
--

CREATE TABLE IF NOT EXISTS `b_seo_adv_region` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENGINE_ID` int(11) NOT NULL,
  `OWNER_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `OWNER_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LAST_UPDATE` timestamp NULL DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SETTINGS` text COLLATE utf8_unicode_ci,
  `PARENT_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_seo_adv_region` (`ENGINE_ID`,`XML_ID`),
  KEY `ix_b_seo_adv_region1` (`PARENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_keywords`
--

CREATE TABLE IF NOT EXISTS `b_seo_keywords` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `KEYWORDS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_seo_keywords_url` (`URL`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_search_engine`
--

CREATE TABLE IF NOT EXISTS `b_seo_search_engine` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `SORT` int(5) DEFAULT '100',
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CLIENT_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CLIENT_SECRET` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REDIRECT_URI` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SETTINGS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_b_seo_search_engine_code` (`CODE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `b_seo_search_engine`
--

INSERT INTO `b_seo_search_engine` (`ID`, `CODE`, `ACTIVE`, `SORT`, `NAME`, `CLIENT_ID`, `CLIENT_SECRET`, `REDIRECT_URI`, `SETTINGS`) VALUES
(1, 'google', 'Y', 200, 'Google', '950140266760.apps.googleusercontent.com', 'IBktWJ_dS3rMKh43PSHO-zo5', 'urn:ietf:wg:oauth:2.0:oob', NULL),
(2, 'yandex', 'Y', 300, 'Yandex', 'f848c7bfc1d34a94ba6d05439f81bbd7', 'da0e73b2d9cc4e809f3170e49cb9df01', 'https://oauth.yandex.ru/verification_code', NULL),
(3, 'yandex_direct', 'Y', 400, 'Yandex.Direct', 'e46a29a748d84036baee1e2ae2a84fc6', '7122987f5a99479bb756d79ed7986e6c', 'https://oauth.yandex.ru/verification_code', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_sitemap`
--

CREATE TABLE IF NOT EXISTS `b_seo_sitemap` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `DATE_RUN` datetime DEFAULT NULL,
  `SETTINGS` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_sitemap_entity`
--

CREATE TABLE IF NOT EXISTS `b_seo_sitemap_entity` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENTITY_TYPE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ENTITY_ID` int(11) NOT NULL,
  `SITEMAP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_seo_sitemap_entity_1` (`ENTITY_TYPE`,`ENTITY_ID`),
  KEY `ix_b_seo_sitemap_entity_2` (`SITEMAP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_sitemap_iblock`
--

CREATE TABLE IF NOT EXISTS `b_seo_sitemap_iblock` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IBLOCK_ID` int(11) NOT NULL,
  `SITEMAP_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_seo_sitemap_iblock_1` (`IBLOCK_ID`),
  KEY `ix_b_seo_sitemap_iblock_2` (`SITEMAP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_sitemap_runtime`
--

CREATE TABLE IF NOT EXISTS `b_seo_sitemap_runtime` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PID` int(11) NOT NULL,
  `PROCESSED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ITEM_PATH` varchar(700) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ITEM_ID` int(11) DEFAULT NULL,
  `ITEM_TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'D',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `ACTIVE_ELEMENT` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `ix_seo_sitemap_runtime1` (`PID`,`PROCESSED`,`ITEM_TYPE`,`ITEM_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_seo_yandex_direct_stat`
--

CREATE TABLE IF NOT EXISTS `b_seo_yandex_direct_stat` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `CAMPAIGN_ID` int(11) NOT NULL,
  `BANNER_ID` int(11) NOT NULL,
  `DATE_DAY` date NOT NULL,
  `CURRENCY` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SUM` float DEFAULT '0',
  `SUM_SEARCH` float DEFAULT '0',
  `SUM_CONTEXT` float DEFAULT '0',
  `CLICKS` int(7) DEFAULT '0',
  `CLICKS_SEARCH` int(7) DEFAULT '0',
  `CLICKS_CONTEXT` int(7) DEFAULT '0',
  `SHOWS` int(7) DEFAULT '0',
  `SHOWS_SEARCH` int(7) DEFAULT '0',
  `SHOWS_CONTEXT` int(7) DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_seo_yandex_direct_stat` (`BANNER_ID`,`DATE_DAY`),
  KEY `ix_seo_yandex_direct_stat1` (`CAMPAIGN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_short_uri`
--

CREATE TABLE IF NOT EXISTS `b_short_uri` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `URI` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `URI_CRC` int(18) NOT NULL,
  `SHORT_URI` varbinary(250) NOT NULL,
  `SHORT_URI_CRC` int(18) NOT NULL,
  `STATUS` int(18) NOT NULL DEFAULT '301',
  `MODIFIED` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LAST_USED` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `NUMBER_USED` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ux_b_short_uri_1` (`SHORT_URI_CRC`),
  KEY `ux_b_short_uri_2` (`URI_CRC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_site_template`
--

CREATE TABLE IF NOT EXISTS `b_site_template` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `CONDITION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) NOT NULL DEFAULT '500',
  `TEMPLATE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_B_SITE_TEMPLATE` (`SITE_ID`,`CONDITION`,`TEMPLATE`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Dumping data for table `b_site_template`
--

INSERT INTO `b_site_template` (`ID`, `SITE_ID`, `CONDITION`, `SORT`, `TEMPLATE`) VALUES
(33, 's1', 'CSite::InDir(''/index.php'')', 150, 'main_tpl'),
(34, 's1', 'CSite::InDir(''/kompaniya/o-kompanii/index.php'')', 151, 'textpage_without_title'),
(35, 's1', 'CSite::InDir(''/kontakty/index.php'')', 152, 'textpage_without_title'),
(36, 's1', 'CSite::InDir(''/kompaniya/index.php'')', 153, 'textpage_without_title'),
(37, 's1', 'CSite::InDir(''/proekty/'')', 154, 'projects'),
(38, 's1', 'CSite::InDir(''/produktsiya/'')', 155, 'products'),
(39, 's1', '', 156, 'textpage');

-- --------------------------------------------------------

--
-- Table structure for table `b_smile`
--

CREATE TABLE IF NOT EXISTS `b_smile` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `SET_ID` int(18) NOT NULL DEFAULT '0',
  `SORT` int(10) NOT NULL DEFAULT '150',
  `TYPING` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CLICKABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `IMAGE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `IMAGE_HR` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IMAGE_WIDTH` int(11) NOT NULL DEFAULT '0',
  `IMAGE_HEIGHT` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `b_smile`
--

INSERT INTO `b_smile` (`ID`, `TYPE`, `SET_ID`, `SORT`, `TYPING`, `CLICKABLE`, `IMAGE`, `IMAGE_HR`, `IMAGE_WIDTH`, `IMAGE_HEIGHT`) VALUES
(1, 'S', 1, 100, ':) :-)', 'Y', 'smile_smile.png', 'Y', 16, 16),
(2, 'S', 1, 105, ';) ;-)', 'Y', 'smile_wink.png', 'Y', 16, 16),
(3, 'S', 1, 110, ':D :-D', 'Y', 'smile_biggrin.png', 'Y', 16, 16),
(4, 'S', 1, 115, '8) 8-)', 'Y', 'smile_cool.png', 'Y', 16, 16),
(5, 'S', 1, 120, ':( :-(', 'Y', 'smile_sad.png', 'Y', 16, 16),
(6, 'S', 1, 125, ':| :-|', 'Y', 'smile_neutral.png', 'Y', 16, 16),
(7, 'S', 1, 130, ':oops:', 'Y', 'smile_redface.png', 'Y', 16, 16),
(8, 'S', 1, 135, ':cry: :~(', 'Y', 'smile_cry.png', 'Y', 16, 16),
(9, 'S', 1, 140, ':evil: >:-<', 'Y', 'smile_evil.png', 'Y', 16, 16),
(10, 'S', 1, 145, ':o :-o :shock:', 'Y', 'smile_eek.png', 'Y', 16, 16),
(11, 'S', 1, 150, ':/ :-/', 'Y', 'smile_confuse.png', 'Y', 16, 16),
(12, 'S', 1, 155, ':{} :-{}', 'Y', 'smile_kiss.png', 'Y', 16, 16),
(13, 'S', 1, 160, ':idea:', 'Y', 'smile_idea.png', 'Y', 16, 16),
(14, 'S', 1, 165, ':?:', 'Y', 'smile_question.png', 'Y', 16, 16),
(15, 'S', 1, 170, ':!:', 'Y', 'smile_exclaim.png', 'Y', 16, 16),
(16, 'I', 1, 175, '', 'Y', 'icon1.gif', 'N', 15, 15),
(17, 'I', 1, 180, '', 'Y', 'icon2.gif', 'N', 15, 15),
(18, 'I', 1, 185, '', 'Y', 'icon3.gif', 'N', 15, 15),
(19, 'I', 1, 190, '', 'Y', 'icon4.gif', 'N', 15, 15),
(20, 'I', 1, 195, '', 'Y', 'icon5.gif', 'N', 15, 15),
(21, 'I', 1, 200, '', 'Y', 'icon6.gif', 'N', 15, 15),
(22, 'I', 1, 205, NULL, 'Y', 'icon7.gif', 'N', 15, 15);

-- --------------------------------------------------------

--
-- Table structure for table `b_smile_lang`
--

CREATE TABLE IF NOT EXISTS `b_smile_lang` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TYPE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'S',
  `SID` int(11) NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UX_SMILE_SL` (`TYPE`,`SID`,`LID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `b_smile_lang`
--

INSERT INTO `b_smile_lang` (`ID`, `TYPE`, `SID`, `LID`, `NAME`) VALUES
(1, 'G', 1, 'ru', 'Основной набор'),
(2, 'G', 1, 'en', 'Default set'),
(3, 'S', 1, 'ru', 'С улыбкой'),
(4, 'S', 1, 'en', 'Smile'),
(5, 'S', 2, 'ru', 'Шутливо'),
(6, 'S', 2, 'en', 'Wink'),
(7, 'S', 3, 'ru', 'Широкая улыбка'),
(8, 'S', 3, 'en', 'Big grin'),
(9, 'S', 4, 'ru', 'Здорово'),
(10, 'S', 4, 'en', 'Cool'),
(11, 'S', 5, 'ru', 'Печально'),
(12, 'S', 5, 'en', 'Sad'),
(13, 'S', 6, 'ru', 'Скептически'),
(14, 'S', 6, 'en', 'Skeptic'),
(15, 'S', 7, 'ru', 'Смущенный'),
(16, 'S', 7, 'en', 'Embarrassed'),
(17, 'S', 8, 'ru', 'Очень грустно'),
(18, 'S', 8, 'en', 'Crying'),
(19, 'S', 9, 'ru', 'Со злостью'),
(20, 'S', 9, 'en', 'Angry'),
(21, 'S', 10, 'ru', 'Удивленно'),
(22, 'S', 10, 'en', 'Surprised'),
(23, 'S', 11, 'ru', 'Смущенно'),
(24, 'S', 11, 'en', 'Confused'),
(25, 'S', 12, 'ru', 'Поцелуй'),
(26, 'S', 12, 'en', 'Kiss'),
(27, 'S', 13, 'ru', 'Идея'),
(28, 'S', 13, 'en', 'Idea'),
(29, 'S', 14, 'ru', 'Вопрос'),
(30, 'S', 14, 'en', 'Question'),
(31, 'S', 15, 'ru', 'Восклицание'),
(32, 'S', 15, 'en', 'Exclamation');

-- --------------------------------------------------------

--
-- Table structure for table `b_smile_set`
--

CREATE TABLE IF NOT EXISTS `b_smile_set` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `STRING_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(10) NOT NULL DEFAULT '150',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `b_smile_set`
--

INSERT INTO `b_smile_set` (`ID`, `STRING_ID`, `SORT`) VALUES
(1, 'main', 150);

-- --------------------------------------------------------

--
-- Table structure for table `b_socialservices_message`
--

CREATE TABLE IF NOT EXISTS `b_socialservices_message` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `SOCSERV_USER_ID` int(11) NOT NULL,
  `PROVIDER` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `MESSAGE` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INSERT_DATE` datetime DEFAULT NULL,
  `SUCCES_SENT` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_socialservices_user`
--

CREATE TABLE IF NOT EXISTS `b_socialservices_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_NAME` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_PHOTO` int(11) DEFAULT NULL,
  `EXTERNAL_AUTH_ID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `XML_ID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CAN_DELETE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `PERSONAL_WWW` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERMISSIONS` varchar(555) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OATOKEN` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OATOKEN_EXPIRES` int(11) DEFAULT NULL,
  `OASECRET` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `REFRESH_TOKEN` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SEND_ACTIVITY` char(1) COLLATE utf8_unicode_ci DEFAULT 'Y',
  `SITE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `INITIALIZED` char(1) COLLATE utf8_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `IX_B_SOCIALSERVICES_USER` (`XML_ID`,`EXTERNAL_AUTH_ID`),
  KEY `IX_B_SOCIALSERVICES_US_1` (`USER_ID`),
  KEY `IX_B_SOCIALSERVICES_US_2` (`INITIALIZED`),
  KEY `IX_B_SOCIALSERVICES_US_3` (`LOGIN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_socialservices_user_link`
--

CREATE TABLE IF NOT EXISTS `b_socialservices_user_link` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `SOCSERV_USER_ID` int(11) NOT NULL,
  `LINK_USER_ID` int(11) DEFAULT NULL,
  `LINK_UID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LINK_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LINK_LAST_NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LINK_PICTURE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sticker`
--

CREATE TABLE IF NOT EXISTS `b_sticker` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PAGE_URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PAGE_TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DATE_CREATE` datetime NOT NULL,
  `DATE_UPDATE` datetime NOT NULL,
  `MODIFIED_BY` int(18) NOT NULL,
  `CREATED_BY` int(18) NOT NULL,
  `PERSONAL` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CONTENT` text COLLATE utf8_unicode_ci,
  `POS_TOP` int(11) DEFAULT NULL,
  `POS_LEFT` int(11) DEFAULT NULL,
  `WIDTH` int(11) DEFAULT NULL,
  `HEIGHT` int(11) DEFAULT NULL,
  `COLOR` int(11) DEFAULT NULL,
  `COLLAPSED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `COMPLETED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `CLOSED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DELETED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `MARKER_TOP` int(11) DEFAULT NULL,
  `MARKER_LEFT` int(11) DEFAULT NULL,
  `MARKER_WIDTH` int(11) DEFAULT NULL,
  `MARKER_HEIGHT` int(11) DEFAULT NULL,
  `MARKER_ADJUST` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_sticker_group_task`
--

CREATE TABLE IF NOT EXISTS `b_sticker_group_task` (
  `GROUP_ID` int(11) NOT NULL,
  `TASK_ID` int(11) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`TASK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_sticker_group_task`
--

INSERT INTO `b_sticker_group_task` (`GROUP_ID`, `TASK_ID`) VALUES
(5, 33);

-- --------------------------------------------------------

--
-- Table structure for table `b_subscription`
--

CREATE TABLE IF NOT EXISTS `b_subscription` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE_INSERT` datetime NOT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FORMAT` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'text',
  `CONFIRM_CODE` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONFIRMED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DATE_CONFIRM` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_SUBSCRIPTION_EMAIL` (`EMAIL`,`USER_ID`),
  KEY `IX_DATE_CONFIRM` (`CONFIRMED`,`DATE_CONFIRM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_subscription_rubric`
--

CREATE TABLE IF NOT EXISTS `b_subscription_rubric` (
  `SUBSCRIPTION_ID` int(11) NOT NULL,
  `LIST_RUBRIC_ID` int(11) NOT NULL,
  UNIQUE KEY `UK_SUBSCRIPTION_RUBRIC` (`SUBSCRIPTION_ID`,`LIST_RUBRIC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_task`
--

CREATE TABLE IF NOT EXISTS `b_task` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `LETTER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `SYS` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `DESCRIPTION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BINDING` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'module',
  PRIMARY KEY (`ID`),
  KEY `ix_task` (`MODULE_ID`,`BINDING`,`LETTER`,`SYS`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=51 ;

--
-- Dumping data for table `b_task`
--

INSERT INTO `b_task` (`ID`, `NAME`, `LETTER`, `MODULE_ID`, `SYS`, `DESCRIPTION`, `BINDING`) VALUES
(1, 'main_denied', 'D', 'main', 'Y', NULL, 'module'),
(2, 'main_change_profile', 'P', 'main', 'Y', NULL, 'module'),
(3, 'main_view_all_settings', 'R', 'main', 'Y', NULL, 'module'),
(4, 'main_view_all_settings_change_profile', 'T', 'main', 'Y', NULL, 'module'),
(5, 'main_edit_subordinate_users', 'V', 'main', 'Y', NULL, 'module'),
(6, 'main_full_access', 'W', 'main', 'Y', NULL, 'module'),
(7, 'fm_folder_access_denied', 'D', 'main', 'Y', NULL, 'file'),
(8, 'fm_folder_access_read', 'R', 'main', 'Y', NULL, 'file'),
(9, 'fm_folder_access_write', 'W', 'main', 'Y', NULL, 'file'),
(10, 'fm_folder_access_full', 'X', 'main', 'Y', NULL, 'file'),
(11, 'fm_folder_access_workflow', 'U', 'main', 'Y', NULL, 'file'),
(12, 'catalog_denied', 'D', 'catalog', 'Y', NULL, 'module'),
(13, 'catalog_read', 'R', 'catalog', 'Y', NULL, 'module'),
(14, 'catalog_price_edit', 'T', 'catalog', 'Y', NULL, 'module'),
(15, 'catalog_store_edit', 'S', 'catalog', 'Y', NULL, 'module'),
(16, 'catalog_export_import', 'U', 'catalog', 'Y', NULL, 'module'),
(17, 'catalog_full_access', 'W', 'catalog', 'Y', NULL, 'module'),
(18, 'clouds_denied', 'D', 'clouds', 'Y', NULL, 'module'),
(19, 'clouds_browse', 'F', 'clouds', 'Y', NULL, 'module'),
(20, 'clouds_upload', 'U', 'clouds', 'Y', NULL, 'module'),
(21, 'clouds_full_access', 'W', 'clouds', 'Y', NULL, 'module'),
(22, 'fileman_denied', 'D', 'fileman', 'Y', '', 'module'),
(23, 'fileman_allowed_folders', 'F', 'fileman', 'Y', '', 'module'),
(24, 'fileman_full_access', 'W', 'fileman', 'Y', '', 'module'),
(25, 'medialib_denied', 'D', 'fileman', 'Y', '', 'medialib'),
(26, 'medialib_view', 'F', 'fileman', 'Y', '', 'medialib'),
(27, 'medialib_only_new', 'R', 'fileman', 'Y', '', 'medialib'),
(28, 'medialib_edit_items', 'V', 'fileman', 'Y', '', 'medialib'),
(29, 'medialib_editor', 'W', 'fileman', 'Y', '', 'medialib'),
(30, 'medialib_full', 'X', 'fileman', 'Y', '', 'medialib'),
(31, 'stickers_denied', 'D', 'fileman', 'Y', '', 'stickers'),
(32, 'stickers_read', 'R', 'fileman', 'Y', '', 'stickers'),
(33, 'stickers_edit', 'W', 'fileman', 'Y', '', 'stickers'),
(34, 'iblock_deny', 'D', 'iblock', 'Y', NULL, 'iblock'),
(35, 'iblock_read', 'R', 'iblock', 'Y', NULL, 'iblock'),
(36, 'iblock_element_add', 'E', 'iblock', 'Y', NULL, 'iblock'),
(37, 'iblock_admin_read', 'S', 'iblock', 'Y', NULL, 'iblock'),
(38, 'iblock_admin_add', 'T', 'iblock', 'Y', NULL, 'iblock'),
(39, 'iblock_limited_edit', 'U', 'iblock', 'Y', NULL, 'iblock'),
(40, 'iblock_full_edit', 'W', 'iblock', 'Y', NULL, 'iblock'),
(41, 'iblock_full', 'X', 'iblock', 'Y', NULL, 'iblock'),
(42, 'security_denied', 'D', 'security', 'Y', NULL, 'module'),
(43, 'security_filter', 'F', 'security', 'Y', NULL, 'module'),
(44, 'security_otp', 'S', 'security', 'Y', NULL, 'module'),
(45, 'security_view_all_settings', 'T', 'security', 'Y', NULL, 'module'),
(46, 'security_full_access', 'W', 'security', 'Y', NULL, 'module'),
(47, 'seo_denied', 'D', 'seo', 'Y', '', 'module'),
(48, 'seo_edit', 'F', 'seo', 'Y', '', 'module'),
(49, 'seo_full_access', 'W', 'seo', 'Y', '', 'module'),
(50, 'Контент-редакторы', 'Q', 'main', 'N', 'Разрешено изменять информацию в своем профайле. Управление кешем', 'module');

-- --------------------------------------------------------

--
-- Table structure for table `b_task_operation`
--

CREATE TABLE IF NOT EXISTS `b_task_operation` (
  `TASK_ID` int(18) NOT NULL,
  `OPERATION_ID` int(18) NOT NULL,
  PRIMARY KEY (`TASK_ID`,`OPERATION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_task_operation`
--

INSERT INTO `b_task_operation` (`TASK_ID`, `OPERATION_ID`) VALUES
(2, 2),
(2, 3),
(3, 2),
(3, 4),
(3, 5),
(3, 6),
(3, 7),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(4, 6),
(4, 7),
(5, 2),
(5, 3),
(5, 5),
(5, 6),
(5, 7),
(5, 8),
(5, 9),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(6, 6),
(6, 7),
(6, 10),
(6, 11),
(6, 12),
(6, 13),
(6, 14),
(6, 15),
(6, 16),
(6, 17),
(6, 18),
(8, 19),
(8, 20),
(8, 21),
(9, 19),
(9, 20),
(9, 21),
(9, 22),
(9, 23),
(9, 24),
(9, 25),
(9, 26),
(9, 27),
(9, 28),
(9, 29),
(9, 30),
(9, 31),
(9, 32),
(9, 33),
(9, 34),
(10, 19),
(10, 20),
(10, 21),
(10, 22),
(10, 23),
(10, 24),
(10, 25),
(10, 26),
(10, 27),
(10, 28),
(10, 29),
(10, 30),
(10, 31),
(10, 32),
(10, 33),
(10, 34),
(10, 35),
(11, 19),
(11, 20),
(11, 21),
(11, 24),
(11, 28),
(13, 36),
(14, 36),
(14, 37),
(14, 38),
(14, 39),
(14, 40),
(14, 41),
(14, 42),
(15, 36),
(15, 37),
(15, 41),
(15, 42),
(15, 43),
(16, 36),
(16, 44),
(16, 45),
(16, 46),
(16, 47),
(17, 36),
(17, 37),
(17, 38),
(17, 39),
(17, 40),
(17, 41),
(17, 42),
(17, 43),
(17, 44),
(17, 45),
(17, 46),
(17, 47),
(17, 48),
(17, 49),
(19, 50),
(20, 50),
(20, 51),
(21, 50),
(21, 51),
(21, 52),
(23, 55),
(23, 56),
(23, 57),
(23, 58),
(23, 59),
(23, 60),
(23, 61),
(23, 63),
(23, 64),
(24, 53),
(24, 54),
(24, 55),
(24, 56),
(24, 57),
(24, 58),
(24, 59),
(24, 60),
(24, 61),
(24, 62),
(24, 63),
(24, 64),
(24, 65),
(26, 66),
(27, 66),
(27, 67),
(27, 71),
(28, 66),
(28, 71),
(28, 72),
(28, 73),
(29, 66),
(29, 67),
(29, 68),
(29, 69),
(29, 71),
(29, 72),
(29, 73),
(30, 66),
(30, 67),
(30, 68),
(30, 69),
(30, 70),
(30, 71),
(30, 72),
(30, 73),
(32, 74),
(33, 74),
(33, 75),
(33, 76),
(33, 77),
(35, 78),
(35, 79),
(36, 80),
(37, 78),
(37, 79),
(37, 81),
(38, 78),
(38, 79),
(38, 80),
(38, 81),
(39, 78),
(39, 79),
(39, 80),
(39, 81),
(39, 82),
(39, 83),
(39, 84),
(39, 85),
(40, 78),
(40, 79),
(40, 80),
(40, 81),
(40, 82),
(40, 83),
(40, 84),
(40, 85),
(40, 86),
(40, 87),
(40, 88),
(40, 89),
(41, 78),
(41, 79),
(41, 80),
(41, 81),
(41, 82),
(41, 83),
(41, 84),
(41, 85),
(41, 86),
(41, 87),
(41, 88),
(41, 89),
(41, 90),
(41, 91),
(41, 92),
(41, 93),
(41, 94),
(41, 95),
(43, 96),
(44, 97),
(45, 98),
(45, 99),
(45, 100),
(45, 101),
(45, 102),
(45, 103),
(45, 104),
(45, 105),
(45, 106),
(45, 107),
(45, 108),
(46, 96),
(46, 97),
(46, 98),
(46, 99),
(46, 100),
(46, 101),
(46, 102),
(46, 103),
(46, 104),
(46, 105),
(46, 106),
(46, 107),
(46, 108),
(46, 109),
(46, 110),
(46, 111),
(46, 112),
(46, 113),
(46, 114),
(46, 115),
(46, 116),
(46, 117),
(46, 118),
(46, 119),
(46, 120),
(46, 121),
(48, 123),
(49, 122),
(49, 123),
(50, 2),
(50, 3),
(50, 14);

-- --------------------------------------------------------

--
-- Table structure for table `b_undo`
--

CREATE TABLE IF NOT EXISTS `b_undo` (
  `ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MODULE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNDO_TYPE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNDO_HANDLER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONTENT` mediumtext COLLATE utf8_unicode_ci,
  `USER_ID` int(11) DEFAULT NULL,
  `TIMESTAMP_X` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_undo`
--

INSERT INTO `b_undo` (`ID`, `MODULE_ID`, `UNDO_TYPE`, `UNDO_HANDLER`, `CONTENT`, `USER_ID`, `TIMESTAMP_X`) VALUES
('1727ffbedccdf6c40f006ab4db9b4a227', 'fileman', 'edit_component_props', 'CFileman::UndoEditFile', 'a:2:{s:7:"absPath";s:35:"D:/Bitrix/www/produktsiya/index.php";s:7:"content";s:4413:"<?\nrequire($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");\n$APPLICATION->SetTitle("Продукция");\n?><?$APPLICATION->IncludeComponent(\n	"bitrix:catalog", \n	"products", \n	array(\n		"IBLOCK_TYPE" => "products",\n		"IBLOCK_ID" => "2",\n		"HIDE_NOT_AVAILABLE" => "N",\n		"TEMPLATE_THEME" => "blue",\n		"COMMON_SHOW_CLOSE_POPUP" => "N",\n		"SHOW_DISCOUNT_PERCENT" => "N",\n		"SHOW_OLD_PRICE" => "N",\n		"DETAIL_SHOW_MAX_QUANTITY" => "N",\n		"MESS_BTN_BUY" => "Купить",\n		"MESS_BTN_ADD_TO_BASKET" => "В корзину",\n		"MESS_BTN_COMPARE" => "Сравнение",\n		"MESS_BTN_DETAIL" => "Подробнее",\n		"MESS_NOT_AVAILABLE" => "Нет в наличии",\n		"DETAIL_USE_VOTE_RATING" => "N",\n		"DETAIL_USE_COMMENTS" => "N",\n		"DETAIL_BRAND_USE" => "N",\n		"SEF_MODE" => "Y",\n		"AJAX_MODE" => "N",\n		"AJAX_OPTION_JUMP" => "N",\n		"AJAX_OPTION_STYLE" => "Y",\n		"AJAX_OPTION_HISTORY" => "N",\n		"CACHE_TYPE" => "A",\n		"CACHE_TIME" => "36000000",\n		"CACHE_FILTER" => "N",\n		"CACHE_GROUPS" => "Y",\n		"SET_STATUS_404" => "N",\n		"SET_TITLE" => "Y",\n		"ADD_SECTIONS_CHAIN" => "Y",\n		"ADD_ELEMENT_CHAIN" => "N",\n		"USE_ELEMENT_COUNTER" => "Y",\n		"USE_SALE_BESTSELLERS" => "Y",\n		"USE_FILTER" => "N",\n		"FILTER_VIEW_MODE" => "VERTICAL",\n		"USE_REVIEW" => "N",\n		"ACTION_VARIABLE" => "action",\n		"PRODUCT_ID_VARIABLE" => "id",\n		"USE_COMPARE" => "N",\n		"PRICE_CODE" => array(\n		),\n		"USE_PRICE_COUNT" => "N",\n		"SHOW_PRICE_COUNT" => "1",\n		"PRICE_VAT_INCLUDE" => "Y",\n		"PRICE_VAT_SHOW_VALUE" => "N",\n		"CONVERT_CURRENCY" => "N",\n		"BASKET_URL" => "/personal/basket.php",\n		"USE_PRODUCT_QUANTITY" => "N",\n		"ADD_PROPERTIES_TO_BASKET" => "Y",\n		"PRODUCT_PROPS_VARIABLE" => "prop",\n		"PARTIAL_PRODUCT_PROPERTIES" => "N",\n		"PRODUCT_PROPERTIES" => array(\n		),\n		"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",\n		"TOP_ADD_TO_BASKET_ACTION" => "ADD",\n		"SECTION_ADD_TO_BASKET_ACTION" => "ADD",\n		"DETAIL_ADD_TO_BASKET_ACTION" => array(\n			0 => "BUY",\n		),\n		"SHOW_TOP_ELEMENTS" => "Y",\n		"TOP_ELEMENT_COUNT" => "9",\n		"TOP_LINE_ELEMENT_COUNT" => "3",\n		"TOP_ELEMENT_SORT_FIELD" => "sort",\n		"TOP_ELEMENT_SORT_ORDER" => "asc",\n		"TOP_ELEMENT_SORT_FIELD2" => "id",\n		"TOP_ELEMENT_SORT_ORDER2" => "desc",\n		"TOP_PROPERTY_CODE" => array(\n			0 => "",\n			1 => "undefined",\n			2 => "",\n		),\n		"SECTION_COUNT_ELEMENTS" => "Y",\n		"SECTION_TOP_DEPTH" => "3",\n		"SECTIONS_VIEW_MODE" => "LIST",\n		"SECTIONS_SHOW_PARENT_NAME" => "Y",\n		"PAGE_ELEMENT_COUNT" => "30",\n		"LINE_ELEMENT_COUNT" => "3",\n		"ELEMENT_SORT_FIELD" => "sort",\n		"ELEMENT_SORT_ORDER" => "asc",\n		"ELEMENT_SORT_FIELD2" => "id",\n		"ELEMENT_SORT_ORDER2" => "desc",\n		"LIST_PROPERTY_CODE" => array(\n			0 => "",\n			1 => "undefined",\n			2 => "",\n		),\n		"INCLUDE_SUBSECTIONS" => "Y",\n		"LIST_META_KEYWORDS" => "-",\n		"LIST_META_DESCRIPTION" => "-",\n		"LIST_BROWSER_TITLE" => "-",\n		"DETAIL_PROPERTY_CODE" => array(\n			0 => "",\n			1 => "undefined",\n			2 => "",\n		),\n		"DETAIL_META_KEYWORDS" => "-",\n		"DETAIL_META_DESCRIPTION" => "-",\n		"DETAIL_BROWSER_TITLE" => "-",\n		"SECTION_ID_VARIABLE" => "SECTION_ID",\n		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",\n		"DETAIL_DISPLAY_NAME" => "Y",\n		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",\n		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",\n		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",\n		"LINK_IBLOCK_TYPE" => "undefined",\n		"LINK_IBLOCK_ID" => "undefined",\n		"LINK_PROPERTY_SID" => "undefined",\n		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",\n		"USE_ALSO_BUY" => "N",\n		"USE_STORE" => "N",\n		"USE_BIG_DATA" => "Y",\n		"BIG_DATA_RCM_TYPE" => "bestsell",\n		"PAGER_TEMPLATE" => ".default",\n		"DISPLAY_TOP_PAGER" => "N",\n		"DISPLAY_BOTTOM_PAGER" => "Y",\n		"PAGER_TITLE" => "Товары",\n		"PAGER_SHOW_ALWAYS" => "N",\n		"PAGER_DESC_NUMBERING" => "N",\n		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",\n		"PAGER_SHOW_ALL" => "N",\n		"TOP_VIEW_MODE" => "SECTION",\n		"SEF_FOLDER" => "/produktsiya/",\n		"ADD_PICT_PROP" => "-",\n		"LABEL_PROP" => "-",\n		"AJAX_OPTION_ADDITIONAL" => "",\n		"PRODUCT_QUANTITY_VARIABLE" => "quantity",\n		"COMMON_ADD_TO_BASKET_ACTION" => "ADD",\n		"SEF_URL_TEMPLATES" => array(\n			"sections" => "",\n			"section" => "#SECTION_CODE_PATH#/",\n			"element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",\n			"compare" => "compare.php?action=#ACTION_CODE#",\n		),\n		"VARIABLE_ALIASES" => array(\n			"compare" => array(\n				"ACTION_CODE" => "action",\n			),\n		)\n	),\n	false\n);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>";}', 1, 1431265157),
('184a75d38d514424ad24c31b58c93b5c9', 'fileman', 'edit_component_props', 'CFileman::UndoEditFile', 'a:2:{s:7:"absPath";s:45:"Z:/home/akvakultura/www/produktsiya/index.php";s:7:"content";s:12275:"<?\nrequire($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");\n$APPLICATION->SetTitle("Продукция");\n?><?$APPLICATION->IncludeComponent("bitrix:catalog", "products", Array(\n	"IBLOCK_TYPE" => "products",	// Тип инфоблока\n		"IBLOCK_ID" => "2",	// Инфоблок\n		"HIDE_NOT_AVAILABLE" => "N",	// Не отображать товары, которых нет на складах\n		"TEMPLATE_THEME" => "blue",	// Цветовая тема\n		"COMMON_SHOW_CLOSE_POPUP" => "N",	// Показывать кнопку продолжения покупок во всплывающих окнах\n		"SHOW_DISCOUNT_PERCENT" => "N",	// Показывать процент скидки\n		"SHOW_OLD_PRICE" => "N",	// Показывать старую цену\n		"DETAIL_SHOW_MAX_QUANTITY" => "N",	// Показывать общее количество товара\n		"MESS_BTN_BUY" => "Купить",	// Текст кнопки "Купить"\n		"MESS_BTN_ADD_TO_BASKET" => "В корзину",	// Текст кнопки "Добавить в корзину"\n		"MESS_BTN_COMPARE" => "Сравнение",	// Текст кнопки "Сравнение"\n		"MESS_BTN_DETAIL" => "Подробнее",	// Текст кнопки "Подробнее"\n		"MESS_NOT_AVAILABLE" => "Нет в наличии",	// Сообщение об отсутствии товара\n		"DETAIL_USE_VOTE_RATING" => "N",	// Включить рейтинг товара\n		"DETAIL_USE_COMMENTS" => "N",	// Включить отзывы о товаре\n		"DETAIL_BRAND_USE" => "N",	// Использовать компонент "Бренды"\n		"SEF_MODE" => "Y",	// Включить поддержку ЧПУ\n		"AJAX_MODE" => "N",	// Включить режим AJAX\n		"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента\n		"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей\n		"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера\n		"CACHE_TYPE" => "A",	// Тип кеширования\n		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)\n		"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре\n		"CACHE_GROUPS" => "Y",	// Учитывать права доступа\n		"SET_STATUS_404" => "N",	// Устанавливать статус 404, если не найдены элемент или раздел\n		"SET_TITLE" => "Y",	// Устанавливать заголовок страницы\n		"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации\n		"ADD_ELEMENT_CHAIN" => "N",	// Включать название элемента в цепочку навигации\n		"USE_ELEMENT_COUNTER" => "Y",	// Использовать счетчик просмотров\n		"USE_SALE_BESTSELLERS" => "Y",	// Показывать список лидеров продаж\n		"USE_FILTER" => "N",	// Показывать фильтр\n		"FILTER_VIEW_MODE" => "VERTICAL",	// Вид отображения умного фильтра\n		"USE_REVIEW" => "N",	// Разрешить отзывы\n		"ACTION_VARIABLE" => "action",	// Название переменной, в которой передается действие\n		"PRODUCT_ID_VARIABLE" => "id",	// Название переменной, в которой передается код товара для покупки\n		"USE_COMPARE" => "N",	// Разрешить сравнение товаров\n		"PRICE_CODE" => "",	// Тип цены\n		"USE_PRICE_COUNT" => "N",	// Использовать вывод цен с диапазонами\n		"SHOW_PRICE_COUNT" => "1",	// Выводить цены для количества\n		"PRICE_VAT_INCLUDE" => "Y",	// Включать НДС в цену\n		"PRICE_VAT_SHOW_VALUE" => "N",	// Отображать значение НДС\n		"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте\n		"BASKET_URL" => "/personal/basket.php",	// URL, ведущий на страницу с корзиной покупателя\n		"USE_PRODUCT_QUANTITY" => "N",	// Разрешить указание количества товара\n		"ADD_PROPERTIES_TO_BASKET" => "Y",	// Добавлять в корзину свойства товаров и предложений\n		"PRODUCT_PROPS_VARIABLE" => "prop",	// Название переменной, в которой передаются характеристики товара\n		"PARTIAL_PRODUCT_PROPERTIES" => "N",	// Разрешить добавлять в корзину товары, у которых заполнены не все характеристики\n		"PRODUCT_PROPERTIES" => "",	// Характеристики товара, добавляемые в корзину\n		"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",	// Одинаковые настройки показа кнопок добавления в корзину или покупки на всех страницах\n		"TOP_ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки на странице с top''ом товаров\n		"SECTION_ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки на странице списка товаров\n		"DETAIL_ADD_TO_BASKET_ACTION" => array(	// Показывать кнопки добавления в корзину и покупки на детальной странице товара\n			0 => "BUY",\n		),\n		"SHOW_TOP_ELEMENTS" => "Y",	// Выводить топ элементов\n		"TOP_ELEMENT_COUNT" => "9",	// Количество выводимых элементов\n		"TOP_LINE_ELEMENT_COUNT" => "3",	// Количество элементов, выводимых в одной строке таблицы\n		"TOP_ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем товары в разделе\n		"TOP_ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки товаров в разделе\n		"TOP_ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки товаров в разделе\n		"TOP_ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки товаров в разделе\n		"TOP_PROPERTY_CODE" => array(	// Свойства\n			0 => "",\n			1 => "undefined",\n			2 => "",\n		),\n		"SECTION_COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе\n		"SECTION_TOP_DEPTH" => "2",	// Максимальная отображаемая глубина разделов\n		"SECTIONS_VIEW_MODE" => "LIST",	// Вид списка подразделов\n		"SECTIONS_SHOW_PARENT_NAME" => "Y",	// Показывать название раздела\n		"PAGE_ELEMENT_COUNT" => "30",	// Количество элементов на странице\n		"LINE_ELEMENT_COUNT" => "3",	// Количество элементов, выводимых в одной строке таблицы\n		"ELEMENT_SORT_FIELD" => "sort",	// По какому полю сортируем товары в разделе\n		"ELEMENT_SORT_ORDER" => "asc",	// Порядок сортировки товаров в разделе\n		"ELEMENT_SORT_FIELD2" => "id",	// Поле для второй сортировки товаров в разделе\n		"ELEMENT_SORT_ORDER2" => "desc",	// Порядок второй сортировки товаров в разделе\n		"LIST_PROPERTY_CODE" => array(	// Свойства\n			0 => "",\n			1 => "undefined",\n			2 => "",\n		),\n		"INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела\n		"LIST_META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства раздела\n		"LIST_META_DESCRIPTION" => "-",	// Установить описание страницы из свойства раздела\n		"LIST_BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства раздела\n		"DETAIL_PROPERTY_CODE" => array(	// Свойства\n			0 => "",\n			1 => "undefined",\n			2 => "",\n		),\n		"DETAIL_META_KEYWORDS" => "-",	// Установить ключевые слова страницы из свойства\n		"DETAIL_META_DESCRIPTION" => "-",	// Установить описание страницы из свойства\n		"DETAIL_BROWSER_TITLE" => "-",	// Установить заголовок окна браузера из свойства\n		"SECTION_ID_VARIABLE" => "SECTION_ID",	// Название переменной, в которой передается код группы\n		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",	// Использовать код группы из переменной, если не задан раздел элемента\n		"DETAIL_DISPLAY_NAME" => "Y",	// Выводить название элемента\n		"DETAIL_DETAIL_PICTURE_MODE" => "IMG",	// Режим показа детальной картинки\n		"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",	// Добавлять детальную картинку в слайдер\n		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",	// Показ описания для анонса на детальной странице\n		"LINK_IBLOCK_TYPE" => "undefined",	// Тип инфоблока, элементы которого связаны с текущим элементом\n		"LINK_IBLOCK_ID" => "undefined",	// ID инфоблока, элементы которого связаны с текущим элементом\n		"LINK_PROPERTY_SID" => "undefined",	// Свойство, в котором хранится связь\n		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",	// URL на страницу, где будет показан список связанных элементов\n		"USE_ALSO_BUY" => "N",	// Показывать блок "С этим товаром покупают"\n		"USE_STORE" => "N",	// Показывать блок "Количество товара на складе"\n		"USE_BIG_DATA" => "Y",	// Показывать персональные рекомендации\n		"BIG_DATA_RCM_TYPE" => "bestsell",	// Тип рекомендации\n		"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации\n		"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком\n		"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком\n		"PAGER_TITLE" => "Товары",	// Название категорий\n		"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда\n		"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию\n		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации\n		"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"\n		"TOP_VIEW_MODE" => "SECTION",	// Показ элементов top''а\n		"SEF_FOLDER" => "/produktsiya/",	// Каталог ЧПУ (относительно корня сайта)\n		"ADD_PICT_PROP" => "-",	// Дополнительная картинка основного товара\n		"LABEL_PROP" => "-",	// Свойство меток товара\n		"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор\n		"PRODUCT_QUANTITY_VARIABLE" => "quantity",	// Название переменной, в которой передается количество товара\n		"COMMON_ADD_TO_BASKET_ACTION" => "ADD",	// Показывать кнопку добавления в корзину или покупки\n		"SEF_URL_TEMPLATES" => array(\n			"sections" => "",\n			"section" => "#SECTION_CODE_PATH#/",\n			"element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",\n			"compare" => "compare.php?action=#ACTION_CODE#",\n		),\n		"VARIABLE_ALIASES" => array(\n			"compare" => array(\n				"ACTION_CODE" => "action",\n			),\n		)\n	),\n	false\n);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>";}', 1, 1431231347);

-- --------------------------------------------------------

--
-- Table structure for table `b_user`
--

CREATE TABLE IF NOT EXISTS `b_user` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `TIMESTAMP_X` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LOGIN` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `PASSWORD` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CHECKWORD` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EMAIL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LAST_LOGIN` datetime DEFAULT NULL,
  `DATE_REGISTER` datetime NOT NULL,
  `LID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_PROFESSION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_WWW` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_ICQ` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_GENDER` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_BIRTHDATE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_PHOTO` int(18) DEFAULT NULL,
  `PERSONAL_PHONE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_FAX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_MOBILE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_PAGER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_STREET` text COLLATE utf8_unicode_ci,
  `PERSONAL_MAILBOX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_CITY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_STATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_ZIP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_COUNTRY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_NOTES` text COLLATE utf8_unicode_ci,
  `WORK_COMPANY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_DEPARTMENT` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_POSITION` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_WWW` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_PHONE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_FAX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_PAGER` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_STREET` text COLLATE utf8_unicode_ci,
  `WORK_MAILBOX` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_CITY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_STATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_ZIP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_COUNTRY` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WORK_PROFILE` text COLLATE utf8_unicode_ci,
  `WORK_LOGO` int(18) DEFAULT NULL,
  `WORK_NOTES` text COLLATE utf8_unicode_ci,
  `ADMIN_NOTES` text COLLATE utf8_unicode_ci,
  `STORED_HASH` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PERSONAL_BIRTHDAY` date DEFAULT NULL,
  `EXTERNAL_AUTH_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CHECKWORD_TIME` datetime DEFAULT NULL,
  `SECOND_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CONFIRM_CODE` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LOGIN_ATTEMPTS` int(18) DEFAULT NULL,
  `LAST_ACTIVITY_DATE` datetime DEFAULT NULL,
  `AUTO_TIME_ZONE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIME_ZONE` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIME_ZONE_OFFSET` int(18) DEFAULT NULL,
  `TITLE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ix_login` (`LOGIN`,`EXTERNAL_AUTH_ID`),
  KEY `ix_b_user_email` (`EMAIL`),
  KEY `ix_b_user_activity_date` (`LAST_ACTIVITY_DATE`),
  KEY `IX_B_USER_XML_ID` (`XML_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `b_user`
--

INSERT INTO `b_user` (`ID`, `TIMESTAMP_X`, `LOGIN`, `PASSWORD`, `CHECKWORD`, `ACTIVE`, `NAME`, `LAST_NAME`, `EMAIL`, `LAST_LOGIN`, `DATE_REGISTER`, `LID`, `PERSONAL_PROFESSION`, `PERSONAL_WWW`, `PERSONAL_ICQ`, `PERSONAL_GENDER`, `PERSONAL_BIRTHDATE`, `PERSONAL_PHOTO`, `PERSONAL_PHONE`, `PERSONAL_FAX`, `PERSONAL_MOBILE`, `PERSONAL_PAGER`, `PERSONAL_STREET`, `PERSONAL_MAILBOX`, `PERSONAL_CITY`, `PERSONAL_STATE`, `PERSONAL_ZIP`, `PERSONAL_COUNTRY`, `PERSONAL_NOTES`, `WORK_COMPANY`, `WORK_DEPARTMENT`, `WORK_POSITION`, `WORK_WWW`, `WORK_PHONE`, `WORK_FAX`, `WORK_PAGER`, `WORK_STREET`, `WORK_MAILBOX`, `WORK_CITY`, `WORK_STATE`, `WORK_ZIP`, `WORK_COUNTRY`, `WORK_PROFILE`, `WORK_LOGO`, `WORK_NOTES`, `ADMIN_NOTES`, `STORED_HASH`, `XML_ID`, `PERSONAL_BIRTHDAY`, `EXTERNAL_AUTH_ID`, `CHECKWORD_TIME`, `SECOND_NAME`, `CONFIRM_CODE`, `LOGIN_ATTEMPTS`, `LAST_ACTIVITY_DATE`, `AUTO_TIME_ZONE`, `TIME_ZONE`, `TIME_ZONE_OFFSET`, `TITLE`) VALUES
(1, '2015-04-22 17:29:17', 'admin', 'x0doXcLC8d973e0a98397c5f98fa9cda0417a52c', 'YiKuhuna074c29109796e00494c86397a439a8d4', 'Y', 'Админ', 'Сайта', 'test@test.ru', '2015-05-10 12:24:03', '2015-04-22 23:29:17', NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-04-22 23:29:17', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_user_access`
--

CREATE TABLE IF NOT EXISTS `b_user_access` (
  `USER_ID` int(11) DEFAULT NULL,
  `PROVIDER_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACCESS_CODE` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `ix_ua_user_provider` (`USER_ID`,`PROVIDER_ID`),
  KEY `ix_ua_user_access` (`USER_ID`,`ACCESS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_user_access`
--

INSERT INTO `b_user_access` (`USER_ID`, `PROVIDER_ID`, `ACCESS_CODE`) VALUES
(0, 'group', 'G2'),
(1, 'group', 'G1'),
(1, 'group', 'G3'),
(1, 'group', 'G4'),
(1, 'group', 'G2'),
(1, 'user', 'U1');

-- --------------------------------------------------------

--
-- Table structure for table `b_user_access_check`
--

CREATE TABLE IF NOT EXISTS `b_user_access_check` (
  `USER_ID` int(11) DEFAULT NULL,
  `PROVIDER_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `ix_uac_user_provider` (`USER_ID`,`PROVIDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_user_access_check`
--

INSERT INTO `b_user_access_check` (`USER_ID`, `PROVIDER_ID`) VALUES
(1, 'group'),
(1, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `b_user_counter`
--

CREATE TABLE IF NOT EXISTS `b_user_counter` (
  `USER_ID` int(18) NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '**',
  `CODE` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `CNT` int(18) NOT NULL DEFAULT '0',
  `LAST_DATE` datetime DEFAULT NULL,
  `TAG` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PARAMS` text COLLATE utf8_unicode_ci,
  `SENT` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`USER_ID`,`SITE_ID`,`CODE`),
  KEY `ix_buc_tag` (`TAG`),
  KEY `ix_buc_sent` (`SENT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_user_digest`
--

CREATE TABLE IF NOT EXISTS `b_user_digest` (
  `USER_ID` int(11) NOT NULL,
  `DIGEST_HA1` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_user_field`
--

CREATE TABLE IF NOT EXISTS `b_user_field` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENTITY_ID` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FIELD_NAME` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `USER_TYPE_ID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SORT` int(11) DEFAULT NULL,
  `MULTIPLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `MANDATORY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SHOW_FILTER` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SHOW_IN_LIST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `EDIT_IN_LIST` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `IS_SEARCHABLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SETTINGS` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_user_type_entity` (`ENTITY_ID`,`FIELD_NAME`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `b_user_field`
--

INSERT INTO `b_user_field` (`ID`, `ENTITY_ID`, `FIELD_NAME`, `USER_TYPE_ID`, `XML_ID`, `SORT`, `MULTIPLE`, `MANDATORY`, `SHOW_FILTER`, `SHOW_IN_LIST`, `EDIT_IN_LIST`, `IS_SEARCHABLE`, `SETTINGS`) VALUES
(1, 'BLOG_POST', 'UF_BLOG_POST_DOC', 'file', 'UF_BLOG_POST_DOC', 100, 'Y', 'N', 'N', 'N', 'Y', 'Y', 'a:0:{}'),
(2, 'BLOG_COMMENT', 'UF_BLOG_COMMENT_DOC', 'file', 'UF_BLOG_COMMENT_DOC', 100, 'Y', 'N', 'N', 'N', 'Y', 'Y', 'a:0:{}'),
(3, 'BLOG_POST', 'UF_GRATITUDE', 'integer', 'UF_GRATITUDE', 100, 'N', 'N', 'N', 'N', 'Y', 'N', 'a:0:{}');

-- --------------------------------------------------------

--
-- Table structure for table `b_user_field_confirm`
--

CREATE TABLE IF NOT EXISTS `b_user_field_confirm` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `DATE_CHANGE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FIELD` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FIELD_VALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CONFIRM_CODE` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_b_user_field_confirm1` (`USER_ID`,`CONFIRM_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_user_field_enum`
--

CREATE TABLE IF NOT EXISTS `b_user_field_enum` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_FIELD_ID` int(11) DEFAULT NULL,
  `VALUE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DEF` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `SORT` int(11) NOT NULL DEFAULT '500',
  `XML_ID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ux_user_field_enum` (`USER_FIELD_ID`,`XML_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_user_field_lang`
--

CREATE TABLE IF NOT EXISTS `b_user_field_lang` (
  `USER_FIELD_ID` int(11) NOT NULL DEFAULT '0',
  `LANGUAGE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `EDIT_FORM_LABEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIST_COLUMN_LABEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LIST_FILTER_LABEL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ERROR_MESSAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HELP_MESSAGE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`USER_FIELD_ID`,`LANGUAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_user_group`
--

CREATE TABLE IF NOT EXISTS `b_user_group` (
  `USER_ID` int(18) NOT NULL,
  `GROUP_ID` int(18) NOT NULL,
  `DATE_ACTIVE_FROM` datetime DEFAULT NULL,
  `DATE_ACTIVE_TO` datetime DEFAULT NULL,
  UNIQUE KEY `ix_user_group` (`USER_ID`,`GROUP_ID`),
  KEY `ix_user_group_group` (`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `b_user_group`
--

INSERT INTO `b_user_group` (`USER_ID`, `GROUP_ID`, `DATE_ACTIVE_FROM`, `DATE_ACTIVE_TO`) VALUES
(1, 1, NULL, NULL),
(1, 3, NULL, NULL),
(1, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `b_user_hit_auth`
--

CREATE TABLE IF NOT EXISTS `b_user_hit_auth` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `HASH` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `URL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TIMESTAMP_X` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_USER_HIT_AUTH_1` (`HASH`),
  KEY `IX_USER_HIT_AUTH_2` (`USER_ID`),
  KEY `IX_USER_HIT_AUTH_3` (`TIMESTAMP_X`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_user_option`
--

CREATE TABLE IF NOT EXISTS `b_user_option` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) DEFAULT NULL,
  `CATEGORY` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `COMMON` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`),
  KEY `ix_user_option_user` (`USER_ID`,`CATEGORY`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `b_user_option`
--

INSERT INTO `b_user_option` (`ID`, `USER_ID`, `CATEGORY`, `NAME`, `VALUE`, `COMMON`) VALUES
(1, NULL, 'intranet', '~gadgets_admin_index', 'a:1:{i:0;a:1:{s:7:"GADGETS";a:11:{s:28:"ADMIN_ORDERS_GRAPH@111111111";a:3:{s:6:"COLUMN";i:0;s:3:"ROW";i:0;s:4:"HIDE";s:1:"N";}s:22:"ADMIN_ORDERS@111111111";a:3:{s:6:"COLUMN";i:0;s:3:"ROW";i:1;s:4:"HIDE";s:1:"N";}s:19:"HTML_AREA@444444444";a:5:{s:6:"COLUMN";i:1;s:3:"ROW";i:0;s:4:"HIDE";s:1:"N";s:8:"USERDATA";a:1:{s:7:"content";s:797:"<table class="bx-gadgets-info-site-table" cellspacing="0"><tr>	<td class="bx-gadget-gray">Создатель сайта:</td>	<td>Группа компаний &laquo;1С-Битрикс&raquo;.</td>	<td class="bx-gadgets-info-site-logo" rowspan="5"><img src="/bitrix/components/bitrix/desktop/templates/admin/images/site_logo.png"></td></tr><tr>	<td class="bx-gadget-gray">Адрес сайта:</td>	<td><a href="http://www.1c-bitrix.ru">www.1c-bitrix.ru</a></td></tr><tr>	<td class="bx-gadget-gray">Сайт сдан:</td>	<td>12 декабря 2010 г.</td></tr><tr>	<td class="bx-gadget-gray">Ответственное лицо:</td>	<td>Иван Иванов</td></tr><tr>	<td class="bx-gadget-gray">E-mail:</td>	<td><a href="mailto:info@1c-bitrix.ru">info@1c-bitrix.ru</a></td></tr></table>";}s:8:"SETTINGS";a:1:{s:9:"TITLE_STD";s:34:"Информация о сайте";}}s:24:"ADMIN_SECURITY@555555555";a:3:{s:6:"COLUMN";i:1;s:3:"ROW";i:1;s:4:"HIDE";s:1:"N";}s:23:"ADMIN_PERFMON@666666666";a:3:{s:6:"COLUMN";i:1;s:3:"ROW";i:2;s:4:"HIDE";s:1:"N";}s:24:"ADMIN_PRODUCTS@111111111";a:3:{s:6:"COLUMN";i:1;s:3:"ROW";i:5;s:4:"HIDE";s:1:"N";}s:20:"ADMIN_INFO@333333333";a:3:{s:6:"COLUMN";i:1;s:3:"ROW";i:6;s:4:"HIDE";s:1:"N";}s:25:"ADMIN_CHECKLIST@777888999";a:3:{s:6:"COLUMN";i:1;s:3:"ROW";i:7;s:4:"HIDE";s:1:"N";}s:19:"RSSREADER@777777777";a:4:{s:6:"COLUMN";i:1;s:3:"ROW";i:8;s:4:"HIDE";s:1:"N";s:8:"SETTINGS";a:3:{s:9:"TITLE_STD";s:33:"Новости 1С-Битрикс";s:3:"CNT";i:10;s:7:"RSS_URL";s:45:"https://www.1c-bitrix.ru/about/life/news/rss/";}}s:23:"ADMIN_MARKETPALCE@22549";a:3:{s:6:"COLUMN";i:1;s:3:"ROW";i:3;s:4:"HIDE";s:1:"N";}s:22:"ADMIN_MOBILESHOP@13391";a:3:{s:6:"COLUMN";i:1;s:3:"ROW";i:4;s:4:"HIDE";s:1:"N";}}}}', 'Y'),
(2, NULL, 'main.interface', 'global', 'a:1:{s:5:"theme";s:9:"pale-blue";}', 'Y'),
(3, 1, 'admin_panel', 'settings', 'a:3:{s:4:"edit";s:3:"off";s:3:"fix";s:2:"on";s:9:"collapsed";s:2:"on";}', 'N'),
(4, 1, 'hot_keys', 'user_defined', 'b:1;', 'N'),
(5, 1, 'favorite', 'favorite_menu', 'a:1:{s:5:"stick";s:1:"N";}', 'N'),
(6, 1, 'admin_menu', 'pos', 'a:1:{s:8:"sections";s:159:"menu_system,menu_iblock,iblock_admin,menu_iblock_/news,menu_iblock_/projects/7,menu_module_settings,menu_site,menu_util,menu_perfmon,diag,menu_iblock_/products";}', 'N'),
(7, 1, 'fileman', 'code_editor', 'a:1:{s:5:"theme";s:5:"light";}', 'N'),
(8, 1, 'html_editor', 'user_settings_', 'a:2:{s:4:"view";s:7:"wysiwyg";s:13:"taskbar_shown";s:1:"1";}', 'N'),
(9, 1, 'fileman', 'file_dialog_config', 's:29:"s1;/produktsiya;list;type;asc";', 'N'),
(10, 1, 'list', 'tbl_iblock_element_d1cb75b5aaf00401fd4a48cc75b13ebc', 'a:4:{s:7:"columns";s:48:"NAME,ACTIVE,SORT,TIMESTAMP_X,ID,DATE_ACTIVE_FROM";s:2:"by";s:11:"timestamp_x";s:5:"order";s:4:"desc";s:9:"page_size";s:2:"20";}', 'N'),
(11, 1, 'html_editor', 'type_selector_PREVIEW_TEXT7', 'a:1:{s:4:"type";s:6:"editor";}', 'N'),
(12, 1, 'html_editor', 'type_selector_PREVIEW_TEXT2', 'a:1:{s:4:"type";s:6:"editor";}', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `b_user_stored_auth`
--

CREATE TABLE IF NOT EXISTS `b_user_stored_auth` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(18) NOT NULL,
  `DATE_REG` datetime NOT NULL,
  `LAST_AUTH` datetime NOT NULL,
  `STORED_HASH` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `TEMP_HASH` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `IP_ADDR` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ux_user_hash` (`USER_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `b_user_stored_auth`
--

INSERT INTO `b_user_stored_auth` (`ID`, `USER_ID`, `DATE_REG`, `LAST_AUTH`, `STORED_HASH`, `TEMP_HASH`, `IP_ADDR`) VALUES
(1, 1, '2015-04-22 23:29:18', '2015-05-10 09:47:34', 'c026131f791126ad8555e4f562b55733', 'N', 2130706433);

-- --------------------------------------------------------

--
-- Table structure for table `b_utm_blog_comment`
--

CREATE TABLE IF NOT EXISTS `b_utm_blog_comment` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VALUE_ID` int(11) NOT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `VALUE_INT` int(11) DEFAULT NULL,
  `VALUE_DOUBLE` float DEFAULT NULL,
  `VALUE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_utm_BLOG_COMMENT_1` (`FIELD_ID`),
  KEY `ix_utm_BLOG_COMMENT_2` (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_utm_blog_post`
--

CREATE TABLE IF NOT EXISTS `b_utm_blog_post` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VALUE_ID` int(11) NOT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `VALUE` text COLLATE utf8_unicode_ci,
  `VALUE_INT` int(11) DEFAULT NULL,
  `VALUE_DOUBLE` float DEFAULT NULL,
  `VALUE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ix_utm_BLOG_POST_1` (`FIELD_ID`),
  KEY `ix_utm_BLOG_POST_2` (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_uts_blog_comment`
--

CREATE TABLE IF NOT EXISTS `b_uts_blog_comment` (
  `VALUE_ID` int(11) NOT NULL,
  `UF_BLOG_COMMENT_DOC` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_uts_blog_post`
--

CREATE TABLE IF NOT EXISTS `b_uts_blog_post` (
  `VALUE_ID` int(11) NOT NULL,
  `UF_BLOG_POST_DOC` text COLLATE utf8_unicode_ci,
  `UF_GRATITUDE` int(18) DEFAULT NULL,
  PRIMARY KEY (`VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_vote`
--

CREATE TABLE IF NOT EXISTS `b_vote` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `CHANNEL_ID` int(18) NOT NULL DEFAULT '0',
  `C_SORT` int(18) DEFAULT '100',
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `NOTIFY` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `AUTHOR_ID` int(18) DEFAULT NULL,
  `TIMESTAMP_X` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DATE_START` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DATE_END` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `URL` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COUNTER` int(11) NOT NULL DEFAULT '0',
  `TITLE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DESCRIPTION` text COLLATE utf8_unicode_ci,
  `DESCRIPTION_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'html',
  `IMAGE_ID` int(18) DEFAULT NULL,
  `EVENT1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EVENT2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EVENT3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UNIQUE_TYPE` int(18) NOT NULL DEFAULT '2',
  `KEEP_IP_SEC` int(18) DEFAULT NULL,
  `DELAY` int(18) DEFAULT NULL,
  `DELAY_TYPE` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RESULT_TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_CHANNEL_ID` (`CHANNEL_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_vote_answer`
--

CREATE TABLE IF NOT EXISTS `b_vote_answer` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `TIMESTAMP_X` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `QUESTION_ID` int(18) NOT NULL DEFAULT '0',
  `C_SORT` int(18) DEFAULT '100',
  `MESSAGE` text COLLATE utf8_unicode_ci,
  `MESSAGE_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'html',
  `COUNTER` int(18) NOT NULL DEFAULT '0',
  `FIELD_TYPE` int(5) NOT NULL DEFAULT '0',
  `FIELD_WIDTH` int(18) DEFAULT NULL,
  `FIELD_HEIGHT` int(18) DEFAULT NULL,
  `FIELD_PARAM` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `COLOR` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_QUESTION_ID` (`QUESTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_vote_channel`
--

CREATE TABLE IF NOT EXISTS `b_vote_channel` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `SYMBOLIC_NAME` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `C_SORT` int(18) DEFAULT '100',
  `FIRST_SITE_ID` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `HIDDEN` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `TIMESTAMP_X` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `TITLE` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `VOTE_SINGLE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `USE_CAPTCHA` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_vote_channel_2_group`
--

CREATE TABLE IF NOT EXISTS `b_vote_channel_2_group` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `CHANNEL_ID` int(18) NOT NULL DEFAULT '0',
  `GROUP_ID` int(18) NOT NULL DEFAULT '0',
  `PERMISSION` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_VOTE_CHANNEL_ID_GROUP_ID` (`CHANNEL_ID`,`GROUP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_vote_channel_2_site`
--

CREATE TABLE IF NOT EXISTS `b_vote_channel_2_site` (
  `CHANNEL_ID` int(18) NOT NULL DEFAULT '0',
  `SITE_ID` char(2) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`CHANNEL_ID`,`SITE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `b_vote_event`
--

CREATE TABLE IF NOT EXISTS `b_vote_event` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `VOTE_ID` int(18) NOT NULL DEFAULT '0',
  `VOTE_USER_ID` int(18) NOT NULL DEFAULT '0',
  `DATE_VOTE` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `STAT_SESSION_ID` int(18) DEFAULT NULL,
  `IP` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VALID` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`ID`),
  KEY `IX_USER_ID` (`VOTE_USER_ID`),
  KEY `IX_B_VOTE_EVENT_2` (`VOTE_ID`,`IP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_vote_event_answer`
--

CREATE TABLE IF NOT EXISTS `b_vote_event_answer` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `EVENT_QUESTION_ID` int(18) NOT NULL DEFAULT '0',
  `ANSWER_ID` int(18) NOT NULL DEFAULT '0',
  `MESSAGE` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `IX_EVENT_QUESTION_ID` (`EVENT_QUESTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_vote_event_question`
--

CREATE TABLE IF NOT EXISTS `b_vote_event_question` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `EVENT_ID` int(18) NOT NULL DEFAULT '0',
  `QUESTION_ID` int(18) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `IX_EVENT_ID` (`EVENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_vote_question`
--

CREATE TABLE IF NOT EXISTS `b_vote_question` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `ACTIVE` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `TIMESTAMP_X` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `VOTE_ID` int(18) NOT NULL DEFAULT '0',
  `C_SORT` int(18) DEFAULT '100',
  `COUNTER` int(11) NOT NULL DEFAULT '0',
  `QUESTION` text COLLATE utf8_unicode_ci NOT NULL,
  `QUESTION_TYPE` varchar(4) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'html',
  `IMAGE_ID` int(18) DEFAULT NULL,
  `DIAGRAM` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `REQUIRED` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `DIAGRAM_TYPE` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'histogram',
  `TEMPLATE` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TEMPLATE_NEW` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_VOTE_ID` (`VOTE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_vote_user`
--

CREATE TABLE IF NOT EXISTS `b_vote_user` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `STAT_GUEST_ID` int(18) DEFAULT NULL,
  `AUTH_USER_ID` int(18) DEFAULT NULL,
  `COUNTER` int(18) NOT NULL DEFAULT '0',
  `DATE_FIRST` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DATE_LAST` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LAST_IP` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `b_xml_tree`
--

CREATE TABLE IF NOT EXISTS `b_xml_tree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PARENT_ID` int(11) DEFAULT NULL,
  `LEFT_MARGIN` int(11) DEFAULT NULL,
  `RIGHT_MARGIN` int(11) DEFAULT NULL,
  `DEPTH_LEVEL` int(11) DEFAULT NULL,
  `NAME` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VALUE` longtext COLLATE utf8_unicode_ci,
  `ATTRIBUTES` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`ID`),
  KEY `ix_b_xml_tree_parent` (`PARENT_ID`),
  KEY `ix_b_xml_tree_left` (`LEFT_MARGIN`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=164 ;

--
-- Dumping data for table `b_xml_tree`
--

INSERT INTO `b_xml_tree` (`ID`, `PARENT_ID`, `LEFT_MARGIN`, `RIGHT_MARGIN`, `DEPTH_LEVEL`, `NAME`, `VALUE`, `ATTRIBUTES`) VALUES
(1, 0, 1, 320, 0, 'КоммерческаяИнформация', NULL, 'a:2:{s:22:"ВерсияСхемы";s:5:"2.021";s:32:"ДатаФормирования";s:19:"2010-06-22T12:53:42";}'),
(2, 1, 2, 67, 1, 'Классификатор', NULL, NULL),
(3, 2, 3, 4, 2, 'Ид', '2', NULL),
(4, 2, 5, 6, 2, 'Наименование', 'Вакансии', NULL),
(5, 2, 7, 64, 2, 'Свойства', NULL, NULL),
(6, 5, 8, 15, 3, 'Свойство', NULL, NULL),
(7, 6, 9, 10, 4, 'Ид', 'CML2_ACTIVE', NULL),
(8, 6, 11, 12, 4, 'Наименование', 'БитриксАктивность', NULL),
(9, 6, 13, 14, 4, 'Множественное', 'false', NULL),
(10, 5, 16, 23, 3, 'Свойство', NULL, NULL),
(11, 10, 17, 18, 4, 'Ид', 'CML2_CODE', NULL),
(12, 10, 19, 20, 4, 'Наименование', 'Символьный код', NULL),
(13, 10, 21, 22, 4, 'Множественное', 'false', NULL),
(14, 5, 24, 31, 3, 'Свойство', NULL, NULL),
(15, 14, 25, 26, 4, 'Ид', 'CML2_SORT', NULL),
(16, 14, 27, 28, 4, 'Наименование', 'Сортировка', NULL),
(17, 14, 29, 30, 4, 'Множественное', 'false', NULL),
(18, 5, 32, 39, 3, 'Свойство', NULL, NULL),
(19, 18, 33, 34, 4, 'Ид', 'CML2_ACTIVE_FROM', NULL),
(20, 18, 35, 36, 4, 'Наименование', 'Начало активности', NULL),
(21, 18, 37, 38, 4, 'Множественное', 'false', NULL),
(22, 5, 40, 47, 3, 'Свойство', NULL, NULL),
(23, 22, 41, 42, 4, 'Ид', 'CML2_ACTIVE_TO', NULL),
(24, 22, 43, 44, 4, 'Наименование', 'Окончание активности', NULL),
(25, 22, 45, 46, 4, 'Множественное', 'false', NULL),
(26, 5, 48, 55, 3, 'Свойство', NULL, NULL),
(27, 26, 49, 50, 4, 'Ид', 'CML2_PREVIEW_TEXT', NULL),
(28, 26, 51, 52, 4, 'Наименование', 'Анонс', NULL),
(29, 26, 53, 54, 4, 'Множественное', 'false', NULL),
(30, 5, 56, 63, 3, 'Свойство', NULL, NULL),
(31, 30, 57, 58, 4, 'Ид', 'CML2_PREVIEW_PICTURE', NULL),
(32, 30, 59, 60, 4, 'Наименование', 'Картинка анонса', NULL),
(33, 30, 61, 62, 4, 'Множественное', 'false', NULL),
(34, 2, 65, 66, 2, 'Группы', NULL, NULL),
(35, 1, 68, 319, 1, 'Каталог', NULL, NULL),
(36, 35, 69, 70, 2, 'Ид', 'furniture_vacancies', NULL),
(37, 35, 71, 72, 2, 'ИдКлассификатора', '2', NULL),
(38, 35, 73, 74, 2, 'Наименование', 'Вакансии', NULL),
(39, 35, 75, 76, 2, 'БитриксКод', 'furniture_vacancies', NULL),
(40, 35, 77, 78, 2, 'БитриксСортировка', '500', NULL),
(41, 35, 79, 80, 2, 'БитриксURLСписок', '#SITE_DIR#/company/vacancies.php', NULL),
(42, 35, 81, 82, 2, 'БитриксURLДеталь', '#SITE_DIR#/company/vacancies.php##ID#', NULL),
(43, 35, 83, 84, 2, 'БитриксURLРаздел', NULL, NULL),
(44, 35, 85, 86, 2, 'БитриксКартинка', NULL, NULL),
(45, 35, 87, 88, 2, 'БитриксИндексироватьЭлементы', 'true', NULL),
(46, 35, 89, 90, 2, 'БитриксИндексироватьРазделы', 'false', NULL),
(47, 35, 91, 92, 2, 'БитриксДокументооборот', 'false', NULL),
(48, 35, 93, 154, 2, 'БитриксПодписи', NULL, NULL),
(49, 48, 94, 99, 3, 'БитриксПодпись', NULL, NULL),
(50, 49, 95, 96, 4, 'Ид', 'ELEMENT_NAME', NULL),
(51, 49, 97, 98, 4, 'Значение', 'Вакансия', NULL),
(52, 48, 100, 105, 3, 'БитриксПодпись', NULL, NULL),
(53, 52, 101, 102, 4, 'Ид', 'ELEMENTS_NAME', NULL),
(54, 52, 103, 104, 4, 'Значение', 'Вакансии', NULL),
(55, 48, 106, 111, 3, 'БитриксПодпись', NULL, NULL),
(56, 55, 107, 108, 4, 'Ид', 'ELEMENT_ADD', NULL),
(57, 55, 109, 110, 4, 'Значение', 'Добавить вакансию', NULL),
(58, 48, 112, 117, 3, 'БитриксПодпись', NULL, NULL),
(59, 58, 113, 114, 4, 'Ид', 'ELEMENT_EDIT', NULL),
(60, 58, 115, 116, 4, 'Значение', 'Изменить вакансию', NULL),
(61, 48, 118, 123, 3, 'БитриксПодпись', NULL, NULL),
(62, 61, 119, 120, 4, 'Ид', 'ELEMENT_DELETE', NULL),
(63, 61, 121, 122, 4, 'Значение', 'Удалить вакансию', NULL),
(64, 48, 124, 129, 3, 'БитриксПодпись', NULL, NULL),
(65, 64, 125, 126, 4, 'Ид', 'SECTION_NAME', NULL),
(66, 64, 127, 128, 4, 'Значение', 'Раздел', NULL),
(67, 48, 130, 135, 3, 'БитриксПодпись', NULL, NULL),
(68, 67, 131, 132, 4, 'Ид', 'SECTIONS_NAME', NULL),
(69, 67, 133, 134, 4, 'Значение', 'Разделы', NULL),
(70, 48, 136, 141, 3, 'БитриксПодпись', NULL, NULL),
(71, 70, 137, 138, 4, 'Ид', 'SECTION_ADD', NULL),
(72, 70, 139, 140, 4, 'Значение', 'Добавить раздел', NULL),
(73, 48, 142, 147, 3, 'БитриксПодпись', NULL, NULL),
(74, 73, 143, 144, 4, 'Ид', 'SECTION_EDIT', NULL),
(75, 73, 145, 146, 4, 'Значение', 'Изменить раздел', NULL),
(76, 48, 148, 153, 3, 'БитриксПодпись', NULL, NULL),
(77, 76, 149, 150, 4, 'Ид', 'SECTION_DELETE', NULL),
(78, 76, 151, 152, 4, 'Значение', 'Удалить раздел', NULL),
(79, 35, 155, 318, 2, 'Товары', NULL, NULL),
(80, 79, 156, 209, 3, 'Товар', NULL, NULL),
(81, 80, 157, 158, 4, 'Ид', '2', NULL),
(82, 80, 159, 160, 4, 'Наименование', 'Продавец-дизайнер (кухни)', NULL),
(83, 80, 161, 162, 4, 'БитриксТеги', NULL, NULL),
(84, 80, 163, 164, 4, 'Описание', '<b>Требования</b> 						 						 \r\n<p>Уверенный пользователь ПК, навыки черчения от руки и в программах, опыт работы дизайнером/конструктором в мебельной сфере, этика делового общения</p>\r\n 						 						<b>Обязанности</b> 						 \r\n<p>Консультирование клиентов по кухонной мебели, оставление художественно-конструкторских проектов, прием и оформление заказов.</p>\r\n 						 						<b>Условия</b> 						 \r\n<p>Зарплата: 13500 оклад + % от личных продаж + премии‚ совокупный доход от 20000 руб,полный соц. пакет‚ оформление согласно ТК РФ</p>\r\n ', NULL),
(85, 80, 165, 208, 4, 'ЗначенияСвойств', NULL, NULL),
(86, 85, 166, 171, 5, 'ЗначенияСвойства', NULL, NULL),
(87, 86, 167, 168, 6, 'Ид', 'CML2_ACTIVE', NULL),
(88, 86, 169, 170, 6, 'Значение', 'true', NULL),
(89, 85, 172, 177, 5, 'ЗначенияСвойства', NULL, NULL),
(90, 89, 173, 174, 6, 'Ид', 'CML2_CODE', NULL),
(91, 89, 175, 176, 6, 'Значение', NULL, NULL),
(92, 85, 178, 183, 5, 'ЗначенияСвойства', NULL, NULL),
(93, 92, 179, 180, 6, 'Ид', 'CML2_SORT', NULL),
(94, 92, 181, 182, 6, 'Значение', '200', NULL),
(95, 85, 184, 189, 5, 'ЗначенияСвойства', NULL, NULL),
(96, 95, 185, 186, 6, 'Ид', 'CML2_ACTIVE_FROM', NULL),
(97, 95, 187, 188, 6, 'Значение', '2010-05-01 00:00:00', NULL),
(98, 85, 190, 195, 5, 'ЗначенияСвойства', NULL, NULL),
(99, 98, 191, 192, 6, 'Ид', 'CML2_ACTIVE_TO', NULL),
(100, 98, 193, 194, 6, 'Значение', NULL, NULL),
(101, 85, 196, 201, 5, 'ЗначенияСвойства', NULL, NULL),
(102, 101, 197, 198, 6, 'Ид', 'CML2_PREVIEW_TEXT', NULL),
(103, 101, 199, 200, 6, 'Значение', NULL, NULL),
(104, 85, 202, 207, 5, 'ЗначенияСвойства', NULL, NULL),
(105, 104, 203, 204, 6, 'Ид', 'CML2_PREVIEW_PICTURE', NULL),
(106, 104, 205, 206, 6, 'Значение', NULL, NULL),
(107, 79, 210, 263, 3, 'Товар', NULL, NULL),
(108, 107, 211, 212, 4, 'Ид', '3', NULL),
(109, 107, 213, 214, 4, 'Наименование', 'Директор магазина', NULL),
(110, 107, 215, 216, 4, 'БитриксТеги', NULL, NULL),
(111, 107, 217, 218, 4, 'Описание', '<b>Требования</b> 						 						 \r\n<p>Высшее образование‚ опыт руководящей работы в рознице от 3 лет‚ опытный пользователь ПК‚ этика делового общения‚ знание методов управления и заключения договоров‚ отличное знание торгового и трудового законодательств‚ ответственность‚ инициативность‚ активность.</p>\r\n 						 						<b>Обязанности</b> 						 \r\n<p>Руководство деятельностью магазина‚ организация эффективной работы персонала магазина‚ финансово-хозяйственная деятельность и отчетность‚ выполнение плана продаж‚ отчетность.</p>\r\n 						 						<b>Условия</b> 						 \r\n<p>Трудоустройство по ТК РФ‚ полный соц. пакет‚ график работы 5 чере 2 (168 час/мес)‚ зарплата: оклад 28000 + % от оборота + премии‚ совокупный доход от 35000 руб</p>\r\n ', NULL),
(112, 107, 219, 262, 4, 'ЗначенияСвойств', NULL, NULL),
(113, 112, 220, 225, 5, 'ЗначенияСвойства', NULL, NULL),
(114, 113, 221, 222, 6, 'Ид', 'CML2_ACTIVE', NULL),
(115, 113, 223, 224, 6, 'Значение', 'true', NULL),
(116, 112, 226, 231, 5, 'ЗначенияСвойства', NULL, NULL),
(117, 116, 227, 228, 6, 'Ид', 'CML2_CODE', NULL),
(118, 116, 229, 230, 6, 'Значение', NULL, NULL),
(119, 112, 232, 237, 5, 'ЗначенияСвойства', NULL, NULL),
(120, 119, 233, 234, 6, 'Ид', 'CML2_SORT', NULL),
(121, 119, 235, 236, 6, 'Значение', '300', NULL),
(122, 112, 238, 243, 5, 'ЗначенияСвойства', NULL, NULL),
(123, 122, 239, 240, 6, 'Ид', 'CML2_ACTIVE_FROM', NULL),
(124, 122, 241, 242, 6, 'Значение', '2010-05-01 00:00:00', NULL),
(125, 112, 244, 249, 5, 'ЗначенияСвойства', NULL, NULL),
(126, 125, 245, 246, 6, 'Ид', 'CML2_ACTIVE_TO', NULL),
(127, 125, 247, 248, 6, 'Значение', NULL, NULL),
(128, 112, 250, 255, 5, 'ЗначенияСвойства', NULL, NULL),
(129, 128, 251, 252, 6, 'Ид', 'CML2_PREVIEW_TEXT', NULL),
(130, 128, 253, 254, 6, 'Значение', NULL, NULL),
(131, 112, 256, 261, 5, 'ЗначенияСвойства', NULL, NULL),
(132, 131, 257, 258, 6, 'Ид', 'CML2_PREVIEW_PICTURE', NULL),
(133, 131, 259, 260, 6, 'Значение', NULL, NULL),
(134, 79, 264, 317, 3, 'Товар', NULL, NULL),
(135, 134, 265, 266, 4, 'Ид', '4', NULL),
(136, 134, 267, 268, 4, 'Наименование', 'Бухгалтер отдела учета готовой продукции', NULL),
(137, 134, 269, 270, 4, 'БитриксТеги', NULL, NULL),
(138, 134, 271, 272, 4, 'Описание', '<b>Требования</b> 						 						 \r\n<p>Жен., 22-45, уверенный пользователь ПК, опыт работы бухгалтером приветсвуется, знание бухгалтерского учета (базовый уровень), самостоятельность, ответственность, коммуникабельность, быстрая обучаемость, желание работать.</p>\r\n 						 						<b>Обязанности</b> 						 \r\n<p>Работа с первичными документами по учету МПЗ Ведение журналов-ордеров по производственным счетам Формирование материальных отчетов подразделений Исполнение дополнительных функций по указанию руководителя</p>\r\n 						 						<b>Условия</b> 						 \r\n<p>График работы 5 дней в неделю, адрес работы г. Шатура, Ботинский пр-д, 37. Зарплата: оклад 10 800 р. + премия 40% от оклада, полный соц. пакет.</p>\r\n ', NULL),
(139, 134, 273, 316, 4, 'ЗначенияСвойств', NULL, NULL),
(140, 139, 274, 279, 5, 'ЗначенияСвойства', NULL, NULL),
(141, 140, 275, 276, 6, 'Ид', 'CML2_ACTIVE', NULL),
(142, 140, 277, 278, 6, 'Значение', 'true', NULL),
(143, 139, 280, 285, 5, 'ЗначенияСвойства', NULL, NULL),
(144, 143, 281, 282, 6, 'Ид', 'CML2_CODE', NULL),
(145, 143, 283, 284, 6, 'Значение', NULL, NULL),
(146, 139, 286, 291, 5, 'ЗначенияСвойства', NULL, NULL),
(147, 146, 287, 288, 6, 'Ид', 'CML2_SORT', NULL),
(148, 146, 289, 290, 6, 'Значение', '400', NULL),
(149, 139, 292, 297, 5, 'ЗначенияСвойства', NULL, NULL),
(150, 149, 293, 294, 6, 'Ид', 'CML2_ACTIVE_FROM', NULL),
(151, 149, 295, 296, 6, 'Значение', '2010-05-01 00:00:00', NULL),
(152, 139, 298, 303, 5, 'ЗначенияСвойства', NULL, NULL),
(153, 152, 299, 300, 6, 'Ид', 'CML2_ACTIVE_TO', NULL),
(154, 152, 301, 302, 6, 'Значение', NULL, NULL),
(155, 139, 304, 309, 5, 'ЗначенияСвойства', NULL, NULL),
(156, 155, 305, 306, 6, 'Ид', 'CML2_PREVIEW_TEXT', NULL),
(157, 155, 307, 308, 6, 'Значение', NULL, NULL),
(158, 139, 310, 315, 5, 'ЗначенияСвойства', NULL, NULL),
(159, 158, 311, 312, 6, 'Ид', 'CML2_PREVIEW_PICTURE', NULL),
(160, 158, 313, 314, 6, 'Значение', NULL, NULL),
(161, 0, 30, 0, 0, '', NULL, NULL),
(162, 0, 31, 0, 0, '', NULL, NULL),
(163, 0, 32, 0, 0, '', NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
