//var interval = 7000;
var isMobile = 0;

if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
  isMobile = 1;
  document.documentElement.className += 'mobile';
}

var pageId = '', subPageID = '';
if (location.href.indexOf('#')>0) {
  if (location.href.lastIndexOf('/')>location.href.indexOf('#')) {
    pageId = location.href.substring(location.href.lastIndexOf('#') + 1, location.href.lastIndexOf('/'));
    subPageID = location.href.substring(location.href.lastIndexOf('/') + 1);
  } else {
    pageId = location.href.substring(location.href.lastIndexOf('#') + 1);
  }
}



$(document).ready(function() {

  // tabs init
  $('section.complex ul.tabs').tabs();
  $('section.services ul.tabs').tabs();
  $('section.main.complex-line ul.tabs').tabs();

  $('section.main .big-table .table-content').tableTabs();

  //$('#top .filter ul').filters();
  // tabs init

  // sliders and carousel init
  $('section.main.success .carousel').slider();

  $('section.main .content .gallery').gallery();
  // sliders and carousel init

  if ($('section.main .big-table').size())
    $('section.main .big-table').tableInit()


  $('section.complex-line .showDialog').click(function(){
    $('#'+$(this).data('dialog')).dialog({content: $(this).closest('.line-img').html()});
    return false
  });
  $('header .showDialog, section.main.services a.showDialog').click(function(){
    $('#'+$(this).data('dialog')).dialog();
  });
  $('#fader .backdoor, div.popup .close').click(function(){
    $('div.popup.active').dialog({destroy: true});
  });

  if ($(".fancybox-thumb").size())
    $(".fancybox-thumb").fancybox({
      prevEffect	: 'none',
      nextEffect	: 'none',
      helpers	: {
        title	: {
          type: 'outside'
        },
        thumbs	: {
          width	: 50,
          height	: 70
        }
      }
    });
});

var tableTabIndex = 1;

$.fn.tableInit = function(){
  var tablesHolder = $(this);

  // table hovers
    tablesHolder.find('.table-content table tr td .tab a').each(function(){
    $(this).parent().css({
      'width': $(this).width()+18,
      'padding': '0 4px'
    });
    $(this).hover(
        function(e){
          $(this).addClass('hover');
          return false
        },
        function(e){
          $(this).removeClass('hover');
        }
    );
  })
  tablesHolder.find('table tr').hover(
      function(){
        var idx = $(this).index();
        $(this).closest('.big-table').find('table').each(function(){
          $(this).find('tr:eq('+idx+')').addClass('hover');
        })
      },
      function(){
        var idx = $(this).index();
        $(this).closest('.big-table').find('table').each(function(){
          $(this).find('tr:eq('+idx+')').removeClass('hover');
        })
      }
  );
  // table hovers

  // table resize
  $('section.main.production .more-table a').click( function(){
    tablesHolder.find('> div').animate({'max-height':tablesHolder.find('table').height()},500)
    $(this).parent().hide();
    return false
  });
  // table resize

  // drag table
  tablesHolder.find('.table-content table').mousedown(function(){
    $(this).addClass('grabbed');
  });
  tablesHolder.find('.table-content table').mouseup(function(){
    $(this).removeClass('grabbed');
  });

  tablesHolder.find('.table-content table').draggable({
    axis: "x",
    drag: function(e,ui) {
      var obj = $(this);
      if (ui.position.left < 0) {
        tablesHolder.addClass('scrolled');
      } else {
        tablesHolder.removeClass('scrolled');
      }
      if (ui.position.left < -obj.width()+obj.parent().width()+62) {
        tablesHolder.addClass('end');
      } else {
        tablesHolder.removeClass('end');
      }
      if (obj.find('td:eq('+tableTabIndex+') .tab').offset().left < 33) {
        obj.find('td:eq('+tableTabIndex+') .tab').addClass('fixed');
        obj.find('td:eq('+tableTabIndex+') .tab a').css('left',-ui.position.left-obj.find('td:eq('+tableTabIndex+') .tab').position().left+13)
      } else {
        obj.find('td:eq('+tableTabIndex+') .tab').removeClass('fixed');
        obj.find('td:eq('+tableTabIndex+') .tab a').css('left',0)
      }
    },
    stop: function(e,ui) {
      var obj = tablesHolder.find('.table-content table');
      if (ui.position.left > 0) {
        obj.animate({'left':0},500);
      } else if (ui.position.left < -obj.width()+obj.parent().width()+62) {
        obj.animate({'left':-obj.width()+obj.parent().width()+62},500);
        obj.find('td:eq('+tableTabIndex+') .tab a').animate({'left':(obj.width()-obj.parent().width())-(obj.find('td:eq('+tableTabIndex+') .tab').position().left-13)-62},500);
      }
    }
  });
  //drag table

}
$.fn.dialog = function( options ) {
  var win = $(this),
      form = win.find('form');

  var oldWin = $('div.popup.active');
  if(oldWin.attr('id')) {
    if(oldWin.attr('id') != win.attr('id')) {
      oldWin.dialog({
        destroy: true,
        keepFader: true
      });
    }
  }

  if (win.attr('id') == 'complexLines') {
    win.find('content').html()
  }

  if (!options || options.content) {
    $('body').addClass('overlay');
    $('#fader').fadeIn();
    win.addClass('active');
    if (options) {
      win.find('.content').html(options.content);
      win.find('.close').click(function(){
        $('div.popup.active').dialog({destroy: true});
        return false;
      });
      imagesLoaded( win, function( instance ) {
        var winTopPos = $(window).height()/2-win.height()/2-25
        if (winTopPos < 20) winTopPos = 20;
        win.css('top',winTopPos);
      });
      $(window).resize(function(){
        var winTopPos = $(window).height()/2-win.height()/2-25
        if (winTopPos < 10) winTopPos = 10;
        win.css('top',winTopPos);
      })

    }

    if (form.size()) {
      //form.initForm();
    }
  }


  if (options)
    if (options.destroy) {
      if (form.size()) {
        //form.initForm({destroy: true});
      }

      win.removeClass('active').find();
      $('body').removeClass('overlay');
      if (!options.keepFader) {
        $('#fader').fadeOut();
      }
    }
};

$.fn.slider = function(){
  var par = $(this);
  function updateContentByTab(idx) {
    par.find('.slider > div').removeClass('active').filter(':eq('+idx+')').addClass('active');
  }
  par.find('.prev').click(function(e){
    var idx = par.find('.slider > div.active').prev().index();
    if (par.find('.slider > div.active').is(':first-child'))
      idx = par.find('.slider > div').size()-1;
    updateContentByTab(idx);
    e.preventDefault();
  });
  par.find('.next').click(function(e){
    var idx = par.find('.slider > div.active').next().index();
    if (par.find('.slider > div.active').is(':last-child'))
      idx = 0;
    updateContentByTab(idx);
    e.preventDefault();
  });
};

$.fn.gallery = function(){
  $(this).each(function(){
    var par = $(this);
    function updateContentByTab(idx) {
      par.find('.previews span').removeClass('active').filter(':eq('+idx+')').addClass('active');
      par.find('.bigImg img').removeClass('active').filter(':eq('+idx+')').addClass('active');
    }
    par.find('.previews span').click(function(e){
      updateContentByTab($(this).index());
      e.preventDefault();
    });
  })
};


$.fn.tabs = function(isCall){
  var par = $(this);
  function updateContentByTab(obj) {
    par.find('li').removeClass('active').filter(obj).addClass('active');
    $(par.data('tabs')).find('> div').removeClass('active').filter(':eq('+obj.index()+')').addClass('active');
  }
  par.find('li').click(function(e){
    updateContentByTab($(this));
    return false;
  });
};



$.fn.filters = function(){
  var par = $(this);
  function updateContentByTab(obj) {
    par.find('li').removeClass('active').filter(obj).addClass('active');
    if (obj.data('filter'))
      $(par.data('src')).find('> .project').slideUp().filter('[data-filter='+obj.data('filter')+']').stop().slideDown();
    else
      $(par.data('src')).find('> .project').slideDown();
  }
  par.find('li').click(function(e){
    updateContentByTab($(this));
    return false;
  });
};



$.fn.tableTabs = function(isCall){
  function updateContentByTab(obj) {
    tableTabIndex = obj.closest('td').index();
    var par = obj.closest('tr');
    par.find('td span.tab.active').find('a').css('left',0);
    par.find('td span.tab').removeClass('active').filter(obj).addClass('active');
    $('#production_table-tabs_content').find('> div').removeClass('active').filter(':eq('+(tableTabIndex-1)+')').addClass('active');
  }
  $(this).find('table tr td .tab').click(function (e) {
    updateContentByTab($(this));
    e.preventDefault()
  });
}