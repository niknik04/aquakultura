<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
  <section class="main contacts">
    <div class="wraper">
      <div itemscope itemtype="http://schema.org/Organization">
        <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
          <span class="address" itemprop="streetAddress">Молодогвардейская улица, 57</span>
          <span itemprop="addressLocality">Москва</span>,
          <span itemprop="postalCode"> 121351</span>
        </div>
        <time itemprop="openingHours" datetime="Mo-Fr 9:00−18:00">Работаем с понедельника по пятницу, с 9 до 18</time>

        <p>
          <b>8 800 700-95-35</b>
          Бесплатный звонок по Роccии
        </p>

        <div>
          <a class="mail" href="mailto:info@aquakultura.ru" itemprop="email">info@aquakultura.ru</a>
          <a class="skype" href="skype:info_aqua" itemprop="skype">info_aqua</a>
          <span itemprop="telephone">+7 495 933-40-08</span>
        </div>
      </div>
    </div>


    <script type="text/javascript">
      ymaps.ready(init);
      var myMap,
              myPlacemark;

      function init(){
        myMap = new ymaps.Map ("map", {
          center: [55.733073, 37.433475],
          zoom: 14
        });

        myMap.controls.add(
                new ymaps.control.ZoomControl()
        );

        myPlacemark = new ymaps.Placemark([55.733594, 37.403306]);

        myMap.geoObjects.add(myPlacemark);
      }
    </script>
    <div id="map" style="width: 100%; height: 735px"></div>
  </section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>